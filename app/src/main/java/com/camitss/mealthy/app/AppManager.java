package com.camitss.mealthy.app;


import android.app.Activity;
import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.fragment.MainTabFifthFragment;

/**
 * Created by Senhly HENG
 * Date 07-03-2017
 */
public class AppManager extends Application {
	public static final String TAG = Application.class.getSimpleName();
	public boolean refresh = false;
	private RequestQueue mRequestQueue;
	public MealthyActivity mealthyActivity;
	public boolean tab2Refresh = false;

	private static AppManager mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
	}


	public boolean isTab2Refresh() {
		return tab2Refresh;
	}

	public void setTab2Refresh(boolean tab2Refresh) {
		this.tab2Refresh = tab2Refresh;
	}

	public MealthyActivity getMealthyActivity() {
		return mealthyActivity;
	}

	public void setMealthyActivity(MealthyActivity mealthyActivity) {
		this.mealthyActivity = mealthyActivity;
	}

	private MainTabFifthFragment mainTabFifthFragment = null;

	public MainTabFifthFragment getMainTabFifthFragment() {
		return mainTabFifthFragment;
	}

	public void setMainTabFifthFragment(MainTabFifthFragment mainTabFifthFragment) {
		this.mainTabFifthFragment = mainTabFifthFragment;
	}

	public static synchronized AppManager getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public boolean isRefresh() {
		return refresh;
	}

	public void set5TabRefresh(boolean refresh) {
		this.refresh = refresh;
	}
}