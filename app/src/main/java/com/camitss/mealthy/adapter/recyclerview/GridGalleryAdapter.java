package com.camitss.mealthy.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.WindowCompat;
import android.support.v7.graphics.Target;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.utils.CustomTransformation;
import com.camitss.mealthy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 4/19/2017.
 */

public class GridGalleryAdapter extends RecyclerView.Adapter<GridGalleryAdapter.GridGalleryViewHolder> {

	private List<FeedItem> feeItemList = new ArrayList<>();
	private Context context;
	private OnItemClickListener listener;
	private Activity activity;

	public GridGalleryAdapter(Activity activity) {
		this.activity = activity;
		this.context = activity.getApplicationContext();
	}

	public void addFeedItemList(List<FeedItem> list) {
		feeItemList.clear();
		feeItemList.addAll(list);
	}

	public List<FeedItem> getFeeItemList() {
		return feeItemList;
	}


	@Override
	public GridGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_gallery_grid_item, parent, false);
		return new GridGalleryViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final GridGalleryViewHolder holder, int position) {
		FeedItem item = getFeeItemList().get(position);
		if (null != item) {
			Glide.with(context).load(item.getMenu().getImageThumb())
					.thumbnail(0.5f)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.into(holder.imageItem);
		}
	}

	@Override
	public int getItemCount() {
		return getFeeItemList().size();
	}

	public class GridGalleryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		public ImageView imageItem;

		public GridGalleryViewHolder(View itemView) {
			super(itemView);
			imageItem = (ImageView) itemView.findViewById(R.id.image_item);
			imageItem.setOnClickListener(this);

		}

		@Override
		public void onClick(View v) {
			if (null != listener) {
				listener.onItemClick(v, getAdapterPosition());
			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View view, int pos);
	}

	public void setOnItemClickListener(OnItemClickListener listener) {
		this.listener = listener;
	}
}
