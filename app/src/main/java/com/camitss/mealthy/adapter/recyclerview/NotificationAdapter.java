package com.camitss.mealthy.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.HistoryFeedItemDisplayActivity;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.NotificationItem;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.object.notificationitem.NotificationItemRealmObject;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Viseth on 5/3/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
	private Context context;
	private final int NCT_GOOD = 1;
	private final int NCT_FOLLOW = 2;
	private final int NCT_COMMENT = 3;
	private final int NCT_ASSIGNEDNUTRISTIONIST = 4;
	private final int NCT_USERINCHARGECOMMENT = 5;
	private final int NCT_INFORMATION = 6;
	private final int NCT_USERINCHARGEPOST = 7;
	private final int NCT_ADDPOINT = 8;
	private List<NotificationItem> notificationItemList = new ArrayList<>();
	private Activity activity;
	private int userNameStartPosition = 0;

	public NotificationAdapter(Activity activity) {
		this.activity = activity;
		this.context = activity.getApplicationContext();
	}

	public List<NotificationItem> getNotificationItemList() {
		return notificationItemList;
	}

	public void addNotificationItemList(List<NotificationItem> notificationItemList) {
//		this.notificationItemList.clear();
		this.notificationItemList.addAll(notificationItemList);
	}

	@Override
	public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_noti_item, parent, false));
	}

	@Override
	public void onBindViewHolder(NotificationViewHolder holder, int position) {
		final NotificationItem item = getNotificationItemList().get(position);
		final ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(final View textView) {
				goToMyPage(item.getFromUser().getId());
			}

			@Override
			public void updateDrawState(final TextPaint textPaint) {
				textPaint.setColor(ContextCompat.getColor(context, android.R.color.black));
				textPaint.setTypeface(Typeface.DEFAULT_BOLD);
				textPaint.setUnderlineText(false);
			}
		};

//		getNotificationString(activity,holder.textContent, 3, item.getActionKind(), item) ;
		String body = getNotificationString(item.getActionKind(), item) + " " + Utils.fromStringDateWithTimeZoneToFormate(item.getCreatedAt());
		SpannableString span = new SpannableString(body);
		if (null != item.getFromUser()) {
			span.setSpan(clickableSpan, userNameStartPosition, (item.getFromUser().getName().length() + userNameStartPosition), 0);
			holder.textContent.setMovementMethod(LinkMovementMethod.getInstance());
			holder.textContent.setText(span);
		} else holder.textContent.setText(body);
		String imageItemUrl = "";
		if (null != item.getFeed() && null != item.getFeed().getMenu()) {
			imageItemUrl = !item.getFeed().getMenu().getThumbnailImageUrl().trim().isEmpty() && !item.getFeed().getMenu().getThumbnailImageUrl().equalsIgnoreCase("null") ? item.getFeed().getMenu().getThumbnailImageUrl() : "";
		} else if (null != item.getFromUser()) {
			imageItemUrl = item.getFromUser().getLastedPostedImageUrl();
		}
		if (!imageItemUrl.trim().isEmpty() && item.getActionKind() != NCT_FOLLOW) {
			Glide.with(context).load(imageItemUrl).placeholder(R.color.grey_light).into(holder.imageThumbnail);
			holder.imageThumbnail.setVisibility(View.VISIBLE);
		} else {
			holder.imageThumbnail.setVisibility(View.GONE);
		}
		if (item.getActionKind() == NCT_INFORMATION || item.getActionKind() == NCT_ADDPOINT) {
			Glide.with(context).load("").placeholder(ContextCompat.getDrawable(context, R.drawable.logo)).error(R.drawable.logo).transform(new CircleTransformation(context)).into(holder.imageProfile);
		} else {
			String imageProfileUrl = null != item.getFromUser().getProfileImageUrl() && !item.getFromUser().getProfileImageUrl().trim().isEmpty() ? item.getFromUser().getProfileImageUrl() : item.getFromUser().getRepNutritionistProfileUrl();
			if (null != imageProfileUrl && !imageProfileUrl.trim().isEmpty())
				Glide.with(context).load(imageProfileUrl).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(context)).into(holder.imageProfile);
			else
				Glide.with(context).load("").placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).into(holder.imageProfile);
		}
		if (item.getStatusKind() == 1) {
			holder.layoutNotiItem.setBackgroundColor(ContextCompat.getColor(context, R.color.default_tab));
		} else
			holder.layoutNotiItem.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
	}

	@Override
	public int getItemCount() {
		return getNotificationItemList().size();
	}

	class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private LinearLayout layoutNotiItem;
		private ImageView imageThumbnail;
		private TextView textContent;
		private ImageView imageProfile;

		public NotificationViewHolder(View itemView) {
			super(itemView);

			imageProfile = (ImageView) itemView.findViewById(R.id.image_profile);
			imageThumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
			textContent = (TextView) itemView.findViewById(R.id.txt_content);
			layoutNotiItem = (LinearLayout) itemView.findViewById(R.id.layout_noti_item);
			itemView.setOnClickListener(this);
			imageProfile.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
//			RealmResults<NotificationItemRealmObject> localNoti = RealmHelper.with(context).getRealm().where(NotificationItemRealmObject.class).findAll();
//			for(int i= 0; i<localNoti.size(); i++){
//				Log.d("notirealm1", localNoti.get(i).getId()+"<<>>"+localNoti.get(i).getStatusKind()+"<<>>"+localNoti.get(i).getFromUser().getName());
//			}
//			localNoti.get(getAdapterPosition()).setStatusKind(3);
//			RealmList<RealmObject> tmpObj = new RealmList<>();
//			tmpObj.addAll(localNoti);
//			RealmHelper.with(context).saveAll(tmpObj);
			if (v == imageProfile) {
				int myId = AppSharedPreferences.getConstant(context).getUserIdPref();
				int userId = 0;
				NotificationItem item = getNotificationItemList().get(getAdapterPosition());
				if (null != item && null != item.getFromUser())
					userId = item.getFromUser().getId();
				goToMyPage(userId);
			} else {
				switch (getNotificationItemList().get(getAdapterPosition()).getActionKind()) {
					case NCT_GOOD:
						goToFeed(getNotificationItemList().get(getAdapterPosition()).getFeed());
						break;
					case NCT_FOLLOW:
						goToMyPage(getNotificationItemList().get(getAdapterPosition()).getFromUser().getId());
						break;
					case NCT_COMMENT:
						goToFeed(getNotificationItemList().get(getAdapterPosition()).getFeed());
						break;
					case NCT_ASSIGNEDNUTRISTIONIST:
						goToMyPage(getNotificationItemList().get(getAdapterPosition()).getFromUser().getId());
						break;
					case NCT_USERINCHARGECOMMENT:
						goToFeed(getNotificationItemList().get(getAdapterPosition()).getFeed());
						break;
					case NCT_INFORMATION:
						goToWebView();
						break;
					case NCT_USERINCHARGEPOST:
						goToFeed(getNotificationItemList().get(getAdapterPosition()).getFeed());
						break;
					case NCT_ADDPOINT:
						Toast.makeText(context, "NCT_ADDPOINT", Toast.LENGTH_SHORT).show();
						break;
					default:
						Toast.makeText(context, "default", Toast.LENGTH_SHORT).show();
						break;
				}
			}
		}
	}

	private String getNotificationString(int actionKind, NotificationItem item) {
		switch (actionKind) {
			case NCT_GOOD:
				userNameStartPosition = 0;
				return String.format(Locale.ENGLISH, "%s が「いいね（♡）」しました。", item.getFromUser().getName());
			case NCT_FOLLOW:
				userNameStartPosition = 0;
				return String.format(Locale.ENGLISH, "%s があなたをフォローしました。", item.getFromUser().getName());
			case NCT_COMMENT:
				userNameStartPosition = 0;
				return String.format(Locale.ENGLISH, "%s が投稿にコメントをしました。", item.getFromUser().getName());
			case NCT_ASSIGNEDNUTRISTIONIST:
				userNameStartPosition = 5;
				return String.format(Locale.ENGLISH, "担当栄養士が %s に決まりました。", item.getFromUser().getName());
			case NCT_USERINCHARGECOMMENT:
				userNameStartPosition = 2;
				return String.format(Locale.ENGLISH, "担当の %s がコメントをしました。", item.getFromUser().getName());
			case NCT_INFORMATION:

				return item.getAdminInfo().getTitle();
			case NCT_USERINCHARGEPOST:
				userNameStartPosition = 2;
				return String.format(Locale.ENGLISH, "担当の %s が投稿をしました。", item.getFromUser().getName());
			case NCT_ADDPOINT:
				return item.getAdminInfo().getTitle();
			default:
				return "取得に失敗しました。";
		}
	}


	private void goToFeed(UserFeedsInProfile feedsInProfile) {

		Intent i = new Intent(activity, HistoryFeedItemDisplayActivity.class);
		i.putExtra("FeedItem", Utils.fromUserFeedsInProfileToFeedItem(feedsInProfile));
		activity.startActivity(i);
	}

	private void goToMyPage(int userId) {
		Intent i = new Intent(activity, UserRecomProfileDisplayActivity.class);
		i.putExtra("userId", userId);
		activity.startActivity(i);
	}

	private void goToWebView() {

	}
	public void updateNotificationItem(Realm realm, NotificationItemRealmObject item) {
		NotificationItemRealmObject toEdit = realm.where(NotificationItemRealmObject.class)
				.equalTo("id", item.getId()).findFirst();
		realm.beginTransaction();
		toEdit.setStatusKind(item.getStatusKind());
		realm.commitTransaction();
	}
}
