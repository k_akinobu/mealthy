package com.camitss.mealthy.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.utils.CustomTransformation;
import com.camitss.mealthy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 5/2/2017.
 */

public class UserFeedsTabOneGridAdapter extends RecyclerView.Adapter<UserFeedsTabOneGridAdapter.UserFeedsViewHolder> {
	private Context context;
	private List<UserFeedsInProfile> userFeedsInProfileList = new ArrayList<>();
	private OnItemClickListener listener;

	public UserFeedsTabOneGridAdapter(Context context) {
		this.context = context;
	}

	public void addUserFeedsInProfileList(List<UserFeedsInProfile> list, boolean hasMore) {
		if (!hasMore) {
			userFeedsInProfileList.clear();
		}
		userFeedsInProfileList.addAll(list);
	}

	public List<UserFeedsInProfile> getUserFeedsInProfileList() {
		return userFeedsInProfileList;
	}

	@Override
	public UserFeedsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new UserFeedsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_feeds_item_layout, parent, false));
	}

	@Override
	public void onBindViewHolder(final UserFeedsViewHolder holder, int position) {
		UserFeedsInProfile item = getUserFeedsInProfileList().get(position);
		String key = item.getKey();
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity) context) / 4,
				ViewGroup.LayoutParams.WRAP_CONTENT); // or set height to any fixed valueTwo you want
		holder.layoutPost.setLayoutParams(lp);
		holder.layoutPost.setPadding(1, 1, 1, 1);
		if (key.equals("user_history.update.weight")) {
			holder.photo.setVisibility(View.GONE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.valueTwo.setVisibility(View.GONE);
			holder.valueOne.setText(item.getUserHistory().getValue());
			holder.valueOne.setTextSize(22);
			holder.unit.setText("kg");
			holder.beforeWeight.setText(null != item.getUserHistory().getBeforeWeight() ? item.getUserHistory().getBeforeWeight() + "kg" : "ーkg");
			String date = Utils.fromStringDateWithTimeZoneToFormateMMddHH(item.getUserHistory().getCreatedAt()).split(":", 2)[0];
			holder.createdAt.setText(date + context.getString(R.string.f_hour));
			holder.beforeWeight.setVisibility(View.VISIBLE);
			holder.unit.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setBackgroundColor(ContextCompat.getColor(context, getBackground(Integer.parseInt(date.split("\\s", 2)[1]))));
			LinearLayout.LayoutParams lps = new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity) context) / 4,
					Utils.getScreenWidth((Activity) context) / 4); // or set height to any fixed valueTwo you want
			holder.weightFeedLayout.setLayoutParams(lps);
		} else if (key.equals("user_history.update.bt_bgl")) {
			holder.photo.setVisibility(View.GONE);
			holder.valueTwo.setVisibility(View.GONE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.valueOne.setText(item.getUserHistory().getValue());
			holder.valueOne.setTextSize(22);
			holder.beforeWeight.setText("mg/dL");
			holder.createdAt.setText(Utils.fromStringDateWithTimeZoneToFormateMMdd(item.getUserHistory().getCreatedAt()));
			holder.unit.setVisibility(View.GONE);
			holder.beforeWeight.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
			LinearLayout.LayoutParams lps = new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity) context) / 4,
					Utils.getScreenWidth((Activity) context) / 4); // or set height to any fixed valueTwo you want
			holder.weightFeedLayout.setLayoutParams(lps);
		} else if (key.equals("user_history.update.bt_bp")) {
			holder.photo.setVisibility(View.GONE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.valueTwo.setVisibility(View.VISIBLE);
			holder.valueTwo.setText(item.getUserHistory().getValue().split("\\|", 2)[1]);
			holder.valueOne.setText(item.getUserHistory().getValue().split("\\|", 2)[0]);
			holder.valueOne.setTextSize(18);
			holder.valueTwo.setTextSize(18);
			holder.beforeWeight.setText("mmHg");
			holder.createdAt.setText(Utils.fromStringDateWithTimeZoneToFormateMMdd(item.getUserHistory().getCreatedAt()));
			holder.unit.setVisibility(View.GONE);
			holder.beforeWeight.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
			LinearLayout.LayoutParams lps = new LinearLayout.LayoutParams(Utils.getScreenWidth((Activity) context) / 4,
					Utils.getScreenWidth((Activity) context) / 4); // or set height to any fixed valueTwo you want
			holder.weightFeedLayout.setLayoutParams(lps);

		} else {
			holder.photo.setVisibility(View.VISIBLE);
			holder.weightFeedLayout.setVisibility(View.GONE);
			Glide.with(context).load(item.getMenu().getThumbnailImageUrl()).asBitmap().override(com.bumptech.glide.request.target.Target.SIZE_ORIGINAL, (Utils.getScreenWidth((Activity) context) / 4)).transform(new CustomTransformation(context)).fitCenter().into(new SimpleTarget<Bitmap>(com.bumptech.glide.request.target.Target.SIZE_ORIGINAL, (Utils.getScreenWidth((Activity) context) / 4)) {

				@Override
				public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
					holder.photo.setImageBitmap(resource);
				}
			});
		}
	}

	@Override
	public int getItemCount() {
		return getUserFeedsInProfileList().size();
	}

	class UserFeedsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private LinearLayout layoutPost;
		private TextView unit;
		private LinearLayout weightFeedLayout;
		private TextView valueOne, valueTwo;
		private TextView createdAt;
		private ImageView photo;
		private TextView beforeWeight;

		public UserFeedsViewHolder(View itemView) {
			super(itemView);
			valueOne = (TextView) itemView.findViewById(R.id.txt_value_one);
			valueTwo = (TextView) itemView.findViewById(R.id.txt_value_two);
			createdAt = (TextView) itemView.findViewById(R.id.txt_created_at);
			photo = (ImageView) itemView.findViewById(R.id.image_feed);
			beforeWeight = (TextView) itemView.findViewById(R.id.txt_before_weight);
			weightFeedLayout = (LinearLayout) itemView.findViewById(R.id.weight_feed);
			unit = (TextView) itemView.findViewById(R.id.txt_unit);
			layoutPost = (LinearLayout) itemView.findViewById(R.id.layout_post);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (null != listener) {
				listener.onItemClick(v, getAdapterPosition());
			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View v, int pos);
	}

	public void setOnItemClickListener(OnItemClickListener itemClickListener) {
		this.listener = itemClickListener;
	}

	private int getBackground(int time) {
		if (time >= 16) {
			return R.color.light_blue;
		} else if (time >= 4) {
			return R.color.light_yellow;
		} else if (time >= 0) {
			return R.color.light_blue;
		}
		return R.color.light_yellow;

	}
}
