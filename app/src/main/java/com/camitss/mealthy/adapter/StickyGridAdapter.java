package com.camitss.mealthy.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.SearchSettingTag;
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;

import java.util.List;

/**
 * Created by camittss007 on 6/5/2017.
 */

public class StickyGridAdapter extends StickyHeaderGridAdapter {

	private List<SearchSettingTag> listTag;
	private TagLikeListener liketaglistener;

	public StickyGridAdapter(List<SearchSettingTag> tableTag) {
		this.listTag = tableTag;
	}

	public void setAdapterListener(TagLikeListener likeListener){
		this.liketaglistener = likeListener;
	}

	@Override
	public int getSectionCount() {
		return listTag.size();
	}

	@Override
	public int getSectionItemCount(int section) {
		return listTag.get(section).getChildTag().size();
	}

	@Override
	public TagHeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
		final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_header, parent, false);
		return new TagHeaderViewHolder(view);
	}

	@Override
	public TagItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
		final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_item, parent, false);
		return new TagItemViewHolder(view);
	}

	@Override
	public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int section) {
		final TagHeaderViewHolder holder = (TagHeaderViewHolder)viewHolder;
		holder.labelView.setText((listTag.get(section)).getName());
		if(listTag.get(section).isLike()){
			holder.labelView.setBackgroundResource(R.drawable.tag_background_black);
		}else {
			holder.labelView.setBackgroundResource(R.drawable.tag_background_gray);
		}
		holder.labelView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final int section = getAdapterPositionSection(holder.getAdapterPosition());
				final int offset = getItemSectionOffset(section, holder.getAdapterPosition());
				liketaglistener.onHeaderSelected(section);
			}
		});
	}

	@Override
	public void onBindItemViewHolder(ItemViewHolder viewHolder, int section, int offset) {
		final TagItemViewHolder holder = (TagItemViewHolder)viewHolder;
		final String label = listTag.get(section).getChildTag().get(offset).getName();
		holder.labelView.setText(label);
		if(listTag.get(section).getChildTag().get(offset).isLike()){
			holder.labelView.setBackgroundResource(R.drawable.tag_background_black);
		}else {
			holder.labelView.setBackgroundResource(R.drawable.tag_background_gray);
		}
		holder.labelView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final int section = getAdapterPositionSection(holder.getAdapterPosition());
				final int offset = getItemSectionOffset(section, holder.getAdapterPosition());
				liketaglistener.onChildSelected(section,offset);
				//   notifySectionItemRemoved(section, offset);
			}
		});
	}

	public static class TagHeaderViewHolder extends HeaderViewHolder {
		TextView labelView;

		TagHeaderViewHolder(View itemView) {
			super(itemView);
			labelView = (TextView) itemView.findViewById(R.id.tag_label_header);
		}
	}

	public static class TagItemViewHolder extends ItemViewHolder {
		TextView labelView;

		TagItemViewHolder(View itemView) {
			super(itemView);
			labelView = (TextView) itemView.findViewById(R.id.tag_label_child);
		}
	}

	public interface TagLikeListener{
		void onHeaderSelected(int headerPosition);
		void onChildSelected(int headerPosition, int childPosition);
	}
}
