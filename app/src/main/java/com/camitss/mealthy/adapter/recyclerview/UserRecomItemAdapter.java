package com.camitss.mealthy.adapter.recyclerview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 4/12/2017.
 */

public class UserRecomItemAdapter extends RecyclerView.Adapter<UserRecomItemAdapter.UserRecomItemViewHolder> {

	private List<UserRecomItem> userRecomItemList = new ArrayList<>();
	private Context context;
	private boolean isHorizontal;

	public interface Orientation {
		int VERTICAL = -1;
		int HORIZONTAL = 0;

	}

	public UserRecomItemAdapter(Context context) {
		this.context = context;
	}

	public boolean isHorizontal() {
		return isHorizontal;
	}

	public void setOrientation(int orientation) {
		if (orientation == Orientation.HORIZONTAL)
			isHorizontal = true;
		else isHorizontal = false;
	}

	public void addAllUserRecomItemList(List<UserRecomItem> list) {
		userRecomItemList.clear();
		userRecomItemList.addAll(list);
	}

	public void addUserRecomItemList(List<UserRecomItem> list) {
		userRecomItemList = list;
	}

	public List<UserRecomItem> getUserRecomItemList() {
		return userRecomItemList;
	}

	@Override
	public UserRecomItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(isHorizontal() ? R.layout.layout_user_recom_item_horizontal : R.layout.layout_user_recom_item_vertical, parent, false);
		return new UserRecomItemViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final UserRecomItemViewHolder holder, int position) {
		UserRecomItem item = getUserRecomItemList().get(position);
		if (null != item) {
			holder.userName.setText(item.getName());
			setFollowButton(holder.textFollow, item.isFollowing());
			holder.textSimilarPercent.setText("類似度" + item.getSimilar() + "%");
			holder.textSimilarPercent.setVisibility(item.getSimilar() > 0 ? View.VISIBLE : View.GONE);
//			Glide.with(context).load(item.getProfileImageUrl()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(context)).into(holder.userProfile);
			if (isHorizontal())
				Glide.with(context).load(item.getLastPostedImageUrl()).placeholder(R.color.grey_light).error(R.color.grey_light).centerCrop().into(holder.userProfile);
			else {
				if (null != item.getProfileImageUrl() && !item.getProfileImageUrl().equalsIgnoreCase("null") && !item.getProfileImageUrl().isEmpty()) {
					Glide.with(context).load(item.getProfileImageUrl()).transform(new CircleTransformation(context)).into(new SimpleTarget<GlideDrawable>() {
						@Override
						public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
							holder.userProfile.setImageDrawable(resource);
							holder.imageBorder.setVisibility(View.VISIBLE);
						}
					});

				} else {
					holder.imageBorder.setVisibility(View.GONE);
					holder.userProfile.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_account_circle));
				}
			}
		}
	}

	private void setFollowButton(TextView textFollow, boolean following) {
		textFollow.setText(following ? "Following" : "+ Follow");
		textFollow.setBackground(following ? ContextCompat.getDrawable(context, R.drawable.bg_orange_fill) : ContextCompat.getDrawable(context, R.drawable.bg_edittext_normal));
		textFollow.setTextColor(following ? ContextCompat.getColor(context, R.color.white) : ContextCompat.getColor(context, R.color.orange));

	}

	@Override
	public int getItemCount() {
		return getUserRecomItemList().size();
	}

	public class UserRecomItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private ImageView imageBorder;
		private TextView userName, textFollow;
		private ImageView userProfile;
		private TextView textSimilarPercent;

		public UserRecomItemViewHolder(View itemView) {
			super(itemView);

			userProfile = (ImageView) itemView.findViewById(R.id.user_profile);
			userName = (TextView) itemView.findViewById(R.id.user_name);
			textFollow = (TextView) itemView.findViewById(R.id.txt_follow);
			textSimilarPercent = (TextView) itemView.findViewById(R.id.txt_similar_percentage);
			if (!isHorizontal())
				imageBorder = (ImageView) itemView.findViewById(R.id.img_border);
			if (!isHorizontal()) {
				LinearLayout layoutNameAndSimilarPercentage = (LinearLayout) itemView.findViewById(R.id.layout_name_and_similar_percentage);
				layoutNameAndSimilarPercentage.setOnClickListener(this);
			}
			userProfile.setOnClickListener(this);
			userName.setOnClickListener(this);
			textFollow.setOnClickListener(this);

		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.user_profile:
				case R.id.user_name:
				case R.id.layout_name_and_similar_percentage:
					UserRecomProfileDisplayActivity.launch(context, getUserRecomItemList().get(getAdapterPosition()).getId());
					break;
				case R.id.txt_follow:
					requestToFollowUser((TextView) v, getUserRecomItemList().get(getAdapterPosition()));
					break;
			}
		}

	}

	private void requestToFollowUser(final TextView view, final UserRecomItem userRecomItem) {
		int method = userRecomItem.isFollowing() ? Request.Method.DELETE : Request.Method.POST;
		RequestHandler request = new RequestHandler(context);
		request.jsonRequest(Utils.convertFunctionName(Constant.REQUEST_FOLLOW_USER, userRecomItem.getId()), method, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (userRecomItem.isFollowing()) userRecomItem.setFollowing(false);
				else userRecomItem.setFollowing(true);
				setFollowButton(view, userRecomItem.isFollowing());
			}
		});
	}

}
