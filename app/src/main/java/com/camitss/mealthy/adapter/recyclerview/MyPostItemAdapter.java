package com.camitss.mealthy.adapter.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 4/3/2017.
 */

public class MyPostItemAdapter extends SocialPostItemAdapter {

	public MyPostItemAdapter(Activity activity) {
		super(activity);
	}

	private int mHieght;

	public int getmHieght() {
		return mHieght;
	}

	public void setmHieght(int mHieght) {
		this.mHieght = mHieght + getmHieght();
	}

	@Override
	public int getItemViewType(int position) {
		return 2;
	}

	@Override
	public int getItemCount() {
		return getListPost().size();
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

		final ItemViewHolder holder = (ItemViewHolder) viewHolder;
		try {
			FeedItem item = getListPost().get(i);
			FeedItem.Menu menu = item.getMenu();
			FeedItem.FeedItemUser user = item.getFeedItemUser();
			Glide.with(context).load(menu.getImageThumb()).placeholder(R.color.white).into(holder.imageItem);
			holder.textUserName.setText(user.getName());
			if (null != user && !user.getProfileImage().equalsIgnoreCase("null") && !user.getProfileImage().isEmpty()) {
				Glide.with(context).load(user.getProfileImage()).transform(new CircleTransformation(activity)).into(new SimpleTarget<GlideDrawable>() {
					@Override
					public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
						holder.imageUserProfile.setImageDrawable(resource);
					}
				});
				holder.imageBorder.setVisibility(user.getUserKind() == 2 ? View.GONE : View.VISIBLE);

			} else {
				holder.imageBorder.setVisibility(View.GONE);
				holder.imageUserProfile.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_account_circle));
			}
			holder.imageProNutrit.setVisibility(user.getUserKind() == 2 ? View.VISIBLE : View.INVISIBLE);
			boolean isHaveShopName;
			boolean isHaveMenuName;
			if (null != menu.getCgmShopName() && !menu.getCgmShopName().equals("null")) {
				isHaveShopName = true;
				holder.shopName.setText(menu.getCgmShopName());
			} else {
				isHaveShopName = false;
			}
			if (null != menu.getCgmMenuName() && !menu.getCgmMenuName().equals("null") && !menu.getCgmMenuName().trim().isEmpty()) {
				isHaveMenuName = true;
				holder.menuName.setText(menu.getCgmMenuName());
			} else isHaveMenuName = false;
			holder.layoutShadowText.setVisibility((isHaveMenuName || isHaveShopName) ? View.VISIBLE : View.GONE);
			holder.shopName.setVisibility(isHaveShopName ? View.VISIBLE : View.GONE);
			holder.menuName.setVisibility(isHaveMenuName ? View.VISIBLE : View.GONE);
			Log.d("test", "size" + menu.getCommentList().size());
			holder.layoutCmt.setVisibility(menu.getCommentCount() > 0 ? View.VISIBLE : View.GONE);
			if (menu.getCommentList().size() > 0) {
				holder.layoutCmt.removeAllViews();
				createCmtChildLayout(activity, context, holder.layoutCmt, item);
			} else holder.layoutCmt.removeAllViews();
			setFeedFavoriteAndTextCount(item.isLike(), holder.imageHeart, holder.textFavorite, item.getLikesCount());
			if (!menu.getCreatedAt().isEmpty()) {
				holder.textDate.setText(Utils.fromStringDateWithTimeZoneToFormate(menu.getCreatedAt()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		holder.layoutWhole.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
		int width = holder.layoutWhole.getMeasuredWidth();
		int height = holder.layoutWhole.getMeasuredHeight();
		setmHieght(height);
		Log.d("resolution", width + "<<>" + height + "<<>>" + getmHieght());

	}

}
