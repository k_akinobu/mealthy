package com.camitss.mealthy.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.fragment.NearestShopFragment;
import com.camitss.mealthy.listener.MenuClickListener;
import com.camitss.mealthy.listener.ShopClickListener;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.Shop;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CircleTransformation;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//
///**
// * Created by DELL on 7/26/2016.
// */
public class MenuDetailRecycleAdapter extends RecyclerView.Adapter {
	private List<MealthyMenu> listMenu;
	private List<Shop> shopList;
	private Activity activity;
	private ShopClickListener clickListener;
	private MenuClickListener menuClickListener;
	private NearestShopFragment shopFragment;

	public MenuClickListener getMenuClickListener() {
		return menuClickListener;
	}

	public void setMenuClickListener(MenuClickListener menuClickListener) {
		this.menuClickListener = menuClickListener;
	}

	public NearestShopFragment getShopFragment() {
		return shopFragment;
	}

	public void setShopFragment(NearestShopFragment shopFragment) {
		this.shopFragment = shopFragment;
	}

	public ShopClickListener getClickListener() {
		return clickListener;
	}

	public MenuDetailRecycleAdapter(Activity activity, List<MealthyMenu> listMenu, NearestShopFragment shopFragment) {
		this.listMenu = listMenu;
		this.activity = activity;
		this.shopFragment = shopFragment;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.shop_item, parent, false);

		return new MenuViewHolder(itemView, viewType);
	}


	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
		final MenuViewHolder menuHolder = (MenuViewHolder) holder;

		final MealthyMenu menu = listMenu.get(position);
		menuHolder.name.setText(menu.getName());
		menuHolder.calorie.setText(Float.toString(menu.getCalorie()) + "\nkcal");

		Glide.with(activity).load(menu.getThumbnal_image_url()).placeholder(R.drawable.bg_grey).into(menuHolder.thumbnail);
		String t = "";
		for (int i = 0; i < menu.getTags().size(); i++) {
			t = t + " #" + menu.getTags().get(i).getName() + " ";
		}
		menuHolder.tag.setText(t);
		if (listMenu.get(position).getNutritionist_comments().size() > 0) {
			menuHolder.nc_layout.setVisibility(View.VISIBLE);
			menuHolder.nc_cmt.setText(listMenu.get(position).getNutritionist_comments().get(0).getNc_body());
			Glide.with(activity).load(listMenu.get(position).getNutritionist_comments().get(0).getNutritionistUser().getNuser_profile_image()).placeholder(R.drawable.bg_grey).transform(new CircleTransformation(activity)).into(menuHolder.nc_profile);
		} else {
			menuHolder.nc_layout.setVisibility(View.GONE);
		}
		menuHolder.detail_shop.setVisibility(View.GONE);
		menuHolder.star.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (menuHolder.star.isSelected()) {
					menuHolder.star.setSelected(false);

				} else {
					menuHolder.star.setSelected(true);
				}
			}
		});
		menuHolder.view_menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				menuClickListener.onMenuClickListener(menu);
			}
		});
		if (listMenu.get(position).isFavorith()) {
			menuHolder.star.setSelected(true);
		} else {
			menuHolder.star.setSelected(false);
		}
		menuHolder.star.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (menuHolder.star.isSelected()) {
					requetsDelFav(listMenu.get(position), menuHolder.star);
				} else {
					requetsFav(listMenu.get(position), menuHolder.star);
				}
			}
		});
		if (listMenu.get(position).getOrigin() == 1) {
			menuHolder.calorie.setBackgroundResource(R.drawable.ic_badge01);
		} else if (listMenu.get(position).getOrigin() == 2) {
			menuHolder.calorie.setBackgroundResource(R.drawable.ic_badge03);
		} else if (listMenu.get(position).getOrigin() == 3) {
			menuHolder.calorie.setBackgroundResource(R.drawable.ic_badge02);
		}
	}


	@Override
	public int getItemViewType(int position) {
		return position;
	}


	@Override
	public int getItemCount() {
		return listMenu.size();
	}

	public class MenuViewHolder extends RecyclerView.ViewHolder {
		public TextView name, calorie, tag, nc_cmt, detail_shop;
		public ImageView thumbnail, nc_profile, star;
		public LinearLayout nc_layout;
		public RelativeLayout view_menu;

		public MenuViewHolder(View view, int viewType) {
			super(view);
			name = (TextView) view.findViewById(R.id.menu_name);
			thumbnail = (ImageView) view.findViewById(R.id.menu_thumbnail);
			calorie = (TextView) view.findViewById(R.id.food_kcal);
			view_menu = (RelativeLayout) view.findViewById(R.id.view_menu);
			tag = (TextView) view.findViewById(R.id.menu_tag);
			nc_cmt = (TextView) view.findViewById(R.id.nc_cmt);
			nc_profile = (ImageView) view.findViewById(R.id.nc_profile);
			nc_layout = (LinearLayout) view.findViewById(R.id.nc_layout);
			detail_shop = (TextView) view.findViewById(R.id.detail_shop);
			star = (ImageView) view.findViewById(R.id.is_favorite);

		}
	}

	private void requetsDelFav(final MealthyMenu obj, final ImageView view) {
		try {
			final RequestHandler requestHandler = new RequestHandler(activity);
			requestHandler.jsonRequest(Constant.BASE_URL + "/favorites/" + obj.getFavId(), Request.Method.DELETE, null, null);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					try {
						if (data.getBoolean("success")) {
							view.setSelected(false);
							obj.setFavorith(false);
							obj.setFavId(0);
//							RealmHelper.with(activity).deleteMealthyMenuFavbyId(obj.getId());
							RealmHelper.with(activity).save(obj);
							getShopFragment().restartData(obj, false);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});


		} catch (Exception e) {

		}
	}

	private void requetsFav(final MealthyMenu obj, final ImageView view) {
		try {
			JSONObject body = new JSONObject();
			try {
				body.put("menu_id", obj.getId());

			} catch (Exception e) {
				e.printStackTrace();
			}

			final RequestHandler requestHandler = new RequestHandler(activity);
			requestHandler.jsonRequest(Constant.BASE_URL + "/favorites", Request.Method.POST, null, body);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					try {
						obj.setFavId(data.getJSONObject("favorite").getInt("id"));
						view.setSelected(true);
						obj.setFavorith(true);
						RealmHelper.with(activity).save(obj);
						getShopFragment().restartData(obj, true);


					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});


		} catch (Exception e) {

		}
	}
}
