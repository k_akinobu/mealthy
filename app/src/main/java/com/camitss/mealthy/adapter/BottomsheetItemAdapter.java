package com.camitss.mealthy.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camitss.mealthy.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 3/24/2017.
 */

public class BottomsheetItemAdapter extends RecyclerView.Adapter {

	private Context mContext;
	private List mCountryList = new ArrayList<>();
	private OnItemClickListener mItemClickListener;


	public BottomsheetItemAdapter(Context context, List<?> countryList) {
		this.mContext = context;
		addAllItem(countryList);
	}

	public void addAllItem(List<?> items) {
		mCountryList.clear();
		mCountryList.addAll(items);

	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.bottomsheet_item, viewGroup, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
		ViewHolder vHolder = (ViewHolder) viewHolder;
		vHolder.countryName.setText((String) mCountryList.get(i));
	}

	@Override
	public int getItemCount() {
		return mCountryList.size();
	}


	public List getItemList() {
		return mCountryList;
	}


	public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		private TextView countryName;

		public ViewHolder(View itemView) {
			super(itemView);
			countryName = (TextView) itemView.findViewById(R.id.txtCountryName);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View view) {
			if (null != mItemClickListener) {
				mItemClickListener.onItemClick(view, getAdapterPosition());
			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View v, int position);
	}

	public void setOnItemClickListner(OnItemClickListener mOnItemClickListener) {
		this.mItemClickListener = mOnItemClickListener;
	}

}
