package com.camitss.mealthy.adapter;

/**
 * Created by User
 * Date 3/21/2017
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.camitss.mealthy.fragment.GuideFragment;

public class CustomPagerAdapter extends FragmentPagerAdapter {
	private static int NUM_ITEMS = 4;//5

	public CustomPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	// Returns total number of pages
	@Override
	public int getCount() {
		return NUM_ITEMS;
	}

	// Returns the fragment to display for that page
	@Override
	public Fragment getItem(int position) {
		switch (position) {
			//			close it for a while since now no search screen => num_of_item=4
//			case 0: // Fragment # 0 - This will show FirstFragment
//				return GuideFragment.newInstant(1);
			case 0: // Fragment # 0 - This will show FirstFragment different title
				return GuideFragment.newInstant(2);
			case 1: // Fragment # 1 - This will show SecondFragment
				return GuideFragment.newInstant(3);
			case 2: // Fragment # 1 - This will show SecondFragment
				return GuideFragment.newInstant(4);
			case 3: // Fragment # 1 - This will show SecondFragment
				return GuideFragment.newInstant(5);
			default:
				return null;
		}
	}

	// Returns the page title for the top indicator
	@Override
	public CharSequence getPageTitle(int position) {
		return "Page " + position;
	}

}