package com.camitss.mealthy.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.listener.SetItemClickListener;
import com.camitss.mealthy.object.Diagnoses;

import java.util.ArrayList;

/**
 * Created by Darith on 3/23/2017.
 */

public class DiagnosisAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Diagnoses> diagnoses;
    private String page;
    private SetItemClickListener setItemClickListener;


    public DiagnosisAdapter(Context context, ArrayList<Diagnoses> diagnoses, String page) {
        this.context = context;
        this.diagnoses = diagnoses;
        this.page = page;

    }

    public SetItemClickListener getSetItemClickListener() {
        return setItemClickListener;
    }

    public void setSetItemClickListener(SetItemClickListener setItemClickListener) {
        this.setItemClickListener = setItemClickListener;
    }

    @Override
    public int getCount() {
        return diagnoses.size();
    }

    @Override
    public Object getItem(int position) {
        return diagnoses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.diagnosis_item, parent, false);
        }
        final Diagnoses diagnos = diagnoses.get(position);

        TextView questionNumber = (TextView) convertView.findViewById(R.id.intro_question_id);
        TextView question = (TextView) convertView.findViewById(R.id.intro_question);
        final ImageButton agreeTick = (ImageButton) convertView.findViewById(R.id.intro_select_option);
        if (diagnos.isSelectedQuestion()) {
            agreeTick.setSelected(true);
        } else {
            agreeTick.setSelected(false);
        }

        agreeTick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (agreeTick.isSelected()) {
                    agreeTick.setSelected(false);
                    diagnos.setSelectedQuestion(false);

                } else {
                    diagnos.setSelectedQuestion(true);
                    agreeTick.setSelected(true);
                }
                setItemClickListener.onItemClickListener(diagnos);
            }
        });

        questionNumber.setText("Q" + diagnos.getPosition());
        question.setText(diagnos.getBody());

        return convertView;
    }
}
