package com.camitss.mealthy.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;

/**
 * Created by Darith on 5/22/2017.
 */

public class NearImageAdapter extends BaseAdapter {
	private Context mContext;
	private Integer[] mThumbIds;
	private String[] mTitle;

	public NearImageAdapter(Context c, Integer[] mThumbIds, String[] mTitle) {
		mContext = c;
		this.mThumbIds = mThumbIds;
		this.mTitle = mTitle;
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		final View result;
		if (convertView == null) {

			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(R.layout.genre_item, parent, false);
			viewHolder.txtName = (TextView) convertView.findViewById(R.id.genre_txt);
			viewHolder.info = (ImageView) convertView.findViewById(R.id.genre_img);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Glide.with(mContext).load(mThumbIds[position]).placeholder(R.drawable.bg_grey).into(viewHolder.info);
		viewHolder.txtName.setText(mTitle[position]);
		return convertView;
	}


	private static class ViewHolder {
		TextView txtName;
		ImageView info;
	}
}