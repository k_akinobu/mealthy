package com.camitss.mealthy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.HistorySearch;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Darith on 5/22/2017.
 */

public class HistorySearchAdapter extends BaseAdapter {
	private Context mContext;
	private List<String> data;

	public HistorySearchAdapter(Context c, List<String> data) {
		mContext = c;
		this.data = data;
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		final View result;
		if (convertView == null) {

			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(R.layout.history_search_item, parent, false);
			viewHolder.history = (TextView) convertView.findViewById(R.id.history_txt);
			viewHolder.layout = (LinearLayout) convertView.findViewById(R.id.linearLayout);
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
//		Glide.with(mContext).load(mThumbIds[position]).placeholder(R.drawable.bg_grey).into(viewHolder.info);
//		viewHolder.txtName.setText(mTitle[position]);
		viewHolder.history.setText(data.get(position));
		return convertView;
	}


	private static class ViewHolder {
		TextView history;
		LinearLayout layout;
	}
}