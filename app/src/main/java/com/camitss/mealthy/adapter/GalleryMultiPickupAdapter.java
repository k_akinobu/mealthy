package com.camitss.mealthy.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.GalleryMultiPickup;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.utils.Utils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User
 * Date 4/26/2017
 */

public class GalleryMultiPickupAdapter extends RecyclerView.Adapter {

	private Context mContext;
	ArrayList<GalleryMultiPickup> itemList;
	private int id = 0;
	private int limitItem = 6, counter = 0;
	private GalleryMultiPickupAdapter.OnItemClickListener listener;

	public GalleryMultiPickupAdapter(Context c) {
		mContext = c;
		itemList = new ArrayList<>();
	}

	public void add(GalleryMultiPickup obj) {
		obj.setId(id);
		itemList.add(obj);
		id++;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_multi_pickup_item, parent, false);
		return new GalleryMultiPickupAdapter.ImageViewHolder(view, itemList);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		GalleryMultiPickup item = itemList.get(position);
		ImageViewHolder mHolder = (ImageViewHolder) holder;
		if (null != item) {
			mHolder.id.setText(item.getId() + "");
			Glide.with(mContext).load(item.getPath()).override(Utils.getScreenWidth((Activity) mContext) / 4, Utils.getScreenWidth((Activity) mContext) / 4).into(mHolder.imageItem);
			if (item.isSelected()) {
				mHolder.checked.setVisibility(View.VISIBLE);
				mHolder.r.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
			} else {
				mHolder.checked.setVisibility(View.INVISIBLE);
				mHolder.r.setBackgroundColor(mContext.getResources().getColor(R.color.white));
			}
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getItemCount() {
		return itemList.size();
	}

	public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		public ImageView imageItem, checked;
		public List<GalleryMultiPickup> list;
		public TextView id;
		public RelativeLayout r;

		public ImageViewHolder(View itemView, List<GalleryMultiPickup> list) {
			super(itemView);
			this.list = list;
			imageItem = (ImageView) itemView.findViewById(R.id.multi_pick_image_item);
			checked = (ImageView) itemView.findViewById(R.id.selected_item);
			id = (TextView) itemView.findViewById(R.id.multi_select_item_id);
			r = (RelativeLayout) itemView.findViewById(R.id.view_item);
			checked.setVisibility(View.INVISIBLE);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (null != listener) {
				TextView idV = (TextView) v.findViewById(R.id.multi_select_item_id);
				RelativeLayout r = (RelativeLayout) v.findViewById(R.id.view_item);
				int id = Integer.parseInt(idV.getText().toString());
				boolean isAdd = true;
				if (list.get(id).isSelected()) {

						isAdd = false;
						list.get(id).setSelected(false);
						checked.setVisibility(View.INVISIBLE);
						r.setBackgroundColor(mContext.getResources().getColor(R.color.white));
						listener.onItemClick(v, list.get(id), isAdd);
//					}
					if (counter > 0) {
						counter--;
					}
				} else {
					if(counter == limitItem){
						listener.onReachLimitItem();
					}
					if (counter < limitItem) {
						isAdd = true;
						list.get(id).setSelected(true);
						checked.setVisibility(View.VISIBLE);
						r.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
						listener.onItemClick(v, list.get(id), isAdd);
						counter++;
					}
				}

			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View view, GalleryMultiPickup galleryMultiPickup, boolean isAdd);
		void onReachLimitItem();
	}

	public void setOnItemClickListener(GalleryMultiPickupAdapter.OnItemClickListener listener) {
		this.listener = listener;
	}
}
