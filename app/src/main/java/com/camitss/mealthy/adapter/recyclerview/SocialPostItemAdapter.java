package com.camitss.mealthy.adapter.recyclerview;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.CommentDetailActivity;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.HistoryFeedItemDisplayActivity;
import com.camitss.mealthy.activity.TestActivity;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.fragment.UserProfileFragment;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.MealthyMenuComment;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 4/3/2017.
 */

public class SocialPostItemAdapter extends RecyclerView.Adapter {

	protected Activity activity;
	protected Context context;
	private List<FeedItem> listPost = new ArrayList<>();
	private List<UserRecomItem> listUserRecom = new ArrayList<>();
	private OnItemClickListener listener;

	public SocialPostItemAdapter(Activity activity) {
		this.activity = activity;
		this.context = activity.getApplicationContext();
	}

	public void addUserRecommendList(List<UserRecomItem> listUserRecom) {
		this.listUserRecom.clear();
		this.listUserRecom.addAll(listUserRecom);
	}

	public List<UserRecomItem> getListUserRecom() {
		return listUserRecom;
	}

	public void addPostLists(List<FeedItem> list) {
		this.listPost.addAll(list);
	}

	public List<FeedItem> getListPost() {
		return this.listPost;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		if (i == 0) {
			View v = LayoutInflater.from(context).inflate(R.layout.recycler_header, viewGroup, false);
			return new HeaderViewHolder(v);
		} else if (i == 1 && getListUserRecom().size() > 0) {
			View v = LayoutInflater.from(context).inflate(R.layout.user_recom_item_suggestion_in_feed_horizontal_layout, viewGroup, false);
			return new SuggestionInFeedViewHolder(v);
		} else {
			View view = LayoutInflater.from(context).inflate(R.layout.layout_social_post_item, viewGroup, false);
			return new ItemViewHolder(view);
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
		if (viewHolder instanceof ItemViewHolder) {
			final ItemViewHolder holder = (ItemViewHolder) viewHolder;
			int pos = 1;
			if (getListUserRecom().size() > 0) {
				pos = i > 2 ? 2 : 1;
			}
			i = i - pos;
			try {
				FeedItem item = getListPost().get(i);
				FeedItem.Menu menu = item.getMenu();
				FeedItem.FeedItemUser user = item.getFeedItemUser();
				Glide.with(context).load(menu.getImageThumb()).placeholder(R.color.white).into(holder.imageItem);
				holder.textUserName.setText(user.getName());
				if (null != user && !user.getProfileImage().equalsIgnoreCase("null") && !user.getProfileImage().isEmpty()) {
					Glide.with(context).load(user.getProfileImage()).transform(new CircleTransformation(activity)).into(new SimpleTarget<GlideDrawable>() {
						@Override
						public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
							holder.imageUserProfile.setImageDrawable(resource);
						}
					});
					holder.imageBorder.setVisibility(user.getUserKind() == 2 ? View.GONE : View.VISIBLE);

				} else {
					holder.imageBorder.setVisibility(View.GONE);
					holder.imageUserProfile.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_account_circle));
				}
				holder.imageProNutrit.setVisibility(user.getUserKind() == 2 ? View.VISIBLE : View.INVISIBLE);
				boolean isHaveShopName;
				boolean isHaveMenuName;
				if (null != menu.getCgmShopName() && !menu.getCgmShopName().equals("null")) {
					isHaveShopName = true;
					holder.shopName.setText(menu.getCgmShopName());
				} else {
					isHaveShopName = false;
				}
				if (null != menu.getCgmMenuName() && !menu.getCgmMenuName().equals("null") && !menu.getCgmMenuName().trim().isEmpty()) {
					isHaveMenuName = true;
					holder.menuName.setText(menu.getCgmMenuName());
				} else isHaveMenuName = false;
				holder.layoutShadowText.setVisibility((isHaveMenuName || isHaveShopName) ? View.VISIBLE : View.GONE);
				holder.shopName.setVisibility(isHaveShopName ? View.VISIBLE : View.GONE);
				holder.menuName.setVisibility(isHaveMenuName ? View.VISIBLE : View.GONE);
				Log.d("test", "size" + menu.getCommentList().size());
				holder.layoutCmt.setVisibility(menu.getCommentCount() > 0 ? View.VISIBLE : View.GONE);
				if (menu.getCommentList().size() > 0) {
					holder.layoutCmt.removeAllViews();
//					createCmtChildLayout(activity, context, holder.layoutCmt, item);
					createCmtView(item, holder.layoutCmt);
				} else holder.layoutCmt.removeAllViews();
				setFeedFavoriteAndTextCount(item.isLike(), holder.imageHeart, holder.textFavorite, item.getLikesCount());
				if (!menu.getCreatedAt().isEmpty()) {
					holder.textDate.setText(Utils.fromStringDateWithTimeZoneToFormate(menu.getCreatedAt()));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (viewHolder instanceof SuggestionInFeedViewHolder) {
			SuggestionInFeedViewHolder holder = (SuggestionInFeedViewHolder) viewHolder;
			setSuggestionUserRecomRecyclerview(holder);
		}
	}

	@Override
	public int getItemCount() {
		return getListUserRecom().size() > 0 ? getListPost().size() + 2 : getListPost().size() + 1;
	}

	public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		protected final ImageView imageShare;
		protected LinearLayout layoutWhole;
		protected ImageView imageBorder;
		protected ImageView imageProNutrit;
		private ImageView imageCmt;
		protected TextView textDate;
		public TextView textFavorite;
		protected LinearLayout layoutCmt;
		protected TextView menuName;
		protected TextView shopName;
		protected LinearLayout layoutShadowText;
		protected ImageView imageUserProfile;
		protected TextView textUserName;
		protected ImageView imageItem;
		protected ImageView imageHeart;

		public ItemViewHolder(View itemView) {
			super(itemView);
			layoutWhole = (LinearLayout) itemView.findViewById(R.id.layout_whole);
			imageItem = (ImageView) itemView.findViewById(R.id.image_post);
			imageUserProfile = (ImageView) itemView.findViewById(R.id.user_profile);
			textUserName = (TextView) itemView.findViewById(R.id.user_name);
			menuName = (TextView) itemView.findViewById(R.id.menu_name);
			shopName = (TextView) itemView.findViewById(R.id.shop_name);
			layoutShadowText = (LinearLayout) itemView.findViewById(R.id.shadow_text);
			layoutCmt = (LinearLayout) itemView.findViewById(R.id.layout_cmt);
			textFavorite = (TextView) itemView.findViewById(R.id.txt_favorite);
			textDate = (TextView) itemView.findViewById(R.id.txt_date);
			imageHeart = (ImageView) itemView.findViewById(R.id.img_heart);
			imageCmt = (ImageView) itemView.findViewById(R.id.img_cmt);
			imageProNutrit = (ImageView) itemView.findViewById(R.id.img_pro_nutritionist);
			imageShare = (ImageView) itemView.findViewById(R.id.img_share);
			imageBorder = (ImageView) itemView.findViewById(R.id.img_border);

			itemView.setOnClickListener(this);
			imageItem.setOnClickListener(this);
			imageHeart.setOnClickListener(this);
			imageCmt.setOnClickListener(this);
			imageUserProfile.setOnClickListener(this);
			textUserName.setOnClickListener(this);
			imageShare.setOnClickListener(this);

		}

		@Override
		public void onClick(View v) {
			if (null != listener) {
				listener.onItemClick(v, getAdapterPosition());
			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View v, int pos);
	}

	public void setOnItemClickListener(OnItemClickListener itemClickListener) {
		this.listener = itemClickListener;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == 0) return 0;
		else if (position == 2 && getListUserRecom().size() > 0) return 1;
		return 2;
	}

	public class HeaderViewHolder extends RecyclerView.ViewHolder {

		public HeaderViewHolder(View itemView) {
			super(itemView);
		}
	}

	public void setFeedFavoriteAndTextCount(boolean isLiked, ImageView view, TextView textFavorite, int likeCount) {
		Drawable drawable = isLiked ? ContextCompat.getDrawable(context, R.drawable.ic_favorite_heart_red) : ContextCompat.getDrawable(context, R.drawable.ic_favorite_heart);
		view.setImageDrawable(drawable);
		textFavorite.setVisibility(likeCount > 0 ? View.VISIBLE : View.INVISIBLE);
		if (likeCount > 0) {
			textFavorite.setText("いいね ! " + likeCount + "件");
		}
	}

	public class SuggestionInFeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		private TextView textMore;
		private RecyclerView recyclerUserRecom;

		public SuggestionInFeedViewHolder(View itemView) {
			super(itemView);
			textMore = (TextView) itemView.findViewById(R.id.text_more);
			recyclerUserRecom = (RecyclerView) itemView.findViewById(R.id.rcl_suggestion_post);
			textMore.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
//			if (v.getId() == R.id.text_more) {
//				UserRecommendationDisplayActivity.launchForResult(activity, true, 251);
//				Intent i = new Intent(activity, UserRecommendationDisplayActivity.class);
//				i.putExtra("isFromSuggestion", true);
//				activity.startActivityForResult(i,251);
//			}
			if (null != listener) {
				listener.onItemClick(v, getAdapterPosition());
			}
		}
	}

	private void setSuggestionUserRecomRecyclerview(SuggestionInFeedViewHolder holder) {
		LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
		holder.recyclerUserRecom.setLayoutManager(layoutManager);
		UserRecomItemAdapter adapter = new UserRecomItemAdapter(context);
		adapter.setOrientation(UserRecomItemAdapter.Orientation.HORIZONTAL);
		holder.recyclerUserRecom.setAdapter(adapter);
		adapter.addAllUserRecomItemList(getListUserRecom());
		adapter.notifyDataSetChanged();
	}

	private void createCmtView(final FeedItem item, LinearLayout layout) {
		layout.removeAllViews();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(20, 10, 20, 10);
		layout.setLayoutParams(params);
		TextView tv;
		List<FeedItem.Comment> cmtList = item.getMenu().getCommentList();
		FeedItem.Comment cmtObj;
		for (int i = 0; i < cmtList.size(); i++) {
			if (i < 5) {
				tv = new TextView(context);
				tv.setPadding(10, 10, 10, 10);
				if (i == 4) {
					tv.setText(cmtList.size() + "件全てのコメントを表示する");
					tv.setTextColor(ContextCompat.getColor(context, R.color.grey_light));
					tv.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent i = new Intent(activity, HistoryFeedItemDisplayActivity.class);
							i.putExtra("FeedItem", item);
							activity.startActivityForResult(i, 250);
						}
					});
				} else {
					cmtObj = cmtList.get(i);
					final int userId = cmtObj.getCmtUser().getId();
					setActionText(cmtObj.getCmtUser().getName() + ":", cmtObj.getBody(), tv, userId, item);
				}
				layout.addView(tv);
			} else return;
		}
	}

	private void setActionText(final String firstString, final String lastString, TextView txtSpan, final int userId, final FeedItem item) {
		final ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(final View textView) {
				int myId = AppSharedPreferences.getConstant(context).getUserIdPref();
				if (myId == userId) {
					UserProfileFragment userFragment = new UserProfileFragment();
					activity.getFragmentManager().beginTransaction().replace(R.id.main_container, userFragment, "USER_PROFILE").addToBackStack("MAIN_TAB_TWO").commit();
				} else {
					Intent i = new Intent(activity, UserRecomProfileDisplayActivity.class);
					i.putExtra("userId", userId);
					activity.startActivityForResult(i, 250);
				}
			}

			@Override
			public void updateDrawState(final TextPaint textPaint) {
				textPaint.setColor(ContextCompat.getColor(context, android.R.color.black));
				textPaint.setTypeface(Typeface.DEFAULT_BOLD);
				textPaint.setUnderlineText(false);
			}
		};
		final ClickableSpan clickableSpanTwo = new ClickableSpan() {
			@Override
			public void onClick(final View textView) {
				Intent i = new Intent(activity, FeedDisplayActivity.class);
				i.putExtra("FeedItem", item);
				activity.startActivityForResult(i, 250);
			}

			@Override
			public void updateDrawState(final TextPaint textPaint) {
				textPaint.setUnderlineText(false);
				textPaint.setColor(ContextCompat.getColor(context, R.color.black));
			}
		};

		SpannableString span1 = new SpannableString(firstString);
		SpannableString span2 = new SpannableString(lastString);

		span1.setSpan(clickableSpan, 0,
				firstString.length(), 0);
		span2.setSpan(clickableSpanTwo,
				0, lastString.length(), 0);
		txtSpan.setMovementMethod(LinkMovementMethod.getInstance());
		txtSpan.setText(TextUtils.concat(span1, " ", span2));
	}

	protected void createCmtChildLayout(final Activity activity, final Context context, LinearLayout layout, final FeedItem item) {
		TextView mUser;
		TextView mValue;
		LinearLayout childLayout;
		List<FeedItem.Comment> cmtList = item.getMenu().getCommentList();
		FeedItem.Comment cmtObj;
		for (int i = 0; i < cmtList.size(); i++) {
			if (i < 5) {
				childLayout = new LinearLayout(
						context);

				LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				childLayout.setLayoutParams(linearParams);

				mUser = new TextView(context);
				mValue = new TextView(context);

				mUser.setLayoutParams(new TableLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT));
				mValue.setLayoutParams(new TableLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

				mUser.setPadding(5, 3, 0, 3);
				mUser.setTypeface(Typeface.DEFAULT_BOLD);
				mUser.setGravity(Gravity.LEFT | Gravity.CENTER);
				mUser.setMaxLines(1);
				mUser.setSingleLine();

				mValue.setPadding(5, 3, 0, 3);
				mValue.setGravity(Gravity.LEFT);
				if (i == 4) {
					mValue.setText("5件全てのコメントを表示する");
					mValue.setTextColor(ContextCompat.getColor(context, R.color.grey_light));
				} else {
					cmtObj = cmtList.get(i);
					final int userId = cmtObj.getCmtUser().getId();
					mUser.setTextColor(ContextCompat.getColor(context, R.color.black));
					mValue.setTextColor(ContextCompat.getColor(context, R.color.black));
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
						mUser.setText(Html.fromHtml("<b>" + cmtObj.getCmtUser().getName() + ": </b>", -1));
					} else {
						mUser.setText(Html.fromHtml("<b>" + cmtObj.getCmtUser().getName() + ": </b>"));
					}
					mValue.setText(cmtObj.getBody());
					mUser.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							int myId = AppSharedPreferences.getConstant(context).getUserIdPref();
							if (myId == userId) {
								UserProfileFragment userFragment = new UserProfileFragment();
								activity.getFragmentManager().beginTransaction().replace(R.id.main_container, userFragment, "USER_PROFILE").addToBackStack("MAIN_TAB_TWO").commit();
							} else {
								Intent i = new Intent(activity, UserRecomProfileDisplayActivity.class);
								i.putExtra("userId", userId);
								activity.startActivityForResult(i, 250);
							}
						}
					});
				}
				mValue.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent i = new Intent(activity, HistoryFeedItemDisplayActivity.class);
						i.putExtra("FeedItem", item);
						activity.startActivityForResult(i, 250);
					}
				});
				childLayout.addView(mValue, 0);
				childLayout.addView(mUser, 0);
				layout.addView(childLayout);
			} else return;
		}

	}

}
