package com.camitss.mealthy.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.utils.Utils;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by User
 * Date 4/26/2017
 */

public class GalleryAdapter extends RecyclerView.Adapter {

	private Context mContext;
	ArrayList<String> itemList;
	private GalleryAdapter.OnItemClickListener listener;

	public GalleryAdapter(Context c) {
		mContext = c;
		itemList = new ArrayList<>();
	}

	public void add(String path) {
		itemList.add(path);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_single_pickup_item, parent, false);
		return new GalleryAdapter.ImageViewHolder(view,itemList);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		String item = itemList.get(position);
		ImageViewHolder mHolder = (ImageViewHolder) holder;
		if (null != item){
			Glide.with(mContext).load(item).override(Utils.getScreenWidth((Activity) mContext)/4, Utils.getScreenWidth((Activity) mContext)/4).fitCenter().into(mHolder.imageItem);
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public int getItemCount() {
		return itemList.size();
	}

	public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		public ImageView imageItem;
		public List<String> list;

		public ImageViewHolder(View itemView,List<String> list) {
			super(itemView);
			this.list = list;
			imageItem = (ImageView) itemView.findViewById(R.id.single_pick_image_item);
			imageItem.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (null != listener) {
				listener.onItemClick(v, getAdapterPosition(),list.get(getAdapterPosition()));
			}
		}
	}

	public interface OnItemClickListener {
		void onItemClick(View view, int pos,String path);
	}

	public void setOnItemClickListener(GalleryAdapter.OnItemClickListener listener) {
		this.listener = listener;
	}
}
