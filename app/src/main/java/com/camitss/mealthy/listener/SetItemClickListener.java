package com.camitss.mealthy.listener;

import com.camitss.mealthy.object.Diagnoses;

import java.util.Objects;

/**
 * Created by Darith on 3/24/2017.
 */

public interface SetItemClickListener {
    void onItemClickListener(Diagnoses objects);
}
