package com.camitss.mealthy.listener;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.camitss.mealthy.object.MealthyLog;

/**
 * Created by User
 * Date 4/6/2017
 */

public class MealthyLocationListener implements LocationListener {
	@Override
	public void onLocationChanged(Location location) {
		MealthyLog.infoLog("latitude",location.getLatitude()+"");
		MealthyLog.infoLog("longitude",location.getLongitude()+"");
	}

	@Override
	public void onStatusChanged(String s, int i, Bundle bundle) {

	}

	@Override
	public void onProviderEnabled(String s) {

	}

	@Override
	public void onProviderDisabled(String s) {

	}
}
