package com.camitss.mealthy.listener;

import com.camitss.mealthy.object.MealthyMenu;

/**
 * Created by Darith on 3/24/2017.
 */

public interface MenuClickListener {
	void onMenuClickListener(MealthyMenu objects);
}
