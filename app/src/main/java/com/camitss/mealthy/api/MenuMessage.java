package com.camitss.mealthy.api;

import android.content.Context;

import com.android.volley.Request;
import com.camitss.mealthy.object.Constant;

import org.json.JSONObject;

/**
 * Created by User
 * Date 3/29/2017
 */

public class MenuMessage extends BaseAPI {

	public MenuMessage(Context context) {
		super(context);
	}

	public void setParameter(JSONObject header, JSONObject body) {
		getRequestHandler().jsonRequest(Constant.MENU_MESSAGE, Request.Method.GET, header, body);
	}
}
