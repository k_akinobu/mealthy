package com.camitss.mealthy.api;

import android.content.Context;

import com.android.volley.Request;
import com.camitss.mealthy.object.Constant;

import org.json.JSONObject;

/**
 * Created by User
 * Date 3/29/2017
 */

public class HomeEvent extends BaseAPI {

	public HomeEvent(Context context) {
		super(context);
	}

	public void setParameter(JSONObject header, JSONObject body) {
		getRequestHandler().jsonRequest(Constant.HOME_EVENT, Request.Method.POST, header, body);
	}
}
