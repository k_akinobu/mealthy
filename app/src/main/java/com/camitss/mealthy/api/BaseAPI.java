package com.camitss.mealthy.api;

import android.content.Context;

import com.camitss.mealthy.request.RequestHandler;

/**
 * Created by User
 * Date 3/24/2017
 */

public class BaseAPI {
	RequestHandler requestHandler;

	public BaseAPI(Context context){
		requestHandler = new RequestHandler(context);
	}

	public RequestHandler getRequestHandler() {
		return requestHandler;
	}
}
