package com.camitss.mealthy.api;

import android.content.Context;

import com.android.volley.Request;
import com.camitss.mealthy.object.Constant;

import org.json.JSONObject;

/**
 * Created by User
 * Date 3/24/2017
 */

public class DiagnoseResult extends BaseAPI {
	String url = Constant.DIAGNOSE_RESULT;

	public DiagnoseResult(Context context){
		super(context);
	}

	public void parseParameter(JSONObject header, JSONObject body) {
		getRequestHandler().jsonRequest(url, Request.Method.GET, header, body);
	}

}
