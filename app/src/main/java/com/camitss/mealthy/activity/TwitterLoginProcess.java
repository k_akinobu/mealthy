package com.camitss.mealthy.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.camitss.mealthy.R;
import com.camitss.mealthy.object.TwitterUserAccInfo;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.twitter.TwitterParams;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TwitterLoginProcess extends BaseActivity {

	private SharedPreferences mpref;
	private Twitter mtwitter;
	private ProgressDialog mprogress_access_token_get;
	private ProgressDialog mprogress_token_get;

	private RequestToken requestToken = null;
	private String oauth_url;
	private String oauth_verifier;

	private String mContentLink;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Apply new theme.
		setTheme(R.style.DialogActivityTheme);
		setContentView(R.layout.twitter_auth_layout);

		mpref = TwitterParams.tw_pref;
		mtwitter = new TwitterFactory().getInstance();
//		mtwitter.setOAuthConsumer(TwitterParams.TW_CONSUMER_KEY, TwitterParams.TW_CONSUMER_SECRET);

		mtwitter.setOAuthConsumer("6u1k2vUt7P5JyXUksXTP7J1Vy", "HXyDN6WaPupWO50djKecTOKeuU1i00iQlbAFR36LiboKDaWzzK");


		Intent intent = getIntent();
		if (intent.getStringExtra("ContentLink") != null) {
			mContentLink = intent.getStringExtra("ContentLink");
		}

		new TokenGet().execute();

	}


	private class TokenGet extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mprogress_token_get = new ProgressDialog(TwitterLoginProcess.this);
			mprogress_token_get.setMessage("Loading ...");
			mprogress_token_get.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mprogress_token_get.setIndeterminate(true);
			mprogress_token_get.show();
		}

		@Override
		protected String doInBackground(String... args) {
			try {
				requestToken = mtwitter.getOAuthRequestToken("oauth://com.camitss.mealthy.Twitter_oAuth");
				oauth_url = requestToken.getAuthorizationURL();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return oauth_url;
		}

		@Override
		protected void onPostExecute(String oauth_url) {
			mprogress_token_get.dismiss();
			if (oauth_url != null) {
				Log.e("URL", oauth_url);
				WebView mwebv = (WebView) findViewById(R.id.webv);
				mwebv.getSettings().setJavaScriptEnabled(true);
				mwebv.loadUrl(oauth_url);

				mwebv.setWebViewClient(new WebViewClient() {
					boolean authComplete = false;

					@Override
					public void onPageStarted(WebView view, String url, Bitmap favicon) {
						super.onPageStarted(view, url, favicon);
					}

					@Override
					public void onPageFinished(WebView view, String url) {
						super.onPageFinished(view, url);
						if (url.contains("oauth_verifier") && !authComplete) {
							authComplete = true;
							Log.e("Url", url);
							Uri uri = Uri.parse(url);
							oauth_verifier = uri.getQueryParameter("oauth_verifier");
							new AccessTokenGet().execute();
							view.setVisibility(View.INVISIBLE);


						} else if (url.contains("denied")) {
							view.setVisibility(View.INVISIBLE);
							Intent returnIntent = new Intent();
							setResult(RESULT_CANCELED, returnIntent);
							//Toast.makeText(TwitterLoginProcess.this, "Sorry !, Permission Denied", Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
						return super.shouldOverrideUrlLoading(view, request);
					}
				});
			} else {
				Toast.makeText(TwitterLoginProcess.this, "Sorry !, Invalid Credentials", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

	private class AccessTokenGet extends AsyncTask<String, String, Boolean> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mprogress_access_token_get = new ProgressDialog(TwitterLoginProcess.this);
			mprogress_access_token_get.setMessage("Fetching Data ...");
			mprogress_access_token_get.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mprogress_access_token_get.setIndeterminate(true);
			mprogress_access_token_get.show();

		}


		@Override
		protected Boolean doInBackground(String... args) {
			try {
				AccessToken accessToken = mtwitter.getOAuthAccessToken(requestToken, oauth_verifier);
				User user = mtwitter.showUser(accessToken.getUserId());
				long userID = accessToken.getUserId();
				String profile_url = user.getOriginalProfileImageURL();

				TwitterUserAccInfo accInfo = new TwitterUserAccInfo();
				accInfo.setRequestToken(String.valueOf(requestToken));
				accInfo.setOauthUrl(String.valueOf(oauth_url));
				accInfo.setOauthVerifier(String.valueOf(oauth_verifier));
				accInfo.setAccessToken(accessToken.getToken());
				accInfo.setAccessTokenSecret(accessToken.getTokenSecret());
				accInfo.setUserName(user.getName());
				accInfo.setUserId(userID);
				accInfo.setProfileUrl(user.getOriginalProfileImageURL());
				accInfo.setTwitterLoginStatus(true);

				AppSharedPreferences.getConstant(getApplicationContext()).setTwitterUserAccInfoPreference(accInfo);

			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean response) {
			mprogress_access_token_get.dismiss();
			if (response) {
				Toast.makeText(TwitterLoginProcess.this, "Succeed Login!", Toast.LENGTH_SHORT).show();
				Intent returnIntent = new Intent();
				returnIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				returnIntent.putExtra("ContentLink", mContentLink);
				setResult(RESULT_OK, returnIntent);
				finish();
				overridePendingTransition(0, 0);
			}
		}


	}


}
