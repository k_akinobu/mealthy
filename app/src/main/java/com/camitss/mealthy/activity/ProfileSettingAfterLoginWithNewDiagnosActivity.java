package com.camitss.mealthy.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.camitss.mealthy.R;

public class ProfileSettingAfterLoginWithNewDiagnosActivity extends ProfileSettingEditActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		title.setText(getString(R.string.profilesettingafterloginwithnewdiagnos_title));
		btn_next.setText(getString(R.string.proceed_next));
		isDiagnosedAgainAfterLoginWithMealthy = true;
	}

}
