package com.camitss.mealthy.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.api.DiagnoseResult;
import com.camitss.mealthy.api.HomeEvent;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmResults;

import static com.camitss.mealthy.db.RealmHelper.getRealm;

public class WelcomeActivity extends BaseActivity {

	String score, type, name, comment, plusTitle, plusComment, plusImage;
	String minusTitle, minusComment, minusImage;
	TextView scoreView, nameView, commentView;
	TextView plusTitleView, plusCommentView, plusSubTitleView;
	TextView minusTitleView, minusCommentView, minusSubTitleView;
	ImageView minusImageView, plusImageView;
	private String answerIds = "";
	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		scoreView = (TextView) findViewById(R.id.diagnose_score);
		nameView = (TextView) findViewById(R.id.name);
		commentView = (TextView) findViewById(R.id.comment);
		plusTitleView = (TextView) findViewById(R.id.plus_title);
		plusCommentView = (TextView) findViewById(R.id.plus_comment);
		plusSubTitleView = (TextView) findViewById(R.id.plus_title_right);

		minusTitleView = (TextView) findViewById(R.id.minus_title);
		minusCommentView = (TextView) findViewById(R.id.minus_comment);
		minusSubTitleView = (TextView) findViewById(R.id.minus_title_right);

		plusImageView = (ImageView) findViewById(R.id.plus_image);
		minusImageView = (ImageView) findViewById(R.id.minus_image);

		Toolbar toolbar = (Toolbar) findViewById(R.id.welcome_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();

			}
		});
		progressDialog = new ProgressDialog(WelcomeActivity.this, R.style.AppCompatProgressDialogStyle);
		Utils.showProgressDialog(WelcomeActivity.this,progressDialog,getString(R.string.loading_welcome));
		Button btn_done = (Button) findViewById(R.id.button);
		btn_done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				startActivity(new Intent(WelcomeActivity.this,GoalSettingActivity.class));
				//GoalSettingActivity.launch(getApplicationContext());

			}
		});

		RealmResults<Diagnoses> list = RealmHelper.with(getApplicationContext()).getRealm().where(Diagnoses.class).findAll();
		User user = RealmHelper.with(getApplicationContext()).getRealm().where(User.class).findFirst();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).isSelectedQuestion()) {
				if (!answerIds.trim().isEmpty()) answerIds = answerIds + ",";
				answerIds = answerIds + list.get(i).getId() + "|1";
			}
		}
		/**
		 * summery diagnose result
		 * param : weight, height, choice of answer from previous question
		 */
		DiagnoseResult diagnoseResult = new DiagnoseResult(getApplicationContext());
		JSONObject header = new JSONObject();
		JSONObject body = new JSONObject();
		try {

			body.put("t", user.getTall());
			body.put("w", user.getWeight());
			body.put("weight", user.getWeight());
			body.put("answer_ids", answerIds);

			diagnoseResult.parseParameter(header, body);
			diagnoseResult.getRequestHandler().setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					MealthyLog.infoLog("xxx", data.toString());
					try {
						type = data.getString("type");
						score = data.getJSONObject("attributes").getInt("score") + "";
						name = data.getJSONObject("attributes").getString("name");
						AppSharedPreferences.getConstant(getApplicationContext()).setUserDietTypeFromDiagnosticResult(name);
						comment = data.getJSONObject("attributes").getString("comment");
						plusTitle = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("title");
						plusImage = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("image");
						plusComment = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("comment");
						minusTitle = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("title");
						minusComment = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("comment");
						minusImage = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("image");

						scoreView.setText(score);
						nameView.setText(name);
						commentView.setText(comment);
						plusTitleView.setText(plusTitle);
						plusCommentView.setText(plusComment);
						plusSubTitleView.setText(plusTitle);
						minusTitleView.setText(minusTitle);
						minusCommentView.setText(minusComment);
						minusSubTitleView.setText(minusTitle);

						Glide.with(WelcomeActivity.this).load(plusImage).into(plusImageView).onLoadStarted(getResources().getDrawable(R.drawable.favorite));
						Glide.with(WelcomeActivity.this).load(minusImage).into(minusImageView);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					progressDialog.dismiss();
				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void setEvent(){
		HomeEvent homeEvent = new HomeEvent(getApplicationContext());
		homeEvent.setParameter(new JSONObject(),new JSONObject());
		homeEvent.getRequestHandler().setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {

			}
		});
	}
}
