package com.camitss.mealthy.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.request.EmptyBodyRequest;
import com.camitss.mealthy.request.PostMenuRequest;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PostActivity extends BaseActivity {

	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);
		Toolbar toolbar = (Toolbar) findViewById(R.id.post_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setTitle(R.string.post_food);
		toolbar.setTitleTextColor(getResources().getColor(R.color.black));
		ImageView img = (ImageView) findViewById(R.id.post_image);
		Button submit = (Button) findViewById(R.id.button_post);
		final EditText shopName = (EditText) findViewById(R.id.shop_name_post);
		final EditText menuName = (EditText) findViewById(R.id.food_name_post);
		final EditText kcalValue = (EditText) findViewById(R.id.food_kcal_post);
		final EditText comment = (EditText) findViewById(R.id.description_post);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				startActivity(new Intent(PostActivity.this, ChoosePhotoActivity.class));
			}
		});
		final String path = Environment.getExternalStorageDirectory() + "/crop.jpg";
		Glide.with(this).load(path).diskCacheStrategy(DiskCacheStrategy.NONE)
				.skipMemoryCache(true).into(img);

		progressDialog = new ProgressDialog(PostActivity.this, R.style.AppCompatProgressDialogStyle);
		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Utils.showProgressDialog(PostActivity.this, progressDialog, getString(R.string.post_progress));
				PostMenuRequest postMenuRequest = new PostMenuRequest(getApplicationContext());
				postMenuRequest.addBody(Constant.REQUEST_POST, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						MealthyLog.infoLog("error result", null != error.getMessage() ? error.getMessage() : error + "");
						Toast.makeText(PostActivity.this, "Error! Upload fail.", Toast.LENGTH_SHORT).show();
					}
				}, new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						MealthyLog.infoLog("post result", response);
						try {
							JSONObject logsParam = new JSONObject(response);
							logPost(logsParam);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						progressDialog.dismiss();
						AppManager.getInstance().getMealthyActivity().dashboradHandler(true);
					}
				}, createParam(shopName, menuName, kcalValue, comment), new File(path));
			}
		});
	}

	private void logPost(JSONObject data) {
		JSONObject param = new JSONObject();
		String token = TokenRealmHelper.with(getApplicationContext()).getRealm().where(TokenObject.class).findFirst().getToken();
		try {
			//EVT_POSTPHOTO = 3008
			param.put("event_id", 3008);
			param.put("created_at", data.getJSONObject("menu").getString("created_at"));
			param.put("device_id", token);
			param.put("content", "");
			param.put("latitude", ((BaseActivity) PostActivity.this).getLatitude());
			param.put("longitude", ((BaseActivity) PostActivity.this).getLongitude());
			param.put("user_id", getUserId());
			param.put("shop_id", data.getJSONObject("menu").getInt("shop_id"));
			param.put("menu_id", data.getJSONObject("menu").getInt("id"));

			EmptyBodyRequest emptyBodyRequest = new EmptyBodyRequest(Constant.LOG_POST_EVENT);
			emptyBodyRequest.execute(param);
			finish();
			AppManager.getInstance().set5TabRefresh(true);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private JSONObject createParam(EditText shopName, EditText menuName, EditText kcalValue, EditText comment) {
		final JSONObject param = new JSONObject();
		try {
			param.put("user_id", getUserId());
			param.put("shop_name", shopName.getText().toString());
			param.put("menu_name", menuName.getText().toString());
			param.put("calorie", kcalValue.getText().toString());
			param.put("comment", comment.getText().toString());
			param.put("privacy_kind", 1);
			param.put("exif_date", beautyDateFormat());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return param;
	}

	private String beautyDateFormat() {
		//int year, int monthOfYear, int dayOfMonth,int hour,int minute,int second
		Calendar calendar = Calendar.getInstance();
//		calendar.set(year, monthOfYear, dayOfMonth);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(calendar.getTime());
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(PostActivity.this, ChoosePhotoActivity.class));
	}
}
