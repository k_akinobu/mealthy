package com.camitss.mealthy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CustomAlertBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class EmailRegisterActivity extends BaseActivity {
	String kind = "company";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_email_register);
		Button registerBtn = (Button) findViewById(R.id.email_register_button);
		final EditText email = (EditText) findViewById(R.id.register_email_value);
		final EditText password = (EditText) findViewById(R.id.register_password_value);
		final EditText confirmPassword = (EditText) findViewById(R.id.register_confirm_password_value);
		final EditText corperateId = (EditText) findViewById(R.id.company_id_value);
		TextView description = (TextView) findViewById(R.id.sign_up_description);
		corperateId.setVisibility(View.INVISIBLE);
		Toolbar toolbar = (Toolbar) findViewById(R.id.email_registe_toolbar);
		TextView toolbarTitle = (TextView) findViewById(R.id.title_toolbar);
		TextView termView = (TextView) findViewById(R.id.register_term);
		termView.setVisibility(View.GONE);
		String from = getIntent().getStringExtra("FROM");
		if (from.contentEquals("EMAIL_BUTTON")) {
			kind = "normal";
			description.setVisibility(View.GONE);
			toolbarTitle.setText(getString(R.string.register_label));
		} else {
			kind = "company";
			description.setVisibility(View.VISIBLE);
			toolbarTitle.setText(getString(R.string.corp_register_label));
			corperateId.setVisibility(View.VISIBLE);
			termView.setVisibility(View.VISIBLE);
		}
		termView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent term = new Intent(EmailRegisterActivity.this, TermConditionActivity.class);
				term.putExtra("FROM", "REGISTER_CORP");
				startActivity(term);
			}
		});

		registerBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject param = new JSONObject();
				try {
					param.put("kind", kind);
					if (kind.equalsIgnoreCase("company"))
						param.put("company_code", corperateId.getText().toString().trim());
					param.put("email", email.getText().toString().trim());
					param.put("password", password.getText().toString().trim());
					param.put("password_confirmation", password.getText().toString().trim());
					if (validateData(email, password, confirmPassword, corperateId)) {
						requestRegister(param);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});

		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
	}

	private boolean validateData(EditText email, EditText password, EditText confirmPassword, EditText corperateId) {

		if (corperateId.getVisibility() == View.VISIBLE) {
			int l = corperateId.getText().length();
			if (corperateId.getText() == null || corperateId.getText().length() < 1) {
				CustomAlertBuilder.create(EmailRegisterActivity.this, getString(R.string.alert_corp_id));
				return false;
			}
		}

		if (email.getText() == null || email.getText().length() <= 0) {
			CustomAlertBuilder.create(EmailRegisterActivity.this, getString(R.string.alert_email));
			return false;
		}
		if (password.getText() == null || password.getText().length() <= 0) {
			CustomAlertBuilder.create(EmailRegisterActivity.this, getString(R.string.alert_password));
			return false;
		}

		if (confirmPassword.getText() == null || password.getText().length() <= 0 || !confirmPassword.getText().toString().contentEquals(password.getText())) {
			CustomAlertBuilder.create(EmailRegisterActivity.this, getString(R.string.alert_confirm_password));
			return false;
		}

		return true;
	}

	private void requestRegister(JSONObject param) {
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REGISTER_CONFIRM, Request.Method.POST, null, param);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					finish();
					Intent completeRegister = new Intent(EmailRegisterActivity.this, CompleteRegisterActivity.class);
					if (kind == "company") {
						completeRegister.putExtra("kind", kind);
					} else {
						completeRegister.putExtra("kind", kind);
					}
					startActivity(new Intent(EmailRegisterActivity.this, CompleteRegisterActivity.class));
				} else {
					CustomAlertBuilder.create(EmailRegisterActivity.this, getString(R.string.sign_up_fail));
				}
				MealthyLog.infoLog("return data ======", data.toString());
			}
		});
	}
}
