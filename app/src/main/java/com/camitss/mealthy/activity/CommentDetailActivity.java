package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.swipemenu.SwipeHorizontalMenuLayout;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommentDetailActivity extends BaseActivity {

	private RecyclerView rclMenuCmt;
	private CmtAdapter menuCmtAdapter;


	public static void launch(Context context, List<FeedItem.Comment> cmtList) {
		Intent i = new Intent(context, CommentDetailActivity.class);
		i.putExtra("cmtList", (Serializable) cmtList);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}
	private List<FeedItem.Comment> getCmtListItem() {
		return (List<FeedItem.Comment>) getIntent().getSerializableExtra("cmtList");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu_comment_detail);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		rclMenuCmt = (RecyclerView) findViewById(R.id.rcl_menu_cmt);
		LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext()) {
			@Override
			public boolean canScrollVertically() {
				return true;
			}
		};
		rclMenuCmt.setLayoutManager(layoutManager);
		menuCmtAdapter = new CmtAdapter(this);
		menuCmtAdapter.addCmtList(getCmtListItem());
		rclMenuCmt.setAdapter(menuCmtAdapter);
	}

	public class CmtAdapter extends RecyclerView.Adapter<CmtAdapter.CommentViewHolder> {

		private Context context;
		private List<FeedItem.Comment> cmtList = new ArrayList<>();

		public CmtAdapter(Context context) {
			this.context = context;
		}

		public List<FeedItem.Comment> getCmtList() {
			return cmtList;
		}

		public void addCmtList(List<FeedItem.Comment> cmtList) {
			this.cmtList.clear();
			this.cmtList.addAll(cmtList);
			notifyDataSetChanged();
		}

		@Override
		public CmtAdapter.CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new CommentViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_comment_feed_display, parent, false));
		}

		@Override
		public void onBindViewHolder(CmtAdapter.CommentViewHolder holder, int position) {
			FeedItem.Comment cmtItem = getCmtList().get(position);
			holder.textUserNameCmt.setText(cmtItem.getCmtUser().getName());
			holder.textCmtBody.setText(cmtItem.getBody());
			if (null != cmtItem.getCmtUser().getProfileImage() && !cmtItem.getCmtUser().getProfileImage().trim().isEmpty())
				Glide.with(context).load(cmtItem.getCmtUser().getProfileImage()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).into(holder.imageUserCmt);
			holder.swipeMenuLayout.setSwipeEnable(cmtItem.getCmtUser().getId() == getUserId() ? true : false);
		}

		@Override
		public int getItemCount() {
			return getCmtList().size();
		}

		public class CommentViewHolder extends RecyclerView.ViewHolder {

			private LinearLayout smMenuViewRightLayout;
			private SwipeHorizontalMenuLayout swipeMenuLayout;
			private TextView textCmtBody;
			private TextView textUserNameCmt;
			private ImageView imageUserCmt;

			public CommentViewHolder(View itemView) {
				super(itemView);

				imageUserCmt = (ImageView) itemView.findViewById(R.id.img_user_cmt);
				textUserNameCmt = (TextView) itemView.findViewById(R.id.txt_username_cmt);
				textCmtBody = (TextView) itemView.findViewById(R.id.txt_cmt_body);
				swipeMenuLayout = (SwipeHorizontalMenuLayout) itemView.findViewById(R.id.sml);
				smMenuViewRightLayout = (LinearLayout) itemView.findViewById(R.id.smMenuViewRight);
				smMenuViewRightLayout.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Toast.makeText(CommentDetailActivity.this, "Delete", Toast.LENGTH_SHORT).show();
						deleteMenuCmtRequest(getCmtListItem().get(getAdapterPosition()).getId(), getAdapterPosition());
					}
				});
			}

		}
	}
	private void deleteMenuCmtRequest(int id, final int pos) {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(Constant.REQUEST_MENU_CMT + "comment/" + id, Request.Method.DELETE, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
//				if (isSuccess) {
				menuCmtAdapter.getCmtList().remove(pos);
				menuCmtAdapter.notifyItemRemoved(pos);
				menuCmtAdapter.notifyItemRangeChanged(pos, menuCmtAdapter.getItemCount());
//				}
			}
		});
	}

}
