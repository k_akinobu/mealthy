package com.camitss.mealthy.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.CapturePhotoFragment;
import com.camitss.mealthy.fragment.GalleryFragment;
import com.camitss.mealthy.fragment.GalleryMultiPickupFragment;
import com.camitss.mealthy.fragment.TabFourthCameraFragment;
import com.camitss.mealthy.fragment.TabFourthGalleryFragment;
import com.camitss.mealthy.fragment.TabOneInTabFifthFragment;
import com.camitss.mealthy.fragment.TabTwoInTabFifthFragment;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.utils.Utils;

public class CameraAndGalleryActivity extends BaseActivity implements View.OnClickListener {

	private ImageView imageClose;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_camera_gallery);
		imageClose = (ImageView) findViewById(R.id.img_close);
		imageClose.setOnClickListener(this);
		final FragmentTabHost tabHost = (FragmentTabHost) findViewById(R.id.tab_host_mealthy);
		tabHost.setup(this, getSupportFragmentManager(), R.id.tab_content);

		View v1 = getLayoutInflater().inflate(R.layout.layout_tab_line, null);
		View v2 = getLayoutInflater().inflate(R.layout.layout_tab_line_two, null);
		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(v1), TabFourthCameraFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(v2), TabFourthGalleryFragment.class, null);
		tabHost.getTabWidget().setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
		tabHost.getTabWidget().getChildAt(0).getLayoutParams().width = Utils.getScreenWidth(CameraAndGalleryActivity.this) / 2;
		tabHost.getTabWidget().getChildAt(1).getLayoutParams().width = Utils.getScreenWidth(CameraAndGalleryActivity.this) / 2;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_close:
				onBackPressed();
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		TabFourthCameraFragment fragment = (TabFourthCameraFragment) getSupportFragmentManager().findFragmentById(R.id.tab_content).getChildFragmentManager().findFragmentByTag("tab1");
//		TabFourthGalleryFragment fragment = (TabFourthGalleryFragment) getSupportFragmentManager().findFragmentById(R.id.tab_content).getChildFragmentManager().findFragmentByTag("tab2");

		fragment.startActivityForResult(data, requestCode);
	}


}
