package com.camitss.mealthy.activity;

import android.os.Bundle;
import android.view.View;

/**
 * Created by Viseth on 5/5/2017.
 */

public class DiagnosesFromSettingInProfileActivity extends PageOneInitDiagnoseActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		toolbar.setVisibility(View.INVISIBLE);
		introTitle.setVisibility(View.GONE);
		imageClose.setVisibility(View.VISIBLE);
		imageClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
