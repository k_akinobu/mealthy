package com.camitss.mealthy.activity;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.CapturePhotoFragment;
import com.camitss.mealthy.fragment.GalleryFragment;
import com.camitss.mealthy.fragment.GalleryMultiPickupFragment;
import com.camitss.mealthy.fragment.TabFourthCameraFragment;
import com.camitss.mealthy.object.GalleryMultiPickup;

import org.w3c.dom.Text;

public class ChoosePhotoActivity extends BaseActivity {

	protected Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fragment_choose_photo);

		toolbar = (Toolbar) findViewById(R.id.choose_photo_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_close);

		final FragmentTabHost tabHost = (FragmentTabHost) findViewById(R.id.tab_host_mealthy);
		tabHost.setup(this, getSupportFragmentManager(), R.id.tab_content);

		toolbar.setTitle(getString(R.string.post_food));
		toolbar.setTitleTextColor(getResources().getColor(R.color.white));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				finish();
				onBackPressed();
			}
		});

		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(getString(R.string.photo), null), CapturePhotoFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(getString(R.string.single_select_photo_tab), null), GalleryFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(getString(R.string.multi_select_photo_tab), null), GalleryMultiPickupFragment.class, null);

		tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {

				for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
					TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
					tv.setTextColor(Color.parseColor("#ffffff"));
				}

				TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
				tv.setTextColor(getResources().getColor(R.color.organge_mealthy));

			}
		});

		for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
		{
			TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			tv.setTextColor(getResources().getColor(R.color.white));
		}
		TextView tv = (TextView) tabHost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
		tv.setTextColor(getResources().getColor(R.color.organge_mealthy));
	}
}