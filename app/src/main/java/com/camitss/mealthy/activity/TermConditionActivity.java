package com.camitss.mealthy.activity;



import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.SettingTermCondition;
import com.camitss.mealthy.fragment.TermConditionFragment;

public class TermConditionActivity extends AppCompatActivity {
	private String from;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_term_condition);
		Toolbar toolbar = (Toolbar) findViewById(R.id.term_condition_setting_toolbar);
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

		from = getIntent().getStringExtra("FROM");
		if(from.contentEquals("REGISTER_CORP")){
			fragmentTransaction.add(R.id.term_setting_container,new TermConditionFragment(2,toolbar,from),"MENU3");
		}else{
			fragmentTransaction.add(R.id.term_setting_container,new SettingTermCondition(toolbar),"TERM_MENU");
		}
		fragmentTransaction.commit();
	}

	public String getFrom(){
		return from;
	}
}
