package com.camitss.mealthy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class EmailLoginActivity extends BaseActivity implements View.OnClickListener {
	String kind = "normal";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_email_login);
		Button loginBtn = (Button) findViewById(R.id.email_login_button);
		final EditText email = (EditText) findViewById(R.id.email_value);
		final EditText password = (EditText) findViewById(R.id.password_value);
		findViewById(R.id.forget_pwd_email).setOnClickListener(this);
		findViewById(R.id.register_email).setOnClickListener(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.email_login_toolbar);
		TextView toolbarTitle = (TextView) findViewById(R.id.title_toolbar);
		String from = getIntent().getStringExtra("FROM");
		if (from.contentEquals("EMAIL_BUTTON")) {
			kind = "normal";
			toolbarTitle.setText(getString(R.string.login_email_title));
		} else {
			toolbarTitle.setText(getString(R.string.cooperate_account_title));
			kind = "company";
		}

		loginBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject param = new JSONObject();
				try {
					param.put("kind", kind);
					param.put("email", email.getText().toString().trim());
					param.put("password", password.getText().toString().trim());
					if (validateData(email, password)) {
						requestLogin(param);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});

		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
	}

	private boolean validateData(EditText email, EditText password) {
		if (email.getText() == null || email.getText().length() <= 0) {
			CustomAlertBuilder.create(EmailLoginActivity.this, getString(R.string.alert_email));
			return false;
		}
		if (password.getText() == null || password.getText().length() <= 0) {
			CustomAlertBuilder.create(EmailLoginActivity.this, getString(R.string.alert_password));
			return false;
		}
		return true;
	}

	private void requestLogin(JSONObject param) {
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.LOGIN_EMAIL, Request.Method.POST, null, param);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					JSONObject authObj = null;
					try {
						authObj = data.getJSONObject("authentication");
						TokenObject token = new TokenObject();
						token.setToken(authObj.getString("token"));
						int userId = authObj.getInt("user_id");
						TokenRealmHelper.with(EmailLoginActivity.this).save(token);
						AppSharedPreferences.getConstant(getApplicationContext()).setUserIdPref(userId);
						AppSharedPreferences.getConstant(getApplicationContext()).setLoginWithSocialPref(true);

						if (kind == "normal") {
							AppSharedPreferences.getConstant(getApplicationContext()).setConnectedSocialType(Constant.CONNECT_EMAIL);
						} else {
							AppSharedPreferences.getConstant(getApplicationContext()).setConnectedSocialType(Constant.CONNECT_COOPERATE);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					finish();
					MealthyLog.infoLog("return data ======", data.toString());
				} else {
					CustomAlertBuilder.create(EmailLoginActivity.this, getString(R.string.login_fail));
				}
			}
		});
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
			case R.id.register_email:
				Intent register = new Intent(EmailLoginActivity.this, EmailRegisterActivity.class);
				if (kind == "normal") {
					register.putExtra("FROM", "EMAIL_BUTTON");
				} else {
					register.putExtra("FROM", "CORP_BUTTOn");
				}
				startActivity(register);
				break;
			case R.id.forget_pwd_email:
				startActivity(new Intent(EmailLoginActivity.this, ForgetPasswordActivity.class));
				break;
		}

	}
}
