package com.camitss.mealthy.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GoalSettingActivity extends BaseActivity implements View.OnClickListener {

	private TextView textTargetWeight;
	private TextView textDefaultKg;
	private TextView textOneKg;
	private TextView textTwoKg;
	private TextView textDefaultKcal;
	private TextView textKcalSunrise;
	private TextView textKcalSunny;
	private TextView textKcalMoon;
	private TextView textPercentSunrise;
	private TextView textPercentSunny;
	private TextView textPercentMoon;
	private List<OneTabObject> mOneTabObjectList;
	private ProgressDialog progressDialog;
	private Button btnNext;
	private int dietId = 1;
	private int bmiMen = 22;
	private int bmiWomen = 20;

	public static void launch(Context context) {
		Intent i = new Intent(context, GoalSettingActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goal_setting);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
//				overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			}
		});
		progressDialog = new ProgressDialog(GoalSettingActivity.this, R.style.AppCompatProgressDialogStyle);
		mOneTabObjectList = new ArrayList<>();

		getUserCaloriesInfo();

		textTargetWeight = (TextView) findViewById(R.id.txt_target_weight);
		textDefaultKg = (TextView) findViewById(R.id.txt_default_kg);
		textOneKg = (TextView) findViewById(R.id.txt_one_kg);
		textTwoKg = (TextView) findViewById(R.id.txt_two_kg);
		textDefaultKcal = (TextView) findViewById(R.id.txt_kcal);
		textKcalSunrise = (TextView) findViewById(R.id.txt_kcal_sunrise);
		textKcalSunny = (TextView) findViewById(R.id.txt_kcal_sunny);
		textKcalMoon = (TextView) findViewById(R.id.txt_kcal_moon);
		textPercentSunrise = (TextView) findViewById(R.id.txt_percent_sunrise);
		textPercentSunny = (TextView) findViewById(R.id.txt_percent_sunny);
		textPercentMoon = (TextView) findViewById(R.id.txt_percent_moon);
		btnNext = (Button) findViewById(R.id.btn_next);

		textDefaultKg.setOnClickListener(this);
		textOneKg.setOnClickListener(this);
		textTwoKg.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		try {
			Log.d(getClass().getSimpleName(), "size>> " + RealmHelper.with(getApplicationContext()).getRealm().where(User.class).findAll().size());
			User user = RealmHelper.with(getApplicationContext()).getUserRealmObjById(getUserId());
			int bmi = user.getSex() == 0 ? 20 : 22;
			double value = Math.pow(user.getTall() / 100, 2) * bmi;
			textTargetWeight.setText(String.format("%.1f", value) + "kg");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setViewData(int i, boolean isDefaultKgEnabled, boolean isOneKgEnabled, boolean isTwoKgEnabled) {

		Drawable bgDrawableDefault = ContextCompat.getDrawable(GoalSettingActivity.this, isDefaultKgEnabled ? R.drawable.bg_textview_disable : R.drawable.bg_textview_normal);
		Drawable bgDrawableOne = ContextCompat.getDrawable(GoalSettingActivity.this, isOneKgEnabled ? R.drawable.bg_textview_disable : R.drawable.bg_textview_normal);
		Drawable bgDrawableTwo = ContextCompat.getDrawable(GoalSettingActivity.this, isTwoKgEnabled ? R.drawable.bg_textview_disable : R.drawable.bg_textview_normal);
		int colorDefaultKg = ContextCompat.getColor(GoalSettingActivity.this, isDefaultKgEnabled ? R.color.white : R.color.orange);
		int colorOneKg = ContextCompat.getColor(GoalSettingActivity.this, isOneKgEnabled ? R.color.white : R.color.orange);
		int colorTwoKg = ContextCompat.getColor(GoalSettingActivity.this, isTwoKgEnabled ? R.color.white : R.color.orange);

		textDefaultKg.setBackground(bgDrawableDefault);
		textOneKg.setBackground(bgDrawableOne);
		textTwoKg.setBackground(bgDrawableTwo);
		textDefaultKg.setTextColor(colorDefaultKg);
		textOneKg.setTextColor(colorOneKg);
		textTwoKg.setTextColor(colorTwoKg);
		try {
			textDefaultKg.setText(mOneTabObjectList.get(0).getNameKg());
			textOneKg.setText(mOneTabObjectList.get(1).getNameKg());
			textTwoKg.setText(mOneTabObjectList.get(2).getNameKg());
			OneTabObject obj = mOneTabObjectList.get(i);
			textDefaultKcal.setText(Utils.IntegerWithCommaFormat(obj.getNumAllKcal()) + "kcal");
			textPercentSunrise.setText(getString(R.string.for_sunrise) + "(" + obj.getMorningPercent() + "%)");
			textKcalSunrise.setText(Utils.IntegerWithCommaFormat(obj.getMorningKcal()) + "kcal");
			textPercentSunny.setText(getString(R.string.for_sunny) + "(" + obj.getLunchPercent() + "%)");
			textKcalSunny.setText(Utils.IntegerWithCommaFormat(obj.getLunchKcal()) + "kcal");
			textPercentMoon.setText(getString(R.string.for_moon) + "(" + obj.getDinnerPercent() + "%)");
			textKcalMoon.setText(Utils.IntegerWithCommaFormat(obj.getDinnerKcal()) + "kcal");

			if (isDefaultKgEnabled) dietId = 1;
			else if (isOneKgEnabled) dietId = 2;
			else if (isTwoKgEnabled) dietId = 3;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getUserCaloriesInfo() {
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REQUEST_GET_USER_CALORIES, Request.Method.GET, null, null);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("JSONObject", isSuccess + ":" + data.toString());
				if (isSuccess) {
					try {
						JSONObject jObj = new JSONObject(data.getString("user"));
						int userId = jObj.getInt("id");
						AppSharedPreferences.getConstant(getApplicationContext()).setUserIdPref(userId);
						JSONArray jsonArray = jObj.getJSONArray("calorie_details");
						JSONObject obj;
						JSONObject lunchObj;
						JSONObject morningObj;
						JSONObject dinnerObj;
						OneTabObject oneTabObject;
						for (int i = 0; i < jsonArray.length(); i++) {
							oneTabObject = new OneTabObject();
							obj = jsonArray.getJSONObject(i);
							morningObj = obj.getJSONObject("value").getJSONObject("morning");
							lunchObj = obj.getJSONObject("value").getJSONObject("lunch");
							dinnerObj = obj.getJSONObject("value").getJSONObject("dinner");
							oneTabObject.setNameKg(obj.getString("name"));
							oneTabObject.setNumAllKcal(obj.getInt("calorie_per_day"));
							oneTabObject.setMorningPercent(morningObj.getInt("percent"));
							oneTabObject.setMorningKcal(morningObj.getInt("calorie"));
							oneTabObject.setLunchPercent(lunchObj.getInt("percent"));
							oneTabObject.setLunchKcal(lunchObj.getInt("calorie"));
							oneTabObject.setDinnerKcal(dinnerObj.getInt("calorie"));
							oneTabObject.setDinnerPercent(dinnerObj.getInt("percent"));
							mOneTabObjectList.add(oneTabObject);
						}
						setViewData(1, false, true, false);

					} catch (JSONException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.txt_default_kg:
				setViewData(0, true, false, false);

				break;
			case R.id.txt_one_kg:
				setViewData(1, false, true, false);
				break;
			case R.id.txt_two_kg:
				setViewData(2, false, false, true);
				break;
			case R.id.btn_next:
				updateUser();
				break;
		}
	}

	private void updateUser() {

		Utils.showProgressDialog(getApplicationContext(), progressDialog, getString(R.string.indicator_title));

		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("diet_id", dietId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		requestHandler.jsonRequest(Constant.REQUEST_WITH_USER + "/" + getUserId(), Request.Method.PUT, null, jsonObj);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				try {
					Log.d("JSONObject", isSuccess + ":" + data.toString());
					progressDialog.dismiss();
					if (isSuccess) {
						AppSharedPreferences.getConstant(getApplicationContext()).setFromUpdateTargetWeightMiddleMenuScreen(false);
						SharedPreferences preferences = getSharedPreferences("first_start", MODE_PRIVATE);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putBoolean("is_first_start", false);
						editor.commit();
//						if (!AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithMealthy()) {
						finishAffinity();
						AppSharedPreferences.getConstant(getApplicationContext()).setLoginWithMealthy(true);
						startActivity(new Intent(GoalSettingActivity.this, MealthyActivity.class));
//						} else {
//							Intent intent = new Intent(getApplicationContext(), SettingInProfileActivity.class);
//							startActivityForResult(intent,309);
//						}

					} else {
						CustomAlertBuilder.create(GoalSettingActivity.this, new JSONObject(data.getString("message")).getJSONObject("error").getString("message"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public class OneTabObject {
		private String nameKg;
		private int numAllKcal;
		private int morningPercent;
		private int morningKcal;
		private int lunchPercent;
		private int lunchKcal;
		private int dinnerPercent;
		private int dinnerKcal;

		public OneTabObject() {
		}

		public String getNameKg() {
			return nameKg;
		}

		public void setNameKg(String nameKg) {
			this.nameKg = nameKg;
		}

		public int getNumAllKcal() {
			return numAllKcal;
		}

		public void setNumAllKcal(int numAllKcal) {
			this.numAllKcal = numAllKcal;
		}

		public int getMorningPercent() {
			return morningPercent;
		}

		public void setMorningPercent(int morningPercent) {
			this.morningPercent = morningPercent;
		}

		public int getMorningKcal() {
			return morningKcal;
		}

		public void setMorningKcal(int morningKcal) {
			this.morningKcal = morningKcal;
		}

		public int getLunchPercent() {
			return lunchPercent;
		}

		public void setLunchPercent(int lunchPercent) {
			this.lunchPercent = lunchPercent;
		}

		public int getLunchKcal() {
			return lunchKcal;
		}

		public void setLunchKcal(int lunchKcal) {
			this.lunchKcal = lunchKcal;
		}

		public int getDinnerPercent() {
			return dinnerPercent;
		}

		public void setDinnerPercent(int dinnerPercent) {
			this.dinnerPercent = dinnerPercent;
		}

		public int getDinnerKcal() {
			return dinnerKcal;
		}

		public void setDinnerKcal(int dinnerKcal) {
			this.dinnerKcal = dinnerKcal;
		}
	}
}
