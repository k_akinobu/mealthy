package com.camitss.mealthy.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.BottomsheetItemAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.GoalItem;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.EmptyBodyRequest;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;
import com.camitss.mealthy.utils.Utils;
import com.camitss.mealthy.utils.customcalendar.WheelPickerDay;
import com.camitss.mealthy.utils.customcalendar.WheelPickerMonth;
import com.camitss.mealthy.utils.customcalendar.WheelPickerYear;
import com.camitss.mealthy.utils.datepicker.WheelDatePickerCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmResults;

public class ProfileSettingActivity extends BaseActivity implements View.OnClickListener {

	private Context mContext;
	protected TextView textUsername;
	protected TextView textGender;
	protected TextView textDob;
	protected TextView textLifeStrength;
	protected TextView textHeight;
	protected TextView textWeight;
	String tmpNum = "";
	protected String globalDOB = "";
	protected Button btn_next;

	private int num = 1;
	private ProgressDialog progressDialog;
	private int method = Request.Method.POST;
	protected TextView title;
	private int mDay = 1;
	private int mMonth = 1;
	private int mYear = 1990;
	protected boolean isDiagnosedAgainAfterLoginWithMealthy = false;


	public static void launch(Context context, RealmList<Diagnoses> list) {
		Intent intent = new Intent(context, ProfileSettingActivity.class);
		intent.putExtra("list", ((Serializable) list));
		context.startActivity(intent);
	}

	public static void launch(Context context) {
		Intent intent = new Intent(context, ProfileSettingActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_setting);
		title = (TextView) findViewById(R.id.title);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		mContext = getApplicationContext();
		progressDialog = new ProgressDialog(ProfileSettingActivity.this, R.style.AppCompatProgressDialogStyle);

		textUsername = (TextView) findViewById(R.id.txt_username);
		textGender = (TextView) findViewById(R.id.txt_gender);
		textDob = (TextView) findViewById(R.id.txt_dob);
		textLifeStrength = (TextView) findViewById(R.id.txt_life_strength);
		textHeight = (TextView) findViewById(R.id.txt_height);
		textWeight = (TextView) findViewById(R.id.txt_weight);
		LinearLayout layout_username = (LinearLayout) findViewById(R.id.layout_username);
		LinearLayout layout_gender = (LinearLayout) findViewById(R.id.layout_gender);
		LinearLayout layout_dob = (LinearLayout) findViewById(R.id.layout_dob);
		LinearLayout layout_life_strength = (LinearLayout) findViewById(R.id.layout_life_strength);
		LinearLayout layout_height = (LinearLayout) findViewById(R.id.layout_height);
		LinearLayout layout_weight = (LinearLayout) findViewById(R.id.layout_weight);
		btn_next = (Button) findViewById(R.id.btn_next);
		layout_username.setOnClickListener(this);
		textUsername.setOnClickListener(this);
		layout_gender.setOnClickListener(this);
		textGender.setOnClickListener(this);
		textDob.setOnClickListener(this);
		layout_dob.setOnClickListener(this);
		textLifeStrength.setOnClickListener(this);
		layout_life_strength.setOnClickListener(this);
		textHeight.setOnClickListener(this);
		layout_height.setOnClickListener(this);
		textWeight.setOnClickListener(this);
		layout_weight.setOnClickListener(this);
		btn_next.setOnClickListener(this);
//		List<RealmObject> list = new ArrayList<>();
//		list.addAll(getFullDataDiagnosesDataList());
//		RealmHelper.with(getApplicationContext()).saveAll(list);
//		RealmResults<Diagnoses> list = RealmHelper.with(getApplicationContext()).getRealm().where(Diagnoses.class).findAll();
//		for (int i = 0; i < list.size(); i++) {
//			Log.d(getClass().getSimpleName(), "data>> " + list.get(i).isSelectedQuestion() + ":" + list.get(i).getSelected() + ":" + list.get(i).getId());
//		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_username:
			case R.id.txt_username:
				showUsernameInput();
				break;
			case R.id.layout_gender:
			case R.id.txt_gender:
				showGenderDialog();
				break;
			case R.id.layout_dob:
			case R.id.txt_dob:
				displayDOBBottomsheet();
				break;
			case R.id.layout_height:
			case R.id.txt_height:
				displayWeightHeightCalculator(true);
				break;
			case R.id.layout_weight:
			case R.id.txt_weight:
				displayWeightHeightCalculator(false);
				break;
			case R.id.layout_life_strength:
			case R.id.txt_life_strength:
				showWorkTypeDialog();
				break;
			case R.id.btn_next:
				createNewUser();
				break;

		}
	}

	private void toWelcomePage() {
		startActivity(new Intent(ProfileSettingActivity.this, WelcomeActivity.class));
	}

	private void showUsernameInput() {
		final View view = LayoutInflater.from(mContext).inflate(R.layout.custom_input_view, null);
		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme)).setView(view).show();
		dialog.setCancelable(false);
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//		dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		ImageView img_close = (ImageView) view.findViewById(R.id.img_close);
		TextView text_save = (TextView) view.findViewById(R.id.txt_save);
		final EditText edtInput = (EditText) view.findViewById(R.id.edt_input);
		img_close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
				dialog.dismiss();

			}
		});
		edtInput.setFocusableInTouchMode(true);
		edtInput.setFocusable(true);
//		edtInput.clearFocus();
		edtInput.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		text_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String input = edtInput.getText().toString().trim();
				if (!input.isEmpty()) {
					textUsername.setText(input);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
					dialog.dismiss();
				}
			}
		});
	}

	private void showGenderDialog() {
		final List<String> genderList = new ArrayList<>();
		genderList.add(getString(R.string.female));
		genderList.add(getString(R.string.male));
		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileSettingActivity.this);
		View sheetView = getLayoutInflater().inflate(R.layout.item_bottomsheet_dialog_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(getString(R.string.gender));
		RecyclerView recyclerview_country = (RecyclerView) sheetView.findViewById(R.id.lvCountryZ);

		recyclerview_country.setHasFixedSize(true);
		recyclerview_country.setLayoutManager(new LinearLayoutManager(this));

		BottomsheetItemAdapter adapter = new BottomsheetItemAdapter(mContext, genderList);
		recyclerview_country.setAdapter(adapter);

		adapter.setOnItemClickListner(new BottomsheetItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				if (genderList.get(position) instanceof String) {
					textGender.setText((String) genderList.get(position));
				}
				mBottomSheetDialog.dismiss();
			}
		});

		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.show();

	}

	public void showWorkTypeDialog() {
		final List<String> workTypeList = new ArrayList<>();
		workTypeList.add(getString(R.string.work_type_one));
		workTypeList.add(getString(R.string.work_type_two));
		workTypeList.add(getString(R.string.work_type_three));
		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileSettingActivity.this);
		View sheetView = getLayoutInflater().inflate(R.layout.item_bottomsheet_dialog_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(getString(R.string.life_strength));
		RecyclerView recyclerview_country = (RecyclerView) sheetView.findViewById(R.id.lvCountryZ);

		recyclerview_country.setHasFixedSize(true);
		recyclerview_country.setLayoutManager(new LinearLayoutManager(this));

		BottomsheetItemAdapter adapter = new BottomsheetItemAdapter(mContext, workTypeList);
		recyclerview_country.setAdapter(adapter);

		adapter.setOnItemClickListner(new BottomsheetItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				if (workTypeList.get(position) instanceof String) {
					textLifeStrength.setText((String) workTypeList.get(position));
				}
				mBottomSheetDialog.dismiss();
			}
		});

		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.show();
	}

	private void displayDOBBottomsheet() {
		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileSettingActivity.this, R.style.BottomSheetDialog);
		final View sheetView = getLayoutInflater().inflate(R.layout.layout_calendarview, null);
		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.show();


		final WheelDatePickerCustom datePicker = (WheelDatePickerCustom) sheetView.findViewById(R.id.calendarview_custom);
		datePicker.setSelectedDay(mDay);
		datePicker.setSelectedMonth(mMonth);
		datePicker.setSelectedYear(mYear);

		datePicker.setCyclic(true);
		datePicker.setYearEnd(Calendar.getInstance().get(Calendar.YEAR));
		datePicker.setYearStart(1900);
//		datePicker.setCurtain(true);
//		datePicker.setAtmospheric(true);
		/////////////////////

		final WheelPickerDay pickerDay = (WheelPickerDay) sheetView.findViewById(R.id.picker_day);
		final WheelPickerMonth pickerMonth = (WheelPickerMonth) sheetView.findViewById(R.id.picker_month);
		final WheelPickerYear pickerYear = (WheelPickerYear) sheetView.findViewById(R.id.picker_year);
		TextView text_save = (TextView) sheetView.findViewById(R.id.txt_save);
		text_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String curDate = "";
				try {
					mDay = datePicker.getCurrentDay();
					mMonth = datePicker.getCurrentMonth();
					mYear = datePicker.getCurrentYear();
					String selectedDate = datePicker.getCurrentYear() + "-" + datePicker.getCurrentMonth() + "-" + datePicker.getCurrentDay();
					curDate = DateFormat.getDateInstance(DateFormat.LONG, new Locale("ja", "JP")).format(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate));
					textDob.setText(curDate);
					globalDOB = selectedDate;
					mBottomSheetDialog.dismiss();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});

		/////////////////////////////


		mBottomSheetDialog.setCancelable(false);
		ImageView image_close = (ImageView) sheetView.findViewById(R.id.img_close);
		image_close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mBottomSheetDialog.dismiss();
			}
		});
	}

	private void displayWeightHeightCalculator(final boolean isHeight) {
		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileSettingActivity.this);
		final View sheetView = getLayoutInflater().inflate(R.layout.custom_calculator_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(isHeight ? "身長" : getString(R.string.weight));
		final TextView text_unit = (TextView) sheetView.findViewById(R.id.txt_unit);
		text_unit.setText(isHeight ? "cm" : "kg");
		final TextView text_input = (TextView) sheetView.findViewById(R.id.edt_input);
		ImageView img_close = (ImageView) sheetView.findViewById(R.id.img_close);
		Button btn_point = (Button) sheetView.findViewById(R.id.btn_point);
		Button btn_zero = (Button) sheetView.findViewById(R.id.btn_zero);
		Button btn_c = (Button) sheetView.findViewById(R.id.btn_c);
		Button btn_three = (Button) sheetView.findViewById(R.id.btn_three);
		Button btn_two = (Button) sheetView.findViewById(R.id.btn_two);
		Button btn_one = (Button) sheetView.findViewById(R.id.btn_one);
		Button btn_six = (Button) sheetView.findViewById(R.id.btn_six);
		Button btn_five = (Button) sheetView.findViewById(R.id.btn_five);
		Button btn_four = (Button) sheetView.findViewById(R.id.btn_four);
		Button btn_nine = (Button) sheetView.findViewById(R.id.btn_nine);
		Button btn_eight = (Button) sheetView.findViewById(R.id.btn_eight);
		Button btn_seven = (Button) sheetView.findViewById(R.id.btn_seven);
		final int number = isHeight ? 220 : 300;

		btn_c.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				text_input.setText("");
			}
		});
		btn_eight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "8".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_seven.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "7".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_five.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "5".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_four.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "4".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_nine.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "9".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_one.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "1".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_two.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "2".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_three.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "3".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_six.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "6".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_zero.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "0".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_point.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!text_input.getText().toString().trim().isEmpty()) {
					if (text_input.getText().toString().trim().contains(".")) return;
					tmpNum = text_input.getText().toString() + ".".trim();
					text_input.setText(tmpNum);
				}
			}
		});

		img_close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mBottomSheetDialog.dismiss();
			}
		});
		TextView text_save = (TextView) sheetView.findViewById(R.id.txt_save);

		text_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String input = ((TextView) sheetView.findViewById(R.id.edt_input)).getText().toString().trim();
				TextView tmpView = (TextView) (isHeight ? textHeight : textWeight);
				if (!input.isEmpty()) {
					if (!input.contains(".")) input = input + ".0";
					tmpView.setText(input + text_unit.getText().toString().trim());
					mBottomSheetDialog.dismiss();
				}
			}
		});
		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.setCancelable(false);
		mBottomSheetDialog.show();
	}

	private List<Diagnoses> getFullDataDiagnosesDataList() {
		return (List<Diagnoses>) getIntent().getSerializableExtra("list");
	}

	private int getTargetTerm(String s) {
		if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item1))) return 1;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item2))) return 2;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item3))) return 3;
		return 1;
	}

	private void createNewUser() {
		String msg = "";
		String username = textUsername.getText().toString().trim();
		String sGenger = textGender.getText().toString().trim();
		String sHeight = textHeight.getText().toString().trim();
		String sWeight = textWeight.getText().toString().trim();
		if (username.isEmpty()) msg = "ユーザー名を設定して下さい。";
		if (sGenger.isEmpty()) msg = msg + "\n性別を設定して下さい。";
		if (globalDOB.trim().isEmpty()) msg = msg + "\n生年月日を設定して下さい。";
		int gender = sGenger.equalsIgnoreCase(getString(R.string.female)) ? Constant.FEMALE : Constant.MALE;
		double height = 0.0;
		if (!sHeight.isEmpty() && Double.valueOf(textHeight.getText().toString().trim().split("cm", 2)[0]) > 0.0)
			height = Double.valueOf(textHeight.getText().toString().trim().split("cm", 2)[0]);
		else msg = msg + "\n身長を設定して下さい。";
		double weight = 0.0;
		if (!sWeight.isEmpty() && Double.valueOf(textWeight.getText().toString().trim().split("kg", 2)[0]) > 0.0)
			weight = Double.valueOf(textWeight.getText().toString().trim().split("kg", 2)[0]);
		else msg = msg + "\n体重を設定して下さい。";
		double workTypeNum = 1.5;
		String workType = textLifeStrength.getText().toString().trim();
		if (workType.equalsIgnoreCase(getString(R.string.work_type_two))) workTypeNum = 1.75;
		else if (workType.equalsIgnoreCase(getString(R.string.work_type_three))) workTypeNum = 2.0;
		else if (workType.equalsIgnoreCase(getString(R.string.work_type_one))) workTypeNum = 1.5;
		else msg = msg + "\n活動量を設定して下さい。";
		int termId = 1;
		int eatOutId = 3;
		int dietId = 2;
		int figureId = 2;
		Constant.termId = AppSharedPreferences.getConstant(getApplicationContext()).getPreviousTermIdPref();
		try {
			GoalItem item = RealmHelper.with(getApplicationContext()).getRealm().where(GoalItem.class).findFirst();
			if (null != item) {
				termId = item.getTermId();//getTargetTerm(item.getPeriod());
				eatOutId = item.getEatOutId();
				dietId = item.getDietId();
				figureId = item.getFigureId();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (username.isEmpty() || sGenger.isEmpty() || globalDOB.trim().isEmpty() || sWeight.isEmpty() || Double.valueOf(weight) <= 0.0 || Double.valueOf(height) <= 0.0 || sHeight.isEmpty() || workType.isEmpty()) {
			CustomAlertBuilder.create(ProfileSettingActivity.this, msg);
			return;
		}
		Utils.showProgressDialog(ProfileSettingActivity.this, progressDialog);
		final RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		final JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("name", username);
			jsonObj.put("sex", gender);
			jsonObj.put("tall", height);
			jsonObj.put("weight", weight);
			jsonObj.put("device_id", getDeviceId());//android_id
			jsonObj.put("birthdate", globalDOB);
			jsonObj.put("category_ids", "1,2,3,4,5,6,7,8,9,10,11");
			jsonObj.put("diet_id", dietId);
			jsonObj.put("dinner_price", "2000");
			jsonObj.put("eat_out_id", eatOutId);
			jsonObj.put("figure_id", figureId);
			jsonObj.put("lunch_price", 2000);
			jsonObj.put("metabolic_id", workTypeNum);
			jsonObj.put("morning_price", 1000);
			jsonObj.put("target_weight", weight);
			jsonObj.put("term_id", termId);
			jsonObj.put("os_type", "android");
			if ((termId != Constant.termId && AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithMealthy()) ||
					(termId == Constant.termId && !AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithMealthy())) {
				jsonObj.put("start_date", Utils.beautyCurrentDateFormat());
				jsonObj.put("end_date", Utils.getExtendedDuration(termId));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String url = Constant.REQUEST_WITH_USER_POST;
		if (null != getUserFromRealm()) {
			url = Constant.REQUEST_WITH_USER + "/" + getUserFromRealm().getId();
			method = Request.Method.PUT;
		}
		requestHandler.jsonRequest(url, method, null, jsonObj);
		final String finalUrl = url;
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				try {
					Log.d("DATA", finalUrl + ">>" + isSuccess + " JSONObject>> " + data.toString());
					if (isSuccess) {
						AppSharedPreferences.getConstant(getApplicationContext()).setFromUpdateTargetWeightMiddleMenuScreen(false);
						User user = mGson.fromJson(data.getString("user").toString(), User.class);
						RealmHelper.with(getApplicationContext()).save(user);
						AppSharedPreferences.getConstant(getApplicationContext()).setUserIdPref(user.getId());
						AppSharedPreferences.getConstant(getApplicationContext()).setPreviousTermIdPref(user.getTermId());

						requestDiagnosesAnswer(user.getId());
						if (method == Request.Method.POST) {
							requestLogginEvent(user.getId());
							toWelcomePage();
						} else if (method == Request.Method.PUT && null != AppSharedPreferences.getConstant(getApplicationContext()) && AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithMealthy() && !isDiagnosedAgainAfterLoginWithMealthy)
							finish();
						else {
							toWelcomePage();
						}
					} else {
						JSONObject mJson = new JSONObject(data.getString("message"));
						JSONObject errorJson = mJson.getJSONObject("error");
						String msg = errorJson.getString("message");
						CustomAlertBuilder.create(ProfileSettingActivity.this, msg);
					}
					progressDialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

//	@Override
//	protected void onRestart() {
//		super.onRestart();
//		num += num;
//		android_id = android_id + num;
//		Log.d(getClass().getSimpleName(), "onRestart " + android_id);
//	}

	@Override
	protected void onResume() {
		super.onResume();
//		User user = RealmHelper.with(getApplicationContext()).getRealm().where(User.class).findFirst();
		if (null != getUserFromRealm()) setView(getUserFromRealm());
	}

	@Override
	public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
		super.onPostCreate(savedInstanceState, persistentState);
		Log.d(getClass().getSimpleName(), "onPostCreate");
	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
		Log.d(getClass().getSimpleName(), "onPostResume");
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
	}

	private void setView(User user) {
		textGender.setText(user.getSex() == 0 ? getString(R.string.female) : getString(R.string.male));
//		try {
//			if (globalDOB.trim().isEmpty())
//				globalDOB = DateFormat.getDateInstance(DateFormat.LONG, new Locale("ja", "JP")).format(new SimpleDateFormat("yyyy-MM-dd").parse(user.getBirthdate()));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		textDob.setText(globalDOB);
		textHeight.setText(user.getTall() + "cm");
		textWeight.setText(user.getWeight() + "kg");

	}

	private User getUserFromRealm() {
		return RealmHelper.with(getApplicationContext()).getRealm().where(User.class).findFirst();
	}

	private void requestDiagnosesAnswer(int id) {
		RequestHandler request = new RequestHandler(getApplicationContext());
		JSONObject jObj = new JSONObject();
		String answerIds = "";
		try {
			RealmResults<Diagnoses> list = RealmHelper.with(getApplicationContext()).getRealm().where(Diagnoses.class).findAll();
			for (int i = 0; i < list.size(); i++) {
				if (!answerIds.trim().isEmpty()) answerIds = answerIds + ",";
				answerIds = answerIds + list.get(i).getId() + "|" + list.get(i).getSelected();
			}
			jObj.put("answer_ids", answerIds);
			jObj.put("user_id", id);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_DIAGNOSES_ANSWER, Request.Method.PUT, null, jObj);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("requestDiagnosesAnswer", isSuccess + ":" + data.toString());
			}
		});
	}

	private void requestLogginEvent(final int id) {
		JSONObject jObj = new JSONObject();
		String androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);
		try {
			jObj.put("content", "");
			jObj.put("created_at", Utils.beautyCurrentDateFormat());//"2017-04-06 16:21:46"
			jObj.put("device_id", androidId);
			jObj.put("event_id", 2);
			jObj.put("latitude", "");
			jObj.put("longitude", "");
			jObj.put("menu_id", "");
			jObj.put("shop_id", "");
			jObj.put("user_id", id);

			new EmptyBodyRequest(Constant.HOME_EVENT).execute(jObj);

		} catch (JSONException e) {
			e.printStackTrace();
		}

//		RequestHandler request = new RequestHandler(getApplicationContext());
//		JSONObject header = new JSONObject();
//		try {
//			header.put("X-Mealthy-Token", RealmHelper.with(getApplicationContext()).getRealm().where(TokenObject.class).findFirst().getToken());
//			request.stringRequest(Constant.HOME_EVENT, Request.Method.POST, header, jObj);
//			request.setStringRequestListener(new StringRequestListener() {
//				@Override
//				public void onResponseResult(boolean isSuccess, String data) {
//					Log.d("requestLogginEvent", isSuccess + ":" + data.toString());
////				if (isSuccess)
////					requestDiagnosesAnswer(id);
//				}
//			});
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}

	}

	public String getDeviceId() {
		String android_id = "";
		try {
			android_id = TokenRealmHelper.with(ProfileSettingActivity.this).getRealm().where(TokenObject.class).findFirst().getToken();
			Log.d("token", "RealmToken size>>" + TokenRealmHelper.with(ProfileSettingActivity.this).getRealm().where(TokenObject.class).findAll().size());
		} catch (Exception e) {
			e.printStackTrace();
//			android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
//					Settings.Secure.ANDROID_ID);
			android_id = UUID.randomUUID().toString().toUpperCase();
		}
		Log.d("token", "deviceId>> " + android_id);
		return android_id;
	}
}