package com.camitss.mealthy.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CustomAlertBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPasswordActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forget_password);
		final EditText email = (EditText) findViewById(R.id.email_forget_password_value);
		Toolbar toolbar = (Toolbar) findViewById(R.id.forget_password_toolbar);
//		toolbar.setTitle(getString(R.string.reset_password_title));
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});

		findViewById(R.id.forget_password_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject param = new JSONObject();
				try {
					param.put("email",email.getText().toString());
					if(validate(email)){
						resetPassword(param);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private boolean validate(EditText email){

		if(email!=null && email.getText().length()==0 && !email.getText().toString().contains("@")){
			CustomAlertBuilder.create(ForgetPasswordActivity.this, "");
			return false;
		}

		return true;
	}

	private void resetPassword(JSONObject param){
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REQUEST_RESET_PASSWORD, Request.Method.PUT,null,param);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if(isSuccess){
					CustomAlertBuilder.create(ForgetPasswordActivity.this, getString(R.string.reset_password_sucess_msg), new CustomAlertBuilder.DismissListener() {
						@Override
						public void onDismiss() {
							finish();
						}
					});
				}else{
					CustomAlertBuilder.create(ForgetPasswordActivity.this,getString(R.string.reset_password_fail), new CustomAlertBuilder.DismissListener() {
						@Override
						public void onDismiss() {
							finish();
						}
					});
				}
			}
		});
	}
}
