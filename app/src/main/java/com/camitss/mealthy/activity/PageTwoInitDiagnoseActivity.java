package com.camitss.mealthy.activity;

import android.content.Intent;
import android.util.Log;

import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.utils.AppSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

/**
 * Created by Viseth on 3/31/2017.
 */

public class PageTwoInitDiagnoseActivity extends BaseInitialDiagnosisActivity {
	@Override
	public List<Diagnoses> getDiagnoseQuestionList() {
		List<Diagnoses> list = new ArrayList<>();
		if (null != getFullDataDiagnosesDataList()) {
			list.add(getFullDataDiagnosesDataList().get(3));
			list.add(getFullDataDiagnosesDataList().get(4));
			list.add(getFullDataDiagnosesDataList().get(5));
		}
		return list;
	}

	@Override
	public String getToolbarTitle() {
		try {
			if (null != getFullDataDiagnosesDataList() && null != getFullDataDiagnosesDataList().get(3)){
				Log.d("title3", getFullDataDiagnosesDataList().get(3).getTitle());
				return getFullDataDiagnosesDataList().get(3).getTitle();
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ダイエット法を選ぶ";
	}

	@Override
	public String getIntroText() {
		return getString(R.string.intro_page2);
	}

	@Override
	public void getNextButtonFunction() {

		List<RealmObject> list = new ArrayList<>();
		list.addAll(getFullDataDiagnosesDataList());
//		List<RealmObject> list = (List<RealmObject>)(Object)getFullDataDiagnosesDataList();
		RealmHelper.with(getApplicationContext()).saveAll(list);
		if(null!= AppSharedPreferences.getConstant(getApplicationContext()) && !AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithMealthy()){
			startActivity(new Intent(getApplicationContext(), ProfileSettingActivity.class));
		}else{
			startActivity(new Intent(getApplicationContext(), ProfileSettingAfterLoginWithNewDiagnosActivity.class));
		}
	}
}

