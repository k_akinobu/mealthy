package com.camitss.mealthy.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.google.gson.Gson;

/**
 * Created by pheakdey on 3/23/2017.
 */

public class BaseActivity extends AppCompatActivity {
	public Gson mGson = new Gson();
	private double latitude, longitude;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window window = getWindow();
			window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}
		startActivityAnimation();

	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	public int getUserId() {
		return (null != AppSharedPreferences.getConstant(getApplicationContext())) ? AppSharedPreferences.getConstant(getApplicationContext()).getUserIdPref() : 30095;
	}

	public String getUserToken() {
		String token = "";
		try {
			token = TokenRealmHelper.with(this).getRealm().where(TokenObject.class).findFirst().getToken();
			Log.d("token", TokenRealmHelper.with(this).getRealm().where(TokenObject.class).findAll().size() + ":" + TokenRealmHelper.with(this).getRealm().where(TokenObject.class).findAll().get(0).getToken());
		} catch (Exception e) {
//			token = "876899F8-27AB-4CFE-9A92-1C364266E815";
			e.printStackTrace();
		}
		return token;
	}

	public void startActivityAnimation() {
		overridePendingTransition(R.anim.slide_in_right_vin, R.anim.slide_out_left_vin);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		overridePendingTransition(R.anim.slide_in_left_vin, R.anim.slide_out_right_vin);
	}
}
