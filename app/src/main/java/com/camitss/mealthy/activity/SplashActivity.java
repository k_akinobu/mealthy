package com.camitss.mealthy.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class SplashActivity extends AppCompatActivity {

	boolean loadComplete;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_activity);

		try {
			Log.d("token", TokenRealmHelper.with(SplashActivity.this).getRealm().where(TokenObject.class).findFirst().getToken());
		} catch (Exception e) {
			createAndSaveTokenIntoRealm();
		}
	}

	private void loadConfig() {

		final RequestHandler loadConfigData = new RequestHandler(getApplicationContext());
		loadConfigData.jsonRequest(Constant.CONFIG_URL, Request.Method.GET, new JSONObject(), new JSONObject());
		loadConfigData.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				try {
					Constant.VERSION = data.getString("version");
					Constant.BASE_URL = data.getString("api_url");
					Constant.LOG_BASE_URL = data.getString("logging_url");
					if (!data.getString("env").contains("production") && !data.getString("env").contains("development")) {
						loadComplete = false;
					} else {
						loadComplete = true;
					}
				} catch (JSONException e) {
					loadComplete = false;
				}

				final SharedPreferences preferences = getSharedPreferences("first_start", MODE_PRIVATE);
				final boolean firstStart = preferences.getBoolean("is_first_start", true);

				if (loadComplete) {
					if (firstStart) {
						Intent startIntent = new Intent(SplashActivity.this, StartActivity.class);
						startActivity(startIntent);
					} else {
						Intent mealthy = new Intent(SplashActivity.this, MealthyActivity.class);
						startActivity(mealthy);
					}
					finish();
					SharedPreferences.Editor editor = preferences.edit();
					editor.putBoolean("is_first_start", true);
					editor.commit();
				} else {
					startActivity(new Intent(SplashActivity.this, ErrorActivity.class));
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadConfig();
	}

	public void createAndSaveTokenIntoRealm() {
		String uuid = UUID.randomUUID().toString().toUpperCase();
		Log.d("token", uuid);
		TokenObject obj = new TokenObject();
		obj.setToken(uuid);
		TokenRealmHelper.with(SplashActivity.this).save(obj);
	}

}