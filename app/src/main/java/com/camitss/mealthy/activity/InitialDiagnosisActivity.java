package com.camitss.mealthy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.DiagnosisAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.listener.SetItemClickListener;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.DiagnosisItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class InitialDiagnosisActivity extends BaseActivity implements View.OnClickListener, SetItemClickListener {
	private ArrayList<Diagnoses> diagnosesDataList, fullDataDiagnosesDataList;
	private DiagnosisAdapter diagnosisAdapter;
	private ListView diagnosesList;
	private String page = "page1";
	private Button btn_next;
	private TextView intro_title;
	private Toolbar toolbar;
	RelativeLayout parentView;
	private TextView textToolbarTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initial_diagnosis);
		parentView = (RelativeLayout) findViewById(R.id.diagnose_parent_view);
		toolbar = (Toolbar) findViewById(R.id.toolbar2);
//		setupToolbar(toolbar);
//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

		btn_next = (Button) findViewById(R.id.btn_next);
		intro_title = (TextView) findViewById(R.id.intro_title);
		btn_next.setOnClickListener(this);
		textToolbarTitle = (TextView) findViewById(R.id.toolbar_title);

		diagnosesDataList = new ArrayList<>();
		fullDataDiagnosesDataList = new ArrayList<Diagnoses>();
		diagnosisAdapter = new DiagnosisAdapter(this, diagnosesDataList, page);
		diagnosesList = (ListView) findViewById(R.id.diagnos_list);
		diagnosesList.setAdapter(diagnosisAdapter);
		diagnosisAdapter.setSetItemClickListener(this);
		requestData();
	}

	private void prepareData(JSONObject object) {
		try {
			JSONArray datas = object.getJSONArray("diagnoses");

			for (int i = 0; i < datas.length(); i++) {
				Diagnoses diagnoses = new Diagnoses();
				RealmList<DiagnosisItem> diagnosisItems = new RealmList<>();

				diagnoses.setId(datas.getJSONObject(i).getInt("id"));
				diagnoses.setTitle(datas.getJSONObject(i).getString("title"));
				diagnoses.setBody(datas.getJSONObject(i).getString("body"));
				diagnoses.setPosition(datas.getJSONObject(i).getInt("position"));
				diagnoses.setImage(datas.getJSONObject(i).getString("image"));
				diagnoses.setDisgnosisKind(datas.getJSONObject(i).getInt("diagnosis_kind"));
				diagnoses.setNotes(datas.getJSONObject(i).getString("notes"));
				diagnoses.setCreatedDate(datas.getJSONObject(i).getString("created_at"));

				for (int j = 0; j < datas.getJSONObject(i).getJSONArray("diagnosis_selects").length(); j++) {
					DiagnosisItem diagnosisItem = new DiagnosisItem();
					diagnosisItem.setId(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getInt("id"));
					diagnosisItem.setLabel(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("label"));
					diagnosisItem.setPosition(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getInt("position"));
					diagnosisItem.setImage(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("image"));
					diagnosisItem.setCreateDate(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("created_at"));
					diagnosisItems.add(diagnosisItem);
				}
				diagnoses.setDiagnosisItem(diagnosisItems);
				if (datas.getJSONObject(i).getJSONArray("diagnosis_selects").length() >= 2) {
					diagnoses.setSelected(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(1).getInt("id"));
					diagnoses.setSelectedQuestion(false);
				}
				diagnosesDataList.add(diagnoses);
			}
			fullDataDiagnosesDataList = (ArrayList<Diagnoses>) diagnosesDataList.clone();

			diagnosisAdapter.notifyDataSetChanged();
			switchData();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void switchData() {
		try {
			diagnosesDataList.clear();
			for (int i = 0; i < fullDataDiagnosesDataList.size(); i++) {
				if (fullDataDiagnosesDataList.get(i).getNotes().equals(page)) {
					diagnosesDataList.add(fullDataDiagnosesDataList.get(i));
				}
			}
//			getSupportActionBar().setTitle(diagnosesDataList.get(0).getTitle());
			textToolbarTitle.setText(diagnosesDataList.get(0).getTitle());
			if (page.equals("page1")) {
				intro_title.setText(getResources().getString(R.string.intro_page1));
			} else {
				intro_title.setText(getResources().getString(R.string.intro_page2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void requestData() {
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REQUEST_DIAGNOSIS, Request.Method.GET, null, null);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					prepareData(data);
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_next:
				if (page.equals("page1")) {
					page = "page2";
				} else {
					// Do something to next page
					List<RealmObject> list = new ArrayList<>();
					list.addAll(fullDataDiagnosesDataList);
					RealmHelper.with(getApplicationContext()).saveAll(list);
					ProfileSettingActivity.launch(InitialDiagnosisActivity.this);
					overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
//              use this as Final Data      fullDataDiagnosesDataList
				}
				switchData();
				break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				if (page.equals("page1")) {
					super.onBackPressed();
					Intent overview = new Intent(InitialDiagnosisActivity.this, OverviewActivity.class);
					startActivity(overview);
					overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
				} else {
					page = "page1";
					switchData();

				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {

		if (page.equals("page1")) {
			super.onBackPressed();
			startActivity(new Intent(InitialDiagnosisActivity.this, OverviewActivity.class));
			finish();
			overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		} else {
			page = "page1";
			switchData();
		}
	}

	@Override
	public void onItemClickListener(Diagnoses objects) {
		fullDataDiagnosesDataList.get(fullDataDiagnosesDataList.indexOf(objects)).setSelectedQuestion(objects.isSelectedQuestion());


	}
}
