package com.camitss.mealthy.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class TextWeightDisplayActivity extends BaseActivity implements View.OnClickListener {

	private AlertDialog dialog;
	private ProgressDialog progressDialog;

	public static void launch(Context context, UserFeedsInProfile item) {
		Intent i = new Intent(context, TextWeightDisplayActivity.class);
		i.putExtra("UserFeedsInProfile", item);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}


	private UserFeedsInProfile getItem() {
		return (UserFeedsInProfile) getIntent().getSerializableExtra("UserFeedsInProfile");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text_weight_display);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		progressDialog = new ProgressDialog(TextWeightDisplayActivity.this, R.style.AppCompatProgressDialogStyle);
		TextView title = (TextView) findViewById(R.id.title);
		TextView name = (TextView) findViewById(R.id.user_name);
		TextView detail = (TextView) findViewById(R.id.txt_detail);
		TextView date = (TextView) findViewById(R.id.txt_date);
		ImageView profile = (ImageView) findViewById(R.id.user_profile);
		if (null != getItem()) {
			title.setText(getTitle(getItem().getKey()));
			date.setText(Utils.fromStringDateWithTimeZoneToFormate(getItem().getCreatedAt()));
			name.setText(getItem().getUser().getName());
			Glide.with(getApplicationContext()).load(getItem().getUser().getProfileImageUrl()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(getApplicationContext())).into(profile);
			String[] values = getItem().getUserHistory().getValue().split("\\|", 2);
			if (values.length == 2) {
				detail.setText("最高血圧:" + values[0] + "mmHg\n最低血圧:" + values[1] + "mmHg");
			} else {
				if (getItem().getKey().equals("user_history.update.weight")) {
					detail.setText("体重:" + values[0] + "kg");
				} else if (getItem().getKey().equals("user_history.update.bt_bgl")) {
					detail.setText("血糖値:" + values[0] + "mg/dl");
				}
			}
		}
		findViewById(R.id.img_more).setOnClickListener(this);
	}

	private String getTitle(String key) {
		switch (key) {
			case "user_history.update.bt_bgl":
				return "血糖値";
			case "user_history.update.bt_bp":
				return "血圧";//最低血圧"最高血圧";
			case "user_history.update.weight":
				return "体重";
			default:
				return "体重";
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_more:
				displayMenu(v);
				break;
			case R.id.txt_ok:
				dialog.dismiss();
				requestToDeleteFeed();
				break;
		}
	}

	private void requestToDeleteFeed() {
		if (null != getItem() && null != getItem().getUserHistory()) {
			Utils.showProgressDialog(TextWeightDisplayActivity.this, progressDialog, getString(R.string.loading));
			UserFeedsInProfile.UserHistory userHistory = getItem().getUserHistory();
			JSONObject body = new JSONObject();
			try {
				body.put("kind", userHistory.getKind());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			RequestHandler request = new RequestHandler(getApplicationContext());
			request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_DELETE_USER_HISTORY, userHistory.getId()), Request.Method.DELETE, null, body);
			request.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					progressDialog.dismiss();
					Log.d("Textweight", data.toString());
					if (isSuccess) {
						finish();
						AppManager.getInstance().set5TabRefresh(true);
					} else
						Toast.makeText(TextWeightDisplayActivity.this, "Cannot delete history", Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	private void displayMenu(View v) {
		final PopupMenu popupMenu = new PopupMenu(TextWeightDisplayActivity.this, v);
		popupMenu.inflate(R.menu.more_options_menu);
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
					case R.id.delete:
						goToConfirmDeleteDialog();
						break;
					case R.id.cancel:
						popupMenu.dismiss();
						break;
				}
				return false;
			}
		});
		popupMenu.show();
	}

	private void goToConfirmDeleteDialog() {
		View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_ok_on_the_left_cancel_on_the_right, null);
		dialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme)).setView(view).show();
		((TextView) view.findViewById(R.id.txt_dialog_title)).setVisibility(View.GONE);
		((TextView) view.findViewById(R.id.txt_dialog_msg)).setText(getString(R.string.txt_confirm_delete_user_history));
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		TextView okBtn = (TextView) view.findViewById(R.id.txt_ok);
		okBtn.setText(getString(R.string.delete));
		okBtn.setOnClickListener(this);
		dialog.setCancelable(false);
	}
}
