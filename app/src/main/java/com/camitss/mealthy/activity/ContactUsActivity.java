package com.camitss.mealthy.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.TermConditionFragment;

public class ContactUsActivity extends BaseActivity {
	private ProgressBar loading;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);
		Toolbar toolbar = (Toolbar) findViewById(R.id.contact_us_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setTitle(R.string.f_setting_text10);
//		toolbar.setTitleTextColor(getResources().getColor(R.color.white));

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				//startActivity(new Intent(ContactUsActivity.this,ChoosePhotoActivity.class));
			}
		});

		WebView webView = (WebView) findViewById(R.id.contact_info_view);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewClient());
		if (18 < Build.VERSION.SDK_INT) {
			webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		}
		webView.loadUrl("http://info.mealthy.co.jp/faq/");
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
		}
	}

}
