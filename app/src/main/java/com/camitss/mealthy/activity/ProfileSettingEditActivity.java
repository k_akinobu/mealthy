package com.camitss.mealthy.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Viseth on 5/4/2017.
 */

public class ProfileSettingEditActivity extends ProfileSettingActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		title.setText(getString(R.string.profilesettingedit_title));
		try {
			User user = RealmHelper.with(this).getRealm().where(User.class).findFirst();
			Log.d("user>>", user.getBirthdate() + ":" + user.getName() + ":" + user.getProfileImageUrl() + ":" + user.getSex());
			textUsername.setText(user.getName());
			textGender.setText(user.getSex() == 0 ? getString(R.string.female) : getString(R.string.male));
			String curDate = "";
			try {
				curDate = DateFormat.getDateInstance(DateFormat.LONG, new Locale("ja", "JP")).format(new SimpleDateFormat("yyyy-MM-dd").parse(user.getBirthdate()));
				textDob.setText(curDate);
				globalDOB = user.getBirthdate();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			textHeight.setText(user.getTall() + "");
			textWeight.setText(user.getWeight() + "");
			textLifeStrength.setText(getWorkTypeString(user.getMetabolicId()));
			btn_next.setText("更新");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String getWorkTypeString(double metalicId) {
		if (metalicId <= 1.5) {
			return getString(R.string.work_type_one);
		} else if (metalicId >= 1.75) {
			return getString(R.string.work_type_two);
		} else if (metalicId >= 2.0) {
			return getString(R.string.work_type_three);
		} else {
			return getString(R.string.work_type_one);
		}
	}
}
