package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.recyclerview.NotificationAdapter;
import com.camitss.mealthy.adapter.recyclerview.listener.HidingScrollListener;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.NotificationItem;
import com.camitss.mealthy.object.notificationitem.NotificationItemRealmObject;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;

public class NotificationActivity extends BaseActivity {

	private NotificationAdapter adapter;
	private TextView textNoNoti;
	private ProgressBar progressBar;
	private String nextLink = "";
	private boolean isLoadMore;

	public static void launch(Context context) {
		context.startActivity(new Intent(context, NotificationActivity.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		progressBar = (ProgressBar) findViewById(R.id.noti_progressbar);
		textNoNoti = (TextView) findViewById(R.id.txt_no_noti);
		textNoNoti.setVisibility(View.GONE);
		RecyclerView notificationRcl = (RecyclerView) findViewById(R.id.rcl_noti);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		notificationRcl.setLayoutManager(layoutManager);
		adapter = new NotificationAdapter(this);
		notificationRcl.setAdapter(adapter);
		progressBar.setVisibility(View.VISIBLE);
		getUserNotification(Constant.REQUEST_GET_USER_NOTIFICATION);//"https://api.mealthy.me/user_notifications");//

		notificationRcl.addOnScrollListener(new HidingScrollListener() {
			@Override
			public void onHide() {
			}

			@Override
			public void onShow() {
			}

			@Override
			public void onLoadMore() {
				if (!isLoadMore && !nextLink.trim().isEmpty() && !nextLink.trim().equalsIgnoreCase("null")) {
					Log.d("bo", "onLoadmore");
					progressBar.setVisibility(View.GONE);
					isLoadMore = true;
					nextLink = Constant.getBaseUrl() + nextLink;//"https://api.mealthy.me"
					getUserNotification(nextLink);
				}
			}
		});
	}


	private void getUserNotification(String url) {
		RequestHandler request = new RequestHandler(this);
		JSONObject header = new JSONObject();
		try {
			header.put("Content-Type", "application/json; charset=utf-8");
			header.put("Accept", "application/json");
			header.put("X-Mealthy-Token", "52B016F5-AC44-4CAB-B080-99756F88BBF6");//"F3D61C8C-C160-4751-9CB0-DF420030F62A");//52B016F5-AC44-4CAB-B080-99756F88BBF6
		} catch (JSONException e) {
			e.printStackTrace();
		}
//		request.jsonRequest(url, Request.Method.GET, header, null);//Constant.REQUEST_GET_USER_NOTIFICATION
		request.jsonRequest(url, Request.Method.GET, null, null);//Constant.REQUEST_GET_USER_NOTIFICATION
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("noti", data.toString());
				if (progressBar.getVisibility() == View.VISIBLE)
					progressBar.setVisibility(View.GONE);
				if (isSuccess) {
					Type listType = new TypeToken<ArrayList<NotificationItem>>() {
					}.getType();
					try {
						List<NotificationItem> notiListRealm = new Gson().fromJson(data.getJSONArray("user_notifications").toString(), listType);

						List<RealmObject> userNotiList = new Gson().fromJson(data.getJSONArray("user_notifications").toString(), new TypeToken<ArrayList<NotificationItemRealmObject>>() {
						}.getType());
						RealmHelper.with(getApplicationContext()).saveAll(userNotiList);
						JSONObject jObj = data.getJSONObject("paging");
						nextLink = jObj.getString("next");
						if (userNotiList.size() > 0) {
							adapter.addNotificationItemList(notiListRealm);
							adapter.notifyDataSetChanged();
						} else textNoNoti.setVisibility(View.VISIBLE);
//
					} catch (JSONException e) {
						e.printStackTrace();
					}
					isLoadMore = false;
				} else isLoadMore = false;
			}
		});
	}

	private void setNotificationRead() {
		Log.d("sizenoti", RealmHelper.getRealm(getApplicationContext()).where(NotificationItemRealmObject.class).findAll().size() + "");
		RequestHandler request = new RequestHandler(this);
		request.jsonRequest(Constant.REQUEST_READ_NOTIFICATION, Request.Method.PUT, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("noti read", data.toString());
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		setNotificationRead();
	}


	@Override
	protected void onResume() {
		super.onResume();
		RealmResults<NotificationItemRealmObject> localNoti = RealmHelper.with(NotificationActivity.this).getRealm().where(NotificationItemRealmObject.class).findAll();
		for (int i = 0; i < localNoti.size(); i++) {
			Log.d("notirealm1", "onresume>> " + localNoti.get(i).getId() + "<<>>" + localNoti.get(i).getStatusKind() + "<<>>" + localNoti.get(i).getFromUser().getName());
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		RealmResults<NotificationItemRealmObject> localNoti = RealmHelper.with(NotificationActivity.this).getRealm().where(NotificationItemRealmObject.class).findAll();
		for (int i = 0; i < localNoti.size(); i++) {
			Log.d("notirealm1", "onrestart>> " + localNoti.get(i).getId() + "<<>>" + localNoti.get(i).getStatusKind() + "<<>>" + localNoti.get(i).getFromUser().getName());
		}
	}
}
