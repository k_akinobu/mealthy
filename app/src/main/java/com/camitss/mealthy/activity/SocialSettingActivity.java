package com.camitss.mealthy.activity;

import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.SettingHomeFragment;

public class SocialSettingActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social_setting);
		Toolbar toolbar = (Toolbar) findViewById(R.id.social_setting_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

		SettingHomeFragment settingHomeFragment = new SettingHomeFragment(toolbar);
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().add(R.id.social_setting_container,settingHomeFragment);
		//fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
		fragmentTransaction.commit();

	}
}
