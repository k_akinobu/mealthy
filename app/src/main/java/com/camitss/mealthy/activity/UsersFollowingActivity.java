package com.camitss.mealthy.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.utils.Utils;

/**
 * Created by Viseth on 4/20/2017.
 */

public class UsersFollowingActivity extends UserRecommendationDisplayActivity {

	public static void launch(Context context, int id){
		Intent i = new Intent(context, UsersFollowingActivity.class);
		i.putExtra("userId", id);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		textCancel.setVisibility(View.GONE);
		edtInputSearch.setVisibility(View.GONE);
		toolbarTitle.setVisibility(View.VISIBLE);
		toolbarTitle.setText(getString(R.string.following));
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		((TextView)findViewById(R.id.text_des)).setVisibility(View.GONE);


	}

	@Override
	public String getStringUrl() {
		return Utils.convertFunctionName(Constant.REQUEST_GET_USER_FOLLOWING, getIntent().getIntExtra("userId",getUserId()));
	}
}
