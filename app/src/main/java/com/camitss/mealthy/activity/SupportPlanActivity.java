package com.camitss.mealthy.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.SupportPlanFragment;

public class SupportPlanActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_support_plan);
		Toolbar toolbar = (Toolbar)findViewById(R.id.support_plan_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

		SupportPlanFragment supportPlanFragment = new SupportPlanFragment(toolbar);
		getSupportFragmentManager().beginTransaction().add(R.id.support_plan_container,supportPlanFragment,"SUPPORT_PLAN").commit();
	}
}
