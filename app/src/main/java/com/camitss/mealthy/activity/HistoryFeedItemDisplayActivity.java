package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FeedItem;

public class HistoryFeedItemDisplayActivity extends FeedDisplayActivity {

	public static void launch(Context context, FeedItem item) {
		Intent i = new Intent(context, HistoryFeedItemDisplayActivity.class);
		i.putExtra("FeedItem", item);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		title.setText("詳細ページ");
		imageMore.setVisibility(View.VISIBLE);
		edtInputCmt.clearFocus();
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
