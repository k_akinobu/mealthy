package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.recyclerview.UserRecomItemAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Case;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class UserRecommendationDisplayActivity extends BaseActivity implements View.OnClickListener {
	protected RecyclerView rclRecom;
	protected UserRecomItemAdapter userRecomAdapter;
	protected Gson mGson = new Gson();
	protected ProgressBar progressBar;
	protected TextView textCancel, textNoUser, textDes;
	protected EditText edtInputSearch;
	protected Toolbar toolbar;
	protected TextView toolbarTitle;
	private List<UserRecomItem> userRecomItemList = new ArrayList<UserRecomItem>();
	private Timer timer = new Timer();

	public static void launch(Context context) {
		Intent i = new Intent(context, UserRecommendationDisplayActivity.class);
		context.startActivity(i);
	}

	public boolean isFromSuggestion() {
		return getIntent().getBooleanExtra("isFromSuggestion", false);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_recommendation_display);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
		setToolbar(toolbar);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		progressBar.setVisibility(View.VISIBLE);
		textNoUser = (TextView) findViewById(R.id.txt_no_user);
		textDes = (TextView) findViewById(R.id.text_des);
		rclRecom = (RecyclerView) findViewById(R.id.rcl_user_recom);
		rclRecom.setLayoutManager(new LinearLayoutManager(this));
		userRecomAdapter = new UserRecomItemAdapter(getApplicationContext());
		rclRecom.setAdapter(userRecomAdapter);

		getUserRecommendRequest();

	}

	private void requestGetAllUsers(final String name) {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_WITH_USER + "?name=%s&sort=similar|desc", name), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						progressBar.setVisibility(View.GONE);
						String nextUrl = data.getJSONObject("paging").getString("next");
						JSONArray userArr = data.getJSONArray("users");
						UserRecomItem item;
						List<UserRecomItem> userRecomItemList = new ArrayList<UserRecomItem>();
						if (userArr.length() > 0) {
							rclRecom.setVisibility(View.VISIBLE);
							textNoUser.setVisibility(View.GONE);
							for (int i = 0; i < userArr.length(); i++) {
								item = mGson.fromJson(userArr.getJSONObject(i).toString(), UserRecomItem.class);
								userRecomItemList.add(item);
							}
							userRecomAdapter.addAllUserRecomItemList(userRecomItemList);
							userRecomAdapter.notifyDataSetChanged();
						} else {
							rclRecom.setVisibility(View.GONE);
							textNoUser.setVisibility(View.VISIBLE);

						}
					} catch (JSONException e) {
						rclRecom.setVisibility(View.GONE);
						textNoUser.setVisibility(View.VISIBLE);
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void searchUser(String name) {

	}

	private void setToolbar(Toolbar toolbar) {
		textCancel = (TextView) toolbar.findViewById(R.id.toolbar_cancel);
		textCancel.setVisibility(isFromSuggestion() ? View.GONE : View.VISIBLE);
		textCancel.setOnClickListener(this);
		edtInputSearch = (EditText) toolbar.findViewById(R.id.edt_input_search);
		edtInputSearch.setVisibility(isFromSuggestion() ? View.GONE : View.VISIBLE);
		toolbarTitle.setVisibility(isFromSuggestion() ? View.VISIBLE : View.GONE);

		if (isFromSuggestion()) {
			toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
		edtInputSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(final Editable s) {
				if (!s.toString().trim().isEmpty()) {
					rclRecom.setVisibility(View.GONE);
					textDes.setVisibility(View.GONE);
					progressBar.setVisibility(View.VISIBLE);
					if (timer != null) {
						timer.cancel();
					}
					timer = new Timer();
					timer.schedule(new TimerTask() {
						@Override
						public void run() {
							requestGetAllUsers(s.toString().trim());
						}
					}, 500);

				} else {
					if (timer != null) {
						timer.cancel();
					}
					userRecomAdapter.addAllUserRecomItemList(userRecomItemList);
					userRecomAdapter.notifyDataSetChanged();
					rclRecom.setVisibility(View.VISIBLE);
					textNoUser.setVisibility(View.GONE);
					textDes.setVisibility(View.VISIBLE);
				}
//				if (timer != null) {
//					timer.cancel();
//				}
//				timer = new Timer();
//				timer.schedule(new TimerTask() {
//					@Override
//					public void run() {
//						searchUser(s.toString().trim());
//					}
//				}, 500);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.toolbar_cancel:
				onBackPressed();
				break;
		}
	}

	@Override
	public void startActivityAnimation() {
		overridePendingTransition(R.anim.slide_in_left_vin, R.anim.slide_out_right_vin);
	}

	private void getUserRecommendRequest() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(getStringUrl(), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressBar.setVisibility(View.GONE);
				if (isSuccess) {
					rclRecom.setVisibility(View.VISIBLE);
					textNoUser.setVisibility(View.GONE);
					try {
						JSONArray userArr = data.getJSONArray("users");
						if (userArr.length() > 0) {
							rclRecom.setVisibility(View.VISIBLE);
							textNoUser.setVisibility(View.GONE);
							UserRecomItem item;
							for (int i = 0; i < userArr.length(); i++) {
								item = mGson.fromJson(userArr.getJSONObject(i).toString(), UserRecomItem.class);
								userRecomItemList.add(item);
							}
							userRecomAdapter.addAllUserRecomItemList(userRecomItemList);
							userRecomAdapter.notifyDataSetChanged();
						} else {
							rclRecom.setVisibility(View.GONE);
							textNoUser.setVisibility(View.VISIBLE);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					rclRecom.setVisibility(View.GONE);
					textNoUser.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	public String getStringUrl() {
		return Constant.REQUEST_USERS_RECOMMEND;
	}
}
