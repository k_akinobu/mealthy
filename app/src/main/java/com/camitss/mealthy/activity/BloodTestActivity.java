package com.camitss.mealthy.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.BloodTestObject;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.camitss.mealthy.utils.Utils.context;

public class BloodTestActivity extends BaseActivity implements View.OnClickListener {

	private String tmpNum = "";
	private TextView txtBloodPressure;
	private TextView txtDiastolicPressure;
	private TextView txtBloodGlucoseLevel;
	private TextView txtHba1c;
	private TextView txtNeutralFat;
	private TextView txtHdlCholesterol;
	private TextView txtLdlCholesterol;
	private TextView txtAst;
	private TextView txtAlt;
	private TextView txtYgtp;
	private TextView txtUricAcid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blood_test);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		LinearLayout layoutBloodPressure = (LinearLayout) findViewById(R.id.layout_blood_pressure);
		txtBloodPressure = (TextView) findViewById(R.id.txt_blood_pressure);
		LinearLayout layoutDiastolicPressure = (LinearLayout) findViewById(R.id.layout_diastolic_pressure);
		txtDiastolicPressure = (TextView) findViewById(R.id.txt_diastolic_pressure);
		LinearLayout layoutBloodGlucoseLevel = (LinearLayout) findViewById(R.id.layout_blood_glucose_level);
		txtBloodGlucoseLevel = (TextView) findViewById(R.id.txt_blood_glucose_level);
		LinearLayout layoutHba1c = (LinearLayout) findViewById(R.id.layout_hba1c);
		txtHba1c = (TextView) findViewById(R.id.txt_hba1c);
		LinearLayout layoutNeutralFat = (LinearLayout) findViewById(R.id.layout_neutral_fat);
		txtNeutralFat = (TextView) findViewById(R.id.txt_neutral_fat);
		LinearLayout layoutHdlCholesterol = (LinearLayout) findViewById(R.id.layout_hdl_cholesterol);
		txtHdlCholesterol = (TextView) findViewById(R.id.txt_hdl_cholesterol);
		LinearLayout layoutLdlCholesterol = (LinearLayout) findViewById(R.id.layout_ldl_cholesterol);
		txtLdlCholesterol = (TextView) findViewById(R.id.txt_ldl_cholesterol);
		LinearLayout layoutAst = (LinearLayout) findViewById(R.id.layout_ast);
		txtAst = (TextView) findViewById(R.id.txt_ast);
		LinearLayout layoutAlt = (LinearLayout) findViewById(R.id.layout_alt);
		txtAlt = (TextView) findViewById(R.id.txt_alt);
		LinearLayout layoutYgtp = (LinearLayout) findViewById(R.id.layout_ygtp);
		txtYgtp = (TextView) findViewById(R.id.txt_ygtp);
		LinearLayout layoutUricAcid = (LinearLayout) findViewById(R.id.layout_uric_acid);
		txtUricAcid = (TextView) findViewById(R.id.txt_uric_acid);
		Button btnNext = (Button) findViewById(R.id.btn_next);
		findViewById(R.id.txt_next).setOnClickListener(this);

		setView();
		btnNext.setOnClickListener(this);
		layoutBloodPressure.setOnClickListener(this);
		layoutBloodGlucoseLevel.setOnClickListener(this);
		layoutDiastolicPressure.setOnClickListener(this);
		layoutHba1c.setOnClickListener(this);
		layoutNeutralFat.setOnClickListener(this);
		layoutHdlCholesterol.setOnClickListener(this);
		layoutLdlCholesterol.setOnClickListener(this);
		layoutAst.setOnClickListener(this);
		layoutAlt.setOnClickListener(this);
		layoutYgtp.setOnClickListener(this);
		layoutUricAcid.setOnClickListener(this);

	}

	private void displayCalculator(final String unit, final int limitNumber, final TextView tmpView, String title) {
		final View sheetView = getLayoutInflater().inflate(R.layout.custom_calculator_layout, null);
		final Dialog dialog = new Dialog(BloodTestActivity.this, R.style.alertdialog_theme_default);
		((TextView) sheetView.findViewById(R.id.title)).setText(title);
		final TextView text_unit = (TextView) sheetView.findViewById(R.id.txt_unit);
		text_unit.setText(unit);
		final TextView text_input = (TextView) sheetView.findViewById(R.id.edt_input);
		ImageView img_close = (ImageView) sheetView.findViewById(R.id.img_close);
		Button btn_point = (Button) sheetView.findViewById(R.id.btn_point);
		Button btn_zero = (Button) sheetView.findViewById(R.id.btn_zero);
		Button btn_c = (Button) sheetView.findViewById(R.id.btn_c);
		Button btn_three = (Button) sheetView.findViewById(R.id.btn_three);
		Button btn_two = (Button) sheetView.findViewById(R.id.btn_two);
		Button btn_one = (Button) sheetView.findViewById(R.id.btn_one);
		Button btn_six = (Button) sheetView.findViewById(R.id.btn_six);
		Button btn_five = (Button) sheetView.findViewById(R.id.btn_five);
		Button btn_four = (Button) sheetView.findViewById(R.id.btn_four);
		Button btn_nine = (Button) sheetView.findViewById(R.id.btn_nine);
		Button btn_eight = (Button) sheetView.findViewById(R.id.btn_eight);
		Button btn_seven = (Button) sheetView.findViewById(R.id.btn_seven);
//		final int number = isHeight ? 220 : 300;
		if (unit.equalsIgnoreCase("%") || title.equalsIgnoreCase("尿酸")) {
			btn_point.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!text_input.getText().toString().trim().isEmpty()) {
						if (text_input.getText().toString().trim().contains(".")) return;
						tmpNum = text_input.getText().toString() + ".".trim();
						text_input.setText(tmpNum);
					}
				}
			});
		}

		btn_c.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				text_input.setText("");
			}
		});
		btn_eight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "8".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_seven.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "7".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_five.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "5".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_four.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "4".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_nine.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "9".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_one.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "1".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_two.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "2".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_three.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "3".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_six.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "6".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});
		btn_zero.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "0".trim();
				if (Double.valueOf(tmpNum) <= limitNumber && isStillWritable(tmpNum))
					text_input.setText(tmpNum);
			}
		});


		img_close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		TextView text_save = (TextView) sheetView.findViewById(R.id.txt_save);

		text_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String input = ((TextView) sheetView.findViewById(R.id.edt_input)).getText().toString().trim();
//				TextView tmpView = (TextView) (isHeight ? textHeight : textWeight);
				if (!input.isEmpty()) {
					tmpView.setText(input + " " + text_unit.getText().toString().trim());
					dialog.dismiss();
				}
			}
		});
		dialog.setCancelable(false);
		dialog.setContentView(sheetView);
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_blood_pressure:
				displayCalculator("mmHG", 300, txtBloodPressure, "最高血圧");
				break;
			case R.id.layout_diastolic_pressure:
				displayCalculator("mmHG", 200, txtDiastolicPressure, "最低血圧");
				break;
			case R.id.layout_blood_glucose_level:
				displayCalculator("mg/dL", 500, txtBloodGlucoseLevel, "血糖値");
				break;
			case R.id.layout_hba1c:
				displayCalculator("%", 30, txtHba1c, "HbA1c(NGSP値)");
				break;
			case R.id.layout_neutral_fat:
				displayCalculator("mg/dL", 500, txtNeutralFat, "中性脂肪");
				break;
			case R.id.layout_hdl_cholesterol:
				displayCalculator("mg/dL", 300, txtHdlCholesterol, "HDL-コレステロール");
				break;
			case R.id.layout_ldl_cholesterol:
				displayCalculator("mg/dL", 300, txtLdlCholesterol, "LDL-コレステロール");
				break;
			case R.id.layout_ast:
				displayCalculator("U/L", 100, txtAst, "AST(GOT)");
				break;
			case R.id.layout_alt:
				displayCalculator("U/L", 100, txtAlt, "ALT(GPT)");
				break;
			case R.id.layout_ygtp:
				displayCalculator("U/L", 200, txtYgtp, "y-GTP");
				break;
			case R.id.layout_uric_acid:
				displayCalculator("mg/dL", 100, txtUricAcid, "尿酸");
				break;
			case R.id.btn_next:
			case R.id.txt_next:
				requestBloodTest();
				break;

		}
	}

	private boolean isStillWritable(String text) {
		if (text.contains(".")) {
			String numberD = text.substring((text.indexOf(".") + 1));//.subString((text.indexOf ( "." )+1 ));
			if (numberD.length() > 1) {
				return false;
			} else return true;
		} else return true;
	}

	private void requestBloodTest() {
		AppManager.getInstance().set5TabRefresh(true);
		String bp = txtBloodPressure.getText().toString().trim();
		String dbp = txtDiastolicPressure.getText().toString().trim();
		String bgl = txtBloodGlucoseLevel.getText().toString().trim();
		String hba1c = txtHba1c.getText().toString().trim();
		String nfat = txtNeutralFat.getText().toString().trim();
		String hdl = txtHdlCholesterol.getText().toString().trim();
		String ldl = txtLdlCholesterol.getText().toString().trim();
		String ast = txtAst.getText().toString().trim();
		String alt = txtAlt.getText().toString().trim();
		String ygtp = txtYgtp.getText().toString().trim();
		String uacid = txtUricAcid.getText().toString().trim();
		if (!bp.isEmpty() || !dbp.isEmpty() || !bgl.isEmpty() || !hba1c.isEmpty() || !nfat.isEmpty() || !hdl.isEmpty() || !ldl.isEmpty() || !ast.isEmpty() ||
				!alt.isEmpty() || !ygtp.isEmpty() || !uacid.isEmpty()) {
			final BloodTestObject bloodTestObject = new BloodTestObject();
			bloodTestObject.setBtsbp(bp);
			bloodTestObject.setBtdbp(dbp);
			bloodTestObject.setBtbgl(bgl);
			bloodTestObject.setBthba1c(hba1c);
			bloodTestObject.setBtnfat(nfat);
			bloodTestObject.setBthdl(hdl);
			bloodTestObject.setBtldl(ldl);
			bloodTestObject.setBtast(ast);
			bloodTestObject.setBtalt(alt);
			bloodTestObject.setBtygtp(ygtp);
			bloodTestObject.setBtuacid(uacid);
			RequestHandler request = new RequestHandler(this);
			JSONObject body = new JSONObject();
			try {
				body.put("bt_sbp", !bp.isEmpty() ? bp.split("mmHG", 2)[0] : bp);
				body.put("bt_dbp", !dbp.isEmpty() ? dbp.split("mmHG", 2)[0] : dbp);
				body.put("bt_bgl", !bgl.isEmpty() ? bgl.split("mg/dL", 2)[0] : bgl);
				body.put("bt_hba1c", !hba1c.isEmpty() ? hba1c.split("%", 2)[0] : hba1c);
				body.put("bt_nfat", !nfat.isEmpty() ? nfat.split("mg/dL", 2)[0] : nfat);
				body.put("bt_hdl", !hdl.isEmpty() ? hdl.split("mg/dL", 2)[0] : hdl);
				body.put("bt_ldl", !ldl.isEmpty() ? ldl.split("mg/dL", 2)[0] : ldl);
				body.put("bt_ast", !ast.isEmpty() ? ast.split("U/L", 2)[0] : ast);
				body.put("bt_alt", !alt.isEmpty() ? alt.split("U/L", 2)[0] : alt);
				body.put("bt_ygtp", !ygtp.isEmpty() ? ygtp.split("U/L", 2)[0] : ygtp);
				body.put("bt_uacid", !uacid.isEmpty() ? uacid.split("mg/dL", 2)[0] : uacid);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_WITH_USER + "/%d", getUserId()), Request.Method.PUT, null, body);
			request.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					if (isSuccess) {
						try {
							User user = mGson.fromJson(data.getJSONObject("user").toString(), User.class);
							RealmHelper.with(getApplicationContext()).save(user);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						RealmHelper.with(BloodTestActivity.this).save(bloodTestObject);
						finish();
//						AppManager.getInstance().getMealthyActivity().dashboradHandler();
					}
				}
			});
		}
	}


	private void setView() {
		BloodTestObject object = RealmHelper.with(BloodTestActivity.this).getRealm().where(BloodTestObject.class).findFirst();
		if (null != object) {
			txtBloodPressure.setText(null != object.getBtsbp() ? object.getBtsbp() : "");
			txtDiastolicPressure.setText(null != object.getBtdbp() ? object.getBtdbp() : "");
			txtBloodGlucoseLevel.setText(null != object.getBtbgl() ? object.getBtbgl() : "");
			txtHba1c.setText(null != object.getBthba1c() ? object.getBthba1c() : "");
			txtNeutralFat.setText(null != object.getBtnfat() ? object.getBtnfat() : "");
			txtHdlCholesterol.setText(null != object.getBthdl() ? object.getBthdl() : "");
			txtLdlCholesterol.setText(null != object.getBtldl() ? object.getBtldl() : "");
			txtAst.setText(null != object.getBtast() ? object.getBtast() : "");
			txtAlt.setText(null != object.getBtalt() ? object.getBtalt() : "");
			txtYgtp.setText(null != object.getBtygtp() ? object.getBtygtp() : "");
			txtUricAcid.setText(null != object.getBtuacid() ? object.getBtuacid() : "");
		}
	}
}
