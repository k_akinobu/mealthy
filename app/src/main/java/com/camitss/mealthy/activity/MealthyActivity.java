package com.camitss.mealthy.activity;


import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.fragment.HomeFragment;
import com.camitss.mealthy.fragment.MainTabFifthFragment;
import com.camitss.mealthy.fragment.MainTabFourthFragment;
import com.camitss.mealthy.fragment.MainTabTwoFirstVisitFragment;
import com.camitss.mealthy.fragment.MainTabTwoFragment;
import com.camitss.mealthy.fragment.MiddleMenuFragment;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.readystatesoftware.viewbadger.BadgeView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;


public class MealthyActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	ImageView menu2, menu0, menu1, menu3, menu4;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;
	private double currentLatitude;
	private double currentLongitude;
	private MainTabFifthFragment fragment;
	private boolean isMiddleMenuSelected = false;
	private LinearLayout footer;
	protected AppManager appManager;
	private boolean goToDashborad = false;
	private BadgeView badge;


	public MainTabFifthFragment getFragment() {
		return fragment;
	}

	public void setFifthFragment(MainTabFifthFragment fragment) {
		this.fragment = fragment;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences preferences = getSharedPreferences("first_start", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean("is_first_start", false);
		editor.commit();
		//// test
		currentLatitude = 35.6894875;
		currentLongitude = 139.6917114;
		setContentView(R.layout.activity_mealthy);
		getUserInfo();
		menu0 = (ImageView) findViewById(R.id.menu_1);
		menu1 = (ImageView) findViewById(R.id.menu_2);
		menu2 = (ImageView) findViewById(R.id.menu_3);
		menu3 = (ImageView) findViewById(R.id.menu_4);
		menu4 = (ImageView) findViewById(R.id.menu_5);
		footer = (LinearLayout) findViewById(R.id.main_footer);
		setMenusClickListener();
		badge = new BadgeView(this, menu4);
		badge.setBadgeBackgroundColor(Color.parseColor("#FF654C"));
		badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
		badge.setBadgeMargin(5, 0);
		menu4.setPadding(0, 0, 15, 0);
		int noti = AppSharedPreferences.getConstant(MealthyActivity.this).getNotificationCount();
		if (noti > 0) {
			badge.setText(noti + "");
			badge.show();

		}
//		getUserNotification(badge);
//		getUnreadNoti(badge);

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				// The next two lines tell the new client that “this” current class will handle connection stuff
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				//fourth line adds the LocationServices API endpoint from GooglePlayServices
				.addApi(LocationServices.API)
				.build();

		// Create the LocationRequest object
		mLocationRequest = LocationRequest.create()
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setInterval(10 * 1000)        // 10 seconds, in milliseconds
				.setFastestInterval(1 * 1000); // 1 second, in milliseconds
		setupTabTwoFragment();
//		String from = getIntent().getStringExtra("FROM");
//		try {
//			if (from.contentEquals("POST")) {
//				setupTabFifthFragment();
//			} else {
////				setupHomeFragment();
//				setupTabTwoFragment();
//			}
//		} catch (NullPointerException npe) {
////			setupHomeFragment();
//			setupTabTwoFragment();
//			npe.printStackTrace();
//		}

		AppManager.getInstance().setMealthyActivity(this);

	}

	private void setMenusClickListener() {
//		menu0.setSelected(true);
		menu1.setSelected(true);
		menu0.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handleMenuClick(v);
			}
		});
		menu1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handleMenuClick(v);
			}
		});
		menu2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handleMenuClick(v);
			}
		});
		menu3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handleMenuClick(v);
			}
		});
		menu4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handleMenuClick(v);
			}
		});
	}

	public LatLng getLatLong() {
		return new LatLng(currentLatitude, currentLongitude);
	}

	private void handleMenuClick(final View chosenMenu) {
		// choose other menu when middle menu is active
		// so turn middle menu to normal state
		if (chosenMenu.getId() != R.id.menu_3) {
			if (isMiddleMenuSelected) {
				footer.setBackgroundColor(getResources().getColor(R.color.toolbar_tran));
				ImageView menu3 = (ImageView) findViewById(R.id.menu_3);
				Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_none_clockwise);
				startRotateAnimation.setFillAfter(true);
				menu3.startAnimation(startRotateAnimation);
				menu3.setImageDrawable(getResources().getDrawable(R.drawable.icon_plus));
				getSupportFragmentManager().popBackStack();
				isMiddleMenuSelected = false;
				return;
			}
			menu1.setSelected(false);
			menu2.setSelected(false);
			menu3.setSelected(false);
			menu4.setSelected(false);
			menu0.setSelected(false);
		}

		switch (chosenMenu.getId()) {
			case R.id.menu_1:
				menu0.setSelected(true);
				if (FragmentTag.getInstant().getTAG() != Constant.FRAGMENT_TAG_HOME) {
					setupHomeFragment();
				}
				break;
			case R.id.menu_2:
				menu1.setSelected(true);
				if (FragmentTag.getInstant().getTAG() != "MAIN_TAB_TWO") {
					setupTabTwoFragment();
				}
				break;
			case R.id.menu_3:
				if (isMiddleMenuSelected) {
					isMiddleMenuSelected = false;
					handleMiddleMenu(chosenMenu, 1);
				} else {
					handleMiddleMenu(chosenMenu, 0);
					isMiddleMenuSelected = true;
				}
				break;
			case R.id.menu_4:
				menu3.setSelected(true);
				if (FragmentTag.getInstant().getTAG() != "MAIN_TAB_FOURTH") {
					User user = RealmHelper.with(MealthyActivity.this).getUserRealmObjById(getUserId());
					if (null != user && user.isFreeConsultation()) {
						startActivity(new Intent(getApplicationContext(), CameraAndGalleryActivity.class));
					} else setupTabFourthFragment();
				}
				break;
			case R.id.menu_5:
				menu4.setSelected(true);
				if (FragmentTag.getInstant().getTAG() != "MAIN_TAB_FIFTH") {
					setupTabFifthFragment(0);
				}
				break;
		}
	}

	/**
	 * Do not call this function and put other view as chosenMenu beside middle footer menu
	 *
	 * @param chosenMenu
	 * @param status
	 */
	private void handleMiddleMenu(final View chosenMenu, final int status) {
		// chosenMenu is this case is middle menu
		Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_none_clockwise);
		// status = 0 mean middle menu is in original position, status = 1 menu is in rotated position
		if (status == 0) {
			startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
			footer.setBackgroundColor(getResources().getColor(R.color.middle_menu_background));
		}
		startRotateAnimation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				chosenMenu.setClickable(false);
				if (status == 0) {
					((ImageView) chosenMenu).setImageDrawable(getResources().getDrawable(R.drawable.icon_plus_white));
					middleMenuFragment();
				} else {
					((ImageView) chosenMenu).setImageDrawable(getResources().getDrawable(R.drawable.icon_plus));
					footer.setBackgroundColor(getResources().getColor(R.color.toolbar_tran));
//					footer.setVisibility(View.VISIBLE);
					//setMenuAction(true);
					getSupportFragmentManager().popBackStack();
				}
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				animation.setFillAfter(true);
				chosenMenu.setClickable(true);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}
		});
		chosenMenu.startAnimation(startRotateAnimation);
	}

	private void setMenuActions(boolean data) {
		menu0.setEnabled(data);
		menu1.setEnabled(data);
		menu3.setEnabled(data);
		menu4.setEnabled(data);
	}

	public void cancelMenu3() {
		footer.setBackgroundColor(getResources().getColor(R.color.toolbar_tran));

		Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_none_clockwise);
		startRotateAnimation.setFillAfter(true);
		final ImageView menu3 = (ImageView) findViewById(R.id.menu_3);
		menu3.startAnimation(startRotateAnimation);
		getSupportFragmentManager().popBackStack();
		menu3.setImageDrawable(getResources().getDrawable(R.drawable.icon_plus));
		//setMenuAction(true);
		isMiddleMenuSelected = false;

	}

	// open the first fragment(HomeFragment) once this activity is visited
	private void setupHomeFragment() {
		HomeFragment homeFragment = new HomeFragment();
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			popBackToEndStack();
		}
		fragmentTransaction.replace(R.id.main_container, homeFragment, Constant.FRAGMENT_TAG_HOME).disallowAddToBackStack();
		fragmentTransaction.commit();
	}

	public void setupTabTwoFragment() {
		menu1.setSelected(true);
		menu2.setSelected(false);
		menu3.setSelected(false);
		menu0.setSelected(false);
		menu4.setSelected(false);


		MainTabTwoFragment fragment = new MainTabTwoFragment();
		MainTabTwoFirstVisitFragment firstVisitFragment = new MainTabTwoFirstVisitFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			popBackToEndStack();
		}
		if (AppSharedPreferences.getConstant(getApplicationContext()).isFirstVisitInTavTwoFragment()) {
			fragmentTransaction.replace(R.id.main_container, firstVisitFragment, "MAIN_TAB_TWO").disallowAddToBackStack();
		} else {
			fragmentTransaction.replace(R.id.main_container, fragment, "MAIN_TAB_TWO").disallowAddToBackStack();
		}
		fragmentTransaction.commit();
	}


	// open middle menu fragment
	private void middleMenuFragment() {
		MiddleMenuFragment fragment = new MiddleMenuFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.add(R.id.main_container, fragment, "MIDDLE_MENU").addToBackStack(FragmentTag.getInstant().getTAG());
		fragmentTransaction.commit();
	}

	private void setupTabFourthFragment() {
		MainTabFourthFragment fragment = new MainTabFourthFragment();
//		MenuDetailFragment fragment = new MenuDetailFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			popBackToEndStack();
		}
		fragmentTransaction.replace(R.id.main_container, fragment, Constant.FRAGMENT_TAG_HOME);
		fragmentTransaction.commit();
	}

	public void setupTabFifthFragment(int selectedTab) {

		menu1.setSelected(false);
		menu2.setSelected(false);
		menu3.setSelected(false);
		menu0.setSelected(false);
		menu4.setSelected(true);

		MainTabFifthFragment fragment = MainTabFifthFragment.newInstance(selectedTab);
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
			popBackToEndStack();
		}
		fragmentTransaction.replace(R.id.main_container, fragment, Constant.FRAGMENT_TAG_HOME);
		fragmentTransaction.commit();

	}

	public void setupTabFifthFragmentCondition(int selectedTab) {

		if (menu4.isSelected()) {
			MainTabFifthFragment fragment = MainTabFifthFragment.newInstance(selectedTab);//new MainTabFifthFragment(selectedTab);
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				popBackToEndStack();
			}
			fragmentTransaction.replace(R.id.main_container, fragment, Constant.FRAGMENT_TAG_HOME);
			fragmentTransaction.commit();
		}

	}

	public void dashboradHandler(boolean goToDashborad) {

		Intent reorderIntent = new Intent(this, MealthyActivity.class);
		reorderIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(reorderIntent);
		this.goToDashborad = goToDashborad;

	}

	private void popBackToEndStack() {
		int count = getSupportFragmentManager().getBackStackEntryCount();
		for (int i = count; i > 0; i--) {
			getSupportFragmentManager().popBackStack();
		}
	}

	@Override
	public void onBackPressed() {
		// handle visited footer middle menu when use press back button
		if (isMiddleMenuSelected) {
			footer.setBackgroundColor(getResources().getColor(R.color.toolbar_tran));
			ImageView menu3 = (ImageView) findViewById(R.id.menu_3);
			Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_none_clockwise);
			startRotateAnimation.setFillAfter(true);
			menu3.startAnimation(startRotateAnimation);
			menu3.setImageDrawable(getResources().getDrawable(R.drawable.icon_plus));
			//setMenuAction(true);
		}
		isMiddleMenuSelected = false;
		super.onBackPressed();
	}

	@Override
	public boolean onSupportNavigateUp() {
		if (getSupportFragmentManager() != null) {
			getSupportFragmentManager().popBackStack();
		} else {
			return false;
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		//Now lets connect to the API
		if (goToDashborad) {
			setupTabFifthFragment(0);
			goToDashborad = false;
		}
		mGoogleApiClient.connect();
		Log.d("mealthyactivity", "onresume");
		getUnreadNoti(badge);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MealthyLog.infoLog(this.getClass().getSimpleName(), "onPause()");

		//Disconnect from API onPause()
		if (mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			mGoogleApiClient.disconnect();
		}

	}

	@Override
	public void onConnected(@Nullable Bundle bundle) {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

		if (location == null) {
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

		} else {
			//If everything went fine lets get latitude and longitude
//			currentLatitude = location.getLatitude();
//			currentLongitude = location.getLongitude();
			currentLatitude = 35.6894875;
			currentLongitude = 139.6917114;

			setLatitude(currentLatitude);
			setLongitude(currentLongitude);
			MealthyLog.infoLog("location ------------------ ", currentLatitude + "," + currentLongitude);
		}
	}

	@Override
	public void onConnectionSuspended(int i) {
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
					/*
					 * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
				/*
				 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
			MealthyLog.errorLog("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		currentLatitude = 35.6894875;
		currentLongitude = 139.6917114;
//		currentLatitude = location.getLatitude();
//		currentLongitude = location.getLongitude();
		setLatitude(currentLatitude);
		setLongitude(currentLongitude);
		MealthyLog.infoLog("location ------------------ ", currentLatitude + "," + currentLongitude);
	}

//	private void getUserNotification(final BadgeView badge) {
//
//		RequestHandler request = new RequestHandler(this);
//		JSONObject header = new JSONObject();
//
//		request.jsonRequest(Constant.REQUEST_GET_USER_NOTIFICATION, Request.Method.GET, header, null);
//		request.setResponseListener(new RequestListener() {
//			@Override
//			public void onResponseResult(boolean isSuccess, JSONObject data) {
//
//				if (isSuccess) {
//					Type listType = new TypeToken<ArrayList<NotificationItem>>() {
//					}.getType();
//					try {
//						List<NotificationItem> userFeedsList = new Gson().fromJson(data.getJSONArray("user_notifications").toString(), listType);
//						if (userFeedsList.size() > 0) {
//							AppSharedPreferences.getConstant(MealthyActivity.this).setNotificationCount(userFeedsList.size());
//							badge.setText(userFeedsList.size() + "");
//							badge.show();
//							ShortcutBadger.applyCount(MealthyActivity.this, userFeedsList.size());
//						}
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		});
//	}

	private void getUnreadNoti(final BadgeView badge) {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(Constant.REQUEST_GET_USER_NOTIFICATION_UNREAD, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("unreadnoti", data.toString());
				if (isSuccess) {
					try {
						int num = data.getJSONObject("user_notifications").getInt("total_unread_count");
						int numNutriNoti = data.getJSONObject("user_notifications").getInt("nutritionist_unread_count");
						AppSharedPreferences.getConstant(getApplicationContext()).setNumNutriNotiPref(numNutriNoti);
						AppSharedPreferences.getConstant(MealthyActivity.this).setNotificationCount(num);
						if (num > 0) {
//							ShortcutBadger.applyCount(getApplicationContext(), num);
							badge.setVisibility(View.VISIBLE);
							badge.setText(num + "");
							badge.show();
							ShortcutBadger.applyCount(MealthyActivity.this, num);
						} else {
							badge.setVisibility(View.INVISIBLE);
							ShortcutBadger.removeCount(getApplicationContext());
						}


					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void getUserInfo() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", getUserId()), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						User user = mGson.fromJson(data.getJSONObject("user").toString(), User.class);
						RealmHelper.with(getApplicationContext()).save(user);
						AppSharedPreferences.getConstant(getApplicationContext()).setUserIdPref(user.getId());
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

			}
		});
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d("mealthyactivity", "onrestart");
		getUnreadNoti(badge);
	}

	@Override
	protected void onPostResume() {
		super.onPostResume();
		getUnreadNoti(badge);
	}
}
