package com.camitss.mealthy.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.camitss.mealthy.MapsActivity;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;
import com.camitss.mealthy.utils.swipemenu.SwipeHorizontalMenuLayout;
import com.camitss.mealthy.utils.twitter.TwitterOnShare;
import com.camitss.mealthy.utils.twitter.TwitterParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import static com.camitss.mealthy.utils.twitter.TwitterParams.TW_REQUEST_CODE_ON_SHARE;

public class FeedDisplayActivity extends BaseActivity implements View.OnClickListener {
	private Context context;
	private ImageView imageItem;
	private ImageView imageUserProfile;
	private TextView textUserName;
	private TextView menuName;
	private TextView shopName;
	private LinearLayout layoutShadowText;
	private LinearLayout layoutCmt;
	private TextView textFavorite;
	private TextView textDate;
	private ImageView imageHeart;
	private ImageView imageCmt;
	private RecyclerView rclCmt;
	private FeedCmtAdapter feedCmtAdapter;
	private ImageView imageEmoji;
	protected EditText edtInputCmt;
	private Button btnSend;
	private ProgressDialog progressDialog;
	private String[] emojiCodes = {getEmojiByUnicode(0x1F44D), getEmojiByUnicode(0x1F44C), getEmojiByUnicode(0x2757), getEmojiByUnicode(0x1F604)};
	private AlertDialog dialogEmoji;
	private ImageView imageProNutrit;
	protected ImageView imageMore;
	private android.app.AlertDialog dialog;
	public static boolean isLogedIn;
	private android.app.AlertDialog twitterDialog;
	public static String TWITTER_CALLBACK_URL = "oauth://com.camitss.mealthy.Twitter_oAuth";
	private BottomSheetDialog dialogBtmSheet;
	private ImageView imageBorder;
	private TextView textLocation;
	protected TextView title;


	public static void launch(Context context, FeedItem item) {
		Intent i = new Intent(context, FeedDisplayActivity.class);
		i.putExtra("FeedItem", item);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	public String getEmojiByUnicode(int unicode) {
		return new String(Character.toChars(unicode));
	}

	private FeedItem getFeedItem() {
		return (FeedItem) getIntent().getSerializableExtra("FeedItem");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed_display);

		context = getApplicationContext();
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		TwitterParams.tw_pref = getSharedPreferences("Tesjor_Twitter", Activity.MODE_PRIVATE);

		progressDialog = new ProgressDialog(FeedDisplayActivity.this, R.style.AppCompatProgressDialogStyle);
		title = (TextView)findViewById(R.id.title);
		imageItem = (ImageView) findViewById(R.id.image_post);
		imageUserProfile = (ImageView) findViewById(R.id.user_profile);
		imageBorder = (ImageView) findViewById(R.id.img_border);
		textUserName = (TextView) findViewById(R.id.user_name);
		menuName = (TextView) findViewById(R.id.menu_name);
		shopName = (TextView) findViewById(R.id.shop_name);
		layoutShadowText = (LinearLayout) findViewById(R.id.shadow_text);
		layoutCmt = (LinearLayout) findViewById(R.id.layout_cmt);
		textFavorite = (TextView) findViewById(R.id.txt_favorite);
		textDate = (TextView) findViewById(R.id.txt_date);
		imageHeart = (ImageView) findViewById(R.id.img_heart);
		imageCmt = (ImageView) findViewById(R.id.img_cmt);
		imageEmoji = (ImageView) findViewById(R.id.img_emoji);
		edtInputCmt = (EditText) findViewById(R.id.edt_input_cmt);
		edtInputCmt.requestFocus();
		btnSend = (Button) findViewById(R.id.btn_send);
		imageProNutrit = (ImageView) findViewById(R.id.img_pro_nutritionist);
		imageMore = (ImageView) findViewById(R.id.img_more);
		imageMore.setOnClickListener(this);
		findViewById(R.id.img_share).setOnClickListener(this);

		setView();

		imageItem.setOnClickListener(this);
		imageHeart.setOnClickListener(this);
		btnSend.setOnClickListener(this);
		imageEmoji.setOnClickListener(this);

		edtInputCmt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				btnSend.setTextColor(s.toString().trim().isEmpty() ? ContextCompat.getColor(context, R.color.black) : ContextCompat.getColor(context, R.color.red));
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.image_post:
				Utils.with(FeedDisplayActivity.this).displayImageItem(FeedDisplayActivity.this,(ImageView) v, getFeedItem().getMenu().getImage());
				break;
			case R.id.img_heart:
				int method = Request.Method.POST;
				if (getFeedItem().isLike())
					method = Request.Method.DELETE;
				requestFeedLike(method, (ImageView) v, (TextView) v.getRootView().findViewById(R.id.txt_favorite));
				break;
			case R.id.btn_send:
				if (!edtInputCmt.getText().toString().trim().isEmpty())
					addMenuCmtRequest(edtInputCmt.getText().toString().trim(), getFeedItem().getMenu(), getFeedItem().getId());
				break;
			case R.id.img_emoji:
				displayEmoji();
				break;
			case R.id.txt_one_emoji:
				onClicEmoji(emojiCodes[0]);
				break;
			case R.id.txt_two_emoji:
				onClicEmoji(emojiCodes[1]);
				break;
			case R.id.txt_three_emoji:
				onClicEmoji(emojiCodes[2]);
				break;
			case R.id.txt_four_emoji:
				onClicEmoji(emojiCodes[3]);
				break;
			case R.id.img_more:
				displayMenu(v);
				break;
			case R.id.txt_ok:
				dialog.dismiss();
				requestToDeleteFeed();
				break;
			case R.id.img_share:
				displayShareDialog();
				break;
			case R.id.txt_one_option:
				dialogBtmSheet.dismiss();
				shareToTwitter();
				break;
			case R.id.txt_two_option:
				break;
		}
	}

	private void displayShareDialog() {
		dialogBtmSheet = new BottomSheetDialog(FeedDisplayActivity.this, R.style.BottomSheetDialog);
		View view = LayoutInflater.from(FeedDisplayActivity.this).inflate(R.layout.layout_f_change_image_profile_dialog, null);
//		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogBtmSheet.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.title)).setVisibility(View.GONE);
		TextView text = (TextView) view.findViewById(R.id.txt_two_option);
		text.setText("不適切なメニューの報告");
		text.setTextColor(ContextCompat.getColor(FeedDisplayActivity.this, R.color.red));
		text.setOnClickListener(this);
		TextView txt = (TextView) view.findViewById(R.id.txt_one_option);
		txt.setText("Twitterでシェア");
		txt.setOnClickListener(this);
		dialogBtmSheet.setCancelable(false);
		dialogBtmSheet.show();
		dialogBtmSheet.setContentView(view);
	}

	public void shareToTwitter() {
//		new TwitterOnShare(FeedDisplayActivity.this, getShareUrl());
		Intent i = new Intent(getApplicationContext(), TwitterLoginProcess.class);
		i.putExtra("ContentLink", getShareUrl());
		startActivityForResult(i, TwitterParams.TW_REQUEST_CODE_ON_SHARE);
	}

	private void requestToDeleteFeed() {
		if (null != getFeedItem()) {
			Utils.showProgressDialog(FeedDisplayActivity.this, progressDialog, getString(R.string.loading));
			RequestHandler request = new RequestHandler(getApplicationContext());
			request.jsonRequest(String.format(Locale.JAPANESE, Constant.REQUEST_DELETE_FEED_ITEM, getFeedItem().getId()), Request.Method.DELETE, null, null);
			request.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					progressDialog.dismiss();
					Log.d("Feeddisplay", data.toString());
					if (isSuccess) {
						finish();
						AppManager.getInstance().set5TabRefresh(true);
					} else {
						Toast.makeText(context, "Cannot delete feed", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}

	}

	private void displayMenu(View v) {
		final PopupMenu popupMenu = new PopupMenu(FeedDisplayActivity.this, v);
		popupMenu.inflate(R.menu.more_options_menu);
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
					case R.id.delete:
						goToConfirmDeleteDialog();
						break;
					case R.id.cancel:
						popupMenu.dismiss();
						break;
				}
				return false;
			}
		});
		popupMenu.show();
	}

	private void goToConfirmDeleteDialog() {
		View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_ok_on_the_left_cancel_on_the_right, null);
		dialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme)).setView(view).show();
		((TextView) view.findViewById(R.id.txt_dialog_title)).setVisibility(View.GONE);
		((TextView) view.findViewById(R.id.txt_dialog_msg)).setText(getString(R.string.txt_confirm_delete_feed_image));
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		TextView okBtn = (TextView) view.findViewById(R.id.txt_ok);
		okBtn.setText(getString(R.string.delete));
		okBtn.setOnClickListener(this);
		dialog.setCancelable(false);
	}

	private void onClicEmoji(String comment) {
		dialogEmoji.dismiss();
		addMenuCmtRequest(comment, getFeedItem().getMenu(), getFeedItem().getId());
	}

	private void displayEmoji() {
		View view = LayoutInflater.from(FeedDisplayActivity.this).inflate(R.layout.layout_emoji_dialog, null);
		dialogEmoji = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme_default)).setView(view).show();
		dialogEmoji.setView(view);
		TextView textOne = (TextView) view.findViewById(R.id.txt_one_emoji);
		TextView textTwo = (TextView) view.findViewById(R.id.txt_two_emoji);
		TextView textThree = (TextView) view.findViewById(R.id.txt_three_emoji);
		TextView textFour = (TextView) view.findViewById(R.id.txt_four_emoji);
		textOne.setText(emojiCodes[0]);
		textTwo.setText(emojiCodes[1]);
		textThree.setText(emojiCodes[2]);
		textFour.setText(emojiCodes[3]);
		ImageView imgClose = (ImageView) view.findViewById(R.id.img_close);
		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogEmoji.dismiss();
			}
		});
		dialogEmoji.setCancelable(false);
	}

	private void requestFeedLike(final int method, final ImageView v, final TextView textView) {
		RequestHandler request = new RequestHandler(context);
		request.jsonRequest(String.format(Constant.REQUEST_LIKE_FEED,getFeedItem().getId()), method, null, null);//Constant.REQUEST_GET_FEED + "/" + getFeedItem().getId() + "/like"
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					int count = getFeedItem().getLikesCount();
					if (method == Request.Method.POST) {
						getFeedItem().setLike(true);
						getFeedItem().setLikesCount(count + 1);
					} else {
						getFeedItem().setLike(false);
						getFeedItem().setLikesCount(count - 1);
					}
					setFeedFavoriteAndTextCount(getFeedItem().isLike(), v, textView, getFeedItem().getLikesCount());
				}
			}
		});
	}

	private void setView() {
		try {
			FeedItem item = getFeedItem();
			FeedItem.Menu menu = item.getMenu();
			FeedItem.FeedItemUser user = item.getFeedItemUser();
			Glide.with(context).load(menu.getImageThumb()).placeholder(R.color.white).into(imageItem);
			textUserName.setText(user.getName());
			imageProNutrit.setVisibility(user.getUserKind() == 2 ? View.VISIBLE : View.INVISIBLE);
			imageMore.setVisibility(user.getUserKind() == 2 || getUserId() != user.getId() ? View.GONE : View.VISIBLE);
			if (null != user && !user.getProfileImage().equalsIgnoreCase("null") && !user.getProfileImage().isEmpty()) {
				Glide.with(context).load(user.getProfileImage()).transform(new CircleTransformation(FeedDisplayActivity.this)).into(new SimpleTarget<GlideDrawable>() {
					@Override
					public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
						imageUserProfile.setImageDrawable(resource);
					}
				});
				imageBorder.setVisibility(user.getUserKind() == 2 ? View.GONE : View.VISIBLE);

			} else {
				imageBorder.setVisibility(View.GONE);
				imageUserProfile.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_account_circle));
			}
//			Glide.with(context).load(user.getProfileImage()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(FeedDisplayActivity.this)).into(imageUserProfile);
			boolean isHaveShopName;
			boolean isHaveMenuName;
			if (null != menu.getCgmShopName() && !menu.getCgmShopName().equals("null")) {
				isHaveShopName = true;
				shopName.setText(menu.getCgmShopName());
			} else {
				isHaveShopName = false;
			}
			if (null != menu.getCgmMenuName() && !menu.getCgmMenuName().equals("null") && !menu.getCgmMenuName().trim().isEmpty()) {
				isHaveMenuName = true;
				menuName.setText(menu.getCgmMenuName());
			} else isHaveMenuName = false;
			layoutShadowText.setVisibility((isHaveMenuName || isHaveShopName) ? View.VISIBLE : View.GONE);
			shopName.setVisibility(isHaveShopName ? View.VISIBLE : View.GONE);
			menuName.setVisibility(isHaveMenuName ? View.VISIBLE : View.GONE);
			Log.d("test", "size" + menu.getCommentList().size());
			layoutCmt.setVisibility(View.GONE);
			setFeedFavoriteAndTextCount(item.isLike(), imageHeart, textFavorite, item.getLikesCount());
			if (!menu.getCreatedAt().isEmpty()) {
				textDate.setText(Utils.fromStringDateWithTimeZoneToFormate(menu.getCreatedAt()));
			}
			setCmtRecyclerView(menu.getCommentList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setFeedFavoriteAndTextCount(boolean isLiked, ImageView view, TextView textFavorite, int likeCount) {
		Drawable drawable = isLiked ? ContextCompat.getDrawable(context, R.drawable.ic_favorite_heart_red) : ContextCompat.getDrawable(context, R.drawable.ic_favorite_heart);
		view.setImageDrawable(drawable);
		textFavorite.setVisibility(likeCount > 0 ? View.VISIBLE : View.INVISIBLE);
		if (likeCount > 0) {
			textFavorite.setText("いいね ! " + likeCount + "件");
		}
	}

	private void setCmtRecyclerView(List<FeedItem.Comment> list) {
		rclCmt = (RecyclerView) findViewById(R.id.rcl_cmt);
		LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
		rclCmt.setLayoutManager(layoutManager);
		rclCmt.setNestedScrollingEnabled(false);
		feedCmtAdapter = new FeedCmtAdapter(context);
		feedCmtAdapter.addCmtList(list);
		rclCmt.setAdapter(feedCmtAdapter);

	}

	private void addMenuCmtRequest(final String comment, FeedItem.Menu menu, int feedId) {
		Utils.showProgressDialog(FeedDisplayActivity.this, progressDialog);
		RequestHandler request = new RequestHandler(context);
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("comment", comment);
			jobj.put("user_id", getUserId());
			jobj.put("shop_id", menu.getShopId());
			jobj.put("feed_id",feedId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(String.format(Constant.REQUEST_ADD_MENU_CMT,menu.getId()), Request.Method.POST, null, jobj);//Constant.REQUEST_MENU_CMT + id + "/comment"
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressDialog.dismiss();
				Log.d("test", data.toString());
				edtInputCmt.setText("");
				if (isSuccess) {
					User user = RealmHelper.with(getApplicationContext()).getUserRealmObjById(getUserId());
					FeedItem.Comment cmt = new FeedItem.Comment();
					cmt.setId(999);
					cmt.setBody(comment);
					FeedItem.FeedItemUser userCmt = new FeedItem.FeedItemUser();
					userCmt.setName(user.getName());
					userCmt.setProfileImage(user.getProfileImageUrl());
					userCmt.setId(getUserId());
					cmt.setCmtUser(userCmt);
					feedCmtAdapter.getCmtList().add(feedCmtAdapter.getCmtList().size(), cmt);
					getFeedItem().getMenu().getCommentList().add(getFeedItem().getMenu().getCommentList().size(), cmt);
					feedCmtAdapter.notifyDataSetChanged();
//					try {
//						List<FeedItem.Comment> cmtList = new ArrayList<FeedItem.Comment>();
//						JSONObject menuObj = data.getJSONObject("menu");
//						JSONArray cmtArray = menuObj.getJSONArray("comments");
//						FeedItem.FeedItemUser userCmt;
//						for (int j = 0; j < cmtArray.length(); j++) {
//							cmt = new FeedItem.Comment();
//							JSONObject cmtObj = new JSONObject(cmtArray.getJSONObject(j).toString());
//							cmt.setId(cmtObj.getInt("id"));
//							cmt.setBody(cmtObj.getString("body"));
//							userCmt = new FeedItem.FeedItemUser();
//							userCmt.setId(cmtObj.getJSONObject("user").getInt("id"));
//							userCmt.setName(cmtObj.getJSONObject("user").getString("name"));
//							cmt.setCmtUser(userCmt);
//							cmtList.add(cmt);
//						}
//						feedCmtAdapter.addCmtList(cmtList);

//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
				}
			}
		});
	}

	private void deleteMenuCmtRequest(int id, final int pos) {
		RequestHandler request = new RequestHandler(context);
		request.jsonRequest(Constant.REQUEST_MENU_CMT + "comment/" + id, Request.Method.DELETE, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
//				if (isSuccess) {
				feedCmtAdapter.getCmtList().remove(pos);
				feedCmtAdapter.notifyItemRemoved(pos);
				feedCmtAdapter.notifyItemRangeChanged(pos, feedCmtAdapter.getItemCount());
//				}
			}
		});
	}

	public class FeedCmtAdapter extends RecyclerView.Adapter<FeedCmtAdapter.CommentViewHolder> {

		private Context context;
		private List<FeedItem.Comment> cmtList = new ArrayList<>();

		public FeedCmtAdapter(Context context) {
			this.context = context;
		}

		public List<FeedItem.Comment> getCmtList() {
			return cmtList;
		}

		public void addCmtList(List<FeedItem.Comment> cmtList) {
			this.cmtList.clear();
			this.cmtList.addAll(cmtList);
			notifyDataSetChanged();
		}

		@Override
		public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new CommentViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_comment_feed_display, parent, false));
		}

		@Override
		public void onBindViewHolder(CommentViewHolder holder, int position) {
			FeedItem.Comment cmtItem = getCmtList().get(position);
			holder.textUserNameCmt.setText(cmtItem.getCmtUser().getName());
			holder.textCmtBody.setText(cmtItem.getBody());
			if (null != cmtItem.getCmtUser().getProfileImage() && !cmtItem.getCmtUser().getProfileImage().trim().isEmpty()) {
				holder.imageBorder.setVisibility(cmtItem.getCmtUser().getUserKind() == 2 ? View.GONE : View.VISIBLE);
				Glide.with(context).load(cmtItem.getCmtUser().getProfileImage()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(FeedDisplayActivity.this)).into(holder.imageUserCmt);
			} else {
				holder.imageBorder.setVisibility(View.GONE);
				holder.imageUserCmt.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_account_circle));
			}
			holder.swipeMenuLayout.setSwipeEnable(cmtItem.getCmtUser().getId() == getUserId() ? true : false);
		}

		@Override
		public int getItemCount() {
			return getCmtList().size();
		}

		public class CommentViewHolder extends RecyclerView.ViewHolder {

			private ImageView imageBorder;
			private LinearLayout smMenuViewRightLayout;
			private SwipeHorizontalMenuLayout swipeMenuLayout;
			private TextView textCmtBody;
			private TextView textUserNameCmt;
			private ImageView imageUserCmt;

			public CommentViewHolder(View itemView) {
				super(itemView);

				imageUserCmt = (ImageView) itemView.findViewById(R.id.img_user_cmt);
				imageBorder = (ImageView) itemView.findViewById(R.id.img_border);
				textUserNameCmt = (TextView) itemView.findViewById(R.id.txt_username_cmt);
				textCmtBody = (TextView) itemView.findViewById(R.id.txt_cmt_body);
				swipeMenuLayout = (SwipeHorizontalMenuLayout) itemView.findViewById(R.id.sml);
				smMenuViewRightLayout = (LinearLayout) itemView.findViewById(R.id.smMenuViewRight);
				smMenuViewRightLayout.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Toast.makeText(FeedDisplayActivity.this, "Delete", Toast.LENGTH_SHORT).show();
						deleteMenuCmtRequest(getFeedItem().getMenu().getCommentList().get(getAdapterPosition()).getId(), getAdapterPosition());
					}
				});
			}

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == TW_REQUEST_CODE_ON_SHARE) {
			if (resultCode == RESULT_OK) {
				isLogedIn = true;
				displayTwitterPostShareDisplay();
			}
		}else if(requestCode == 250 && resultCode == RESULT_OK){
			textLocation.setText(data.getStringExtra("location"));
		}
	}

	private String getShareUrl() {
		return TWITTER_CALLBACK_URL;
	}

	private void displayTwitterPostShareDisplay() {
		View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_twitter_post_share_display, null);
		twitterDialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(FeedDisplayActivity.this, R.style.alertdialog_theme_softkey_resize)).setView(view).show();
		final EditText edtCmt = (EditText) view.findViewById(R.id.edt_twitter_cmt);
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);
				twitterDialog.dismiss();
			}
		});
		LinearLayout layoutLocation = (LinearLayout)view.findViewById(R.id.layout_location);
		textLocation = (TextView)view.findViewById(R.id.text_location);
		layoutLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(FeedDisplayActivity.this, MapsActivity.class), 250);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);

			}
		});
		final TextView textPost = (TextView) view.findViewById(R.id.txt_post);
		textPost.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!edtCmt.getText().toString().trim().isEmpty()) {
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);
					twitterDialog.dismiss();
					new TwitterOnShare(FeedDisplayActivity.this).execute(edtCmt.getText().toString().trim(), getFeedItem().getMenu().getImageThumb(), textLocation.getText().toString().trim());

				}
			}
		});

		String menuName = null != getFeedItem().getMenu().getCgmMenuName() && !getFeedItem().getMenu().getCgmMenuName().equalsIgnoreCase("null") ? getFeedItem().getMenu().getCgmMenuName() + "に決めました！" : "";
		edtCmt.setText(menuName + " #mealthy #diet #healthyfood https://appsto.re/jp/Jqpx4.i");
		ImageView icon = (ImageView) view.findViewById(R.id.img_post);
		Glide.with(context).load(getFeedItem().getMenu().getImage()).listener(new com.bumptech.glide.request.RequestListener<String, GlideDrawable>() {
			@Override
			public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
				return false;
			}

			@Override
			public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
				return false;
			}
		}).error(R.drawable.thum_14578).into(icon);//.override(w, h)
		final TextView txtLength = (TextView) view.findViewById(R.id.txt_twitter_length);
		txtLength.setText(String.valueOf(calculateTextLength(edtCmt.getText().toString().trim())));
		twitterDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		edtCmt.setFocusable(true);
		edtCmt.setFocusableInTouchMode(true);
		edtCmt.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		twitterDialog.setCancelable(false);
		edtCmt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				txtLength.setText(String.valueOf(calculateTextLength(s.toString().trim())));
			}
		});
	}

	private int calculateTextLength(String text) {
		if (!text.isEmpty()) {
			int i = 92 - (text.length());
			return i;
		} else {
			return 92;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		AppManager.getInstance().setTab2Refresh(true);
	}
}
