package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.utils.Utils;

/**
 * Created by Viseth on 4/20/2017.
 */

public class UsersFollowersActivity extends UsersFollowingActivity {
	public static void launch(Context context, int id) {
		Intent i = new Intent(context, UsersFollowersActivity.class);
		i.putExtra("userId", id);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		toolbarTitle.setText(getString(R.string.follower));
		textNoUser.setText(getString(R.string.no_follower));
	}

	@Override
	public String getStringUrl() {
		return Utils.convertFunctionName(Constant.REQUEST_GET_USER_FOLLOWERS, getIntent().getIntExtra("userId", getUserId()));
	}
}
