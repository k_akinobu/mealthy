package com.camitss.mealthy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.CustomPagerAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.DiagnosisItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class OverviewActivity extends AppCompatActivity {

	FragmentPagerAdapter fragmentPagerAdapter;
	int lastPosition = 4, lastVisitCounter = 0;
	boolean isSkip = false;
	private List<Diagnoses> diagnosesDataList;
	private boolean isGoNext = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_overview);
		TextView skip = (TextView) findViewById(R.id.skip);
		String from = null != getIntent().getStringExtra("FROM") ? getIntent().getStringExtra("FROM") : "SETTING";
		if (from.contains("SETTING")) {
			isGoNext = false;
		} else {
			isGoNext = true;
		}
		diagnosesDataList = new ArrayList<>();
		if (isGoNext) {
			requestData();
		}
		ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		fragmentPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
		viewPager.setPageTransformer(true, new FadePageTransformer());
		viewPager.setAdapter(fragmentPagerAdapter);


		skip.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isGoNext) {
					startActivity(new Intent(OverviewActivity.this, PageOneInitDiagnoseActivity.class));
				}
//				PageOneInitDiagnoseActivity.launch(getApplicationContext(), diagnosesDataList);
				finish();
			}
		});

		TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
		tabLayout.setupWithViewPager(viewPager, true);
		for (int i = 0; i < tabLayout.getTabCount(); i++) {
			tabLayout.getTabAt(i).setText("");
		}

		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				Log.i("state", position + "--" + isSkip);
				if (isSkip && position == lastPosition) {
					if (lastVisitCounter == 2) {
						if (isGoNext) {
							startActivity(new Intent(OverviewActivity.this, PageOneInitDiagnoseActivity.class));
						}
//						PageOneInitDiagnoseActivity.launch(getApplicationContext(), diagnosesDataList);
						finish();
					}
					lastVisitCounter++;
				}
			}

			@Override
			public void onPageSelected(int position) {
				if (position == lastPosition) {
					isSkip = true;
				} else {
					lastVisitCounter = 0;
					isSkip = false;
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	private void requestData() {
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REQUEST_DIAGNOSIS, Request.Method.GET, null, null);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray datas = data.getJSONArray("diagnoses");

						for (int i = 0; i < datas.length(); i++) {
							Diagnoses diagnoses = new Diagnoses();
							RealmList<DiagnosisItem> diagnosisItems = new RealmList<>();

							diagnoses.setId(datas.getJSONObject(i).getInt("id"));
							diagnoses.setTitle(datas.getJSONObject(i).getString("title"));
							diagnoses.setBody(datas.getJSONObject(i).getString("body"));
							diagnoses.setPosition(datas.getJSONObject(i).getInt("position"));
							diagnoses.setImage(datas.getJSONObject(i).getString("image"));
							diagnoses.setDisgnosisKind(datas.getJSONObject(i).getInt("diagnosis_kind"));
							diagnoses.setNotes(datas.getJSONObject(i).getString("notes"));
							diagnoses.setCreatedDate(datas.getJSONObject(i).getString("created_at"));

							for (int j = 0; j < datas.getJSONObject(i).getJSONArray("diagnosis_selects").length(); j++) {
								DiagnosisItem diagnosisItem = new DiagnosisItem();
								diagnosisItem.setId(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getInt("id"));
								diagnosisItem.setLabel(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("label"));
								diagnosisItem.setPosition(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getInt("position"));
								diagnosisItem.setImage(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("image"));
								diagnosisItem.setCreateDate(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(j).getString("created_at"));
								diagnosisItems.add(diagnosisItem);
							}
							diagnoses.setDiagnosisItem(diagnosisItems);
							if (datas.getJSONObject(i).getJSONArray("diagnosis_selects").length() >= 2) {
								diagnoses.setSelected(datas.getJSONObject(i).getJSONArray("diagnosis_selects").getJSONObject(1).getInt("id"));
								diagnoses.setSelectedQuestion(false);
							}
							diagnosesDataList.add(diagnoses);
						}
						List<RealmObject> list = new ArrayList<>();
						list.addAll(diagnosesDataList);
						RealmHelper.with(getApplicationContext()).saveAll(list);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

	}

	class FadePageTransformer implements ViewPager.PageTransformer {
		public void transformPage(View view, float position) {
			view.setTranslationX(view.getWidth() * -position);

			if (position <= -1.0F || position >= 1.0F) {
				view.setAlpha(0.0F);
			} else if (position == 0.0F) {
				view.setAlpha(1.0F);
			} else {
				// position is between -1.0F & 0.0F OR 0.0F & 1.0F
				view.setAlpha(1.0F - Math.abs(position));
			}
		}
	}
}