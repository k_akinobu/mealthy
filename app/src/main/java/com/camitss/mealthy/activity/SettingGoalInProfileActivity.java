package com.camitss.mealthy.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.BottomsheetItemAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.GoalItem;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import io.realm.RealmResults;

/**
 * Created by Viseth on 5/11/2017.
 */

public class SettingGoalInProfileActivity extends BaseActivity implements View.OnClickListener {
	private List<String> list = new ArrayList<>();
	private TextView txtCalendar;
	private TextView txtCovered;
	private TextView txtCurve;
	private TextView txtSize;
	private double mWeight;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_a_goal_in_profile);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		txtCalendar = (TextView) findViewById(R.id.txt_calendar_display);
		txtCovered = (TextView) findViewById(R.id.txt_covered_display);
		txtCurve = (TextView) findViewById(R.id.txt_curve_display);
		txtSize = (TextView) findViewById(R.id.txt_size_display);
		try {
			RealmResults<GoalItem> goalItems = RealmHelper.with(getApplicationContext()).getRealm().where(GoalItem.class).findAll();
			Log.d("goalitem", goalItems.size() + "<<>>" + goalItems.last() + "<<>>" + goalItems.first());
			GoalItem item = goalItems.first();
			txtCalendar.setText(item.getPeriod());
			txtCovered.setText(item.getEatOutFrequency());
			txtCurve.setText(item.getCurve());
			txtSize.setText(item.getSize());
		} catch (Exception e) {
			Log.d("ebo", e.getMessage());
			txtCalendar.setText(getString(R.string.f73_text_calendar_item1));
			txtCovered.setText(getString(R.string.f73_text_covered_item3));
			txtCurve.setText(getString(R.string.f73_text_curved_item2));
			txtSize.setText(getString(R.string.f73_text_size_item_2));
		}
		findViewById(R.id.layout_calendar).setOnClickListener(this);
		findViewById(R.id.layout_covered).setOnClickListener(this);
		findViewById(R.id.layout_curve).setOnClickListener(this);
		findViewById(R.id.layout_size).setOnClickListener(this);
		findViewById(R.id.btn_next).setOnClickListener(this);
		try {
			User user = RealmHelper.with(getApplicationContext()).getUserRealmObjById(getUserId());
			int bmi = user.getSex() == 0 ? 20 : 22;
			double value = (Math.pow((user.getTall() / 100), 2)) * bmi;
			((TextView) findViewById(R.id.txt_tagretweight)).setText(String.format(Locale.ENGLISH, getString(R.string.f73_text1), value));
			mWeight = user.getWeight();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void showDialog(final List<String> list, final TextView text, String title) {
		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(SettingGoalInProfileActivity.this);
		View sheetView = getLayoutInflater().inflate(R.layout.item_bottomsheet_dialog_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(title);
		RecyclerView recyclerview_country = (RecyclerView) sheetView.findViewById(R.id.lvCountryZ);

		recyclerview_country.setHasFixedSize(true);
		recyclerview_country.setLayoutManager(new LinearLayoutManager(this));

		BottomsheetItemAdapter adapter = new BottomsheetItemAdapter(getApplicationContext(), list);
		recyclerview_country.setAdapter(adapter);

		adapter.setOnItemClickListner(new BottomsheetItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				if (list.get(position) instanceof String) {
					text.setText((String) list.get(position));
				}
				mBottomSheetDialog.dismiss();
			}
		});

		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.show();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_calendar:
				list.clear();
				list.add(getString(R.string.f73_text_calendar_item1));
				list.add(getString(R.string.f73_text_calendar_item2));
				list.add(getString(R.string.f73_text_calendar_item3));
				showDialog(list, txtCalendar, getString(R.string.f73_text_calendar));
				break;
			case R.id.layout_covered:
				list.clear();
//				list.add(getString(R.string.f73_text_covered_item1));
				list.add(getString(R.string.f73_text_covered_item2));
				list.add(getString(R.string.f73_text_covered_item3));
				list.add(getString(R.string.f73_text_covered_item4));
				showDialog(list, txtCovered, getString(R.string.f73_text_covered));
				break;
			case R.id.layout_curve:
				list.clear();
				list.addAll(getCurveItem(txtCovered.getText().toString().trim()));
				showDialog(list, txtCurve, getString(R.string.f73_text_curved));
				break;
			case R.id.layout_size:
				list.clear();
				list.add(getString(R.string.f73_text_size_item_1));
				list.add(getString(R.string.f73_text_size_item_2));
				list.add(getString(R.string.f73_text_size_item_3));
				showDialog(list, txtSize, getString(R.string.f73_text_size));
				break;
			case R.id.btn_next:
				requestUpdateUser();
				break;
		}
	}

	private List<String> getCurveItem(String covered) {
		List<String> list = new ArrayList<>();
		if (covered.equalsIgnoreCase(getString(R.string.f73_text_covered_item2))) {
			list.add(getString(R.string.f73_text_curved_item1));
			list.add(getString(R.string.f73_text_curved_item2));
		} else if (covered.equalsIgnoreCase(getString(R.string.f73_text_covered_item3))) {
			list.add(getString(R.string.f73_text_curved_item1));
			list.add(getString(R.string.f73_text_curved_item2));
			list.add(getString(R.string.f73_text_curved_item3));
		} else if (covered.equalsIgnoreCase(getString(R.string.f73_text_covered_item4))) {
			list.add(getString(R.string.f73_text_curved_item1));
			list.add(getString(R.string.f73_text_curved_item2));
			list.add(getString(R.string.f73_text_curved_item3));
			list.add(getString(R.string.f73_text_curved_item4));
		}
		return list;
	}

	private double getTargetWeight(int targetReduce) {
		switch (targetReduce) {
			case 1:
				return 0.0;
			case 2:
				return -1.0;
			case 3:
				return -2.0;
			case 4:
				return -3.0;
			default:
				return 0.0;
		}
	}

	private double getTargetMonth(int targetTerm) {
		switch (targetTerm) {
			case 1:
				return 1;
			case 2:
				return 3;
			case 3:
				return 0;
			default:
				return 0;
		}
	}

	private double culcServerTargetWeight(int targetTerm, int targetReduce, double userWeight) {
		double weight = getTargetMonth(targetTerm) * getTargetWeight(targetReduce);
		if (weight == 0) {
			return userWeight;
		}
		double targetWeight = userWeight + weight;

		return targetWeight;
	}

	private void requestUpdateUser() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		JSONObject body = new JSONObject();
		try {
			body.put("term_id", getTargetTerm());
			body.put("eat_out_id", getEatOutFrequency());
			body.put("diet_id", getWeightLossGoal());
			body.put("figure_id", getBodyType());
			body.put("target_weight", culcServerTargetWeight(getTargetTerm(), getWeightLossGoal(), mWeight));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_USER_WITH_ID, getUserId()), Request.Method.PUT, null, body);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					AppSharedPreferences.getConstant(getApplicationContext()).setFromUpdateTargetWeightMiddleMenuScreen(false);
					Log.d("SettingGoal", data.toString());
					try {
						GoalItem item = new GoalItem();
						item.setId(1);
						item.setPeriod(txtCalendar.getText().toString().trim());
						item.setEatOutFrequency(txtCovered.getText().toString().trim());
						item.setCurve(txtCurve.getText().toString().trim());
						item.setSize(txtSize.getText().toString().trim());
						RealmHelper.with(getApplicationContext()).save(item);
						User user = mGson.fromJson(data.getJSONObject("user").toString(), User.class);
						RealmHelper.with(getApplicationContext()).save(user);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					finish();
				} else CustomAlertBuilder.create(SettingGoalInProfileActivity.this, "error");
			}
		});
	}

	private int getTargetTerm() {
		String s = txtCalendar.getText().toString().trim();
		if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item1))) return 1;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item2))) return 2;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_calendar_item3))) return 3;
		return 1;
	}

	private int getEatOutFrequency() {
		String s = txtCovered.getText().toString().trim();
		if (s.equalsIgnoreCase(getString(R.string.f73_text_covered_item1))) return 1;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_covered_item2))) return 2;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_covered_item3))) return 3;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_covered_item4))) return 4;
		return 1;
	}

	private int getWeightLossGoal() {
		String s = txtCurve.getText().toString().trim();
		if (s.equalsIgnoreCase(getString(R.string.f73_text_curved_item1))) return 1;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_curved_item2))) return 2;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_curved_item3))) return 3;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_curved_item4))) return 4;
		return 1;
	}

	private int getBodyType() {
		String s = txtSize.getText().toString().trim();
		if (s.equalsIgnoreCase(getString(R.string.f73_text_size_item_1))) return 1;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_size_item_2))) return 2;
		else if (s.equalsIgnoreCase(getString(R.string.f73_text_size_item_3))) return 3;
		return 1;
	}
}
