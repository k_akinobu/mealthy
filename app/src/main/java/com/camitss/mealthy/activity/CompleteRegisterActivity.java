package com.camitss.mealthy.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CustomAlertBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CompleteRegisterActivity extends BaseActivity {
	String kind;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_complete_register);
		Toolbar toolbar = (Toolbar) findViewById(R.id.email_complete_register_toolbar);
		toolbar.setTitle(getString(R.string.input_passcode_title));
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});
		Intent intent = getIntent();
		kind = intent.getStringExtra("kind");
		final EditText code = (EditText) findViewById(R.id.one_time_passcode);

		findViewById(R.id.complete_register).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				JSONObject param = new JSONObject();
				try {
					param.put("passcode",code.getText().toString());
					sendOneTimeCode(param);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		});
	}

	private void sendOneTimeCode(JSONObject param){
		RequestHandler requestHandler = new RequestHandler(getApplicationContext());
		requestHandler.jsonRequest(Constant.REGISTER_COMPLETE, Request.Method.POST,null,param);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if(isSuccess){
					CustomAlertBuilder.create(CompleteRegisterActivity.this, getString(R.string.sign_up_success), new CustomAlertBuilder.DismissListener() {
						@Override
						public void onDismiss() {
							finish();
						}
					});
					MealthyLog.infoLog("return data ======",data.toString());
				}else{
					CustomAlertBuilder.create(CompleteRegisterActivity.this,getString(R.string.error_passcode), new CustomAlertBuilder.DismissListener() {
						@Override
						public void onDismiss() {
							finish();
						}
					});
					MealthyLog.errorLog("return data ======",data.toString());
				}

			}
		});
	}
}
