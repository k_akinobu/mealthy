package com.camitss.mealthy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.Diagnoses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 3/31/2017.
 */

public class PageOneInitDiagnoseActivity extends BaseInitialDiagnosisActivity {
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);
	}

	@Override
	public List<Diagnoses> getDiagnoseQuestionList() {
		List<Diagnoses> list = new ArrayList<>();
		try {
			if (null != getFullDataDiagnosesDataList()) {
				list.add(getFullDataDiagnosesDataList().get(0));
				list.add(getFullDataDiagnosesDataList().get(1));
				list.add(getFullDataDiagnosesDataList().get(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public String getToolbarTitle() {
		try {
			if (null != getFullDataDiagnosesDataList() && null != getFullDataDiagnosesDataList().get(0)) {
				Log.d("title", getFullDataDiagnosesDataList().get(0).getTitle());
				return getFullDataDiagnosesDataList().get(0).getTitle();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "あなたのカラダチェック";
	}

	@Override
	public String getIntroText() {
		return getString(R.string.intro_page1);
	}

	@Override
	public void getNextButtonFunction() {
		startActivity(new Intent(getApplicationContext(), PageTwoInitDiagnoseActivity.class));
//		PageTwoInitDiagnoseActivity.launch(getApplicationContext(), getFullDataDiagnosesDataList());
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(PageOneInitDiagnoseActivity.this, OverviewActivity.class));
	}
}