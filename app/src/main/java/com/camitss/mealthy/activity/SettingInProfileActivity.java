package com.camitss.mealthy.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.PhotoMultipartRequest;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SettingInProfileActivity extends BaseActivity implements View.OnClickListener {
	private ImageView userProfile;
	private ImageView imageBorder;
	//	private LinearLayout layoutProfile;
	private FrameLayout layoutImageProfile;
	private AlertDialog dialog;
	private Uri mImageCaptureUri;
	private File mOutputFile;
	private ProgressDialog progressDialog;

	public static void launch(Context context) {
		context.startActivity(new Intent(context, SettingInProfileActivity.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_in_profile);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppManager.getInstance().set5TabRefresh(true);
				onBackPressed();

			}
		});
		mOutputFile = new File(this.getExternalFilesDir(null).getAbsolutePath(), "image_crop_tmp.jpg");
		progressDialog = new ProgressDialog(SettingInProfileActivity.this, R.style.AppCompatProgressDialogStyle);

		userProfile = (ImageView) findViewById(R.id.image_profile);
		imageBorder = (ImageView) findViewById(R.id.img_border);
		TextView appVersion = (TextView) findViewById(R.id.app_version);
		appVersion.setText(Constant.VERSION);

		try {
			String image = RealmHelper.with(this).getUserRealmObjById(getUserId()).getProfileImageUrl();
			Glide.with(SettingInProfileActivity.this).load(image).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(SettingInProfileActivity.this)).into(userProfile);
			imageBorder.setVisibility(null != image && !image.trim().isEmpty() ? View.VISIBLE : View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
			imageBorder.setVisibility(View.GONE);
		}

		TextView socialLoginMenu = (TextView) findViewById(R.id.social_login_menu);
		if (AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithSocialPref()) {
			socialLoginMenu.setText(getResources().getString(R.string.connected_social_account));
		}

		LinearLayout layoutProfile = (LinearLayout) findViewById(R.id.layout_profile);
		layoutImageProfile = (FrameLayout) findViewById(R.id.layout_image_profile);
		LinearLayout contactUsMenu = (LinearLayout) findViewById(R.id.profile_setting_contact_us_menu);
		LinearLayout termCondition = (LinearLayout) findViewById(R.id.term_condition_in_setting);
		LinearLayout layoutDiagnoses = (LinearLayout) findViewById(R.id.layout_diagnoses);
		LinearLayout layoutBloodtest = (LinearLayout) findViewById(R.id.layout_bloddtest);
		LinearLayout layoutDashboard = (LinearLayout) findViewById(R.id.layout_dashboard);
		findViewById(R.id.guide_setting).setOnClickListener(this);
		findViewById(R.id.social_setting).setOnClickListener(this);
		findViewById(R.id.support_plan_menu).setOnClickListener(this);
		findViewById(R.id.layout_goal).setOnClickListener(this);
		findViewById(R.id.txt_under_image_profile).setOnClickListener(this);

		layoutProfile.setOnClickListener(this);
		layoutImageProfile.setOnClickListener(this);
		layoutDiagnoses.setOnClickListener(this);
		layoutBloodtest.setOnClickListener(this);
		contactUsMenu.setOnClickListener(this);
		termCondition.setOnClickListener(this);
		layoutDashboard.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_profile:
				startActivity(new Intent(SettingInProfileActivity.this, ProfileSettingEditActivity.class));
				break;
			case R.id.layout_image_profile:
			case R.id.txt_under_image_profile:
				changeImageProfileOptionDialog();
				break;
			case R.id.layout_diagnoses:
				goToDiagnoseDialog();
				break;
			case R.id.layout_bloddtest:
				startActivity(new Intent(SettingInProfileActivity.this, BloodTestActivity.class));
				break;
			case R.id.layout_dashboard:
				startActivity(new Intent(SettingInProfileActivity.this, DashboardInProfileActivity.class));
				break;
			case R.id.layout_goal:
				startActivity(new Intent(SettingInProfileActivity.this, SettingGoalInProfileActivity.class));
				break;
			case R.id.txt_two_option:
				dialog.dismiss();
				try {
					selectFile();
				} catch (NoSuchMethodError e) {
					loadFile();
				}
				break;
			case R.id.txt_one_option:
				dialog.dismiss();
				try {
					showCamera();
				} catch (NoSuchMethodError e) {
					openCamera();
				}
				break;
			case R.id.txt_ok:
				dialog.dismiss();
				startActivity(new Intent(SettingInProfileActivity.this, DiagnosesFromSettingInProfileActivity.class));
				break;
			case R.id.profile_setting_contact_us_menu:
				// menu 10
				Intent i = new Intent(SettingInProfileActivity.this, ContactUsActivity.class);
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				startActivity(i);
				break;
			case R.id.term_condition_in_setting:
				// menu 9
				Intent is = new Intent(SettingInProfileActivity.this, TermConditionActivity.class);
				is.putExtra("FROM", "SETTING");
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				startActivity(is);
				break;
			case R.id.guide_setting:
				Intent overview = new Intent(this, OverviewActivity.class);
				overview.putExtra("FROM", "SETTING");
				startActivity(overview);
				break;
			case R.id.social_setting:
				if (AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithSocialPref()) {
					Intent socialSetting = new Intent(this, SocialSettingActivity.class);
					startActivity(socialSetting);
				} else {
					Intent soicalLogin = new Intent(SettingInProfileActivity.this, SocialLoginActivity.class);
					soicalLogin.putExtra("FROM", "DASHBOARD");
					startActivity(soicalLogin);
				}

				break;
			case R.id.support_plan_menu:
				Intent supportPlan = new Intent(this, SupportPlanActivity.class);
				startActivity(supportPlan);
				break;
		}
	}

	private void goToDiagnoseDialog() {
		View view = LayoutInflater.from(this).inflate(R.layout.layout_f_beforegotodiagnoseactivity_dialog, null);
		dialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.txt_ok)).setOnClickListener(this);
		dialog.setCancelable(false);
	}

	private void changeImageProfileOptionDialog() {
		View view = LayoutInflater.from(this).inflate(R.layout.layout_f_change_image_profile_dialog, null);
		dialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.txt_two_option)).setOnClickListener(this);
		((TextView) view.findViewById(R.id.txt_one_option)).setOnClickListener(this);
		dialog.setCancelable(false);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		int size = permissions.length;
		switch (requestCode) {

			case Constant.REQUEST_OPEN_CAMERA:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					openCamera();
				} else {
					Toast.makeText(this, "Camera permission was not granted!", Toast.LENGTH_LONG).show();
				}
				break;
			case Constant.REQUEST_STORAGE:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					loadFile();
				} else {
					Toast.makeText(this, "Storage permission wan not granted!", Toast.LENGTH_LONG).show();
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				break;
		}
	}


	@TargetApi(Build.VERSION_CODES.M)
	private void selectFile() {
		if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
			loadFile();
		} else {
			if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
				Toast.makeText(this, "Storage permission is needed to access file.", Toast.LENGTH_LONG).show();
			}
			requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_STORAGE);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void showCamera() {
		if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
			openCamera();
		} else {
			if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
				Toast.makeText(this, "Camera permission is needed to open camera.", Toast.LENGTH_LONG).show();
			}
			requestPermissions(new String[]{Manifest.permission.CAMERA}, Constant.REQUEST_OPEN_CAMERA);
		}
	}

	private void loadFile() {
		Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, Constant.REQUEST_LOAD_PHOTO);
	}

	private void openCamera() {
		dispatchTakePictureIntent(Constant.REQUEST_TAKE_PHOTO);
	}

	private void dispatchTakePictureIntent(int requestCode) {
		Intent intentTakePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File dirFile = new File(getExternalFilesDir(null).getAbsolutePath() + "/Mealthy");
		if (!dirFile.exists()) {
			dirFile.mkdir();
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = Constant.JPEG_FILE_PREFIX + timeStamp + "_" + Constant.JPEG_FILE_SUFFIX;

		File f = new File(getExternalFilesDir(null).getAbsolutePath() + "/Mealthy/", imageFileName);
		if (f != null) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
				mImageCaptureUri = Uri.fromFile(f);
			} else {
				mImageCaptureUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", f);
			}
			intentTakePhoto.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intentTakePhoto.putExtra("listPhotoName", imageFileName);
			intentTakePhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			startActivityForResult(intentTakePhoto, requestCode);
			timeStamp = null;
			imageFileName = null;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {

			case Constant.RESULT_LOAD_IMAGE_COVER:
				if (resultCode == RESULT_OK) {
					if (data != null && data.getData() != null) {
						mImageCaptureUri = data.getData();
						doCrop(Constant.REQUEST_CROP_PHOTO);
					}
				}
				break;
			case Constant.REQUEST_TAKE_PHOTO_COVER:
				if (resultCode == RESULT_OK) {
					if (mImageCaptureUri != null) {
						doCrop(Constant.REQUEST_CROP_PHOTO_COVER);
					}
				}
				break;
			case Constant.REQUEST_CROP_PHOTO_COVER:
				if (mOutputFile != null) {
				} else {
					Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_LONG).show();
				}

				break;
			case Constant.REQUEST_LOAD_PHOTO:
				if (resultCode == RESULT_OK) {
					if (data != null && data.getData() != null) {
						mImageCaptureUri = data.getData();
						doCrop(Constant.REQUEST_CROP_PHOTO);
					}
				}
				break;

			case Constant.REQUEST_TAKE_PHOTO:
				if (resultCode == RESULT_OK) {
					if (mImageCaptureUri != null) {
						doCrop(Constant.REQUEST_CROP_PHOTO);
					}
				}
				break;

			case Constant.REQUEST_CROP_PHOTO:
				if (resultCode == RESULT_OK) {
					if (mOutputFile != null) {
						setImageFileToUpload(mOutputFile);
					}
				}
				break;
//            case Constant.REQUEST_CROP_PHOTO_COVER:
//                if (resultCode == RESULT_OK) {
//                    if (mOutputFile != null) {
//                        setImageFileToUpload(mOutputFile.getPath());
//                    }
//                }
//                break;
			default:
				break;
		}
	}


	private void doCrop(int requestCode) {
		mOutputFile = new File(this.getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "image_crop_tmp.jpg");
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(mImageCaptureUri, "image/*");
		if (requestCode == Constant.REQUEST_CROP_PHOTO_COVER) {
			intent.putExtra("outputX", 1024);
			intent.putExtra("outputY", 1024);
			intent.putExtra("aspectX", 1);
		} else if (requestCode == Constant.REQUEST_CROP_PHOTO) {
			intent.putExtra("outputX", 1024);
			intent.putExtra("aspectX", 1);
			intent.putExtra("outputY", 1024);
		}

		intent.putExtra("aspectY", 1);
		intent.putExtra("scale", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mOutputFile));
		startActivityForResult(intent, requestCode);
	}

	private void setImageFileToUpload(File file) {
		Utils.showProgressDialog(SettingInProfileActivity.this, progressDialog);
		PhotoMultipartRequest imageUploadReq = new PhotoMultipartRequest(SettingInProfileActivity.this);
		final String url = String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", getUserId());
		final Response.Listener listener = new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				progressDialog.dismiss();
				Toast.makeText(SettingInProfileActivity.this, "upload success", Toast.LENGTH_SHORT).show();
				try {
					RealmHelper.with(getApplicationContext()).save(mGson.fromJson(response, User.class));
					String image = new JSONObject(response).getJSONObject("user").getString("profile_image_url");
					Glide.with(SettingInProfileActivity.this).load(image).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(SettingInProfileActivity.this)).into(userProfile);
					imageBorder.setVisibility(null != image && !image.trim().isEmpty() ? View.VISIBLE : View.GONE);
				} catch (NullPointerException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
		final Response.ErrorListener errorListener = new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				try {
					progressDialog.dismiss();
					VolleyError errors = new VolleyError(new String(error.networkResponse.data));

					JSONObject err = new JSONObject();
					err.put("status_code", error.networkResponse.statusCode);
					err.put("message", errors.getMessage());
					Log.d("PhotoMultipartRequest", error.networkResponse.statusCode + ":" + errors.getMessage());
				} catch (JSONException e) {
					Log.d("PhotoMultipartRequest", "<<error>>" + e.getMessage());
				} catch (NullPointerException npe) {
					Log.d("PhotoMultipartRequest", "<<error npe>>" + npe.getMessage());
					npe.printStackTrace();
				}
				Toast.makeText(SettingInProfileActivity.this, "upload fail", Toast.LENGTH_SHORT).show();

			}
		};
		imageUploadReq.addBody(url, errorListener, listener, "", getUserToken(), file);
	}
}
