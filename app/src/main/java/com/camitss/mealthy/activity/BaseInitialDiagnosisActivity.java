package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.listener.SetItemClickListener;
import com.camitss.mealthy.object.Diagnoses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.realm.RealmResults;


public abstract class BaseInitialDiagnosisActivity extends BaseActivity implements View.OnClickListener, SetItemClickListener {
	protected ArrayList<Diagnoses> diagnosesDataList;
	protected DiagnosisAdapter diagnosisAdapter;
	protected ListView diagnosesList;
	private Button btn_next;
	protected TextView introTitle;
	protected Toolbar toolbar;
	protected TextView textToolbarTitle;
	protected ImageView imageClose;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pinitial_diagnosis);
		toolbar = (Toolbar) findViewById(R.id.toolbar2);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		diagnosesDataList = new ArrayList<>();
		btn_next = (Button) findViewById(R.id.btn_next);
		introTitle = (TextView) findViewById(R.id.intro_title);
		imageClose = (ImageView) findViewById(R.id.img_close);
		btn_next.setOnClickListener(this);
		textToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
		try {
			RealmResults<Diagnoses> list = RealmHelper.with(getApplicationContext()).getRealm().where(Diagnoses.class).findAll();
			diagnosesDataList.addAll(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		diagnosisAdapter = new DiagnosisAdapter(this);
		diagnosesList = (ListView) findViewById(R.id.diagnos_list);
		diagnosesList.setAdapter(diagnosisAdapter);
		diagnosisAdapter.setSetItemClickListener(this);
	}

	public abstract List<Diagnoses> getDiagnoseQuestionList();

	public abstract String getToolbarTitle();

	public abstract String getIntroText();

	public abstract void getNextButtonFunction();

	public List<Diagnoses> getFullDataDiagnosesDataList() {
		return diagnosesDataList;
	}

	@Override
	protected void onResume() {
		super.onResume();
		setView();
	}

	public void setView() {
		textToolbarTitle.setText(getToolbarTitle());
		introTitle.setText(getIntroText());
		diagnosisAdapter.addDiagnosisList(getDiagnoseQuestionList());
		diagnosisAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_next:
				getNextButtonFunction();
				break;
		}
	}

	@Override
	public void onItemClickListener(Diagnoses objects) {
		RealmHelper.with(getApplicationContext()).editDiagnose(objects.getId(), objects.isSelectedQuestion());
	}


	public class DiagnosisAdapter extends BaseAdapter {

		private Context context;
		private List<Diagnoses> diagnosesList = new ArrayList<>();
		private SetItemClickListener setItemClickListener;


		public DiagnosisAdapter(Context context) {
			this.context = context;
		}

		public void addDiagnosisList(List<Diagnoses> dList) {
			this.diagnosesList = dList;
		}

		public List<Diagnoses> getDiagnosesList() {
			return diagnosesList;
		}

		public SetItemClickListener getSetItemClickListener() {
			return setItemClickListener;
		}

		public void setSetItemClickListener(SetItemClickListener setItemClickListener) {
			this.setItemClickListener = setItemClickListener;
		}

		@Override
		public int getCount() {
			return diagnosesList.size();
		}

		@Override
		public Object getItem(int position) {
			return diagnosesList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(context).
						inflate(R.layout.diagnosis_item, parent, false);
			}
			final Diagnoses diagnos = getDiagnosesList().get(position);

			TextView questionNumber = (TextView) convertView.findViewById(R.id.intro_question_id);
			TextView question = (TextView) convertView.findViewById(R.id.intro_question);
//			final ImageButton agreeTick = (ImageButton) convertView.findViewById(R.id.intro_select_option);
			final ImageView agreeTick = (ImageView) convertView.findViewById(R.id.intro_select_option);
			LinearLayout layoutAgreeTick = (LinearLayout) convertView.findViewById(R.id.layout_select_option);
			View.OnClickListener listener = new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (agreeTick.isSelected()) {
						agreeTick.setSelected(false);
						RealmHelper.with(getApplicationContext()).editDiagnose(diagnos.getId(), false);

					} else {
						agreeTick.setSelected(true);
						RealmHelper.with(getApplicationContext()).editDiagnose(diagnos.getId(), true);
					}
					setItemClickListener.onItemClickListener(diagnos);

				}
			};
			if (diagnos.isSelectedQuestion()) {
				agreeTick.setSelected(true);
			} else {
				agreeTick.setSelected(false);
			}
			layoutAgreeTick.setOnClickListener(listener);
			agreeTick.setOnClickListener(listener);

			questionNumber.setText("Q" + diagnos.getPosition());
			question.setText(diagnos.getBody());

			return convertView;
		}
	}

}
