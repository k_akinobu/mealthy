package com.camitss.mealthy.activity;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.camitss.mealthy.R;

public class TestActivity extends AppCompatActivity {

	private TextView txtSpan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);

		txtSpan = (TextView) findViewById(R.id.text_test);
		final String changeString = "Zoey Bo";
		final String lastString = "ご飯が足らず、私のご飯はグラノーラでした。ヨーグルトと組み合わせて整腸作用効果を期待…";
		String totalString = changeString + lastString;
		final ClickableSpan clickableSpan = new ClickableSpan() {
			@Override
			public void onClick(final View textView) {
				//Your onClick code here
				Toast.makeText(TestActivity.this, changeString + " isClicked.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void updateDrawState(final TextPaint textPaint) {
				textPaint.setColor(ContextCompat.getColor(TestActivity.this, android.R.color.black));
				textPaint.setTypeface(Typeface.DEFAULT_BOLD);
				textPaint.setUnderlineText(false);
			}
		};
		final ClickableSpan clickableSpanTwo = new ClickableSpan() {
			@Override
			public void onClick(final View textView) {
				//Your onClick code here
				Toast.makeText(TestActivity.this, lastString + " isClicked.", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void updateDrawState(final TextPaint textPaint) {
				textPaint.setUnderlineText(false);
			}
		};

		SpannableString span1 = new SpannableString(changeString);
		SpannableString span2 = new SpannableString(lastString);

		span1.setSpan(clickableSpan, 0,
				changeString.length(), 0);
		span2.setSpan(clickableSpanTwo,
				0, lastString.length(), 0);
		txtSpan.setMovementMethod(LinkMovementMethod.getInstance());
		txtSpan.setText(TextUtils.concat("Hello ", span1, " ", span2));
		Spannable spanText = new SpannableString(totalString);
		spanText.setSpan(clickableSpan, 0,
				changeString.length(), 0);

//		txtSpan.setText(spanText);
	}

	private void doStyleSpanForFirstString(String firstString,
										   String lastString, TextView txtSpan) {
		String changeString = (firstString != null ? firstString : "");
		String totalString = changeString + lastString;
		Spannable spanText = new SpannableString(totalString);
		spanText.setSpan(new StyleSpan(Typeface.BOLD), 0,
				changeString.length(), 0);
		txtSpan.setText(spanText);
	}
}
