package com.camitss.mealthy.activity;

import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.camitss.mealthy.R;
import com.camitss.mealthy.fragment.SplashFragment;
import com.camitss.mealthy.fragment.StartFragment;

public class StartActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

	private FragmentManager fragmentManager = getSupportFragmentManager();
	private FragmentTransaction fragmentTransaction;
	Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_activity);
		toolbar = (Toolbar) findViewById(R.id.toolbar_term_condition);
		setSupportActionBar(toolbar);

		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.setCustomAnimations(R.anim.in_from_left,R.anim.out_to_left);
		fragmentTransaction.replace(R.id.start_screen_container, new StartFragment(toolbar), "START");
		fragmentTransaction.commit();

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().hide();
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getSupportFragmentManager().popBackStack();
			}
		});
	}

	@Override
	public boolean onSupportNavigateUp() {
		if (getFragmentManager() != null) {
			getFragmentManager().popBackStack();
		} else {
			return false;
		}
		return true;
	}

	@Override
	public void onBackStackChanged() {
		super.onBackPressed();
	}
}