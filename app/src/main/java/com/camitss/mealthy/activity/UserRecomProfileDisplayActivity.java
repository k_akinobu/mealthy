package com.camitss.mealthy.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.recyclerview.GridGalleryAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;
import com.google.android.gms.vision.text.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserRecomProfileDisplayActivity extends BaseActivity implements View.OnClickListener {
	private TextView textUserName, textUserFollower, textUserFollowing, textFollow, textDietType;
	private ImageView imgUserProfile;
	private RecyclerView gridGallery;
	private GridGalleryAdapter adapter;
	private boolean isFollowing;
	private ImageView imageBorder;
	private int followerCount;
	private LinearLayout layoutNoti;

	public static void launch(Context context, int userId) {
		Intent i = new Intent(context, UserRecomProfileDisplayActivity.class);
		i.putExtra("userId", userId);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_recom_profile_display);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		textUserName = (TextView) findViewById(R.id.user_name);
		textUserFollower = (TextView) findViewById(R.id.user_follower);
		textUserFollowing = (TextView) findViewById(R.id.user_following);
		textDietType = (TextView) findViewById(R.id.user_diet_type);
		textFollow = (TextView) findViewById(R.id.txt_follow);
		imgUserProfile = (ImageView) findViewById(R.id.image_user_profile);
		imageBorder = (ImageView) findViewById(R.id.img_border);
		layoutNoti = (LinearLayout) findViewById(R.id.layout_noti);

		layoutNoti.setVisibility(getUserFeedId()==getUserId()?View.VISIBLE:View.GONE);
		textFollow.setVisibility(getUserFeedId()==getUserId()?View.GONE:View.VISIBLE);

		gridGallery = (RecyclerView) findViewById(R.id.rcl_gallery_grid);
		GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);
		gridGallery.setLayoutManager(layoutManager);
		adapter = new GridGalleryAdapter(UserRecomProfileDisplayActivity.this);
		gridGallery.setAdapter(adapter);
		requestGetUserFeed();

		adapter.setOnItemClickListener(new GridGalleryAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int pos) {
				HistoryFeedItemDisplayActivity.launch(getApplicationContext(), adapter.getFeeItemList().get(pos));
			}
		});

		textUserFollower.setOnClickListener(this);
		textUserFollowing.setOnClickListener(this);
		textFollow.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
		requestGetUser();
	}

	private int getUserFeedId() {
		return getIntent().getIntExtra("userId", 0);
	}

	private void requestGetUser() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(Constant.REQUEST_WITH_USER + "/" + getUserFeedId(), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONObject user = data.getJSONObject("user");
						textUserName.setText(user.getString("name"));
						textDietType.setText(user.getString("diet_type"));
						followerCount = user.getInt("followers_count");
						textUserFollower.setText(followerCount + getString(R.string.follower));
						textUserFollowing.setText(user.getInt("following_count") + getString(R.string.following));
						textUserFollower.setVisibility(user.getInt("user_kind") == 2 ? View.INVISIBLE : View.VISIBLE);
//						textUserFollowing.setVisibility(user.getInt("following_count") > 0 ? View.VISIBLE : View.INVISIBLE);
						imageBorder.setVisibility(null != user.getString("profile_image_url") && !user.getString("profile_image_url").isEmpty() ? View.VISIBLE : View.INVISIBLE);
						Glide.with(getApplicationContext()).load(user.getString("profile_image_url")).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(getApplicationContext())).into(imgUserProfile);
						isFollowing = user.getBoolean("is_following");
						setFollowButton();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void requestGetUserFeed() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(Utils.convertFunctionName(Constant.REQUEST_GET_USER_FEED, getUserFeedId()), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray array = data.getJSONArray("feeds");
						JSONObject jObj = data.getJSONObject("paging");
						String nextLink = jObj.getString("next");
						boolean hasMore = jObj.getBoolean("has_more");
						Log.d("JSONOBj", array.length() + ">>" + nextLink + ">>" + hasMore);
						JSONObject obj;
						List<FeedItem> list = new ArrayList<FeedItem>();
						FeedItem item;
						FeedItem.Menu menu;
						FeedItem.FeedItemUser user;
						FeedItem.Comment cmt;
						JSONObject userObj;
						JSONObject menuObj;
						JSONArray cmtArray;
						List<FeedItem.Comment> cmtList;
						for (int i = 0; i < array.length(); i++) {
							obj = new JSONObject(array.get(i).toString());
							Log.d("JSONOBj", obj.getString("id"));
							item = new FeedItem();
							menu = new FeedItem.Menu();
							user = new FeedItem.FeedItemUser();
							cmtList = new ArrayList<FeedItem.Comment>();
							item.setId(obj.getInt("id"));
							item.setLike(obj.getBoolean("is_like"));
							item.setLikesCount(obj.getInt("likes_count"));
							menuObj = obj.getJSONObject("menu");
							menu.setId(menuObj.getInt("id"));
							menu.setCgmMenuName(menuObj.getString("cgm_menu_name"));
							menu.setCgmShopName(menuObj.getString("cgm_shop_name"));
							menu.setImage(menuObj.getString("image"));
							menu.setImageThumb(menuObj.getString("thumbnail_image_url"));
							menu.setCreatedAt(menuObj.getString("created_at"));
							menu.setCommentCount(menuObj.getInt("comments_count"));
							cmtArray = menuObj.getJSONArray("comments");
							FeedItem.FeedItemUser userCmt;
							for (int j = 0; j < cmtArray.length(); j++) {
								cmt = new FeedItem.Comment();
								JSONObject cmtObj = new JSONObject(cmtArray.getJSONObject(j).toString());
								cmt.setId(cmtObj.getInt("id"));
								cmt.setBody(cmtObj.getString("body"));
								userCmt = new FeedItem.FeedItemUser();
								userCmt.setId(cmtObj.getJSONObject("user").getInt("id"));
								userCmt.setName(cmtObj.getJSONObject("user").getString("name"));
								userCmt.setProfileImage(cmtObj.getJSONObject("user").getString("profile_image_url"));
								cmt.setCmtUser(userCmt);
								cmtList.add(cmt);
							}
							menu.setCommentList(cmtList);
							item.setMenu(menu);
							item.setLikesCount(obj.getInt("likes_count"));
							userObj = obj.getJSONObject("user");
							user.setId(userObj.getInt("id"));
							user.setName(userObj.getString("name"));
							user.setProfileImage(userObj.getString("profile_image_url"));
							item.setFeedItemUser(user);
							list.add(item);
						}
						adapter.addFeedItemList(list);
						adapter.notifyDataSetChanged();
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.txt_follow:
				requestToFollowUser();
				break;
			case R.id.user_follower:
				UsersFollowersActivity.launch(UserRecomProfileDisplayActivity.this, getUserFeedId());
				break;
			case R.id.user_following:
				UsersFollowingActivity.launch(UserRecomProfileDisplayActivity.this, getUserFeedId());
				break;
		}
	}

	private void requestToFollowUser() {
		int method = isFollowing ? Request.Method.DELETE : Request.Method.POST;
		RequestHandler request = new RequestHandler(UserRecomProfileDisplayActivity.this);
		request.jsonRequest(Utils.convertFunctionName(Constant.REQUEST_FOLLOW_USER, getUserFeedId()), method, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isFollowing) {
					isFollowing = false;
					followerCount = followerCount - 1;
				} else {
					isFollowing = true;
					followerCount = followerCount + 1;
				}
				setFollowButton();
				textUserFollower.setText(followerCount + getString(R.string.follower));


			}
		});
	}

	private void setFollowButton() {
		textFollow.setText(isFollowing ? "Following" : "+ Follow");
		textFollow.setBackground(isFollowing ? ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_orange_fill) : ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_edittext_normal));
		textFollow.setTextColor(isFollowing ? ContextCompat.getColor(getApplicationContext(), R.color.white) : ContextCompat.getColor(getApplicationContext(), R.color.orange));

	}
}
