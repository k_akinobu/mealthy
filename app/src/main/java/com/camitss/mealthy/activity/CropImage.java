package com.camitss.mealthy.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class CropImage extends AppCompatActivity {

	CropImageView mCropView;
	File file;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_crop_image);
		mCropView = (CropImageView) findViewById(R.id.cropImageView);
		file = new File(Environment.getExternalStorageDirectory() + "/crop.jpg");
		TextView cancel = (TextView) findViewById(R.id.cancel_crop);
		TextView choose = (TextView) findViewById(R.id.done_crop);
		mCropView.setOutputMaxSize(512,512);
		final String path = getIntent().getStringExtra("PATH");
		Uri uri = Uri.fromFile(new File(path));

		mCropView.startLoad(uri,
				new LoadCallback() {
					@Override
					public void onSuccess() {
					}

					@Override
					public void onError() {
					}
				});

		mCropView.setCropMode(CropImageView.CropMode.SQUARE);

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				startActivity(new Intent(CropImage.this,ChoosePhotoActivity.class));
			}
		});

		choose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				mCropView.startCrop(createSaveUri(CropImage.this), mCropCallback, new SaveCallback() {
					@Override
					public void onSuccess(Uri outputUri) {
						saveFile(outputUri);
					}

					@Override
					public void onError() {

					}
				});

			}
		});
	}

	private void saveFile(Uri uri){
		File f = new File(uri.getPath());
		int size = (int) f.length();
		byte[] bytes = new byte[size];
		try {

			BufferedInputStream buf = new BufferedInputStream(new FileInputStream(f));
			buf.read(bytes, 0, bytes.length);
			buf.close();
			save(bytes);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Intent postIntent = new Intent(CropImage.this,PostActivity.class);
		startActivity(postIntent);
		finish();
	}

	private void save(byte[] bytes) throws IOException {
		OutputStream output = null;
		try {
			output = new FileOutputStream(file);
			output.write(bytes);
		} finally {
			if (null != output) {
				output.close();
			}
		}
	}

	private final CropCallback mCropCallback = new CropCallback() {
		@Override
		public void onSuccess(Bitmap cropped) {
		}

		@Override
		public void onError() {
		}
	};

	public Uri createSaveUri(Activity activity) {
		return Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivity(new Intent(CropImage.this,ChoosePhotoActivity.class));
		finish();
	}
}
