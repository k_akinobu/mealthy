package com.camitss.mealthy.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.camitss.mealthy.R;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.PhotoMultipartRequest;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class SocialLoginActivity extends BaseActivity implements View.OnClickListener {

	private CallbackManager callbackManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getApplicationContext());
		setContentView(R.layout.activity_social_login);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		LinearLayout layoutFacebook = (LinearLayout) findViewById(R.id.layout_fb);
		layoutFacebook.setOnClickListener(this);
		LinearLayout layoutSns = (LinearLayout) findViewById(R.id.layout_sns);
		layoutSns.setOnClickListener(this);
		LinearLayout layoutMealthy = (LinearLayout) findViewById(R.id.layout_mealthy);
		layoutMealthy.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_fb:
				onLoginWithFbClick();
				break;
			case R.id.layout_sns:
				Intent email = new Intent(SocialLoginActivity.this,EmailLoginActivity.class);
				email.putExtra("FROM","EMAIL_BUTTON");
				startActivity(email);
				break;
			case R.id.layout_mealthy:
				Intent cooperate = new Intent(SocialLoginActivity.this,EmailLoginActivity.class);
				cooperate.putExtra("FROM","COOPERATE_BUTTON");
				startActivity(cooperate);
				break;
		}
	}

	private void loginWithFb(String whatSocial, String email, String id, String token, final String profileUrl, final String name) {
		RequestHandler request = new RequestHandler(SocialLoginActivity.this);
		JSONObject body = new JSONObject();
		try {
			body.put("access_token", token);
			body.put("provider", whatSocial);
			body.put("email", email);
			body.put("uid", id);
			body.put("password", null);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_lOGIN_WITH_SOCIAL, Request.Method.POST, null, body);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					AppSharedPreferences.getConstant(SocialLoginActivity.this).setLoginWithSocialPref(true);
					try {
						JSONObject authObj = data.getJSONObject("authentication");
						TokenObject token = new TokenObject();
						token.setToken(authObj.getString("token"));
						TokenRealmHelper.with(SocialLoginActivity.this).save(token);
						getUserInfo(authObj.getInt("user_id"), profileUrl, name, authObj.getString("token"));
						AppSharedPreferences.getConstant(getApplicationContext()).setConnectedSocialType(Constant.CONNECT_FACEBOOK);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});

	}

	private void requestToLoginWithFb() {
		final LoginManager manager = LoginManager.getInstance();//
		manager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
		callbackManager = CallbackManager.Factory.create();

		manager.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						// App code
						Toast.makeText(SocialLoginActivity.this, "Success", Toast.LENGTH_SHORT).show();
						final AccessToken accessToken = loginResult.getAccessToken();
						GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
							@Override
							public void onCompleted(JSONObject user, GraphResponse graphResponse) {
								String id = user.optString("id");
								String profileUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
								String name = user.optString("first_name") + " " + user.optString("last_name");
								loginWithFb("facebook", user.optString("email"), id, String.valueOf(accessToken), profileUrl, name);
							}
						});
						Bundle parameters = new Bundle();
						parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
						request.setParameters(parameters);
						request.executeAsync();

						manager.logOut();

					}

					@Override
					public void onCancel() {
						// App code
						Toast.makeText(SocialLoginActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onError(FacebookException exception) {
						// App code
						Toast.makeText(SocialLoginActivity.this, "Error", Toast.LENGTH_SHORT).show();
						Log.d("exception",exception.getMessage());
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	private void getUserInfo(final int id, final String profileUrl, final String name, final String authtoken) {
		RequestHandler request = new RequestHandler(getApplicationContext());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", id), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				AppSharedPreferences.getConstant(SocialLoginActivity.this).setLoginWithSocialPref(isSuccess);
				if (isSuccess) {
					try {
						AppSharedPreferences.getConstant(getApplicationContext()).setUserIdPref(data.getJSONObject("user").getInt("id"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					displayQuestionAlert(profileUrl, id, name, authtoken);
				}
			}
		});
	}

	private void displayQuestionAlert(final String profileUrl, final int id, final String name, final String authtoken) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.alertdialog_theme_default);
		dialog.setMessage(getString(R.string.dialog_msg));
		dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
//				updateUserInfo(profileUrl, id);
				upload(id, profileUrl, name, authtoken);
				dialog.dismiss();
			}
		});
		dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				goToCameraAndGalleryActivity();
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
		dialog.show();
	}

	private void goToCameraAndGalleryActivity() {
		startActivity(new Intent(SocialLoginActivity.this, CameraAndGalleryActivity.class));
	}

	private void upload(int id, final String photoUrl, final String name, final String authtoken) {
		final String url = String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", id);
		final Response.Listener listener = new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Toast.makeText(SocialLoginActivity.this, "success", Toast.LENGTH_SHORT).show();
				try {
					User user = mGson.fromJson(response, User.class);
					RealmHelper.with(getApplicationContext()).save(user);
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				goToCameraAndGalleryActivity();
				finish();
			}
		};
		final Response.ErrorListener errorListener = new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				try {
					AppSharedPreferences.getConstant(SocialLoginActivity.this).setLoginWithSocialPref(false);
					VolleyError errors = new VolleyError(new String(error.networkResponse.data));

					JSONObject err = new JSONObject();
					err.put("status_code", error.networkResponse.statusCode);
					err.put("message", errors.getMessage());
					Log.d("PhotoMultipartRequest", error.networkResponse.statusCode + ":" + errors.getMessage());
				} catch (JSONException e) {
					Log.d("PhotoMultipartRequest", "<<error>>" + e.getMessage());
				} catch (NullPointerException npe) {
					Log.d("PhotoMultipartRequest", "<<error npe>>" + npe.getMessage());
					npe.printStackTrace();
				}
				Toast.makeText(SocialLoginActivity.this, "error update with fb info", Toast.LENGTH_SHORT).show();

			}
		};
		Glide.with(SocialLoginActivity.this).load(photoUrl).asBitmap().into(new SimpleTarget<Bitmap>(420, 420) {
			@Override
			public void onResourceReady(final Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
				new AsyncTask<Object, Object, File>() {
					@Override
					protected File doInBackground(Object... params) {
						File file = new File(getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "test.jpg");//tputFile = new File(this.getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "image_crop_tmp.jpg");
						OutputStream os = null;
						try {
							os = new FileOutputStream(file);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}

						resource.compress(Bitmap.CompressFormat.JPEG, 100, os);
						return file;
					}

					@Override
					protected void onPostExecute(File file) {
						super.onPostExecute(file);
						PhotoMultipartRequest imageUploadReq = new PhotoMultipartRequest(SocialLoginActivity.this);
						imageUploadReq.addBody(url, errorListener, listener, name, authtoken, file);

					}
				}.execute();
			}
		});
	}

	private void onLoginWithFbClick() {
			requestToLoginWithFb();
	}


	@Override
	protected void onPostResume() {
		super.onPostResume();
		if (AppSharedPreferences.getConstant(getApplicationContext()).isLoginWithSocialPref()) {
			finish();
			Intent socialSetting = new Intent(this, SocialSettingActivity.class);
			startActivity(socialSetting);
		}
	}
}
