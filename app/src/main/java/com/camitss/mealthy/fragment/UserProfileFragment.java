package com.camitss.mealthy.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.activity.NotificationActivity;
import com.camitss.mealthy.activity.SettingInProfileActivity;
import com.camitss.mealthy.activity.UsersFollowersActivity;
import com.camitss.mealthy.activity.UsersFollowingActivity;
import com.camitss.mealthy.adapter.recyclerview.GridGalleryAdapter;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Viseth on 4/26/2017.
 */

public class UserProfileFragment extends Fragment implements View.OnClickListener {
	private Gson mGson = new Gson();
	private ImageView userProfile;
	private TextView userName;
	private TextView userFollowing;
	private TextView userFollower;
	private TextView userDietType;
	private TextView point;
	private TextView pointDescript;
	private final String largehypen = "ー";
	private final String smallhypen = "-";
	private String value = "";
	private final int term = 1;
	private ImageView imageBorder;
	private ProgressDialog progressDialog;
	private TextView textUnreadNotiNum;
	private boolean firstLoad = true;
	GridGalleryAdapter adapter;
	private RecyclerView gridGallery;
	Toolbar toolbar;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentTag.getInstant().setTAG("USER_PROFILE");
		View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
		toolbar = (Toolbar) view.findViewById(R.id.toolbar_user_profile);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatProgressDialogStyle);
		Utils.showProgressDialog(getActivity(), progressDialog);
		gridGallery = (RecyclerView)view.findViewById(R.id.profile_gallery_grid);
		GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);
		gridGallery.setLayoutManager(layoutManager);
		adapter = new GridGalleryAdapter(getActivity());
		gridGallery.setAdapter(adapter);
		requestGetUserFeed();

		adapter.setOnItemClickListener(new GridGalleryAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int pos) {
				FeedDisplayActivity.launch(getApplicationContext(), adapter.getFeeItemList().get(pos));
			}
		});
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		userProfile = (ImageView) view.findViewById(R.id.user_profile);
		imageBorder = (ImageView) view.findViewById(R.id.img_border);
		imageBorder.setVisibility(View.GONE);
		ImageView notiIcon = (ImageView) view.findViewById(R.id.noti_icon);
		ImageView settingIcon = (ImageView) view.findViewById(R.id.setting_icon);
		userName = (TextView) view.findViewById(R.id.user_name);
		userFollowing = (TextView) view.findViewById(R.id.user_following);
		userFollower = (TextView) view.findViewById(R.id.user_follower);
		userDietType = (TextView) view.findViewById(R.id.user_diet_type);
		point = (TextView) view.findViewById(R.id.txt_point);
		pointDescript = (TextView) view.findViewById(R.id.txt_point_descript);
		textUnreadNotiNum = (TextView) view.findViewById(R.id.txt_noti_unread_number);

		getUnreadNoti();
		getUserHistory();
		getUserInfo();

		userFollower.setOnClickListener(this);
		userFollowing.setOnClickListener(this);
		notiIcon.setOnClickListener(this);
		settingIcon.setOnClickListener(this);


	}

	@Override
	public void onResume() {
		super.onResume();
		if (!firstLoad && AppManager.getInstance().isRefresh()) {
			AppManager.getInstance().set5TabRefresh(false);
			((MealthyActivity) getActivity()).setupTabFifthFragment(0);
		}
	}

	public void getUserHistory() {
		RequestHandler request = new RequestHandler(getActivity());
		JSONObject body = new JSONObject();
		try {
			body.put("kind", 2);
			body.put("from", term);
			body.put("to", term);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_GET_USER_HISTORY, getUserId()), Request.Method.GET, null, body);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray histories = data.getJSONArray("user_histories");
						if (histories.length() > 0) {
							value = histories.getJSONObject(0).getString("value");
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void getUserInfo() {
		RequestHandler request = new RequestHandler(getActivity());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", getUserId()), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressDialog.dismiss();
				if (isSuccess) {
					try {
						User user = mGson.fromJson(data.getJSONObject("user").toString(), User.class);
						RealmHelper.with(getActivity()).save(user);
						setupView(user);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					User user = RealmHelper.with(getActivity()).getRealm().where(User.class).findFirst();
					setupView(user);
				}

			}
		});
	}

	private int getUserId() {
		return AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
	}

	private void setupView(User user) {
		if (null != user) {
			firstLoad = false;
			Glide.with(getActivity()).load(user.getProfileImageUrl()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(getActivity())).into(userProfile);
			imageBorder.setVisibility(null != user.getProfileImageUrl() && !user.getProfileImageUrl().trim().isEmpty() ? View.VISIBLE : View.GONE);
			userName.setText(user.getName());
			userFollower.setText(user.getFollowersCount() + getString(R.string.follower));
			userFollowing.setText(user.getFollowingAccount() + getString(R.string.following));
			userDietType.setText(null != user.getDietType() ? user.getDietType() : "");
			point.setText(user.getPoint() + "");
			pointDescript.setText(String.format(Locale.ENGLISH, "あと%d回\n栄養診断可能", Integer.valueOf(user.getPoint() / 100)));


		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.user_follower:
				UsersFollowersActivity.launch(getActivity(), getUserId());
				break;
			case R.id.user_following:
				UsersFollowingActivity.launch(getActivity(), getUserId());
				break;
			case R.id.noti_icon:
				NotificationActivity.launch(getActivity());
				break;
			case R.id.setting_icon:
				SettingInProfileActivity.launch(getActivity());
				break;
		}
	}

	private void getUnreadNoti() {
		RequestHandler request = new RequestHandler(getActivity());
		request.jsonRequest(Constant.REQUEST_GET_USER_NOTIFICATION_UNREAD, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						int num = data.getJSONObject("user_notifications").getInt("total_unread_count");
						textUnreadNotiNum.setText(num > 0 ? String.valueOf(num) : "");
						textUnreadNotiNum.setVisibility(num > 0 ? View.VISIBLE : View.INVISIBLE);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void requestGetUserFeed() {
		RequestHandler request = new RequestHandler(getApplicationContext());
		int userId = AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
		request.jsonRequest(Utils.convertFunctionName(Constant.REQUEST_GET_USER_FEED, userId), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray array = data.getJSONArray("feeds");
						JSONObject jObj = data.getJSONObject("paging");
						String nextLink = jObj.getString("next");
						boolean hasMore = jObj.getBoolean("has_more");
						Log.d("JSONOBj", array.length() + ">>" + nextLink + ">>" + hasMore);
						JSONObject obj;
						List<FeedItem> list = new ArrayList<FeedItem>();
						FeedItem item;
						FeedItem.Menu menu;
						FeedItem.FeedItemUser user;
						FeedItem.Comment cmt;
						JSONObject userObj;
						JSONObject menuObj;
						JSONArray cmtArray;
						List<FeedItem.Comment> cmtList;
						for (int i = 0; i < array.length(); i++) {
							obj = new JSONObject(array.get(i).toString());
							Log.d("JSONOBj", obj.getString("id"));
							item = new FeedItem();
							menu = new FeedItem.Menu();
							user = new FeedItem.FeedItemUser();
							cmtList = new ArrayList<FeedItem.Comment>();
							item.setId(obj.getInt("id"));
							item.setLike(obj.getBoolean("is_like"));
							item.setLikesCount(obj.getInt("likes_count"));
							menuObj = obj.getJSONObject("menu");
							menu.setId(menuObj.getInt("id"));
							menu.setCgmMenuName(menuObj.getString("cgm_menu_name"));
							menu.setCgmShopName(menuObj.getString("cgm_shop_name"));
							menu.setImage(menuObj.getString("image"));
							menu.setImageThumb(menuObj.getString("thumbnail_image_url"));
							menu.setCreatedAt(menuObj.getString("created_at"));
							menu.setCommentCount(menuObj.getInt("comments_count"));
							cmtArray = menuObj.getJSONArray("comments");
							FeedItem.FeedItemUser userCmt;
							for (int j = 0; j < cmtArray.length(); j++) {
								cmt = new FeedItem.Comment();
								JSONObject cmtObj = new JSONObject(cmtArray.getJSONObject(j).toString());
								cmt.setId(cmtObj.getInt("id"));
								cmt.setBody(cmtObj.getString("body"));
								userCmt = new FeedItem.FeedItemUser();
								userCmt.setId(cmtObj.getJSONObject("user").getInt("id"));
								userCmt.setName(cmtObj.getJSONObject("user").getString("name"));
								userCmt.setProfileImage(cmtObj.getJSONObject("user").getString("profile_image_url"));
								cmt.setCmtUser(userCmt);
								cmtList.add(cmt);
							}
							menu.setCommentList(cmtList);
							item.setMenu(menu);
							item.setLikesCount(obj.getInt("likes_count"));
							userObj = obj.getJSONObject("user");
							user.setId(userObj.getInt("id"));
							user.setName(userObj.getString("name"));
							user.setProfileImage(userObj.getString("profile_image_url"));
							item.setFeedItemUser(user);
							list.add(item);
						}
						adapter.addFeedItemList(list);
						adapter.notifyDataSetChanged();
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		});
	}
}
