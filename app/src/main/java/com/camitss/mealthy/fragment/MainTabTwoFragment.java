
package com.camitss.mealthy.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.Target;
import com.camitss.mealthy.MapsActivity;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.TwitterLoginProcess;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.activity.UserRecommendationDisplayActivity;
import com.camitss.mealthy.adapter.recyclerview.SocialPostItemAdapter;
import com.camitss.mealthy.adapter.recyclerview.listener.HidingScrollListener;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;
import com.camitss.mealthy.utils.Utils;
import com.camitss.mealthy.utils.twitter.TwitterOnShare;
import com.camitss.mealthy.utils.twitter.TwitterParams;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.camitss.mealthy.utils.twitter.TwitterParams.TW_REQUEST_CODE_ON_SHARE;

/**
 * Created by Viseth on 4/3/2017.
 */

public class MainTabTwoFragment extends Fragment implements View.OnClickListener {
	private RecyclerView rclSocialPostItem;
	private SocialPostItemAdapter postAdapter;
	private Toolbar toolbar;
	private ProgressDialog progressDialog;
	private Gson mGson = new Gson();
	private String nextLink = "";
	private boolean fristLoad = true;
	private boolean isLoadMore;
	private LinearLayout container;
	private BottomSheetDialog dialogBtmSheet;
	private android.app.AlertDialog twitterDialog;
	private int clickedItemPosition;
	private TextView textLocation;
	private FrameLayout framelayoutHeader;

	public int getClickedItemPosition() {
		return clickedItemPosition;
	}

	public void setClickedItemPosition(int clickedItemPosition) {
		this.clickedItemPosition = clickedItemPosition;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentTag.getInstant().setTAG("MAIN_TAB_TWO");
		return inflater.inflate(R.layout.tab_two_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
		Utils.showProgressDialog(getContext(), progressDialog);
		getUserRecommendRequest();
		getFeedRequest(Constant.REQUEST_GET_FEED);
		toolbar = (Toolbar) view.findViewById(R.id.toolbar);
		framelayoutHeader = (FrameLayout)view.findViewById(R.id.framelayout_header);
		container = (LinearLayout) view.findViewById(R.id.toolbar_container_tabtwo);
		try {
			if (null != RealmHelper.with(getContext()).getRealm().where(User.class).findFirst()) {
				User user = RealmHelper.with(getContext()).getRealm().where(User.class).findFirst();
				((TextView) view.findViewById(R.id.user_diet_type)).setText("タイムライン");//null != user.getDietType() && !user.getDietType().equalsIgnoreCase("null") ? user.getDietType() : "ヘルシーダイエッター");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		ImageView imageUserRecom = (ImageView) view.findViewById(R.id.img_user_recommend);

		rclSocialPostItem = (RecyclerView) view.findViewById(R.id.rcl_social_post_item);
		LinearLayoutManager ll = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
		rclSocialPostItem.setLayoutManager(ll);
		postAdapter = new SocialPostItemAdapter(getActivity());
		rclSocialPostItem.setAdapter(postAdapter);
		rclSocialPostItem.addOnScrollListener(new HidingScrollListener() {
			@Override
			public void onHide() {
				hideToolbar();
			}

			@Override
			public void onShow() {
				showToolbar();
			}

			@Override
			public void onLoadMore() {
				if (!isLoadMore && !nextLink.trim().isEmpty() && !nextLink.trim().equalsIgnoreCase("null")) {
					Log.d("bo", "onLoadmore");
					isLoadMore = true;
					nextLink = Constant.getBaseUrl() + nextLink;
					getFeedRequest(nextLink);
				}
			}
		});

		postAdapter.setOnItemClickListener(new SocialPostItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int pos) {
				int j = 1;
				if (postAdapter.getListUserRecom().size() > 0) {
					j = pos > 2 ? 2 : 1;
				}
				pos = pos - j;
//				int j = pos > 2 ? 2 : 1;
//				pos = pos - j;
				Intent i;
				switch (v.getId()) {
					case R.id.image_post:
						Utils.with(getContext()).displayImageItem(getActivity(), (ImageView) v, postAdapter.getListPost().get(pos).getMenu().getImage());
						break;
					case R.id.img_heart:
						int method = Request.Method.POST;
						if (postAdapter.getListPost().get(pos).isLike())
							method = Request.Method.DELETE;
						requestFeedLike(pos, postAdapter.getListPost().get(pos).getId(), method, (ImageView) v, (TextView) v.getRootView().findViewById(R.id.txt_favorite));
						break;
					case R.id.img_cmt:
						i = new Intent(getActivity(), FeedDisplayActivity.class);
						i.putExtra("FeedItem", postAdapter.getListPost().get(pos));
						startActivityForResult(i, 250);
						break;
					case R.id.text_more:
						i = new Intent(getActivity(), UserRecommendationDisplayActivity.class);
						i.putExtra("isFromSuggestion", true);
						startActivityForResult(i, 251);
						break;
					case R.id.user_profile:
					case R.id.user_name:
						int userId = postAdapter.getListPost().get(pos).getFeedItemUser().getId();
						int myId = AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
						if (myId == userId) {
							getActivity().getFragmentManager().beginTransaction().replace(R.id.main_container, new UserProfileFragment(), "USER_PROFILE").addToBackStack("MAIN_TAB_TWO").commit();
						} else {
							i = new Intent(getActivity(), UserRecomProfileDisplayActivity.class);
							i.putExtra("userId", userId);
							startActivityForResult(i, 250);
						}

						break;
					case R.id.img_share:
						setClickedItemPosition(pos);
						displayShareDialog();
						break;
				}
			}
		});
		imageUserRecom.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UserRecommendationDisplayActivity.launch(getContext());
			}
		});
		fristLoad = false;
	}

	private void hideToolbar() {
		container.animate().translationY(-framelayoutHeader.getHeight()).setInterpolator(new AccelerateInterpolator(3));

	}

	private void showToolbar() {
		container.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
	}

	private void getUserRecommendRequest() {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest(Constant.REQUEST_USERS_RECOMMEND, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray userArr = data.getJSONArray("users");
						UserRecomItem item;
						List<UserRecomItem> userRecomItemList = new ArrayList<UserRecomItem>();
						int length = userArr.length() < 5 ? userArr.length() : 5;
						for (int i = 0; i < length; i++) {
							item = mGson.fromJson(userArr.getJSONObject(i).toString(), UserRecomItem.class);
							userRecomItemList.add(item);
						}
						postAdapter.addUserRecommendList(userRecomItemList);
						postAdapter.notifyDataSetChanged();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void getFeedRequest(String url) {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest(url, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressDialog.dismiss();
				if (isSuccess) {
					try {
						JSONArray array = data.getJSONArray("feeds");
						JSONObject jObj = data.getJSONObject("paging");
						nextLink = jObj.getString("next");
						boolean hasMore = jObj.getBoolean("has_more");
						Log.d("JSONOBj", array.length() + ">>" + nextLink + ">>" + hasMore);
						JSONObject obj;
						List<FeedItem> list = new ArrayList<FeedItem>();
						FeedItem item;
						FeedItem.Menu menu;
						FeedItem.FeedItemUser user;
						FeedItem.Comment cmt;
						JSONObject userObj;
						JSONObject menuObj;
						JSONArray cmtArray;
						List<FeedItem.Comment> cmtList;
						for (int i = 0; i < array.length(); i++) {
							obj = new JSONObject(array.get(i).toString());
							Log.d("JSONOBj", obj.getString("id"));
							item = new FeedItem();
							menu = new FeedItem.Menu();
							user = new FeedItem.FeedItemUser();
							cmtList = new ArrayList<FeedItem.Comment>();
							item.setId(obj.getInt("id"));
							item.setLike(obj.getBoolean("is_like"));
							item.setLikesCount(obj.getInt("likes_count"));
							item.setPrivacyCount(obj.getInt("privacy_kind"));
							menuObj = obj.getJSONObject("menu");
							menu.setId(menuObj.getInt("id"));
							menu.setCgmMenuName(menuObj.getString("cgm_menu_name"));
							menu.setCgmShopName(menuObj.getString("cgm_shop_name"));
							menu.setImage(menuObj.getString("image"));
							menu.setImageThumb(menuObj.getString("thumbnail_image_url"));
							menu.setCreatedAt(menuObj.getString("created_at"));
							menu.setCommentCount(menuObj.getInt("comments_count"));
							menu.setShopId(menuObj.getInt("shop_id"));
							cmtArray = menuObj.getJSONArray("comments");
							FeedItem.FeedItemUser userCmt;
							for (int j = 0; j < cmtArray.length(); j++) {
								cmt = new FeedItem.Comment();
								JSONObject cmtObj = new JSONObject(cmtArray.getJSONObject(j).toString());
								cmt.setId(cmtObj.getInt("id"));
								cmt.setBody(cmtObj.getString("body"));
								userCmt = new FeedItem.FeedItemUser();
								userCmt.setId(cmtObj.getJSONObject("user").getInt("id"));
								userCmt.setName(cmtObj.getJSONObject("user").getString("name"));
								userCmt.setProfileImage(cmtObj.getJSONObject("user").getString("profile_image_url"));
//								userCmt.setUserKind(cmtObj.getJSONObject("user").getInt("user_kind"));
								cmt.setCmtUser(userCmt);
								cmtList.add(cmt);
							}
							menu.setCommentList(cmtList);
							item.setMenu(menu);
							item.setLikesCount(obj.getInt("likes_count"));
							userObj = obj.getJSONObject("user");
							user.setId(userObj.getInt("id"));
							user.setName(userObj.getString("name"));
							user.setProfileImage(userObj.getString("profile_image_url"));
							user.setUserKind(userObj.getInt("user_kind"));
							item.setFeedItemUser(user);
							list.add(item);
						}
						postAdapter.addPostLists(list);
						postAdapter.notifyDataSetChanged();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					isLoadMore = false;
				} else {
					isLoadMore = false;
					CustomAlertBuilder.create(getActivity(), getString(R.string.error));
				}
			}
		});
	}

	private void requestFeedLike(final int pos, int id, final int method, final ImageView v, final TextView textView) {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest(Constant.REQUEST_GET_FEED + "/" + id + "/like", method, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					int count = postAdapter.getListPost().get(pos).getLikesCount();
					if (method == Request.Method.POST) {
						postAdapter.getListPost().get(pos).setLike(true);
						postAdapter.getListPost().get(pos).setLikesCount(count + 1);
					} else {
						postAdapter.getListPost().get(pos).setLike(false);
						postAdapter.getListPost().get(pos).setLikesCount(count - 1);
					}
					postAdapter.setFeedFavoriteAndTextCount(postAdapter.getListPost().get(pos).isLike(), v, textView, postAdapter.getListPost().get(pos).getLikesCount());
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 250) {
//			getFeedRequest(Constant.REQUEST_GET_FEED);
		} else if (requestCode == 251) {
			getUserRecommendRequest();
		} else if (requestCode == TW_REQUEST_CODE_ON_SHARE) {
			if (resultCode == getActivity().RESULT_OK) {
				displayTwitterPostShareDisplay();
			}
		} else if (requestCode == 252 && resultCode == getActivity().RESULT_OK) {
			textLocation.setText(data.getStringExtra("location"));
		}
	}

	private void displayTwitterPostShareDisplay() {
		final FeedItem item = postAdapter.getListPost().get(getClickedItemPosition());
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_twitter_post_share_display, null);
		twitterDialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme_softkey_resize)).setView(view).show();
		final EditText edtCmt = (EditText) view.findViewById(R.id.edt_twitter_cmt);
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);
				twitterDialog.dismiss();
			}
		});
		LinearLayout layoutLocation = (LinearLayout) view.findViewById(R.id.layout_location);
		textLocation = (TextView) view.findViewById(R.id.text_location);
		layoutLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(), MapsActivity.class), 252);
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);

			}
		});
		final TextView textPost = (TextView) view.findViewById(R.id.txt_post);
		textPost.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!edtCmt.getText().toString().trim().isEmpty()) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtCmt.getWindowToken(), 0);
					twitterDialog.dismiss();
					new TwitterOnShare(getActivity()).execute(edtCmt.getText().toString().trim(), item.getMenu().getImageThumb());

				}
			}
		});
		String menuName = null != item.getMenu().getCgmMenuName() && !item.getMenu().getCgmMenuName().equalsIgnoreCase("null") ? item.getMenu().getCgmMenuName() + " に決めました！" : "";
		edtCmt.setText(menuName + " #mealthy #diet #healthyfood https://appsto.re/jp/Jqpx4.i");
		ImageView icon = (ImageView) view.findViewById(R.id.img_post);
		Glide.with(getContext()).load(item.getMenu().getImage()).listener(new com.bumptech.glide.request.RequestListener<String, GlideDrawable>() {
			@Override
			public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
				return false;
			}

			@Override
			public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
				return false;
			}
		}).error(R.drawable.thum_14578).into(icon);//.override(w, h)

		final TextView txtLength = (TextView) view.findViewById(R.id.txt_twitter_length);
		txtLength.setText(String.valueOf(calculateTextLength(edtCmt.getText().toString().trim())));
		twitterDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		edtCmt.setFocusable(true);
		edtCmt.setFocusableInTouchMode(true);
		edtCmt.requestFocus();
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
		twitterDialog.setCancelable(false);
		edtCmt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				txtLength.setText(String.valueOf(calculateTextLength(s.toString().trim())));
			}
		});
	}

	private int calculateTextLength(String text) {
		if (!text.isEmpty()) {
			int i = 92 - (text.length());
			return i;
		} else {
			return 92;
		}
	}

	private void displayShareDialog() {
		dialogBtmSheet = new BottomSheetDialog(getActivity(), R.style.BottomSheetDialog);
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_f_change_image_profile_dialog, null);
//		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogBtmSheet.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.title)).setVisibility(View.GONE);
		TextView text = (TextView) view.findViewById(R.id.txt_two_option);
		text.setText("不適切なメニューの報告");
		text.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
		text.setOnClickListener(this);
		TextView txt = (TextView) view.findViewById(R.id.txt_one_option);
		txt.setText("Twitterでシェア");
		txt.setOnClickListener(this);
		dialogBtmSheet.setCancelable(false);
		dialogBtmSheet.show();
		dialogBtmSheet.setContentView(view);
	}

	public void shareToTwitter() {
		Intent i = new Intent(getActivity(), TwitterLoginProcess.class);
		i.putExtra("ContentLink", "oauth://com.camitss.mealthy.Twitter_oAuth");
		startActivityForResult(i, TwitterParams.TW_REQUEST_CODE_ON_SHARE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

			case R.id.txt_one_option:
				dialogBtmSheet.dismiss();
				shareToTwitter();
				break;
			case R.id.txt_two_option:
				dialogBtmSheet.dismiss();
				reportItemRequest();
				break;
		}
	}

	private void reportItemRequest() {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject params = new JSONObject();
		try {
			params.put("object", "Menu");
			params.put("oid", postAdapter.getListPost().get(getClickedItemPosition()).getMenu().getId());
			params.put("report_type", 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_REPORT_FEED_ITEM, Request.Method.POST, null, params);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					CustomAlertBuilder.create(getActivity(), getString(R.string.report_success_msg));
				} else {
					Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!fristLoad && AppManager.getInstance().isTab2Refresh()) {
			AppManager.getInstance().setTab2Refresh(false);
			AppManager.getInstance().getMealthyActivity().setupTabTwoFragment();
		}

	}
}
