package com.camitss.mealthy.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.BaseActivity;
import com.camitss.mealthy.activity.CropImage;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.activity.PostActivity;
import com.camitss.mealthy.adapter.GalleryAdapter;
import com.camitss.mealthy.adapter.GalleryMultiPickupAdapter;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.GalleryMultiPickup;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.request.EmptyBodyRequest;
import com.camitss.mealthy.request.PostMenuRequest;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GalleryMultiPickupFragment extends Fragment {

	List<GalleryMultiPickup> galleryMultiPickupList;
	TextView counterView, sendText;
	RelativeLayout bottomBar;
	int previousCounter = 0, numberRequest = 1;
	ProgressDialog progressDialog;
	Map<Long, GalleryMultiPickup> listFilePath;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("GALLERY");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_gallery_multipickup, container, false);
		RecyclerView photoView = (RecyclerView) homeView.findViewById(R.id.gallery_multi_pickup_view);
		counterView = (TextView) homeView.findViewById(R.id.selected_count);
		sendText = (TextView) homeView.findViewById(R.id.submit_post_multi_photo);
		bottomBar = (RelativeLayout) homeView.findViewById(R.id.hide_show_button_bar);
		bottomBar.setVisibility(View.INVISIBLE);
		galleryMultiPickupList = new ArrayList<>();
		listFilePath = new HashMap<>();
		final GalleryMultiPickupAdapter galleryAdapter = new GalleryMultiPickupAdapter(getActivity());
		GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
		mLayoutManager.setAutoMeasureEnabled(true);
		photoView.setLayoutManager(mLayoutManager);
		photoView.setItemAnimator(new DefaultItemAnimator());
		photoView.setAdapter(galleryAdapter);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 300);
			}
		}
		String externalStorageDirectoryPath = Environment
				.getExternalStorageDirectory()
				.getAbsolutePath();

		// get all image from folder DCIM
		fetchAllImage(externalStorageDirectoryPath + "/", galleryAdapter);//externalStorageDirectoryPath + "/", galleryAdapter);externalStorageDirectoryPath + "/DCIM"
		Map<Long, GalleryMultiPickup> map = new TreeMap<>(listFilePath);

		for (Map.Entry<Long, GalleryMultiPickup> entry : map.entrySet()) {
			galleryAdapter.add(entry.getValue());
		}

		galleryAdapter.setOnItemClickListener(new GalleryMultiPickupAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, GalleryMultiPickup galleryMultiPickup, boolean isAdded) {
				if (isAdded) {
					galleryMultiPickupList.add(galleryMultiPickup);
				} else {
					galleryMultiPickupList.remove(galleryMultiPickup);
				}
				counterView.setText(galleryMultiPickupList.size() + "");
				if (galleryMultiPickupList.size() == 0 && previousCounter == 1) {
					bottomBar.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down));
					bottomBar.setVisibility(View.INVISIBLE);
					previousCounter = 0;
				} else if (galleryMultiPickupList.size() == 1 && previousCounter == 0) {
					previousCounter = 1;
					bottomBar.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up));
					bottomBar.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onReachLimitItem() {
				showLimitItemDialog();
			}
		});

		sendText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
				Utils.showProgressDialog(getActivity(), progressDialog, getString(R.string.multi_select_photo_tab));
				int numberRequest = 1;
				for (int i = 0; i < galleryMultiPickupList.size(); i++) {
					File file = compressImage(galleryMultiPickupList.get(i).getPath());
					uploadImage(file, numberRequest);
					numberRequest++;
				}
			}
		});
		return homeView;
	}

	private void uploadImage(File file, final int numberRequest) {
		PostMenuRequest postMenuRequest = new PostMenuRequest(getActivity());
		postMenuRequest.addBody(Constant.REQUEST_POST, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				progressDialog.dismiss();
				MealthyLog.infoLog("error result", error + "");
			}
		}, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				Log.d("multi", response);
				if (numberRequest == galleryMultiPickupList.size()) {
					progressDialog.dismiss();
					JSONObject logsParam = null;
					try {
						logsParam = new JSONObject(response);
						logPost(logsParam);
					} catch (JSONException e) {
						e.printStackTrace();
					}
//					AppManager.getInstance().getMealthyActivity().setupTabFifthFragment(0);
				}
			}
		}, createParam(), file);//new File(galleryMultiPickupList.get(i).getPath())
	}

	public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		final float totalPixels = width * height;
		final float totalReqPixelsCap = reqWidth * reqHeight * 2;
		while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
			inSampleSize++;
		}

		return inSampleSize;
	}

	public File compressImage(String filePath) {

//		String filePath = getRealPathFromURI(imageUri);
		Bitmap scaledBitmap = null;

		BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
		options.inJustDecodeBounds = true;
		Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

		int actualHeight = options.outHeight;
		int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x816

		float maxHeight = 1024.0f;
		float maxWidth = 1024.0f;
		float imgRatio = actualWidth / actualHeight;
		float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

		if (actualHeight > maxHeight || actualWidth > maxWidth) {
			if (imgRatio < maxRatio) {
				imgRatio = maxHeight / actualHeight;
				actualWidth = (int) (imgRatio * actualWidth);
				actualHeight = (int) maxHeight;
			} else if (imgRatio > maxRatio) {
				imgRatio = maxWidth / actualWidth;
				actualHeight = (int) (imgRatio * actualHeight);
				actualWidth = (int) maxWidth;
			} else {
				actualHeight = (int) maxHeight;
				actualWidth = (int) maxWidth;

			}
		}

//      setting inSampleSize value allows to load a scaled down version of the original image

		options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
		options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inTempStorage = new byte[16 * 1024];

		try {
//          load the bitmap from its path
			bmp = BitmapFactory.decodeFile(filePath, options);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();

		}
		try {
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
		} catch (OutOfMemoryError exception) {
			exception.printStackTrace();
		}

		float ratioX = actualWidth / (float) options.outWidth;
		float ratioY = actualHeight / (float) options.outHeight;
		float middleX = actualWidth / 2.0f;
		float middleY = actualHeight / 2.0f;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
		ExifInterface exif;
		try {
			exif = new ExifInterface(filePath);

			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION, 0);
			Log.d("EXIF", "Exif: " + orientation);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 3) {
				matrix.postRotate(180);
				Log.d("EXIF", "Exif: " + orientation);
			} else if (orientation == 8) {
				matrix.postRotate(270);
				Log.d("EXIF", "Exif: " + orientation);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
					scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
					true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileOutputStream out = null;
//		String filename = (getActivity().getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "image_crop_tmp.jpg");//getFilename();
		File filename = new File(getActivity().getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "image_crop_tmp.jpg");

		try {
			out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return filename;

	}

	private File savebitmap(String filename) {
		File file = new File(filename);
		File f = new File(getActivity().getExternalFilesDir(null).getAbsolutePath(), Calendar.getInstance().getTimeInMillis() + "image_crop_tmp.jpg");
		try {

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inScaled = false;
			FileOutputStream outStream = new FileOutputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
			Bitmap resized = Bitmap.createScaledBitmap(bitmap, 1024, 1024, true);

			resized.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.e("file", "" + f);
		return f;
	}

	public Bitmap getBitmap(String path) {
		try {
			Bitmap bitmap = null;
			File f = new File(path);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inScaled = false;

			bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private JSONObject createParam() {
		JSONObject param = new JSONObject();
		//params: [user_id: , menu_name: “”, privacy_kind: 1, exif_date: yyy-MM-dd HH:mm:ss]
		try {
			param.put("user_id", ((BaseActivity) getActivity()).getUserId());
			param.put("menu_name", "");
			param.put("privacy_kind", 1);
			param.put("exif_date", Utils.with(getActivity()).beautyCurrentDateFormat());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return param;
	}

	private void showLimitItemDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(getResources().getString(R.string.limitation_reach))
				.setPositiveButton("Close", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		builder.create();
		builder.show();

	}

	private void logPost(JSONObject data) {
		JSONObject param = new JSONObject();
		String token = TokenRealmHelper.with(getActivity()).getRealm().where(TokenObject.class).findFirst().getToken();
		try {
			//EVT_POSTPHOTO = 3008
			param.put("event_id", 3008);
			param.put("created_at", data.getJSONObject("menu").getString("created_at"));
			param.put("device_id", token);
			param.put("content", "");
			param.put("latitude", ((BaseActivity) getActivity()).getLatitude());
			param.put("longitude", ((BaseActivity) getActivity()).getLongitude());
			param.put("user_id", ((BaseActivity) getActivity()).getUserId());
			param.put("shop_id", data.getJSONObject("menu").getInt("shop_id"));
			param.put("menu_id", data.getJSONObject("menu").getInt("id"));

			EmptyBodyRequest emptyBodyRequest = new EmptyBodyRequest(Constant.LOG_POST_EVENT);
			emptyBodyRequest.execute(param);
			progressDialog.dismiss();
			getActivity().finish();
			AppManager.getInstance().getMealthyActivity().dashboradHandler(true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void fetchAllImage(String dirPath, GalleryMultiPickupAdapter galleryAdapter) {

		File targetDirector = new File(dirPath);
		File[] files = targetDirector.listFiles();

		for (File file : files) {
			if (file.isFile()) {
				if (isImage(file)) {
					if (file.getName().endsWith(".png")
							|| file.getName().endsWith(".jpg")
							|| file.getName().endsWith(".jpeg")
							) {
						if (!file.getAbsolutePath().contains(".thumbnails")) {
							GalleryMultiPickup galleryMultiPickup = new GalleryMultiPickup();
							galleryMultiPickup.setPath(file.getAbsolutePath());
							listFilePath.put(file.lastModified() * (-1), galleryMultiPickup);
							//galleryAdapter.add(galleryMultiPickup);
						}
					}
				}
			} else {
				fetchAllImage(file.getAbsolutePath(), galleryAdapter);
			}
		}
	}

	private boolean isImage(File file) {
		String type = "mimetype";
		String extension = MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath());
		if (extension != null) {
			type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
			if (type != null && type.contains("image")) {
				return true;
			}
		}
		return false;
	}

}