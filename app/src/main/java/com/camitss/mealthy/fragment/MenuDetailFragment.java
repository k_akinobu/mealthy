package com.camitss.mealthy.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.BaseActivity;
import com.camitss.mealthy.activity.CommentDetailActivity;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.MealthyMenuComment;
import com.camitss.mealthy.object.MenuTag;
import com.camitss.mealthy.object.NearbyObj;
import com.camitss.mealthy.object.NutritionistUser;
import com.camitss.mealthy.object.Shop;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.EmptyBodyRequest;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.RealmList;

/**
 * Created by Viseth on 5/23/2017.
 */

public class MenuDetailFragment extends Fragment implements View.OnClickListener {
	private Gson mGson = new Gson();
	private ProgressDialog progressDialog;
	private TextView title;
	private TextView foodKcal;
	private TextView shopName;
	private TextView menuName;
	private RecyclerView layoutTags;
	private ImageView imagePost;
	private TextView txtPrice;
	private TextView txtShop;
	private DecimalFormat df = new DecimalFormat("#.#", new DecimalFormatSymbols(Locale.US));
	private GridTagsAdapter adapter;
	private TextView txtFlag;
	private TextView txtCrown;
	private LinearLayout layoutCrown;
	private FrameLayout layoutImgBadge;
	private ImageView imgStar, favStar;
	private TextView txtShoplbl;
	private TextView txtStationlbl;
	private TextView txtTel, mapLink;
	private android.support.v7.app.AlertDialog dialogEmoji;
	private String[] emojiCodes = {getEmojiByUnicode(0x1F44D), getEmojiByUnicode(0x1F44C), getEmojiByUnicode(0x2757), getEmojiByUnicode(0x1F604)};
	private int menuId;
	private int shopId;
	private String createdAt;
	private LinearLayout layoutCmt;
	private List<MealthyMenuComment> cmtList = new ArrayList<>();
	private AlertDialog cmtDialog;
	private JSONObject header = new JSONObject();
	private MealthyMenu menuObj;
	private Shop shop;
	private Toolbar toolbar;
	private NearestShopFragment shopFragment;
	private MenuShopFragment menuShopFragment;

	public String getEmojiByUnicode(int unicode) {
		return new String(Character.toChars(unicode));
	}

	public MealthyMenu getMenuObj() {
		return menuObj;
	}

	public void setMenuObj(MealthyMenu menuObj) {
		this.menuObj = menuObj;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public NearestShopFragment getShopFragment() {
		return shopFragment;
	}

	public void setShopFragment(NearestShopFragment shopFragment) {
		this.shopFragment = shopFragment;
	}

	public MenuShopFragment getMenuShopFragment() {
		return menuShopFragment;
	}

	public void setMenuShopFragment(MenuShopFragment menuShopFragment) {
		this.menuShopFragment = menuShopFragment;
	}

	public MenuDetailFragment(MealthyMenu menuObj, Shop shop, NearestShopFragment shopFragment) {
		this.menuObj = menuObj;
		this.shop = shop;
		this.shopFragment = shopFragment;
	}

	public MenuDetailFragment(MealthyMenu menuObj, Shop shop, MenuShopFragment menuShopFragment, NearestShopFragment shopFragment) {
		this.menuObj = menuObj;
		this.shop = shop;
		this.menuShopFragment = menuShopFragment;
		this.shopFragment = shopFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("MAIN_TAB_FOURTH");//MenuDetail
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_menu_datail, container, false);
		toolbar = (Toolbar) view.findViewById(R.id.toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.home);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();

			}
		});
		MenuItem item = toolbar.getMenu().getItem(0);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				MealthyLog.infoLog("action", "search");
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				SearchHomeFragment searchHomeFragment = new SearchHomeFragment();
				fragmentTransaction.hide(MenuDetailFragment.this);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.add(R.id.main_container, searchHomeFragment, "SEARCH_HOME").addToBackStack("NEAR");
				fragmentTransaction.commit();
				return false;
			}
		});
		progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatProgressDialogStyle);

		try {
			header.put("X-Mealthy-Token", "1C785FE8-6108-44C3-8663-361A4E37E899");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		title = (TextView) view.findViewById(R.id.title);
		foodKcal = (TextView) view.findViewById(R.id.food_kcal);
		menuName = (TextView) view.findViewById(R.id.menu_name);
		shopName = (TextView) view.findViewById(R.id.shop_name);
		imagePost = (ImageView) view.findViewById(R.id.image_post);
		txtPrice = (TextView) view.findViewById(R.id.txt_price);
		txtFlag = (TextView) view.findViewById(R.id.txt_flag);
		mapLink = (TextView) view.findViewById(R.id.map_link);
		txtCrown = (TextView) view.findViewById(R.id.txt_crown);
		layoutCrown = (LinearLayout) view.findViewById(R.id.layout_crown);
		layoutTags = (RecyclerView) view.findViewById(R.id.layout_tags);
		layoutImgBadge = (FrameLayout) view.findViewById(R.id.img_badge);
		imgStar = (ImageView) view.findViewById(R.id.img_star);
		txtShoplbl = (TextView) view.findViewById(R.id.txt_shoplbl);
		txtStationlbl = (TextView) view.findViewById(R.id.txt_stationlbl);
		txtTel = (TextView) view.findViewById(R.id.txt_tel);
		layoutCmt = (LinearLayout) view.findViewById(R.id.layout_cmt);
		favStar = (ImageView) view.findViewById(R.id.img_favorite);
		favStar.setOnClickListener(this);
		view.findViewById(R.id.img_cmt).setOnClickListener(this);
		view.findViewById(R.id.img_share).setOnClickListener(this);
		GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
		layoutTags.setLayoutManager(layoutManager);
		adapter = new GridTagsAdapter();
		layoutTags.setAdapter(adapter);
//		getMenudetailRequest();
		setupView(getShop());
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	private void getMenudetailRequest() {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest("https://api.mealthy.me/shops?latitude=35.690551&longitude=139.69257", Request.Method.GET, header, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					Type listType = new TypeToken<ArrayList<Shop>>() {
					}.getType();
					try {
						List<Shop> shopsList = new Gson().fromJson(data.getJSONArray("shops").toString(), listType);
						Shop shop = shopsList.get(0);
						setupView(shop);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void setupView(final Shop shop) {
		if (null != shop) {
			MealthyMenu menu = getMenuObj();
			title.setText(menu.getName());
			foodKcal.setText(df.format(menu.getCalorie()) + "\nkcal");
			menuName.setText(menu.getName());
			shopName.setText("P\t" + (menu.getProtein() <= 0 ? "不明" : df.format(menu.getProtein())) + "g\t\t\tF\t" + (menu.getLipid() <= 0 ? "不明" : df.format(menu.getLipid())) + "g\t\t\tC\t" + (menu.getCarbohydrate() <= 0 ? "不明" : df.format(menu.getCarbohydrate())) + "g");
			txtPrice.setText(df.format(menu.getPrice()) + "円");
//			txtShop.setText(shop.getStation().getName());
			Glide.with(getContext()).load(menu.getImage()).placeholder(R.drawable.bg_grey).into(imagePost);
			adapter.addTagList(menu.getTags());
			adapter.notifyDataSetChanged();

			txtFlag.setText(getFlag(menu.getTakeout_flag()));
			txtCrown.setText(df.format(menu.getSocial_score()));
			layoutCrown.setVisibility(menu.getSocial_score() <= 0 ? View.GONE : View.VISIBLE);
			layoutImgBadge.setBackground(ContextCompat.getDrawable(getContext(), getBadgeDrawable(menu.getOrigin())));
			imgStar.setImageDrawable(ContextCompat.getDrawable(getContext(), getStarDrawable((int) menu.getScore())));
			if (null != shop.getTel()) {
				txtShoplbl.setText(shop.getName());
				txtTel.setText(shop.getTel());
				txtTel.setPaintFlags(txtTel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
				txtTel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Intent.ACTION_CALL);
						intent.setData(Uri.parse("tel:" + shop.getTel() + ""));
						if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
							ActivityCompat.requestPermissions(getActivity(),
									new String[]{Manifest.permission.CALL_PHONE},
									120);
							return;
						}
						getActivity().startActivity(intent);
					}
				});
			}
			if (null != shop.getStation()) txtStationlbl.setText(shop.getStation().getName());
			setShopId(shop.getId());
			setMenuId(menu.getId());
			setCreatedAt(shop.getCreated_at());
			setCmtList(menu.getComments());
			Log.d("tesh", menu.getComments().size() + ";");
			createCmtView(menu.getComments(), layoutCmt);
			mapLink.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					NearbyObj obj = new NearbyObj();
					obj.setShopName(getMenuObj().getShopName());
					obj.setLatitude(getMenuObj().getShopLat());
					obj.setLongitude(getMenuObj().getShopLng());

					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					MapsFragment searchHomeFragment = new MapsFragment(obj);
					fragmentTransaction.hide(MenuDetailFragment.this);
					fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
					fragmentTransaction.add(R.id.activity_mealthy, searchHomeFragment, "SEARCH_HOME").addToBackStack("NEAR");
					fragmentTransaction.commit();
				}
			});

			MealthyMenu tmpMenu = RealmHelper.with(getActivity()).getMealthyMenuFavById(getMenuId());
			if (tmpMenu != null) {
				if (tmpMenu.isFavorith()) {
					favStar.setSelected(true);
					getMenuObj().setFavId(tmpMenu.getFavId());
				}
			}
		}
	}

	private int getStarDrawable(int score) {
		if (score >= 40) {
			return R.drawable.star_1;
		} else if (score >= 60) {
			return R.drawable.star_2;
		} else if (score >= 80) {
			return R.drawable.star_3;
		} else return R.drawable.star_0;

	}

	private int getBadgeDrawable(double origin) {
		switch ((int) origin) {
			case 1:
				return R.drawable.img_badge01;
			case 2:
				return R.drawable.img_badge03;
			case 3:
				return R.drawable.img_badge02;
			default:
				return R.drawable.img_badge03;
		}
	}

	private String getFlag(String flag) {
		switch (flag) {
			case "1":
				return "外食";
			case "2":
				return "テイクアウト";
			case "3":
				return "コンビニ";
			case "1|2":
				return "外食・テイクアウト";
			default:
				String[] flags = flag.split("\\|", 2);
				return getFlag(flags[0]) + "・" + getFlag(flags[1]);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_cmt:
				displayCmtDialog();
				break;
			case R.id.img_share:
				displayShareDialog();
				break;
			case R.id.img_favorite:
				favoriteEventClick((ImageView) v);
				break;
			case R.id.txt_one_option:
				break;
			case R.id.txt_two_option:
				break;
			case R.id.img_icon:
				displayEmoji();
				break;
			case R.id.txt_one_emoji:
				onClickEmoji(emojiCodes[0]);
				break;
			case R.id.txt_two_emoji:
				onClickEmoji(emojiCodes[1]);
				break;
			case R.id.txt_three_emoji:
				onClickEmoji(emojiCodes[2]);
				break;
			case R.id.txt_four_emoji:
				onClickEmoji(emojiCodes[3]);
				break;
		}
	}

	private void displayShareDialog() {
		final BottomSheetDialog dialog = new BottomSheetDialog(getActivity(), R.style.BottomSheetDialog);
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_f_change_image_profile_dialog, null);
//		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.title)).setVisibility(View.GONE);
		TextView text = (TextView) view.findViewById(R.id.txt_two_option);
		text.setText("不適切なメニューの報告");
		text.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
		text.setOnClickListener(this);
		TextView txt = (TextView) view.findViewById(R.id.txt_one_option);
		txt.setText("Twitterでシェア");
		txt.setOnClickListener(this);
		dialog.setCancelable(false);
		dialog.show();
		dialog.setContentView(view);
	}

	private void favoriteEventClick(ImageView view) {
//		Drawable drawable = isLiked ? ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_on) : ContextCompat.getDrawable(getContext(), R.drawable.ic_favorite_off);
//		view.setImageDrawable(drawable);
//		AppSharedPreferences.getConstant(getContext()).setFavoriteMenuPref(!isLiked);
		try {
			if (view.isSelected()) {
				requetsDelFav(getMenuObj(), view);

			} else {
				requetsFav(getMenuObj(), view);

			}

//			if (getShopFragment() != null) {
//				getShopFragment().restartData();
//			} else {
//				getMenuShopFragment().restartData();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void displayCmtDialog() {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_menu_cmt, null);
		cmtDialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme_softkey_resize)).setView(view).show();
//		dialog.getWindow().setLayout(Utils.getScreenWidth(getActivity()), (Utils.getScreenHeight(getActivity()) - 80));//setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((Utils.getScreenWidth(getActivity()) - 100), ((Utils.getScreenHeight(getActivity()) - 220) - (Utils.getActionBarHeight(getActivity()) * 2)));
		Log.d("tesh", ((Utils.getScreenHeight(getActivity()) - 220) - (Utils.getActionBarHeight(getActivity()) * 2)) + ">>" + Utils.getActionBarHeight(getActivity()) + ">>" + cmtDialog.getWindow().getDecorView().getHeight() + ">>" + Utils.getScreenHeight(getActivity()));
		final EditText edtCmt = (EditText) view.findViewById(R.id.edt_cmt);
		params.setMargins(20, 40, 0, 20);
		edtCmt.setLayoutParams(params);
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cmtDialog.dismiss();
			}
		});
		final TextView textOk = (TextView) view.findViewById(R.id.txt_ok);
		textOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!edtCmt.getText().toString().trim().isEmpty()) {
					cmtDialog.dismiss();
					addMenuCmtRequest(edtCmt.getText().toString().trim(), getMenuId());
				}
			}
		});
		ImageView icon = (ImageView) view.findViewById(R.id.img_icon);
		icon.setOnClickListener(this);
		final TextView txtLength = (TextView) view.findViewById(R.id.edt_length);
		cmtDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		cmtDialog.setCancelable(false);
		edtCmt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!s.toString().trim().isEmpty()) {
					textOk.setTextColor(ContextCompat.getColor(getContext(), R.color.orange));
					int i = 250 - (s.toString().length());
					txtLength.setText(String.valueOf(i));
				} else {
					txtLength.setText("250");
					textOk.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_light));
				}
			}
		});
	}

	private void onClickEmoji(String comment) {
		dialogEmoji.dismiss();
		cmtDialog.dismiss();
		addMenuCmtRequest(comment, getMenuId());
	}

	private void displayEmoji() {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_emoji_dialog, null);
		dialogEmoji = new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme_default)).setView(view).show();
		dialogEmoji.setView(view);
		TextView textOne = (TextView) view.findViewById(R.id.txt_one_emoji);
		TextView textTwo = (TextView) view.findViewById(R.id.txt_two_emoji);
		TextView textThree = (TextView) view.findViewById(R.id.txt_three_emoji);
		TextView textFour = (TextView) view.findViewById(R.id.txt_four_emoji);
		textOne.setText(emojiCodes[0]);
		textTwo.setText(emojiCodes[1]);
		textThree.setText(emojiCodes[2]);
		textFour.setText(emojiCodes[3]);
		textOne.setOnClickListener(this);
		textTwo.setOnClickListener(this);
		textThree.setOnClickListener(this);
		textFour.setOnClickListener(this);
		ImageView imgClose = (ImageView) view.findViewById(R.id.img_close);
		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogEmoji.dismiss();
			}
		});
		dialogEmoji.setCancelable(false);
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public int getShopId() {
		return shopId;
	}

	public int getMenuId() {
		return menuId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public List<MealthyMenuComment> getCmtList() {
		return cmtList;
	}

	public void setCmtList(List<MealthyMenuComment> cmtList) {
		this.cmtList = cmtList;
	}

	public class GridTagsAdapter extends RecyclerView.Adapter<GridTagsAdapter.TagsHolder> {

		private List<MenuTag> tagList = new ArrayList<>();

		public List<MenuTag> getTagList() {
			return tagList;
		}

		public void addTagList(List<MenuTag> tagList) {
			this.tagList = tagList;
		}

		@Override
		public TagsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			return new TagsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_comment_layout, parent, false));
		}

		@Override
		public void onBindViewHolder(TagsHolder holder, int position) {
			holder.txtTag.setText("#" + getTagList().get(position).getName());//+ getTagList().get(position).getName()
		}

		@Override
		public int getItemCount() {
			return getTagList().size();
		}

		class TagsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

			private TextView txtTag;

			public TagsHolder(View itemView) {
				super(itemView);

				txtTag = (TextView) itemView.findViewById(R.id.txt_tag);
				txtTag.setOnClickListener(this);
			}

			@Override
			public void onClick(View v) {
				Toast.makeText(getContext(), ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
			}
		}

	}

	private void addMenuCmtRequest(final String comment, final int id) {
		Utils.showProgressDialog(getActivity(), progressDialog);
		RequestHandler request = new RequestHandler(getContext());
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("comment", comment);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_MENU_CMT + id + "/comment", Request.Method.POST, header, jobj);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressDialog.dismiss();
				Log.d("tesh", data.toString());
				if (isSuccess) {

					try {
						MealthyMenu menu = new Gson().fromJson(data.getJSONObject("menu").toString(), MealthyMenu.class);
						List<MealthyMenuComment> cmtl = menu.getComments();
						User user = RealmHelper.with(getContext()).getRealm().where(User.class).findFirst();
						MealthyMenuComment cmt = new MealthyMenuComment();
						cmt.setId(168);
						cmt.setBody(comment);
						NutritionistUser userCmt = new NutritionistUser();
						userCmt.setNuser_name(user.getName());
						userCmt.setNuser_profile_image(user.getProfileImageUrl());
						userCmt.setNuser_id(31274);//((BaseActivity)getActivity()).getUserId());
//						userCmt.setNuser_id(((BaseActivity) getActivity()).getUserId());
						cmt.setUser(userCmt);
//						Collections.reverse(cmtl);
						cmtl.add(cmt);
						getCmtList().clear();
						getCmtList().addAll(cmtl);
						createCmtView(getCmtList(), layoutCmt);
						logPost(comment);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void logPost(String comment) {
		try {
			EmptyBodyRequest emptyBodyRequest = new EmptyBodyRequest(Constant.LOG_POST_EVENT);
			emptyBodyRequest.execute(createLogPostJSONObjParam(comment));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JSONObject createLogPostJSONObjParam(String comment) {
		JSONObject logPostJSONObjParam = new JSONObject();
		String token = TokenRealmHelper.with(getActivity()).getRealm().where(TokenObject.class).findFirst().getToken();
		try {
			//EVT_POSTPHOTO = 3008
			logPostJSONObjParam.put("event_id", 3001);
			logPostJSONObjParam.put("created_at", getCreatedAt());
			logPostJSONObjParam.put("device_id", token);
			logPostJSONObjParam.put("content", "");
			logPostJSONObjParam.put("latitude", ((BaseActivity) getActivity()).getLatitude());
			logPostJSONObjParam.put("longitude", ((BaseActivity) getActivity()).getLongitude());
			logPostJSONObjParam.put("user_id", 31274);//((BaseActivity) getActivity()).getUserId());
			logPostJSONObjParam.put("shop_id", getShopId());
			logPostJSONObjParam.put("menu_id", getMenuId());
			logPostJSONObjParam.put("comment", comment);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return logPostJSONObjParam;
	}

	private void createCmtView(final List<MealthyMenuComment> list, LinearLayout layout) {
		layout.removeAllViews();
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(20, 10, 20, 10);
		layout.setLayoutParams(params);
		TextView tv;
		final List<FeedItem.Comment> cmtList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			MealthyMenuComment cmtObj = list.get(i);
			try {
				cmtList.add(i, makeMealthyMenuCmtToFeedItemCmt(cmtObj));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (i < 5) {
				tv = new TextView(getContext());
				tv.setPadding(10, 10, 10, 10);
				if (i == 4) {
					tv.setText("5件全てのコメントを表示する");
					tv.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_light));
				} else {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
						tv.setText(Html.fromHtml("<b><font color='black'>" + cmtObj.getUser().getNuser_name() + ":</font> </b>" + cmtObj.getBody(), -1));
					} else {
						tv.setText(Html.fromHtml("<b><font color='black'>" + cmtObj.getUser().getNuser_name() + ":</font> </b>" + cmtObj.getBody()));
					}
				}
				tv.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						Toast.makeText(getContext(), "go to new screen", Toast.LENGTH_SHORT).show();
						CommentDetailActivity.launch(getContext(), cmtList);
					}
				});
				layout.addView(tv);
			} else return;
		}
	}

	private FeedItem.Comment makeMealthyMenuCmtToFeedItemCmt(MealthyMenuComment cmtObj) {
		FeedItem.Comment cmt = new FeedItem.Comment();
		cmt.setId(cmtObj.getId());
		cmt.setBody(cmtObj.getBody());
		FeedItem.FeedItemUser user = new FeedItem.FeedItemUser();
		user.setId(cmtObj.getUser().getNuser_id());
		user.setProfileImage(cmtObj.getUser().getNuser_profile_image());
		user.setName(cmtObj.getUser().getNuser_name());
		cmt.setCmtUser(user);
		return cmt;
	}

	private void requetsDelFav(final MealthyMenu obj, final ImageView view) {
		try {
			final RequestHandler requestHandler = new RequestHandler(getActivity());
			JSONObject header = new JSONObject();
			header.put("X-Mealthy-Token", "52B016F5-AC44-4CAB-B080-99756F88BBF6");
			requestHandler.jsonRequest(Constant.BASE_URL + "/favorites/" + obj.getFavId(), Request.Method.DELETE, header, null);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					try {
						if (data.getBoolean("success")) {

							view.setSelected(false);
							obj.setFavorith(false);
							obj.setFavId(0);
							RealmHelper.with(getActivity()).save(obj);
							try {
								getShopFragment().restartData(getMenuObj(), false);
								getMenuShopFragment().restartData(obj, false);
								if (getMenuShopFragment() != null) {
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});


		} catch (Exception e) {

		}
	}

	private void requetsFav(final MealthyMenu obj, final ImageView view) {
		try {
			JSONObject body = new JSONObject();
			try {
				body.put("menu_id", obj.getId());

			} catch (Exception e) {
				e.printStackTrace();
			}

			final RequestHandler requestHandler = new RequestHandler(getActivity());
			JSONObject header = new JSONObject();
			header.put("X-Mealthy-Token", "52B016F5-AC44-4CAB-B080-99756F88BBF6");
			requestHandler.jsonRequest(Constant.BASE_URL + "/favorites", Request.Method.POST, header, body);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					try {
						obj.setFavId(data.getJSONObject("favorite").getInt("id"));
						view.setSelected(true);
						obj.setFavorith(true);
						RealmHelper.with(getActivity()).save(obj);
						try {
							getShopFragment().restartData(getMenuObj(), true);
							getMenuShopFragment().restartData(obj, true);
							if (getMenuShopFragment() != null) {
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});


		} catch (Exception e) {

		}
	}
}
