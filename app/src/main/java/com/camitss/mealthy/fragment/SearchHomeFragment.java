package com.camitss.mealthy.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.HistorySearchAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.HistorySearch;
import com.camitss.mealthy.object.NearbyObj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;

public class SearchHomeFragment extends Fragment {
	FragmentManager fragmentManager;
	FragmentTransaction fragmentTransaction;
	Toolbar toolbar;
	ImageView searchBtn;
	private ListView historyList;
	private List<RealmObject> historySearches;
	private RealmResults<HistorySearch> data;
	private List<String> historyDataList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("SEARCH_HOME");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_search_home, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.search_home_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		ImageView ig = (ImageView) homeView.findViewById(R.id.search_logo);

		toolbar.setTitle("概要");
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		final EditText searchBox = (EditText) homeView.findViewById(R.id.search_box);
		searchBtn = (ImageView) homeView.findViewById(R.id.fire_search);
		historyList = (ListView) homeView.findViewById(R.id.history_list_list);
		fragmentManager = getFragmentManager();
		searchBox.requestFocus();
		historySearches = new ArrayList<>();
		data = RealmHelper.with(getActivity()).getRealm().where(HistorySearch.class).findAll();
		historyDataList = new ArrayList<>();
		for (int i = 0; i < data.size(); i++) {
			historyDataList.add(data.get(i).getData());
		}
		Collections.reverse(historyDataList);
		final HistorySearchAdapter adapter = new HistorySearchAdapter(getActivity(), historyDataList);
		historyList.setAdapter(adapter);

		searchBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (searchBox.getText().toString().length() > 0) {
					try {
						HistorySearch historySearch = new HistorySearch();
						historySearch.setId((int) System.currentTimeMillis());
						historySearch.setData(searchBox.getText().toString());
						RealmHelper.with(getActivity()).saveRelevant(historySearch);
						adapter.notifyDataSetChanged();
						NearbyObj nearbyObj = new NearbyObj();
						nearbyObj.setMenu_name(searchBox.getText().toString());
						nearbyObj.setTypeSearch(5);
						nearbyObj.setTag(searchBox.getText().toString());

						View view = getActivity().getCurrentFocus();
						if (view != null) {
							InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
						}
						fragmentTransaction = fragmentManager.beginTransaction();
						NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
						fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
						fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "LOWCRAB").addToBackStack(Constant.FRAGMENT_TAG_HOME);
						fragmentTransaction.commit();
						if (historyDataList.size() > 0) {
							historyList.smoothScrollByOffset(0);
						}
					} catch (Exception e) {

						e.printStackTrace();
					}
				}
			}
		});
		historyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				View viewRe = getActivity().getCurrentFocus();
				if (viewRe != null) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name(historyDataList.get(position));
				nearbyObj.setTypeSearch(5);
				nearbyObj.setTag(historyDataList.get(position));
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "LOWCRAB").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
				getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				if (historyDataList.size() > 0) {
					historyList.smoothScrollByOffset(0);
				}
			}
		});

		return homeView;
	}

}