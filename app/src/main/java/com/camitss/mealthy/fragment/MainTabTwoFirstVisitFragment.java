package com.camitss.mealthy.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.util.Util;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.activity.UserRecommendationDisplayActivity;
import com.camitss.mealthy.adapter.recyclerview.SocialPostItemAdapter;
import com.camitss.mealthy.adapter.recyclerview.UserRecomItemAdapter;
import com.camitss.mealthy.adapter.recyclerview.listener.HidingScrollListener;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.UserRecomItem;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 6/8/2017.
 */

public class MainTabTwoFirstVisitFragment extends Fragment {
	private ProgressDialog progressDialog;
	protected RecyclerView rclRecom;
	protected UserRecomItemAdapter userRecomAdapter;
	protected ProgressBar progressBar;
	private TextView textNoUser, textDes, textDescription, textUpperLongDescription;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentTag.getInstant().setTAG("MAIN_TAB_TWO");
		return inflater.inflate(R.layout.tab_two_fragment_first_visit_layout, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
//		progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
//		Utils.showProgressDialog(getContext(), progressDialog);
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
		progressBar.setVisibility(View.VISIBLE);
		textNoUser = (TextView) view.findViewById(R.id.txt_no_user);
		textDes = (TextView) view.findViewById(R.id.text_des);
		textDescription = (TextView) view.findViewById(R.id.txt_description);
		textUpperLongDescription = (TextView) view.findViewById(R.id.txt_add_long_desc);
		rclRecom = (RecyclerView) view.findViewById(R.id.rcl_user_recom);
		rclRecom.setLayoutManager(new LinearLayoutManager(getActivity()));
		userRecomAdapter = new UserRecomItemAdapter(getContext());
		rclRecom.setAdapter(userRecomAdapter);
		getUserRecommendRequest();

		view.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AppSharedPreferences.getConstant(getContext()).setFirstVisitInTavTwoFragment(false);
				((MealthyActivity) getActivity()).setupTabTwoFragment();
			}
		});

	}

	private void getUserRecommendRequest() {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject params = new JSONObject();
		try {
			params.put("Content-Type", "application/json; charset=utf-8");
			params.put("Accept", "application/json");
			params.put("X-Mealthy-Token", "4FFCD52E-5143-407F-9BE3-AFF7954768BF");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_USERS_RECOMMEND, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
//				progressDialog.dismiss();
				progressBar.setVisibility(View.GONE);
				if (isSuccess) {
					try {
						JSONArray userArr = data.getJSONArray("users");
						UserRecomItem item;
						List<UserRecomItem> userRecomItemList = new ArrayList<UserRecomItem>();
						int length = userArr.length() < 5 ? userArr.length() : 5;
						for (int i = 0; i < length; i++) {
							item = new Gson().fromJson(userArr.getJSONObject(i).toString(), UserRecomItem.class);
							userRecomItemList.add(item);
						}
						userRecomAdapter.addUserRecomItemList(userRecomItemList);
						userRecomAdapter.notifyDataSetChanged();
						setupDescriptionView(userRecomAdapter.getItemCount());
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void setupDescriptionView(int length) {
		int width = Utils.getScreenWidth(getActivity());
		int height = Utils.getScreenHeight(getActivity());
		Log.d("widdddd", width + ":" + height + ":" + length);
		if (length > 0) {
			if (width <= 720 && height <= 1250) {
				textDescription.setVisibility(View.GONE);
				textUpperLongDescription.setVisibility(View.GONE);
			} else if (width >= 720) {
				if (length < 4) {
					textDescription.setVisibility(View.VISIBLE);
					textUpperLongDescription.setVisibility(View.VISIBLE);
					textDescription.setText(getString(R.string.k_long_desc_string));
				} else {
					textDescription.setVisibility(View.VISIBLE);
					textUpperLongDescription.setVisibility(View.GONE);
					textDescription.setText(getString(R.string.k_short_desc_string));
				}
			} else {
				if (length < 4) {
					textDescription.setVisibility(View.VISIBLE);
					textUpperLongDescription.setVisibility(View.VISIBLE);
					textDescription.setText(getString(R.string.k_long_desc_string));
				} else {
					textDescription.setVisibility(View.VISIBLE);
					textUpperLongDescription.setVisibility(View.GONE);
					textDescription.setText(getString(R.string.k_short_desc_string));
				}
			}
		} else {
			textDes.setVisibility(View.GONE);
			textNoUser.setVisibility(View.VISIBLE);
		}

	}


}
