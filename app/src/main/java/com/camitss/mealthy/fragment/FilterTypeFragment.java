package com.camitss.mealthy.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.StickyGridAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.MenuTag;
import com.camitss.mealthy.object.SearchSettingTag;
import com.camitss.mealthy.object.Shop;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.Utils;
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FilterTypeFragment extends Fragment {

	Toolbar toolbar;
	RecyclerView listTagView;
	List<SearchSettingTag> allTags;
	StickyGridAdapter adapter;
	private LoadTagListener loadTagListener;
	ProgressDialog progressDialog;
	String listIdtag = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("FILTER_BY_TYPE");
		setHasOptionsMenu(true);
		if (allTags == null) {
			allTags = new ArrayList<>();
		}
		progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatProgressDialogStyle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_filter_by_type, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.filter_type_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.filter_type);
		listTagView = (RecyclerView) homeView.findViewById(R.id.list_tag_view);
		getAllTag();
		Utils.showProgressDialog(getActivity(), progressDialog, getString(R.string.loading_menu_tag));

		StickyHeaderGridLayoutManager mLayoutManager = new StickyHeaderGridLayoutManager(3);
//		mLayoutManager.setHeaderBottomOverlapMargin(R.dimen.dimen_30);

		mLayoutManager.setSpanSizeLookup(new StickyHeaderGridLayoutManager.SpanSizeLookup() {
			@Override
			public int getSpanSize(int section, int position) {
				return 1;
			}
		});
		listTagView.setLayoutManager(mLayoutManager);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});

		MenuItem item = toolbar.getMenu().getItem(0);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				if (formParameter()) {
					try {
						updateTag();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				return false;
			}
		});

		loadTagListener = new LoadTagListener() {
			@Override
			public void onLoaded() {

				adapter = new StickyGridAdapter(allTags);
				listTagView.setAdapter(adapter);
				adapter.setAdapterListener(new StickyGridAdapter.TagLikeListener() {
					@Override
					public void onHeaderSelected(int headerPosition) {
						switchLikeHeader(headerPosition);
					}

					@Override
					public void onChildSelected(int headerPosition, int childPosition) {
						if (allTags.get(headerPosition).getChildTag().get(childPosition).isLike()) {
							allTags.get(headerPosition).getChildTag().get(childPosition).setLike(false);
						} else {
							allTags.get(headerPosition).getChildTag().get(childPosition).setLike(true);
						}

						adapter.notifyDataSetChanged();
					}
				});
				progressDialog.dismiss();
			}
		};
		return homeView;
	}

	private void getAllTag() {
		RequestHandler requestHandler = new RequestHandler(getActivity());
		requestHandler.jsonRequest(Constant.REQUEST_ALL_TAG, Request.Method.GET, null, null);
		final Gson mGson = new Gson();
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				int i = 0, length = 0;
				try {
					length = data.getJSONArray("tags").length();
					for (i = 0; i < length; i++) {
						allTags.add(mGson.fromJson(data.getJSONArray("tags").getJSONObject(i).toString(), SearchSettingTag.class));
					}
					getAllLikeTag();
				} catch (Exception e) {
				}
				loadTagListener.onLoaded();
			}
		});
	}

	private void getAllLikeTag() {
		RequestHandler requestHandler = new RequestHandler(getActivity());
		requestHandler.jsonRequest(Constant.REQUEST_USER_LIKE_TAG, Request.Method.GET, null, null);

		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				MealthyLog.infoLog("data", data.toString());
				int i = 0, length = 0;
				try {
					length = data.getJSONObject("user").getJSONArray("like_tags").length();
					JSONArray likeTag = data.getJSONObject("user").getJSONArray("like_tags");
					for (i = 0; i < length; i++) {
						setLikeTag(Integer.parseInt(likeTag.getString(i)));
					}
					adapter.notifyDataSetChanged();
				} catch (Exception e) {
				}
			}
		});
	}

	private void setLikeTag(int id) {
		int i = 0, j = 0, length = allTags.size(), childLength = 0;

		for (i = 0; i < length; i++) {
			childLength = allTags.get(i).getChildTag().size();
			for (j = 0; j < childLength; j++) {
				if (allTags.get(i).getChildTag().get(j).getId() == id) {
					allTags.get(i).getChildTag().get(j).setLike(true);
				}
			}
		}

		adapter.notifyDataSetChanged();
	}

	private void switchLikeHeader(int headerPosition) {
		int j = 0, childLength;

		childLength = allTags.get(headerPosition).getChildTag().size();
		boolean isLike;
		SearchSettingTag headerItem = allTags.get(headerPosition);

		if (headerItem.isLike()) {
			isLike = false;
		} else {
			isLike = true;

		}
		allTags.get(headerPosition).setLike(isLike);
		for (j = 0; j < childLength; j++) {
			allTags.get(headerPosition).getChildTag().get(j).setLike(isLike);
		}

		adapter.notifyDataSetChanged();
	}

	private boolean formParameter() {
		listIdtag = "";
		for (SearchSettingTag itemHeader : allTags) {
			if(itemHeader.isLike()){
				listIdtag = listIdtag + itemHeader.getId() + ",";
			}
			for (MenuTag itemChild : itemHeader.getChildTag()) {
				if(itemChild.isLike()){
					listIdtag = listIdtag + itemChild.getId() + ",";
				}
			}
		}
		try {
			listIdtag = listIdtag.substring(0, listIdtag.length() - 1);
			MealthyLog.infoLog("list like id", listIdtag);
			return true;
		} catch (StringIndexOutOfBoundsException sioe) {
			return false;
		} catch (NullPointerException npe) {
			return false;
		}
	}

	private void updateTag() throws JSONException {
		RequestHandler requestHandler = new RequestHandler(getActivity());
		JSONObject body = new JSONObject();
		body.put("tag_ids", listIdtag);
		requestHandler.jsonRequest(Constant.REQUEST_USER_LIKE_TAG, Request.Method.PUT, null, body);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					getFragmentManager().popBackStack();
					MealthyLog.infoLog("resturn data ", data.toString());
				}
			}
		});
	}

	interface LoadTagListener {
		void onLoaded();
	}
}