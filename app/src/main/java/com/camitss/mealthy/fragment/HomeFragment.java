package com.camitss.mealthy.fragment;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.api.HomeEvent;
import com.camitss.mealthy.api.MenuMessage;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.NearbyObj;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends Fragment {

	TextView headerMessage;
	FragmentManager fragmentManager;
	FragmentTransaction fragmentTransaction;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		//setHasOptionsMenu(true);

		FragmentTag.getInstant().setTAG(Constant.FRAGMENT_TAG_HOME);
		LocationManager locationManager = (LocationManager)
				getActivity().getSystemService(Context.LOCATION_SERVICE);

//		LocationListener locationListener =  new MealthyLocationListener();
//		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);

		View homeView = inflater.inflate(R.layout.fragment_home, container, false);

		Toolbar toolbar = (Toolbar) homeView.findViewById(R.id.home_toolbar);
		toolbar.inflateMenu(R.menu.home);

		headerMessage = (TextView) homeView.findViewById(R.id.notic_message);
		ImageView near = (ImageView) homeView.findViewById(R.id.option_1);
		ImageView lowCarb = (ImageView) homeView.findViewById(R.id.option_2);
		ImageView favorite = (ImageView) homeView.findViewById(R.id.option_3);
		ImageView genre = (ImageView) homeView.findViewById(R.id.option_4);
		ImageView nutrition = (ImageView) homeView.findViewById(R.id.option_5);
		fragmentManager = getFragmentManager();

		toolbar.setTitle(R.string.home_title);
		toolbar.setNavigationIcon(R.drawable.filter_path);

		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				startActivity(new Intent(getActivity(),MapsFragment.class));
				fragmentTransaction = fragmentManager.beginTransaction();
				FilterHomeFragment filterHomeFragment = new FilterHomeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_right, R.anim.in_from_right, R.anim.out_to_left);
				fragmentTransaction.replace(R.id.main_container, filterHomeFragment, "FILTER_HOME").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		headerMessage.setText(AppSharedPreferences.getConstant(getActivity()).getHomeMessage());
		getHomeMessage(headerMessage);

		near.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setTypeSearch(1);
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "NEAR").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		lowCarb.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name("糖質制限");
				nearbyObj.setTypeSearch(2);
				nearbyObj.setTag("低糖質[C:~40g],糖質オフ[C:~10g]");
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "LOWCRAB").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		favorite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name("人気メニュー");
				nearbyObj.setTypeSearch(3);
				nearbyObj.setShop_group_kind(2);
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "FAVORITE").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		genre.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name("ジャンルから選ぶ");
				nearbyObj.setTypeSearch(4);
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopCategoryFragment nearestShopFragment = new NearestShopCategoryFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "GENRE").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		nutrition.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name("栄養重視");
				nearbyObj.setTypeSearch(5);
				nearbyObj.setTag("高たんぱく,低糖質[C:~40g],糖質オフ[C:~10g],野菜,マクロビ,ベジタリアン,ヴィーガン,グルテンフリー,薬膳,豆腐");
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "NUTRITION").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});

		MenuItem item = toolbar.getMenu().getItem(0);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				MealthyLog.infoLog("action", "search");
				fragmentTransaction = fragmentManager.beginTransaction();
				SearchHomeFragment searchHomeFragment = new SearchHomeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, searchHomeFragment, "SEARCH_HOME").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
				return false;
			}
		});

		return homeView;
	}

	private void getHomeMessage(final TextView headerMessage) {
		RequestHandler requestHandler = new RequestHandler(getContext());
		requestHandler.jsonRequest(Constant.MENU_MESSAGE, Request.Method.GET, null, null);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						headerMessage.setText(data.getJSONObject("notices").getString("notice"));
						AppSharedPreferences.getConstant(getActivity()).setHomeMessage(headerMessage.getText().toString());
					} catch (JSONException e) {
						e.printStackTrace();
						headerMessage.setText(AppSharedPreferences.getConstant(getActivity()).getHomeMessage());
					}
				}else{
					headerMessage.setText(AppSharedPreferences.getConstant(getActivity()).getHomeMessage());
				}
			}
		});
	}
}