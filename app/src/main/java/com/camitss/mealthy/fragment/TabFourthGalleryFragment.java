package com.camitss.mealthy.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.CropImage;
import com.camitss.mealthy.activity.TabFourthCropImageActivity;
import com.camitss.mealthy.adapter.GalleryAdapter;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.GalleryMultiPickup;
import com.camitss.mealthy.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Viseth on 5/17/2017.
 */

public class TabFourthGalleryFragment extends Fragment {
	Map<Long,String> listFilePath;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("GALLERY");
		listFilePath = new HashMap<>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_gallery, container, false);
		RecyclerView photoView = (RecyclerView) homeView.findViewById(R.id.gallery_view);

		GalleryAdapter galleryAdapter = new GalleryAdapter(getActivity());
		GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
		mLayoutManager.setAutoMeasureEnabled(true);
		photoView.setLayoutManager(mLayoutManager);
		photoView.setItemAnimator(new DefaultItemAnimator());
		photoView.setAdapter(galleryAdapter);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 300);
			}
		}
		String externalStorageDirectoryPath = Environment
				.getExternalStorageDirectory()
				.getAbsolutePath();

		// get all image from folder DCIM
//		fetchAllImage(externalStorageDirectoryPath + "/DCIM", galleryAdapter);
		File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
		getAllImagesFromDevice(new File(externalStorageDirectoryPath + "/DCIM"));
		Map<Long, String> map = new TreeMap<>(listFilePath);
		for (Map.Entry<Long, String> entry : map.entrySet()) {
			galleryAdapter.add(entry.getValue());
		}
		galleryAdapter.setOnItemClickListener(new GalleryAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int pos, String path) {
				//MealthyLog.infoLog("click view", pos + "-"+path);
				Intent crop = new Intent(getActivity(), TabFourthCropImageActivity.class);
				crop.putExtra("PATH", path);
				getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
				startActivity(crop);
				getActivity().finish();
			}
		});
		return homeView;
	}

	public void getAllImagesFromDevice(File root) {
//		File root = new File(path);
		List<String> fileList = new ArrayList<>();
		File listFile[] = root.listFiles();
		if (listFile != null && listFile.length > 0) {
			for (File file : listFile) {
				if (file.isDirectory()) {
					getAllImagesFromDevice(file);
				} else {
					if (file.getName().endsWith(".png")
							|| file.getName().endsWith(".jpg")
							|| file.getName().endsWith(".jpeg")
							) {
						String temp = file.getAbsolutePath();//file.getPath().substring(0, file.getPath().lastIndexOf('/'));
						Log.d("getAllImageFromDevice", temp + "<<>>" + file.getAbsolutePath());
						if(!temp.contains(".thumbnails")) {
//							adapter.add(temp);
							listFilePath.put(file.lastModified()*(-1),temp);
						}


					}
				}
			}
		}
	}

	private void fetchAllImage(String dirPath, GalleryAdapter galleryAdapter) {

		File targetDirector = new File(dirPath);
		File[] files = targetDirector.listFiles();

		for (File file : files) {
			if (file.isFile()) {
				if (isImage(file)) {
					galleryAdapter.add(file.getAbsolutePath());
				}
			} else {
				fetchAllImage(file.getAbsolutePath(), galleryAdapter);
			}
		}
	}


	private boolean isImage(File file) {
		String type = "mimetype";
		String extension = MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath());
		if (extension != null) {
			type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
			if (type != null && type.contains("image")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		int size = permissions.length;
		switch (requestCode) {
			case 300:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(getActivity(), "Camera permission was granted!", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getActivity(), "Camera permission was not granted!", Toast.LENGTH_LONG).show();
					getFragmentManager().popBackStack();
				}
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(getActivity(), "Storage permission was granted!", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), "Storage permission was not granted!", Toast.LENGTH_LONG).show();
					getFragmentManager().popBackStack();
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				break;
		}
	}
}