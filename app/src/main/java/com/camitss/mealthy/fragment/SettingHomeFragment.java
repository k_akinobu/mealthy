package com.camitss.mealthy.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.EmailLoginActivity;
import com.camitss.mealthy.activity.SocialLoginActivity;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

public class SettingHomeFragment extends Fragment {

	Toolbar toolbar;
	private CallbackManager callbackManager;
	Switch fb_switch;

	public SettingHomeFragment(Toolbar toolbar) {
		this.toolbar = toolbar;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("SETTING_HOME");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_social_setting, container, false);

		fb_switch = (Switch) homeView.findViewById(R.id.switch_facebook);
		final Switch email_switch = (Switch) homeView.findViewById(R.id.switch_email);
		final Switch legal_switch = (Switch) homeView.findViewById(R.id.switch_legal_id);

		if (AppSharedPreferences.getConstant(getActivity()).isLoginWithSocialPref()) {
			MealthyLog.infoLog("social type",AppSharedPreferences.getConstant(getActivity()).getConnectedSocialType()+"");
			if (AppSharedPreferences.getConstant(getActivity()).getConnectedSocialType() == Constant.CONNECT_FACEBOOK) {
				fb_switch.setChecked(true);
			} else if (AppSharedPreferences.getConstant(getActivity()).getConnectedSocialType() == Constant.CONNECT_EMAIL) {
				email_switch.setChecked(true);
			} else if (AppSharedPreferences.getConstant(getActivity()).getConnectedSocialType() == Constant.CONNECT_COOPERATE) {
				legal_switch.setChecked(true);
			}
		}

		fb_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				if(b){
					onLoginWithFbClick();
				}else{
					fb_switch.setChecked(true);
				}
			}
		});

		email_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				if(b){
//					Intent email = new Intent(getActivity(),EmailLoginActivity.class);
//					email.putExtra("FROM","EMAIL_BUTTON");
//					startActivity(email);
				}else{
					email_switch.setChecked(true);
				}
			}
		});

		fb_switch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(fb_switch.isChecked()){
//					fb_switch.setChecked(false);
				}else{
//					fb_switch.setChecked(true);
					onLoginWithFbClick();
				}
			}
		});

		email_switch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(email_switch.isChecked()){
					//email_switch.setChecked(false);
				}else{
					Intent email = new Intent(getActivity(),EmailLoginActivity.class);
					email.putExtra("FROM","EMAIL_BUTTON");
					startActivity(email);
					//email_switch.setChecked(true);
				}
			}
		});

		legal_switch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent cooperate = new Intent(getActivity(),EmailLoginActivity.class);
				cooperate.putExtra("FROM","COOPERATE_BUTTON");
				startActivity(cooperate);
				if(legal_switch.isChecked()){
					legal_switch.setChecked(false);
				}else{
					legal_switch.setChecked(true);
				}
			}
		});

		toolbar.setTitle(getString(R.string.social_setting_title));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getActivity().finish();
			}
		});
		return homeView;
	}

	private void onLoginWithFbClick() {
		requestToLoginWithFb();
	}

	private void requestToLoginWithFb() {
		final LoginManager manager = LoginManager.getInstance();
		manager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
		callbackManager = CallbackManager.Factory.create();

		manager.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						// App code
						final AccessToken accessToken = loginResult.getAccessToken();
						GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
							@Override
							public void onCompleted(JSONObject user, GraphResponse graphResponse) {
								String id = user.optString("id");
								String profileUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
								String name = user.optString("first_name") + " " + user.optString("last_name");
								loginWithFb("facebook", user.optString("email"), id, String.valueOf(accessToken), profileUrl, name);
							}
						});
						Bundle parameters = new Bundle();
						parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
						request.setParameters(parameters);
						request.executeAsync();

						manager.logOut();
						fb_switch.setChecked(true);
					}

					@Override
					public void onCancel() {
						// App code
						Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
						fb_switch.setChecked(false);
					}

					@Override
					public void onError(FacebookException exception) {
						// App code
						Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
						Log.d("exception",exception.getMessage());
						fb_switch.setChecked(false);
					}
				});
	}

	private void loginWithFb(String whatSocial, String email, String id, String token, final String profileUrl, final String name) {
		RequestHandler request = new RequestHandler(getActivity());
		JSONObject body = new JSONObject();
		try {
			body.put("access_token", token);
			body.put("provider", whatSocial);
			body.put("email", email);
			body.put("uid", id);
			body.put("password", null);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_lOGIN_WITH_SOCIAL, Request.Method.POST, null, body);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					AppSharedPreferences.getConstant(getActivity()).setLoginWithSocialPref(true);
					AppSharedPreferences.getConstant(getContext()).setConnectedSocialType(Constant.CONNECT_FACEBOOK);
					try {
						JSONObject authObj = data.getJSONObject("authentication");
						TokenObject token = new TokenObject();
						token.setToken(authObj.getString("token"));
						TokenRealmHelper.with(getActivity()).save(token);
						getUserInfo(authObj.getInt("user_id"), profileUrl, name, authObj.getString("token"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void getUserInfo(final int id, final String profileUrl, final String name, final String authtoken) {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", id), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				AppSharedPreferences.getConstant(getActivity()).setLoginWithSocialPref(isSuccess);
				if (isSuccess) {
					try {
						AppSharedPreferences.getConstant(getActivity()).setUserIdPref(data.getJSONObject("user").getInt("id"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				//	displayQuestionAlert(profileUrl, id, name, authtoken);
				}
			}
		});
	}

//	private void displayQuestionAlert(final String profileUrl, final int id, final String name, final String authtoken) {
//		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.alertdialog_theme_default);
//		dialog.setMessage(getString(R.string.dialog_msg));
//		dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
////				updateUserInfo(profileUrl, id);
//				upload(id, profileUrl, name, authtoken);
//				dialog.dismiss();
//			}
//		});
//		dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				goToCameraAndGalleryActivity();
//				dialog.dismiss();
//			}
//		});
//		dialog.setCancelable(false);
//		dialog.show();
//	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
}