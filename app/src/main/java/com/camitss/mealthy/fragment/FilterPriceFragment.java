package com.camitss.mealthy.fragment;


import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.ProfileSettingActivity;
import com.camitss.mealthy.adapter.BottomsheetItemAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class FilterPriceFragment extends Fragment {

	Toolbar toolbar;
	final List<String> priceList = new ArrayList<>();
	final List<String> priceListValue = new ArrayList<>();
	int breakfastSelectedIndex,lunchSelectedIndex,dinnerSelectedIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("FILTER_BY_PRICE");
		setHasOptionsMenu(true);
		priceList.add("200円");
		priceList.add("300円");
		priceList.add("400円");
		priceList.add("500円");
		priceList.add("600円");
		priceList.add("700円");
		priceList.add("800円");
		priceList.add("900円");
		priceList.add("1,000円");
		priceList.add("1,500円");
		priceList.add("2,000円");
		priceList.add("3,000円");
		priceList.add("4,000円");
		priceList.add("5,000円");
		priceList.add("上限なし");

		priceListValue.add("200");
		priceListValue.add("300");
		priceListValue.add("400");
		priceListValue.add("500");
		priceListValue.add("600");
		priceListValue.add("700");
		priceListValue.add("800");
		priceListValue.add("900");
		priceListValue.add("1000");
		priceListValue.add("1500");
		priceListValue.add("2000");
		priceListValue.add("3000");
		priceListValue.add("4000");
		priceListValue.add("5000");
		priceListValue.add("1000000");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_search_by_price, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.filter_by_price_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		final TextView breakfast = (TextView) homeView.findViewById(R.id.breakfast_price);
		final TextView lunch = (TextView) homeView.findViewById(R.id.lunch_price);
		final TextView dinner = (TextView) homeView.findViewById(R.id.dinner_price);

		breakfastSelectedIndex = AppSharedPreferences.getConstant(getActivity()).getBreakfastPriceIndex();
		lunchSelectedIndex  = AppSharedPreferences.getConstant(getActivity()).getLunchPriceIndex();
		dinnerSelectedIndex = AppSharedPreferences.getConstant(getActivity()).getDinnerPriceIndex();
		// set previous setting
		breakfast.setText(priceList.get(breakfastSelectedIndex));
		lunch.setText(priceList.get(lunchSelectedIndex));
		dinner.setText(priceList.get(dinnerSelectedIndex));
		homeView.findViewById(R.id.btn_saveChange).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					saveChange(breakfastSelectedIndex,lunchSelectedIndex,dinnerSelectedIndex);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});

		homeView.findViewById(R.id.layout_breakfast).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showPriceListDialog(breakfast, 0);
			}
		});

		homeView.findViewById(R.id.layout_lunch).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showPriceListDialog(lunch, 1);
			}
		});

		homeView.findViewById(R.id.layout_dinner).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showPriceListDialog(dinner, 2);
			}
		});
		return homeView;
	}

	private void showPriceListDialog(final TextView mealTime, final int titleId) {

		List<String> titleList = new ArrayList<>();
		titleList.add(getString(R.string.breakfast_price));
		titleList.add(getString(R.string.lunch_price));
		titleList.add(getString(R.string.dinner_price));

		final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
		View sheetView = getActivity().getLayoutInflater().inflate(R.layout.item_bottomsheet_dialog_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(titleList.get(titleId));
		RecyclerView recyclerview_country = (RecyclerView) sheetView.findViewById(R.id.lvCountryZ);

		recyclerview_country.setHasFixedSize(true);
		recyclerview_country.setLayoutManager(new LinearLayoutManager(getActivity()));

		BottomsheetItemAdapter adapter = new BottomsheetItemAdapter(getActivity(), priceList);
		recyclerview_country.setAdapter(adapter);

		adapter.setOnItemClickListner(new BottomsheetItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				if (priceList.get(position) instanceof String) {
					mealTime.setText(priceList.get(position));
				}
				if(titleId == 0){
					breakfastSelectedIndex = position;
				}else if (titleId== 1){
					lunchSelectedIndex = position;
				}else {
					dinnerSelectedIndex = position;
				}
				mBottomSheetDialog.dismiss();
			}
		});

		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.show();
	}

	private void saveChange(final int breakfastIndex,final int lunchIndex,final int dinnerIndex) throws JSONException {
		JSONObject body = new JSONObject();
		body.put("morning_price", priceListValue.get(breakfastIndex));
		body.put("lunch_price", priceListValue.get(lunchIndex));
		body.put("dinner_price", priceListValue.get(dinnerIndex));
		RequestHandler requestHandler = new RequestHandler(getActivity());
		String url = Constant.REQUEST_UPDATE_MEAL_PRICE+AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
		requestHandler.jsonRequest(url, Request.Method.PUT,null,body);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if(isSuccess){
					AppSharedPreferences.getConstant(getActivity()).setBreakFirstPriceSettingIndex(breakfastIndex);
					AppSharedPreferences.getConstant(getActivity()).setLunchPriceSettingIndex(lunchIndex);
					AppSharedPreferences.getConstant(getActivity()).setDinnerPriceSettingIndex(dinnerIndex);
					MealthyLog.infoLog("result",data.toString());
					getFragmentManager().popBackStack();
				}
			}
		});
	}
}