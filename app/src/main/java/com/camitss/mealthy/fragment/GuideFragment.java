package com.camitss.mealthy.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;

public class GuideFragment extends Fragment {

	private static int position;
	ImageView viewOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			position = getArguments().getInt("POS");
		}
	}

	public GuideFragment() {
	}

	public static GuideFragment newInstant(int pos) {
		GuideFragment fragment = new GuideFragment();
		Bundle args = new Bundle();
		args.putInt("POS", pos);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View homeView = inflater.inflate(R.layout.guide_one, container, false);
		viewOne = (ImageView) homeView.findViewById(R.id.guide_one);
		if (getArguments() != null) {
			position = getArguments().getInt("POS");
		}
		switch (position) {
			case 1:
				Glide.with(this).load(R.drawable.bg1).into(viewOne);
				break;
			case 2:
				Glide.with(this).load(R.drawable.bg2).into(viewOne);
				break;
			case 3:
				Glide.with(this).load(R.drawable.bg3).into(viewOne);
				break;
			case 4:
				Glide.with(this).load(R.drawable.bg4).into(viewOne);
				break;
			case 5:
				Glide.with(this).load(R.drawable.bg5).into(viewOne);
				break;
		}
		return homeView;
	}
}