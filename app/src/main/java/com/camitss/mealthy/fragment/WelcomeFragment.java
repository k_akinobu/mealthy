package com.camitss.mealthy.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.WelcomeActivity;
import com.camitss.mealthy.api.DiagnoseResult;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmResults;

public class WelcomeFragment extends Fragment {
	String score, type, name, comment, plusTitle, plusComment, plusImage;
	String minusTitle, minusComment, minusImage;
	TextView scoreView, nameView, commentView;
	TextView plusTitleView, plusCommentView, plusSubTitleView;
	TextView minusTitleView, minusCommentView, minusSubTitleView;
	ImageView minusImageView, plusImageView;
	private String answerIds = "";
	ProgressDialog progressDialog;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatProgressDialogStyle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_welcome, container, false);
		scoreView = (TextView) homeView.findViewById(R.id.re_diagnose_score);
		nameView = (TextView) homeView.findViewById(R.id.re_name);
		commentView = (TextView) homeView.findViewById(R.id.re_comment);
		plusTitleView = (TextView) homeView.findViewById(R.id.re_plus_title);
		plusCommentView = (TextView) homeView.findViewById(R.id.re_plus_comment);
		plusSubTitleView = (TextView) homeView.findViewById(R.id.re_plus_title_right);

		minusTitleView = (TextView) homeView.findViewById(R.id.re_minus_title);
		minusCommentView = (TextView) homeView.findViewById(R.id.re_minus_comment);
		minusSubTitleView = (TextView) homeView.findViewById(R.id.re_minus_title_right);

		plusImageView = (ImageView) homeView.findViewById(R.id.re_plus_image);
		minusImageView = (ImageView) homeView.findViewById(R.id.re_minus_image);

		Utils.showProgressDialog(getActivity(),progressDialog,getString(R.string.loading_welcome));
		Toolbar toolbar = (Toolbar) homeView.findViewById(R.id.re_welcome_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		RealmResults<Diagnoses> list = RealmHelper.with(getActivity()).getRealm().where(Diagnoses.class).findAll();
		User user = RealmHelper.with(getActivity()).getRealm().where(User.class).findFirst();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).isSelectedQuestion()) {
				if (!answerIds.trim().isEmpty()) answerIds = answerIds + ",";
				answerIds = answerIds + list.get(i).getId() + "|1";
			}
		}
		DiagnoseResult diagnoseResult = new DiagnoseResult(getActivity());
		JSONObject header = new JSONObject();
		JSONObject body = new JSONObject();
		try {

			body.put("t", user.getTall());
			body.put("w", user.getWeight());
			body.put("weight", user.getWeight());
			body.put("answer_ids", answerIds);

			diagnoseResult.parseParameter(header, body);
			diagnoseResult.getRequestHandler().setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					MealthyLog.infoLog("xxx", data.toString());
					try {
						type = data.getString("type");
						score = data.getJSONObject("attributes").getInt("score") + "";
						name = data.getJSONObject("attributes").getString("name");
						comment = data.getJSONObject("attributes").getString("comment");
						plusTitle = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("title");
						plusImage = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("image");
						plusComment = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("plus").getString("comment");
						minusTitle = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("title");
						minusComment = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("comment");
						minusImage = data.getJSONObject("attributes").getJSONObject("recommend").getJSONObject("minus").getString("image");

						scoreView.setText(score);
						nameView.setText(name);
						commentView.setText(comment);
						plusTitleView.setText(plusTitle);
						plusCommentView.setText(plusComment);
						plusSubTitleView.setText(plusTitle);
						minusTitleView.setText(minusTitle);
						minusCommentView.setText(minusComment);
						minusSubTitleView.setText(minusTitle);

						Glide.with(getActivity()).load(plusImage).into(plusImageView).onLoadStarted(getResources().getDrawable(R.drawable.favorite));
						Glide.with(getActivity()).load(minusImage).into(minusImageView);
						progressDialog.dismiss();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return homeView;
	}
}