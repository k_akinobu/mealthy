package com.camitss.mealthy.fragment;


import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.TermConditionActivity;

public class TermConditionFragment extends Fragment {

	private ProgressBar loading;
	private  int index;
	private Toolbar toolbar;
	private String from;

	public TermConditionFragment(int index, Toolbar toolbar,String from){
		this.index = index;
		this.toolbar = toolbar;
		this.from = from;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		toolbar.setTitle(getString(R.string.term_of_service_title));
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.setVisibility(View.VISIBLE);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// can not case to TermConditionActivity if it not yet start
				if(from.contentEquals("CORP")){
					getActivity().finish();
				}else if(from.equalsIgnoreCase("REGISTER_CORP")){
					getActivity().finish();
				}else{
					getFragmentManager().popBackStack();
				}
			}
		});
		View homeView = inflater.inflate(R.layout.fragment_term_condition, container, false);
		loading = (ProgressBar) homeView.findViewById(R.id.term_service_loading);
		loading.setVisibility(View.VISIBLE);
		WebView webView = (WebView) homeView.findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewClient());
		if (18 < Build.VERSION.SDK_INT) {
			webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		}

		String links[] = {
				"http://info.mealthy.co.jp/terms_of_service",
				"http://info.mealthy.co.jp/privacy_policy/",
				"http://info.mealthy.co.jp/terms_of_healthcare_service/",
				"http://info.mealthy.co.jp/tokutei"};

		webView.loadUrl(links[index]);
		//webView.loadUrl("http://info.mealthy.co.jp/terms_of_service");

		return homeView;
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			loading.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			loading.setVisibility(View.GONE);
			super.onPageFinished(view, url);
		}
	}
}