package com.camitss.mealthy.fragment;


import android.os.Bundle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camitss.mealthy.R;

public class SettingTermCondition extends Fragment implements View.OnClickListener{

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Toolbar toolbar;

    public SettingTermCondition(Toolbar toolbar){
        this.toolbar = toolbar;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        toolbar.setTitle(getString(R.string.term_of_service_title));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        View termView = inflater.inflate(R.layout.fragment_setting_term_condition, container, false);

        termView.findViewById(R.id.term_condition_menu1).setOnClickListener(this);
        termView.findViewById(R.id.term_condition_menu2).setOnClickListener(this);
        termView.findViewById(R.id.term_condition_menu3).setOnClickListener(this);
        termView.findViewById(R.id.term_condition_menu4).setOnClickListener(this);

        return termView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.term_condition_menu1:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.term_setting_container,new TermConditionFragment(0,toolbar,"SETTING"),"MENU1").addToBackStack("TERM_MENU").commit();
                break;
            case R.id.term_condition_menu2:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.term_setting_container,new TermConditionFragment(1,toolbar,"SETTING"),"MENU2").addToBackStack("TERM_MENU").commit();
                break;
            case R.id.term_condition_menu3:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.term_setting_container,new TermConditionFragment(2,toolbar,"SETTING"),"MENU3").addToBackStack("TERM_MENU").commit();
                break;
            case R.id.term_condition_menu4:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.term_setting_container,new TermConditionFragment(3,toolbar,"SETTING"),"MENU4").addToBackStack("TERM_MENU").commit();
                break;
        }
    }
}
