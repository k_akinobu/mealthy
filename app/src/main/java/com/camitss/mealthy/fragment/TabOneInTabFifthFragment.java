package com.camitss.mealthy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.HistoryFeedItemDisplayActivity;
import com.camitss.mealthy.activity.TextWeightDisplayActivity;
import com.camitss.mealthy.adapter.recyclerview.UserFeedsTabOneGridAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Viseth on 4/28/2017.
 */

public class TabOneInTabFifthFragment extends Fragment {
	private RecyclerView feedsRcl;
	public UserFeedsTabOneGridAdapter adapter;
	private boolean hasMore = false;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_e_tab_one_fragment, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		feedsRcl = (RecyclerView) view.findViewById(R.id.rcl_feeds);
		feedsRcl.setNestedScrollingEnabled(false);
		GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 4);
		feedsRcl.setLayoutManager(layoutManager);
		adapter = new UserFeedsTabOneGridAdapter(getContext());
		feedsRcl.setAdapter(adapter);
		getUserFeedInfo(String.format(Locale.ENGLISH, Constant.REQUEST_GET_FEED_USER, getUserId()));
		adapter.setOnItemClickListener(new UserFeedsTabOneGridAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int pos) {
				switch (adapter.getUserFeedsInProfileList().get(pos).getKey()) {
					case "menu.create.post_image":
						HistoryFeedItemDisplayActivity.launch(getActivity(), fromUserFeedsInProfileToFeedItem(adapter.getUserFeedsInProfileList().get(pos)));
						break;
					case "user_history.update.weight":
					case "user_history.update.bt_bp":
					case "user_history.update.bt_bgl":
						TextWeightDisplayActivity.launch(getContext(), adapter.getUserFeedsInProfileList().get(pos));
						break;
				}
			}
		});

	}

	private void getUserFeedInfo(String url) {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject obj = new JSONObject();
		try {
			obj.put("user_id", getUserId());
			obj.put("id", getUserId());
			obj.put("keys", "menu.create.post_image,user_history.update.weight,user_history.update.bt_bp,user_history.update.bt_bgl");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(url, Request.Method.GET, null, obj);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					Type listType = new TypeToken<ArrayList<UserFeedsInProfile>>() {
					}.getType();
					try {
						List<UserFeedsInProfile> userFeedsList = new Gson().fromJson(data.getJSONArray("feeds").toString(), listType);
//						if (null != getChildFragmentManager().getFragments().get(0)) {
//						TabOneInTabFifthFragment fragment = (TabOneInTabFifthFragment) getActivity().getSupportFragmentManager()
//								.findFragmentByTag("tab1");
						JSONObject jObj = data.getJSONObject("paging");
						String nextLink = jObj.getString("next");
						adapter.addUserFeedsInProfileList(userFeedsList, hasMore);
						adapter.notifyDataSetChanged();
						hasMore = jObj.getBoolean("has_more");
						if (hasMore) {
							getUserFeedInfo(Constant.getBaseUrl() + nextLink);
						}
						int extra = 0;

						if ((adapter.getItemCount() % 4) > 0) {
							extra = 1;
						}
//						LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (((Utils.getScreenWidth(getActivity())) / 4) * ((adapter.getItemCount() / 4) + extra)));
//						params.setMargins(10, 10, 10, 10);
//						feedsRcl.setLayoutParams(params);
//						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}

	private int getUserId() {
		return AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
	}

	private FeedItem fromUserFeedsInProfileToFeedItem(UserFeedsInProfile obj) {
		FeedItem item = new FeedItem();
		FeedItem.Menu menu = new FeedItem.Menu();
		FeedItem.FeedItemUser user = new FeedItem.FeedItemUser();
		ArrayList<FeedItem.Comment> cmtList = new ArrayList<FeedItem.Comment>();
		item.setId(obj.getId());
		item.setLike(obj.isLike());
		item.setLikesCount(obj.getLikesCount());
		item.setPrivacyCount(obj.getPrivacyKind());
		menu.setId(obj.getMenu().getId());
		menu.setCgmMenuName(obj.getMenu().getCgmMenuName());
		menu.setCgmShopName(obj.getMenu().getCgmShopName());
		menu.setImage(obj.getMenu().getImage());
		menu.setImageThumb(obj.getMenu().getThumbnailImageUrl());
		menu.setCreatedAt(obj.getMenu().getCreatedAt());
		menu.setCommentCount(obj.getMenu().getCommentsCount());
		FeedItem.FeedItemUser userCmt;
		for (int j = 0; j < obj.getMenu().getComments().size(); j++) {
			FeedItem.Comment cmt = new FeedItem.Comment();
			cmt.setId(obj.getMenu().getComments().get(j).getId());
			cmt.setBody(obj.getMenu().getComments().get(j).getBody());
			userCmt = new FeedItem.FeedItemUser();
			userCmt.setId(obj.getMenu().getComments().get(j).getCmtUser().getId());
			userCmt.setName(obj.getMenu().getComments().get(j).getCmtUser().getName());
			userCmt.setProfileImage(obj.getMenu().getComments().get(j).getCmtUser().getProfileImage());
			cmt.setCmtUser(userCmt);
			cmtList.add(cmt);
		}
		menu.setCommentList(cmtList);
		item.setMenu(menu);
		item.setLikesCount(obj.getLikesCount());
		user.setId(obj.getUser().getId());
		user.setName(obj.getUser().getName());
		user.setProfileImage(obj.getUser().getProfileImageUrl());
		user.setUserKind(obj.getUser().getUserKind());
		item.setFeedItemUser(user);
		return item;
	}
}
