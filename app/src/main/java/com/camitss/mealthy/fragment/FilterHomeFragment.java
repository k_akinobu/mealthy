package com.camitss.mealthy.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FragmentTag;

public class FilterHomeFragment extends Fragment {

	Toolbar toolbar;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("FILTER_HOME");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_filter_home, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.filter_home_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

		homeView.findViewById(R.id.filter_by_type).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				FilterTypeFragment filterTypeFragment = new FilterTypeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_right, R.anim.in_from_right, R.anim.out_to_left);
				fragmentTransaction.replace(R.id.main_container,filterTypeFragment,"FILTER_TYPE").addToBackStack("FILTER_HOME");
				fragmentTransaction.commit();
			}
		});

		homeView.findViewById(R.id.filter_by_price).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				FilterPriceFragment filterTypeFragment = new FilterPriceFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_right, R.anim.in_from_right, R.anim.out_to_left);
				fragmentTransaction.replace(R.id.main_container,filterTypeFragment,"FILTER_PRICE").addToBackStack("FILTER_HOME");
				fragmentTransaction.commit();
			}
		});

		homeView.findViewById(R.id.filter_by_advice).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				WelcomeFragment welcomeFragment = new WelcomeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_right, R.anim.in_from_right, R.anim.out_to_left);
				fragmentTransaction.replace(R.id.main_container,welcomeFragment,"FILTER_ADVICE").addToBackStack("FILTER_HOME");
				fragmentTransaction.commit();
			}
		});

//		toolbar.setTitle(getResources().getString(R.string.filter_setting));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		return homeView;
	}
}