package com.camitss.mealthy.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.activity.NotificationActivity;
import com.camitss.mealthy.activity.SettingInProfileActivity;
import com.camitss.mealthy.activity.UsersFollowersActivity;
import com.camitss.mealthy.activity.UsersFollowingActivity;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.object.CalorieDetail;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CircleTransformation;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Viseth on 4/26/2017.
 */

public class MainTabFifthFragment extends Fragment implements View.OnClickListener {
	private Gson mGson = new Gson();
	private ImageView userProfile;
	private TextView userName;
	private TextView userFollowing;
	private TextView userFollower;
	private TextView userDietType;
	protected FragmentTabHost tabHost;
	View v1, v2;
	private TextView point;
	private TextView pointDescript;
	private TextView userWeight;
	private TextView userTargetWeight;
	private TextView culcRemainWeight;
	private TextView chartValue;
	private TextView stepGoal;
	private TextView remainStep;
	private TextView userCaloriePerday;
	private final String largehypen = "ー";
	private final String smallhypen = "-";
	private String value = "";
	private final int term = 1;
	private ImageView imageBorder;
	private ProgressDialog progressDialog;
	private TextView textUnreadNotiNum;
	private AppManager appManager;
	private boolean firstLoad = true;
	private int selectedTab;
	protected TextView txtNutriNotiNum;

	public int getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(int selectedTab) {
		this.selectedTab = selectedTab;
	}

	public MainTabFifthFragment() {//int selectedTab) {
//		this.selectedTab = selectedTab;
	}

	public static MainTabFifthFragment newInstance(int selectedTab) {
		MainTabFifthFragment fragment = new MainTabFifthFragment();
		Bundle bundle = new Bundle(1);
		bundle.putInt("selectedTab", selectedTab);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSelectedTab(getArguments().getInt("selectedTab"));
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentTag.getInstant().setTAG("MAIN_TAB_FIFTH");
		View view = inflater.inflate(R.layout.fragment_tab_fifth, container, false);
		v1 = inflater.inflate(R.layout.layout_tab_traingle, container, false);
		v2 = inflater.inflate(R.layout.layout_tab_traingle_two, container, false);
		progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatProgressDialogStyle);
		Utils.showProgressDialog(getActivity(), progressDialog);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		tabHost = (FragmentTabHost) view.findViewById(R.id.tab_host_mealthy);
		tabHost.setup(getActivity(), getActivity().getSupportFragmentManager(), R.id.tab_content);
		userProfile = (ImageView) view.findViewById(R.id.user_profile);
		imageBorder = (ImageView) view.findViewById(R.id.img_border);
		imageBorder.setVisibility(View.GONE);
		ImageView notiIcon = (ImageView) view.findViewById(R.id.noti_icon);
		ImageView settingIcon = (ImageView) view.findViewById(R.id.setting_icon);
		userName = (TextView) view.findViewById(R.id.user_name);
		userFollowing = (TextView) view.findViewById(R.id.user_following);
		userFollower = (TextView) view.findViewById(R.id.user_follower);
		userDietType = (TextView) view.findViewById(R.id.user_diet_type);
		point = (TextView) view.findViewById(R.id.txt_point);
		pointDescript = (TextView) view.findViewById(R.id.txt_point_descript);
		userWeight = (TextView) view.findViewById(R.id.txt_user_weight);
		userTargetWeight = (TextView) view.findViewById(R.id.txt_user_target_weight);
		culcRemainWeight = (TextView) view.findViewById(R.id.txt_culcremainweight);
		chartValue = (TextView) view.findViewById(R.id.txt_value);
		stepGoal = (TextView) view.findViewById(R.id.txt_step_goal);
		remainStep = (TextView) view.findViewById(R.id.txt_remain_step);
		userCaloriePerday = (TextView) view.findViewById(R.id.txt_calorie_per_day);
		textUnreadNotiNum = (TextView) view.findViewById(R.id.txt_noti_unread_number);

		getUnreadNoti();
		getUserHistory();
		getUserInfo();
		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(v1), TabOneInTabFifthFragment.class, null);
		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(v2), TabTwoInTabFifthFragment.class, null);//getString(R.string.e_tab2_title), ContextCompat.getDrawable(getActivity(), R.drawable.tab_traingle_indicator)
		tabHost.getTabWidget().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
		tabHost.getTabWidget().getChildAt(0).getLayoutParams().width = Utils.getScreenWidth(getActivity()) / 2;
		tabHost.getTabWidget().getChildAt(1).getLayoutParams().width = Utils.getScreenWidth(getActivity()) / 2;
		userFollower.setOnClickListener(this);
		userFollowing.setOnClickListener(this);
		notiIcon.setOnClickListener(this);
		settingIcon.setOnClickListener(this);
		appManager = (AppManager) getApplicationContext();
		appManager.setMainTabFifthFragment(this);

		tabHost.setCurrentTab(getSelectedTab());
		txtNutriNotiNum = (TextView) tabHost.getTabWidget().getChildAt(1).findViewById(R.id.txt_noti_unread_number);
		int numNutriNoti = AppSharedPreferences.getConstant(getActivity()).getNumNutriNotiPref();

		setNutriNotiNum(numNutriNoti);
		tabHost.getTabWidget().getChildAt(1).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("onTabChanged", v.getId() + "is clicked");
				tabHost.setCurrentTab(1);
				AppSharedPreferences.getConstant(getApplicationContext()).setNumNutriNotiPref(0);
				setNutriNotiNum(0);
			}
		});
	}

	protected void setNutriNotiNum(int numNutriNoti) {
		if (null != txtNutriNotiNum) {
			txtNutriNotiNum.setVisibility(numNutriNoti > 0 ? View.VISIBLE : View.INVISIBLE);
			txtNutriNotiNum.setText(String.valueOf(numNutriNoti));
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!firstLoad && AppManager.getInstance().isRefresh()) {
			AppManager.getInstance().set5TabRefresh(false);
			((MealthyActivity) getActivity()).setupTabFifthFragment(0);
		}
	}

	public void getUserHistory() {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject body = new JSONObject();
		try {
			body.put("kind", 2);
			body.put("from", term);
			body.put("to", term);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.REQUEST_GET_USER_HISTORY, getUserId()), Request.Method.GET, null, body);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					try {
						JSONArray histories = data.getJSONArray("user_histories");
						if (histories.length() > 0) {
							value = histories.getJSONObject(0).getString("value");
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void getUserInfo() {
		RequestHandler request = new RequestHandler(getActivity());
		request.jsonRequest(String.format(Locale.ENGLISH, Constant.getBaseUrl() + "/users/%d", getUserId()), Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				progressDialog.dismiss();
				if (isSuccess) {
					try {
						User user = mGson.fromJson(data.getJSONObject("user").toString(), User.class);
						RealmHelper.with(getContext()).save(user);
						setupView(user);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					User user = RealmHelper.with(getContext()).getRealm().where(User.class).findFirst();
					setupView(user);
				}

			}
		});
	}

	private int getUserId() {
		return AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
	}

	private void setupView(User user) {
		try {
			if (null != user) {
				firstLoad = false;
				Glide.with(getActivity()).load(user.getProfileImageUrl()).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).transform(new CircleTransformation(getActivity())).into(userProfile);
				imageBorder.setVisibility(null != user.getProfileImageUrl() && !user.getProfileImageUrl().trim().isEmpty() ? View.VISIBLE : View.GONE);
				userName.setText(user.getName());
				userFollower.setText(user.getFollowersCount() + getString(R.string.follower));
//			userFollower.setVisibility(user.getFollowersCount() > 0 ? View.VISIBLE : View.INVISIBLE);
				userFollowing.setText(user.getFollowingAccount() + getString(R.string.following));
//			userFollowing.setVisibility(user.getFollowingAccount() > 0 ? View.VISIBLE : View.INVISIBLE);
				userDietType.setText(null != AppSharedPreferences.getConstant(getApplicationContext()) ? AppSharedPreferences.getConstant(getApplicationContext()).getUserDietTypeFromDiagnosticResult() : "");
				point.setText(user.getPoint() + "");
				pointDescript.setText(String.format(Locale.ENGLISH, "あと%d回\n栄養診断可能", Integer.valueOf(user.getPoint() / 100)));
				userWeight.setText(String.format(Locale.JAPANESE,"%.1fkg",user.getWeight()));
				if (AppSharedPreferences.getConstant(getContext()).isFromUpdateTargetWeightMiddleMenuScreen()) {
					Log.d("wiiiii", AppSharedPreferences.getConstant(getContext()).isFromUpdateTargetWeightMiddleMenuScreen() + "");
					userTargetWeight.setText(String.format(Locale.ENGLISH, "目標%.1fkg", AppSharedPreferences.getConstant(getApplicationContext()).getPreviousTargetWeightPref()));
				} else {
					Log.d("wiiiii", AppSharedPreferences.getConstant(getContext()).isFromUpdateTargetWeightMiddleMenuScreen() + "");
					double targetweight = user.getWeight() - getDietNum(user.getDietId());
					userTargetWeight.setText(String.format(Locale.ENGLISH, "目標%.1fkg", targetweight));// user.getTargetWeight()
					AppSharedPreferences.getConstant(getApplicationContext()).setPreviousTargetWeightPref((float) targetweight);
				}
				userCaloriePerday.setText(getCaloriesPerday(user.getDietId(), user.getCalorieDetails()));
				stepGoal.setText(String.format("目標%,d歩/日", getStepGoal(user.getSex())));
				culcRemainWeight.setText(getCulcRemainWeight(userTargetWeight.getText().toString().trim(), user.getWeight()));
				remainStep.setText(String.format("あと%,d歩", getRemainStep(user.getSex())));
				chartValue.setText(!value.trim().isEmpty() ? value : "0");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private double getDietNum(int dietId) {
		if (dietId == 1) return 0.0;
		else if (dietId == 2) return 1.0;
		else if (dietId == 3) return 2.0;
		else if (dietId == 4) return 3.0;
		else return 0.0;
	}

	private String getCaloriesPerday(int dietId, List<CalorieDetail> calorieDetails) {
		try {
			switch (dietId) {
				case 1:
					return String.format("%,d", calorieDetails.get(0).getCaloriePerDay());
				case 2:
					return String.format("%,d", calorieDetails.get(1).getCaloriePerDay());
				case 3:
					return String.format("%,d", calorieDetails.get(2).getCaloriePerDay());
				default:
					return String.format("%,d", calorieDetails.get(1).getCaloriePerDay());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private String getCulcRemainWeight(String targetWeight, double weight) {
		String s1 = targetWeight.split("目標", 2)[1];
		double tw = Double.valueOf(s1.split("kg", 2)[0]);
		Log.d("wiiiiiii", s1 + ":" + tw);
		double remain = tw - weight;
		if (remain >= 0) {
			return "達成★";//largehypen;
		}
		return String.format(Locale.ENGLISH, "あと%.1fkg↓", remain);
	}

	private int getStepGoal(int sex) {
		switch (sex) {
			case Constant.FEMALE:
				return 8300;
			case Constant.MALE:
				return 9200;
			default:
				return 9000;
		}
	}

	private int getRemainStep(int sex) {
		int i = !value.trim().isEmpty() ? Integer.valueOf(value) : 0;
		return getStepGoal(sex) - i;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.user_follower:
				UsersFollowersActivity.launch(getActivity(), getUserId());
				break;
			case R.id.user_following:
				UsersFollowingActivity.launch(getActivity(), getUserId());
				break;
			case R.id.noti_icon:
				NotificationActivity.launch(getContext());
				break;
			case R.id.setting_icon:
				SettingInProfileActivity.launch(getContext());
				break;
		}
	}

	private void getUnreadNoti() {
		RequestHandler request = new RequestHandler(getActivity());
		request.jsonRequest(Constant.REQUEST_GET_USER_NOTIFICATION_UNREAD, Request.Method.GET, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				Log.d("unreadnoti", data.toString());
				if (isSuccess) {
					try {
						int num = data.getJSONObject("user_notifications").getInt("total_unread_count");
						int numNutriNoti = data.getJSONObject("user_notifications").getInt("nutritionist_unread_count");
						AppSharedPreferences.getConstant(getActivity()).setNumNutriNotiPref(numNutriNoti);
						if (num > 0) {
							ShortcutBadger.applyCount(getActivity(), num);
						} else ShortcutBadger.removeCount(getApplicationContext());
						textUnreadNotiNum.setText(num > 0 ? String.valueOf(num) : "");
						textUnreadNotiNum.setVisibility(num > 0 ? View.VISIBLE : View.INVISIBLE);


					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

}
