package com.camitss.mealthy.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.NearImageAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.NearbyObj;

public class NearestShopCategoryFragment extends Fragment {

	Toolbar toolbar;
	private GridView gridview;
	FragmentTransaction fragmentTransaction;
	FragmentManager fragmentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("NEAR_CATEGORY");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_near_stop_category, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.nearest_stop_category_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.home);
		toolbar.setTitle(getResources().getString(R.string.genre_title));
		MenuItem item = toolbar.getMenu().getItem(0);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				MealthyLog.infoLog("action", "search");
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				fragmentTransaction.hide(NearestShopCategoryFragment.this);
				SearchHomeFragment searchHomeFragment = new SearchHomeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.add(R.id.main_container, searchHomeFragment, "SEARCH_HOME").addToBackStack("NEAR");
				fragmentTransaction.commit();
				return false;
			}
		});
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		fragmentManager = getFragmentManager();
		GridView gridview = (GridView) homeView.findViewById(R.id.gridview);
		gridview.setAdapter(new NearImageAdapter(getActivity(), mThumbIds, mTitle));


		gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				NearbyObj nearbyObj = new NearbyObj();
				nearbyObj.setMenu_name("ジャンル");
				nearbyObj.setTypeSearch(5);
				nearbyObj.setTag(mTag[position]);
				fragmentTransaction = fragmentManager.beginTransaction();
				NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.main_container, nearestShopFragment, "NUTRITION").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();
			}
		});


		return homeView;
	}

	private Integer[] mThumbIds = {
			R.drawable.ic_near_1_meat, R.drawable.ic_near_2_fish, R.drawable.ic_near_3_vege,
			R.drawable.ic_near_4_noodle, R.drawable.ic_near_5_wa, R.drawable.ic_near_6_you,
			R.drawable.ic_near_7_italian, R.drawable.ic_near_8_china,
			R.drawable.ic_near_9_ethnic, R.drawable.ic_near_10_humberger
	};
	private String[] mTitle = {
			"肉料理", "魚料理", "野菜料理", "麺類", "和食", "洋食", "イタリアン", "中華", "エスニック", "パン類"
	};
	private String[] mTag = {
			"鶏肉,豚肉,牛肉,ローストビーフ,ステーキ,牛かつ,牛たん,とんかつ,唐揚げ,その他肉料理", "さば,さんま,あじ,いわし,鮭,ツナ,海老,焼魚,海鮮丼,寿司,うな丼,その他魚料理",
			"豆腐,納豆,サラダ,マクロビ,ベジタリアン,ヴィーガン", "ラーメン,担々麺,そば,うどん,フォー,ビーフン,焼そば,その他麺料理",
			"定食,親子丼,牛丼,豚丼,かつ丼,天丼,天ぷら,その他和食", "オムライス,ハンバーグ,スープ,カレー,ハヤシライス,シチュー,グラタン,海老フライ,その他洋食",
			"パスタ,ピザ,その他イタリアン", "麻婆豆腐,餃子,中華粥,炒飯,肉まん,焼売,点心,その他中華",
			"韓国,タイ,ベトナム,シンガポール,インド,その他アジア,メキシコ,中東,南米,アフリカ", "ハンバーガー,サンドイッチ,ホットドッグ,キッシュ,ベーグル,パン,パンケーキ,ドーナツ,その他パン類"
	};

}
