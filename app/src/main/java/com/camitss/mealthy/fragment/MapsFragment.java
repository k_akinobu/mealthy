package com.camitss.mealthy.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.NearbyObj;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsFragment extends Fragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	private GoogleMap mMap;
	private MapView mapView;
	private GoogleApiClient mGoogleApiClient;
	private Location mLastLocation;
	private LatLng latLng;
	private LocationRequest mLocationRequest;
	private Toolbar toolbar;
	private NearbyObj nearbyObj;
	private LatLng shopLatLng;
	private ImageView userLocation, shopLocation;


	public MapsFragment(NearbyObj nearbyObj) {

		this.nearbyObj = nearbyObj;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.activity_maps, container, false);
		shopLocation = (ImageView) homeView.findViewById(R.id.shop_location);
		userLocation = (ImageView) homeView.findViewById(R.id.user_location);
		toolbar = (Toolbar) homeView.findViewById(R.id.nearest_stop_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.home);
		toolbar.setTitle(nearbyObj.getShopName());
		MenuItem item = toolbar.getMenu().getItem(0);
		item.setVisible(false);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		mapView = (MapView) homeView.findViewById(R.id.mapView);
		try {
			mapView.onCreate(savedInstanceState);
		} catch (Exception e) {
		}
		mapView.onResume();// needed to get the map to display immediately
		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		mapView.getMapAsync(this);
		shopLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(shopLatLng).zoom(16).build();
					mMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		userLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(latLng).zoom(16).build();
					mMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				} catch (Exception e) {

				}
			}
		});
		return homeView;
	}


	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		mMap.setMyLocationEnabled(true);
		mMap.getUiSettings().setCompassEnabled(false);
		buildGoogleApiClient();
		mGoogleApiClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		latLng = new LatLng(location.getLatitude(), location.getLongitude());
	}

	@Override
	public void onConnected(Bundle bundle) {
		if (nearbyObj.getMenu() != null) {
			shopLatLng = new LatLng(nearbyObj.getMenu().getShopLat(), nearbyObj.getMenu().getShopLng());
		} else if (nearbyObj.getLatitude() != 0) {
			shopLatLng = new LatLng(nearbyObj.getLatitude(), nearbyObj.getLongitude());
		}
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(shopLatLng).zoom(16).build();
//		mMap.animateCamera(CameraUpdateFactory
//				.newCameraPosition(cameraPosition));
		MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.position(shopLatLng);
		markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mealthy_shop_marker));
		mMap.addMarker(markerOptions);
		mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
				mGoogleApiClient);
//		if (mLastLocation != null) {
//			latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//			MarkerOptions markerOptions = new MarkerOptions();
//			markerOptions.position(latLng);
//			markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mealthy_shop_marker));
//
//		}

		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(5000); //5 seconds
		mLocationRequest.setFastestInterval(3000); //3 seconds
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setSmallestDisplacement(1F); //1/10 meter
		mLocationRequest.setSmallestDisplacement(20F);
		try {
			if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				return;
			}
		} catch (Exception e) {

		}
//		mMap.animateCamera(CameraUpdateFactory
//				.newCameraPosition(cameraPosition));
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}
}
