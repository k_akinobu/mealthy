package com.camitss.mealthy.fragment;


import android.app.ProgressDialog;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.adapter.MenuRecycleAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.listener.MenuClickListener;
import com.camitss.mealthy.listener.ShopClickListener;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.NearbyObj;
import com.camitss.mealthy.object.RequestShopObj;
import com.camitss.mealthy.object.Shop;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.EndlessRecyclerOnScrollListener;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersTouchListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.utils.URLEncodedUtils;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class NearestShopFragment extends Fragment implements View.OnClickListener, ShopClickListener, MenuClickListener {
	public Gson mGson;
	private Toolbar toolbar;
	private List<Shop> listShop;
	private List<MealthyMenu> listMenuData;
	private MenuRecycleAdapter menuRecycleAdapter;
	private RecyclerView listMenuView;
	private TextView noDataLabel;
	private ProgressDialog progressDialog;
	private Button option1, option2, option3, option4;
	private String moreURL = "";
	private List<RequestShopObj> reShopLists;
	private List<Integer> filterList = new ArrayList<Integer>();
	private NearbyObj nearbyObj;
	private String keyJson;
	private FragmentTransaction fragmentTransaction;
	private FragmentManager fragmentManager;
	private StickyRecyclerHeadersDecoration headersDecor;
	private LinearLayoutManager mLayoutManager;
	private MealthyMenu tmpMenuFav;
	private NearestShopFragment shopFragment;

	public NearestShopFragment(NearbyObj nearbyObj) {
		this.nearbyObj = nearbyObj;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		mGson = new Gson();
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("NEAR");
		progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_near, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.nearest_stop_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.home);
		noDataLabel = (TextView) homeView.findViewById(R.id.no_data_message);
		option1 = (Button) homeView.findViewById(R.id.btn_option1);
		option2 = (Button) homeView.findViewById(R.id.btn_option2);
		option3 = (Button) homeView.findViewById(R.id.btn_option3);
		option4 = (Button) homeView.findViewById(R.id.btn_option4);
		Utils.showProgressDialog(getActivity(), progressDialog, getString(R.string.loading_near_food));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		fragmentManager = getFragmentManager();
		MenuItem item = toolbar.getMenu().getItem(0);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				MealthyLog.infoLog("action", "search");
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				fragmentTransaction.hide(NearestShopFragment.this);
				SearchHomeFragment searchHomeFragment = new SearchHomeFragment();
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.add(R.id.main_container, searchHomeFragment, "SEARCH_HOME").addToBackStack("NEAR");
				fragmentTransaction.commit();
				return false;
			}
		});
		shopFragment = this;
		listMenuView = (RecyclerView) homeView.findViewById(R.id.near_list_menu);
		mLayoutManager = new LinearLayoutManager(getActivity());
		mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		listMenuView.setLayoutManager(mLayoutManager);
		listMenuView.setHasFixedSize(true);
		listShop = new ArrayList<>();
		listMenuData = new ArrayList<>();
		menuRecycleAdapter = new MenuRecycleAdapter(getActivity(), listMenuData);
		menuRecycleAdapter.setClickListener(this);
		menuRecycleAdapter.setMenuClickListener(this);
		listMenuView.setAdapter(menuRecycleAdapter);
		headersDecor = new StickyRecyclerHeadersDecoration(menuRecycleAdapter);

		StickyRecyclerHeadersTouchListener touchListener =
				new StickyRecyclerHeadersTouchListener(listMenuView, headersDecor);
		touchListener.setOnHeaderClickListener(
				new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
					@Override
					public void onHeaderClick(View header, int position, long headerId) {
						try {
							TextView txt = (TextView) header.findViewById(R.id.shop_name_header);
							MealthyMenu tmpMenu;
							if (position == 0) {
								tmpMenu = listMenuData.get(mLayoutManager.findFirstVisibleItemPosition());
								linkShopHandler(tmpMenu);
							} else {
								tmpMenu = findData(txt.getText().toString());
								if (tmpMenu != null) {
									linkShopHandler(tmpMenu);
								}
							}

						} catch (Exception e) {
							e.printStackTrace();
						}


					}
				});
		listMenuView.addOnItemTouchListener(touchListener);
		menuRecycleAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
			@Override
			public void onChanged() {
				headersDecor.invalidateHeaders();
			}
		});
		listMenuView.addItemDecoration(headersDecor);
		listMenuView.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int current_page) {
				if (moreURL.length() > 0) {
					if (nearbyObj.getTypeSearch() == 1 || nearbyObj.getTypeSearch() == 3) {
						requestData("", null, false);
					} else if (nearbyObj.getTypeSearch() == 6) {
//						requestData("/shops", TagMenu2LayerBodyDataHandler(nearbyObj.getShopName()), false);
						requestData("", null, false);
					} else {
//						requestData("/tags/menus", tagMenuBodyDataHandler(nearbyObj.getTag()), false);
						requestData("", null, false);
					}
				}
			}
		});

		titleHandler();
		clickhandler();


		return homeView;
	}

	private MealthyMenu findData(String data) {
		for (int i = 0; i < listMenuData.size(); i++) {
			if (listMenuData.get(i).getShopName().equalsIgnoreCase(data)) {
				return listMenuData.get(i);
			}
		}

		return null;
	}

	private MealthyMenu findData(long data) {
		for (int i = 0; i < listMenuData.size(); i++) {
			if (listMenuData.get(i).getShop_id() == data) {
				return listMenuData.get(i);
			}
		}
		return null;
	}

	private void titleHandler() {
		if (nearbyObj.getTypeSearch() == 1) {
			toolbar.setTitle(getResources().getString(R.string.nearby_title));
			requestData("/shops", shopBodyDataHandler(), false);
		} else {
			toolbar.setTitle(nearbyObj.getMenu_name() + "@" + getResources().getString(R.string.nearby_title));
			if (nearbyObj.getTypeSearch() == 2) {
				requestData("/tags/menus", tagMenuBodyDataHandler(nearbyObj.getTag()), false);
			} else if (nearbyObj.getTypeSearch() == 3) {
				requestData("/shops", favoriteMenuBodyDataHandler(), false);
			} else if (nearbyObj.getTypeSearch() == 5) {
				requestData("/tags/menus", tagMenuBodyDataHandler(nearbyObj.getTag()), false);
			} else if (nearbyObj.getTypeSearch() == 6) {
				requestData("/shops", TagMenu2LayerBodyDataHandler(nearbyObj.getShopName()), false);
			}
		}
	}

	private void clickhandler() {
		filterList.add(R.id.option_1);
		filterList.add(R.id.option_2);
		filterList.add(R.id.option_3);
		filterList.add(R.id.option_4);
		reShopLists = new ArrayList<>();
		reShopLists.add(new RequestShopObj(0, false, ""));
		reShopLists.add(new RequestShopObj(1, false, ""));
		reShopLists.add(new RequestShopObj(2, false, ""));
		reShopLists.add(new RequestShopObj(3, false, ""));
		reShopLists.add(new RequestShopObj(4, false, ""));

		option1.setOnClickListener(this);
		option2.setOnClickListener(this);
		option3.setOnClickListener(this);
		option4.setOnClickListener(this);

	}

	private void requestData(final String requestPath, JSONObject body, final boolean renew) {
		final RequestHandler requestHandler = new RequestHandler(getContext());
		if (renew) {
			listShop.clear();
			listMenuData.clear();
		}
		progressDialog.show();
		try {
			JSONObject header = new JSONObject();
			header.put("X-Mealthy-Token", "52B016F5-AC44-4CAB-B080-99756F88BBF6");
			requestHandler.jsonRequest(Constant.BASE_URL + requestPath + moreURL, Request.Method.GET, header, body);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {

					progressDialog.dismiss();
					Log.i("XXXXXXXXX", data.toString());
					try {
						switch (nearbyObj.getTypeSearch()) {
							case 1:
								keyJson = "shops";
								break;
							case 2:
								keyJson = "tags";
								break;
							case 3:
								keyJson = "shops";
								break;
							case 4:
								keyJson = "shops";
								break;

							case 5:
								keyJson = "tags";
								break;
							case 6:
								keyJson = "shops";
								break;
						}
						if (listShop.size() > 0) {
							listShop.clear();
						}
						for (int i = 0; i < data.getJSONArray(keyJson).length(); i++) {
							Shop shop = mGson.fromJson(data.getJSONArray(keyJson).getJSONObject(i).toString(), Shop.class);
							List<MealthyMenu> listMenu = shop.getMenus();
							float[] res = new float[3];
							Location.distanceBetween(35.6894875, 139.6917114, shop.getLatitude(), shop.getLongitude(), res);
							shop.setDistance(Math.round(res[0]) + "");
							for (int j = 0; j < listMenu.size(); j++) {
								listMenu.get(j).setShopName(shop.getName());
								listMenu.get(j).setShop_id(shop.getId());
								listMenu.get(j).setDistance(Math.round(res[0]) + "");
								listMenu.get(j).setAddress(shop.getAddress());
								listMenu.get(j).setPhoneNumber(shop.getTel());
								listMenu.get(j).setShopLat(shop.getLatitude());
								listMenu.get(j).setShopLng(shop.getLongitude());
								if (j == listMenu.size() - 1) {
									listMenu.get(j).setLastIndex(true);
								}
								tmpMenuFav = RealmHelper.with(getActivity()).getMealthyMenuFavById(listMenu.get(j).getId());
								if (tmpMenuFav != null) {
									if (tmpMenuFav.isFavorith()) {
										listMenu.get(j).setFavorith(true);
										listMenu.get(j).setFavId(tmpMenuFav.getFavId());
									}
								}
							}

							listShop.add(shop);
						}
						for (int j = 0; j < listShop.size(); j++) {
							listMenuData.addAll(listShop.get(j).getMenus());
						}


						menuRecycleAdapter.notifyDataSetChanged();
						if (listMenuData.size() == 0) {
							noDataLabel.setVisibility(View.VISIBLE);
							listMenuView.setVisibility(View.INVISIBLE);
						} else {
							noDataLabel.setVisibility(View.INVISIBLE);
							listMenuView.setVisibility(View.VISIBLE);
						}
						try {
							moreURL = data.getJSONObject("paging").getString("next");
							if (data.getJSONObject("paging").getString("next").equalsIgnoreCase("null") || data.getJSONObject("paging").getString("next") == null) {
								moreURL = "";
							} else {
								moreURL = data.getJSONObject("paging").getString("next");
							}
							if (renew) {
								moreURL = "";
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (JSONException e) {
						e.printStackTrace();
					} catch (IndexOutOfBoundsException ofbe) {

					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void requestDataFilter(final String requestPath, JSONObject body, final boolean renew) {
		final RequestHandler requestHandler = new RequestHandler(getContext());
		if (renew) {
			listShop.clear();
			listMenuData.clear();
			progressDialog.show();
			menuRecycleAdapter.notifyDataSetChanged();
		}
		try {
			JSONObject header = new JSONObject();
			header.put("X-Mealthy-Token", "52B016F5-AC44-4CAB-B080-99756F88BBF6");
			requestHandler.jsonRequest(Constant.BASE_URL + requestPath + moreURL, Request.Method.GET, header, body);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {

					progressDialog.dismiss();
					Log.i("XXXXXXXXX111", data.toString());
					try {
						switch (nearbyObj.getTypeSearch()) {
							case 1:
								keyJson = "shops";
								break;
							case 2:
								keyJson = "tags";
								break;
							case 3:
								keyJson = "shops";
								break;
							case 4:
								keyJson = "shops";
								break;

							case 5:
								keyJson = "tags";
								break;
							case 6:
								keyJson = "shops";
								break;
						}

						for (int i = 0; i < data.getJSONArray(keyJson).length(); i++) {
							Shop shop = mGson.fromJson(data.getJSONArray(keyJson).getJSONObject(i).toString(), Shop.class);
							List<MealthyMenu> listMenu = shop.getMenus();
							float[] res = new float[3];
							Location.distanceBetween(35.6894875, 139.6917114, shop.getLatitude(), shop.getLongitude(), res);
							shop.setDistance(Math.round(res[0]) + "");
							for (int j = 0; j < listMenu.size(); j++) {
								listMenu.get(j).setShopName(shop.getName());
								listMenu.get(j).setShop_id(shop.getId());
								listMenu.get(j).setDistance(Math.round(res[0]) + "");
								listMenu.get(j).setAddress(shop.getAddress());
								listMenu.get(j).setPhoneNumber(shop.getTel());
								listMenu.get(j).setShopLat(shop.getLatitude());
								listMenu.get(j).setShopLng(shop.getLongitude());
								if (j == listMenu.size() - 1) {
									listMenu.get(j).setLastIndex(true);
								}
								tmpMenuFav = RealmHelper.with(getActivity()).getMealthyMenuFavById(listMenu.get(j).getId());
								if (tmpMenuFav != null) {
									if (tmpMenuFav.isFavorith()) {
										listMenu.get(j).setFavorith(true);
										listMenu.get(j).setFavId(tmpMenuFav.getFavId());
									}
								}
							}
							listShop.add(shop);
						}
						for (int j = 0; j < listShop.size(); j++) {
							listMenuData.addAll(listShop.get(j).getMenus());
						}


						menuRecycleAdapter.notifyDataSetChanged();
						if (listMenuData.size() == 0) {
							noDataLabel.setVisibility(View.VISIBLE);
							listMenuView.setVisibility(View.INVISIBLE);
						}
						try {
							moreURL = data.getJSONObject("paging").getString("next");
							if (data.getJSONObject("paging").getString("next").equalsIgnoreCase("null") || data.getJSONObject("paging").getString("next") == null) {
								moreURL = "";
							} else {
								moreURL = data.getJSONObject("paging").getString("next");
							}
							if (renew) {
								moreURL = "";
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (JSONException e) {
						e.printStackTrace();
					} catch (IndexOutOfBoundsException ofbe) {

					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private JSONObject shopBodyDataHandler() {
		JSONObject body = new JSONObject();
		try {
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject tagMenuBodyDataHandler(String data) {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("nut_recommend_only", 0);
			body.put("tag", URLEncoder.encode(data, "utf-8"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject TagMenu2LayerBodyDataHandler(String tag) {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("tag", tag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject shopLowBodyDataHandler() {
		JSONObject body = new JSONObject();
		try {
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("distance", 1500);
			body.put("nut_recommend_only", 0);
			body.put("tag", URLEncoder.encode("低糖質[C:~40g],糖質オフ[C:~10g]", "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject nutritionOrientedBodyDataHandler() {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("nut_recommend_only", 0);
			body.put("tag", URLEncoder.encode("高たんぱく,低糖質[C:~40g],糖質オフ[C:~10g],野菜,マクロビ,ベジタリアン,ヴィーガン,グルテンフリー,薬膳,豆腐", "utf-8"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}


	private JSONObject favoriteBodyDataHandler() {
		JSONObject body = new JSONObject();
		try {
			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("nut_recommend_only", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject favoriteMenuBodyDataHandler() {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", 35.6894875);
//			body.put("longitude", 139.6917114);
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("shop_group_kind", 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}


	private JSONObject NutritionistsBodyDataHandler(String tag) {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("nut_recommend_only", 1);
			body.put("tag", URLEncoder.encode(tag, "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject EatOutBodyDataHandler(String tag) {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("takeout_flag", 1);
			body.put("tag", URLEncoder.encode(tag, "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}

	private JSONObject TakeoutBodyDataHandler(String tag) {
		JSONObject body = new JSONObject();
		try {
//			body.put("latitude", ((MealthyActivity) getActivity()).getLatitude());
//			body.put("longitude", ((MealthyActivity) getActivity()).getLatitude());
			body.put("latitude", 35.6894875);
			body.put("longitude", 139.6917114);
			body.put("distance", 1500);
			body.put("takeout_flag", 2 | 3);
			body.put("tag", URLEncoder.encode(tag, "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return body;
	}


	@Override
	public void onClick(View v) { //tttt
		switch (v.getId()) {
			case R.id.btn_option1:

				option2.setTextColor(getResources().getColor(R.color.black));
				option2.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option3.setTextColor(getResources().getColor(R.color.black));
				option3.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option4.setTextColor(getResources().getColor(R.color.black));
				option4.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				operateFilter(1);

				if (reShopLists.get(1).isSelected()) {
					reShopLists.get(1).setSelected(false);
					listShop.clear();
					listMenuData.clear();
					titleHandler();
					option1.setTextColor(getResources().getColor(R.color.black));
					option1.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				} else {
					reShopLists.get(1).setSelected(true);
					moreURL = "";
					if (nearbyObj.getTypeSearch() == 2 || nearbyObj.getTypeSearch() == 5) {
						requestData("/tags/menus", NutritionistsBodyDataHandler(nearbyObj.getTag()), true);
					} else {
						requestData("/shops", NutritionistsBodyDataHandler(""), true);
					}

					option1.setTextColor(getResources().getColor(R.color.white));
					option1.setBackground(getResources().getDrawable(R.drawable.option_click_btn_background));
				}


//				setUpFilter(R.id.btn_option1);
				break;
			case R.id.btn_option2:

				option1.setTextColor(getResources().getColor(R.color.black));
				option1.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option3.setTextColor(getResources().getColor(R.color.black));
				option3.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option4.setTextColor(getResources().getColor(R.color.black));
				option4.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				operateFilter(2);
				if (reShopLists.get(2).isSelected()) {
					reShopLists.get(2).setSelected(false);
					listShop.clear();
					listMenuData.clear();
					titleHandler();
					option2.setTextColor(getResources().getColor(R.color.black));
					option2.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				} else {
					reShopLists.get(2).setSelected(true);
					moreURL = "";
					if (nearbyObj.getTypeSearch() == 2 || nearbyObj.getTypeSearch() == 5) {
						requestData("/favorites/tags/menus", null, true);
					} else {
						requestData("/favorites/shops/menus", null, true);
					}
					option2.setTextColor(getResources().getColor(R.color.white));
					option2.setBackground(getResources().getDrawable(R.drawable.option_click_btn_background));
				}

				break;
			case R.id.btn_option3:
				option2.setTextColor(getResources().getColor(R.color.black));
				option2.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option1.setTextColor(getResources().getColor(R.color.black));
				option1.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option4.setTextColor(getResources().getColor(R.color.black));
				option4.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				operateFilter(3);
				if (reShopLists.get(3).isSelected()) {
					reShopLists.get(3).setSelected(false);
					listShop.clear();
					listMenuData.clear();
					titleHandler();
					option3.setTextColor(getResources().getColor(R.color.black));
					option3.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				} else {
					reShopLists.get(3).setSelected(true);
					moreURL = "";
					if (nearbyObj.getTypeSearch() == 2 || nearbyObj.getTypeSearch() == 5) {
						requestData("/tags/menus", EatOutBodyDataHandler(nearbyObj.getTag()), true);
					} else {
						requestData("/shops", EatOutBodyDataHandler(""), true);
					}
					option3.setTextColor(getResources().getColor(R.color.white));
					option3.setBackground(getResources().getDrawable(R.drawable.option_click_btn_background));
				}


				break;
			case R.id.btn_option4:

				option2.setTextColor(getResources().getColor(R.color.black));
				option2.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option3.setTextColor(getResources().getColor(R.color.black));
				option3.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				option1.setTextColor(getResources().getColor(R.color.black));
				option1.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				operateFilter(4);
				if (reShopLists.get(4).isSelected()) {
					reShopLists.get(4).setSelected(false);
					listShop.clear();
					listMenuData.clear();
					titleHandler();
					option4.setTextColor(getResources().getColor(R.color.black));
					option4.setBackground(getResources().getDrawable(R.drawable.option_search_btn_background));
				} else {
					reShopLists.get(4).setSelected(true);
					moreURL = "";
					if (nearbyObj.getTypeSearch() == 2 || nearbyObj.getTypeSearch() == 5) {
						requestData("/tags/menus", TakeoutBodyDataHandler(nearbyObj.getTag()), true);
					} else {
						requestData("/shops", TakeoutBodyDataHandler(""), true);
					}


					option4.setTextColor(getResources().getColor(R.color.white));
					option4.setBackground(getResources().getDrawable(R.drawable.option_click_btn_background));
				}


				break;

		}
	}

	private void operateFilter(int pos) {
		for (int i = 0; i < reShopLists.size(); i++) {
			if (i != pos) {
				reShopLists.get(i).setSelected(false);
			}
		}
	}

	@Override
	public void onItemClickListener(MealthyMenu objects) {
		linkShopHandler(objects);
	}

	private void linkShopHandler(MealthyMenu objects) {
		if (nearbyObj.getTypeSearch() == 1 || nearbyObj.getTypeSearch() == 3 || nearbyObj.getTypeSearch() == 6) {
			NearbyObj nearbyObj = new NearbyObj();
			nearbyObj.setTypeSearch(1);
			nearbyObj.setShopName(objects.getShopName());
			nearbyObj.setShop_id(objects.getShop_id());
			nearbyObj.setMenu(objects);
			fragmentTransaction = fragmentManager.beginTransaction();
			MenuShopFragment menuShopFragment = new MenuShopFragment(nearbyObj, findShopByMenuId(objects), shopFragment);
			fragmentTransaction.hide(NearestShopFragment.this);
			fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
			fragmentTransaction.add(R.id.main_container, menuShopFragment, "NUTRITION").addToBackStack(Constant.FRAGMENT_TAG_HOME);
			fragmentTransaction.commit();
		} else {
			NearbyObj nearbyObj = new NearbyObj();
			nearbyObj.setMenu_name(objects.getShopName());
			nearbyObj.setTypeSearch(6);
			nearbyObj.setTag(objects.getShopName());
			fragmentTransaction = fragmentManager.beginTransaction();
			NearestShopFragment nearestShopFragment = new NearestShopFragment(nearbyObj);
			fragmentTransaction.hide(NearestShopFragment.this);
			fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
			fragmentTransaction.add(R.id.main_container, nearestShopFragment, "NUTRITION").addToBackStack(Constant.FRAGMENT_TAG_HOME);
			fragmentTransaction.commit();
		}
	}


	private ShopClickListener getListener() {
		return this;
	}

	private Shop findShopByMenuId(MealthyMenu obj) {
		for (int i = 0; i < listShop.size(); i++) {
			if (obj.getShop_id() == listShop.get(i).getId()) {
				return listShop.get(i);
			}
		}
		return null;
	}

	public void restartData(MealthyMenu mealthyMenu, boolean fav) {
		try {
			if (listMenuData.size() > 0) {
				for (int i = 0; i < listMenuData.size(); i++) {

					if (fav) {
						if (listMenuData.get(i).getId() == mealthyMenu.getId()) {
							tmpMenuFav = RealmHelper.with(getActivity()).getMealthyMenuFavById(listMenuData.get(i).getId());
							listMenuData.get(i).setFavorith(true);
							listMenuData.get(i).setFavId(tmpMenuFav.getFavId());
							menuRecycleAdapter.notifyDataSetChanged();
							break;
						}


					} else {
						if (mealthyMenu.getId() == listMenuData.get(i).getId()) {
							listMenuData.get(i).setFavorith(false);
							listMenuData.get(i).setFavId(0);
							menuRecycleAdapter.notifyDataSetChanged();
							break;
						}
					}

				}
				menuRecycleAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMenuClickListener(MealthyMenu objects) {
		fragmentTransaction = fragmentManager.beginTransaction();
		MenuDetailFragment menuDetailFragment = new MenuDetailFragment(objects, findShopByMenuId(objects), shopFragment);
		fragmentTransaction.hide(NearestShopFragment.this);
		fragmentTransaction.add(R.id.main_container, menuDetailFragment);
		fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
		fragmentTransaction.addToBackStack(Constant.FRAGMENT_TAG_HOME);
		fragmentTransaction.commit();
	}

	@Override
	public void onResume() {
		super.onResume();

	}

}