package com.camitss.mealthy.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.AudioManager;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.CropImage;
import com.camitss.mealthy.activity.TabFourthCropImageActivity;
import com.camitss.mealthy.object.Constant;
import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Viseth on 5/18/2017.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class TabFourthCameraFragment extends Fragment {

	private static final String TAG = "AndroidCameraApi";
	private Button takePictureButton;
	private TextureView textureView;
	private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

	static {
		ORIENTATIONS.append(Surface.ROTATION_0, 90);
		ORIENTATIONS.append(Surface.ROTATION_90, 0);
		ORIENTATIONS.append(Surface.ROTATION_180, 270);
		ORIENTATIONS.append(Surface.ROTATION_270, 180);
	}

	private String cameraId;
	protected CameraDevice cameraDevice;
	protected CameraCaptureSession cameraCaptureSessions;
	protected CaptureRequest captureRequest;
	protected CaptureRequest.Builder captureRequestBuilder;
	private Size imageDimension;
	private ImageReader imageReader;
	private static final int REQUEST_CAMERA_PERMISSION = 200;
	private boolean mFlashSupported;
	private Handler mBackgroundHandler;
	private HandlerThread mBackgroundThread;
	private int flashModeValue = CameraMetadata.FLASH_MODE_SINGLE;
	private int controlMode = CameraMetadata.CONTROL_AE_MODE_ON;
	private CameraView camera;
	private int modeCamera = 0;
	private ImageView flashMode;
	private static final int REQUEST_PERMISSION_CODE = 300;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE);
			}
		}
		View homeView = inflater.inflate(R.layout.fragment_tabfourth_camera, container, false);
		takePictureButton = (Button) homeView.findViewById(R.id.take_button);
		textureView = (TextureView) homeView.findViewById(R.id.live_view_photo);
		flashMode = (ImageView) homeView.findViewById(R.id.set_flash_mode);
		camera = (CameraView) homeView.findViewById(R.id.camera);
		camera.setCameraListener(new CameraListener() {
			@Override
			public void onPictureTaken(byte[] picture) {
				super.onPictureTaken(picture);
				try {
					saveMaster(picture);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});
		takePictureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				camera.captureImage();
			}
		});

		flashModeValue = CameraMetadata.FLASH_MODE_OFF;
		controlMode = CameraMetadata.CONTROL_MODE_AUTO;
		Boolean isFlashAvailable = getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
		if (!isFlashAvailable) {
			flashMode.setVisibility(View.INVISIBLE);
			flashMode.setEnabled(false);
		}

		flashMode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				checkCameraMode();

			}
		});
		return homeView;
	}


	private void checkCameraMode() {
		if (modeCamera == 0) {
			camera.setFlash(CameraKit.Constants.FLASH_OFF);
			Glide.with(getActivity()).load(R.drawable.flash_off).into(flashMode);
			modeCamera = 1;
		} else if (modeCamera == 1) {
			camera.setFlash(CameraKit.Constants.FLASH_ON);
			Glide.with(getActivity()).load(R.drawable.flash_on).into(flashMode);
			modeCamera = 2;
		} else if (modeCamera == 2) {
			camera.setFlash(CameraKit.Constants.FLASH_AUTO);
			Glide.with(getActivity()).load(R.drawable.flash_auto).into(flashMode);
			modeCamera = 0;
		}
		saveStageCamera(modeCamera);


	}

	private void setCamera() {
		if (modeCamera == 0) {
			camera.setFlash(CameraKit.Constants.FLASH_AUTO);
			Glide.with(getActivity()).load(R.drawable.flash_auto).into(flashMode);
		} else if (modeCamera == 1) {
			camera.setFlash(CameraKit.Constants.FLASH_OFF);
			Glide.with(getActivity()).load(R.drawable.flash_off).into(flashMode);
		} else if (modeCamera == 2) {
			camera.setFlash(CameraKit.Constants.FLASH_ON);
			Glide.with(getActivity()).load(R.drawable.flash_on).into(flashMode);
		}

	}

	private void saveMaster(byte[] bytes) throws IOException {
		OutputStream output = null;
		final File file = new File(Environment.getExternalStorageDirectory() + "/pic.jpg");
		String path = Environment.getExternalStorageDirectory() + "/pic.jpg";
		try {
			output = new FileOutputStream(file);
			output.write(bytes);
			Intent crop = new Intent(getActivity(), TabFourthCropImageActivity.class);

			crop.putExtra("PATH", path);
			getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
			startActivity(crop);
			getActivity().finish();
		} finally {
			if (null != output) {
				output.close();
			}
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			camera.stop();
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}


	@Override
	public void onResume() {
		super.onResume();
		camera.start();
		modeCamera = getCameraMode();
		setCamera();

	}

	//	@Override
//	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//										   @NonNull int[] grantResults) {
//		if (requestCode == REQUEST_CAMERA_PERMISSION) {
//			if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
//				// close the app
//				getFragmentManager().popBackStack();
//			}
//		}
//	}
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		int size = permissions.length;
		switch (requestCode) {
			case REQUEST_PERMISSION_CODE:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(getActivity(), "Camera permission was granted!", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getActivity(), "Camera permission was not granted!", Toast.LENGTH_LONG).show();
					getFragmentManager().popBackStack();
				}
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(getActivity(), "Storage permission was granted!", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), "Storage permission was not granted!", Toast.LENGTH_LONG).show();
					getFragmentManager().popBackStack();
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				break;
		}
	}

	private void saveStageCamera(int mode) {
		SharedPreferences.Editor editor = getActivity().getSharedPreferences("mealthy-app", Context.MODE_PRIVATE).edit();
		editor.putInt("cameraMode", mode);
		editor.commit();
	}

	private int getCameraMode() {
		SharedPreferences sharedPref = getActivity().getSharedPreferences("mealthy-app", Context.MODE_PRIVATE);
		return sharedPref.getInt("cameraMode", 0);

	}
}
