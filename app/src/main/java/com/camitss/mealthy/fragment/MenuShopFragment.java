package com.camitss.mealthy.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.adapter.MenuDetailRecycleAdapter;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.listener.MenuClickListener;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.NearbyObj;
import com.camitss.mealthy.object.RequestShopObj;
import com.camitss.mealthy.object.Shop;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.EndlessRecyclerOnScrollListener;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MenuShopFragment extends Fragment implements View.OnClickListener, MenuClickListener {
	public Gson mGson;
	Toolbar toolbar;
	List<MealthyMenu> listMenu;
	public MenuDetailRecycleAdapter menuRecycleAdapter;
	RecyclerView listMenuView;
	TextView noDataLabel;
	private ProgressDialog progressDialog;
	private int lastVisibleItem, totalItemCount;
	private String moreURL = "";
	private boolean isLoading;
	private int visibleThreshold = 5;
	private List<RequestShopObj> reShopLists;
	List<Integer> filterList = new ArrayList<Integer>();
	private NearbyObj nearbyObj;
	private String keyJson;
	private TextView shopAdress, shopTel, shopGuide;
	FragmentTransaction fragmentTransaction;
	FragmentManager fragmentManager;
	private LinearLayout mapView;
	private Shop shop;
	private NearestShopFragment shopFragment;

	public NearestShopFragment getShopFragment() {
		return shopFragment;
	}

	public void setShopFragment(NearestShopFragment shopFragment) {
		this.shopFragment = shopFragment;
	}

	public MenuShopFragment(NearbyObj nearbyObj, Shop shop, NearestShopFragment shopFragment) {
		this.nearbyObj = nearbyObj;
		this.shop = shop;
		this.shopFragment = shopFragment;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		mGson = new Gson();
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("NEAR");
		progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_menu_shop, container, false);
		toolbar = (Toolbar) homeView.findViewById(R.id.nearest_stop_toolbar);
		toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
		toolbar.inflateMenu(R.menu.home);
		noDataLabel = (TextView) homeView.findViewById(R.id.no_data_message);
		shopAdress = (TextView) homeView.findViewById(R.id.shop_adress);
		shopTel = (TextView) homeView.findViewById(R.id.shop_phone);
		shopGuide = (TextView) homeView.findViewById(R.id.shop_guide);
		mapView = (LinearLayout) homeView.findViewById(R.id.view_map);
		try {
			shopGuide.setText("<店舗案内>");
			shopAdress.setText(nearbyObj.getMenu().getAddress());
			shopTel.setText(nearbyObj.getMenu().getPhoneNumber());
			shopTel.setPaintFlags(shopTel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Utils.showProgressDialog(getActivity(), progressDialog, getString(R.string.loading_near_food));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getFragmentManager().popBackStack();
			}
		});
		fragmentManager = getFragmentManager();
		MenuItem item = toolbar.getMenu().getItem(0);
		item.setVisible(false);
		item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				MealthyLog.infoLog("action", "search");
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
				SearchHomeFragment searchHomeFragment = new SearchHomeFragment();
				fragmentTransaction.hide(MenuShopFragment.this);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.add(R.id.main_container, searchHomeFragment, "SEARCH_HOME").addToBackStack("NEAR");
				fragmentTransaction.commit();
				return false;
			}
		});

		listMenuView = (RecyclerView) homeView.findViewById(R.id.near_list_menu);
		listMenu = new ArrayList<>();
		menuRecycleAdapter = new MenuDetailRecycleAdapter(getActivity(), listMenu, getShopFragment());
		menuRecycleAdapter.setMenuClickListener(this);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mLayoutManager.setAutoMeasureEnabled(true);
		mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		listMenuView.setLayoutManager(mLayoutManager);
		listMenuView.setItemAnimator(new DefaultItemAnimator());
		listMenuView.setAdapter(menuRecycleAdapter);
		listMenuView.setOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
			@Override
			public void onLoadMore(int current_page) {
				if (nearbyObj.getTypeSearch() == 1 || nearbyObj.getTypeSearch() == 3) {
					requestData("/shops/" + nearbyObj.getShop_id() + "/menus", null, false);
				}
			}
		});

		titleHandler();
		shopTel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + nearbyObj.getMenu().getPhoneNumber() + ""));
				if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
					// TODO: Consider calling
					ActivityCompat.requestPermissions(getActivity(),
							new String[]{Manifest.permission.CALL_PHONE},
							120);
					return;
				}
				getActivity().startActivity(intent);
			}
		});
		mapView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				NearbyObj obj = new NearbyObj();
				obj.setShopName(nearbyObj.getShopName());
				obj.setLatitude(nearbyObj.getMenu().getShopLat());
				obj.setLongitude(nearbyObj.getMenu().getShopLng());

				fragmentTransaction = fragmentManager.beginTransaction();
				MapsFragment mapsFragment = new MapsFragment(nearbyObj);
				fragmentTransaction.hide(MenuShopFragment.this);
				fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
				fragmentTransaction.replace(R.id.activity_mealthy, mapsFragment, "MAP").addToBackStack(Constant.FRAGMENT_TAG_HOME);
				fragmentTransaction.commit();

			}
		});
		return homeView;
	}


	private void titleHandler() {
		if (nearbyObj.getTypeSearch() == 1) {
			toolbar.setTitle(nearbyObj.getShopName());
			requestData("/shops/" + nearbyObj.getShop_id() + "/menus", null, false);
		}
	}


	private void requestData(final String requestPath, JSONObject body, final boolean renew) {
		final RequestHandler requestHandler = new RequestHandler(getContext());
		if (renew) {
			listMenu.clear();
			progressDialog.show();
		}
		try {
			JSONObject header = new JSONObject();
			header.put("X-Mealthy-Token", "1C785FE8-6108-44C3-8663-361A4E37E899");
			requestHandler.jsonRequest(Constant.BASE_URL + requestPath + moreURL, Request.Method.GET, header, body);
			requestHandler.setResponseListener(new RequestListener() {
				@Override
				public void onResponseResult(boolean isSuccess, JSONObject data) {
					progressDialog.dismiss();
					try {
						switch (nearbyObj.getTypeSearch()) {
							case 1:
								keyJson = "menus";
								break;
						}
						for (int i = 0; i < data.getJSONArray(keyJson).length(); i++) {
							MealthyMenu menu = mGson.fromJson(data.getJSONArray(keyJson).getJSONObject(i).toString(), MealthyMenu.class);
							MealthyMenu tmpMenu = RealmHelper.with(getActivity()).getMealthyMenuFavById(menu.getId());
							if (tmpMenu != null) {
								if (tmpMenu.isFavorith()) {
									menu.setFavorith(true);
									menu.setFavId(tmpMenu.getFavId());
								} else {
									menu.setFavorith(false);
									menu.setFavId(0);
								}
							}


							listMenu.add(menu);
						}
						menuRecycleAdapter.notifyDataSetChanged();
						if (listMenu.size() == 0) {
							noDataLabel.setVisibility(View.VISIBLE);
							listMenuView.setVisibility(View.INVISIBLE);
						}
						try {
							moreURL = data.getJSONObject("paging").getString("next");
							if (data.getJSONObject("paging").getString("next").equalsIgnoreCase("null") || data.getJSONObject("paging").getString("next") == null) {
								moreURL = "";
							} else {
								moreURL = data.getJSONObject("paging").getString("next");
							}
							if (renew) {
								moreURL = "";
							}
						} catch (Exception e) {
							e.printStackTrace();
						}


					} catch (JSONException e) {
						e.printStackTrace();
					} catch (IndexOutOfBoundsException ofbe) {

					}
				}
			});
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void restartData(MealthyMenu mealthyMenu, boolean fav) {
		for (int i = 0; i < listMenu.size(); i++) {
			if (fav) {
				if (listMenu.get(i).getId() == mealthyMenu.getId()) {
					Log.i("FFFFFFF", "True");
					listMenu.get(i).setFavorith(true);
					listMenu.get(i).setFavId(mealthyMenu.getFavId());
					menuRecycleAdapter.notifyDataSetChanged();
					break;
				}
			} else {
				if (listMenu.get(i).getId() == mealthyMenu.getId()) {
					listMenu.get(i).setFavorith(false);
					listMenu.get(i).setFavId(0);
					menuRecycleAdapter.notifyDataSetChanged();
					Log.i("FFFFFFF", "False");
					break;
				}
			}

		}
		menuRecycleAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {

	}


	@Override
	public void onMenuClickListener(MealthyMenu objects) {
		fragmentTransaction = fragmentManager.beginTransaction();
		MenuDetailFragment menuDetailFragment = new MenuDetailFragment(objects, getShop(), this, getShopFragment());
		fragmentTransaction.hide(MenuShopFragment.this);
		fragmentTransaction.add(R.id.main_container, menuDetailFragment);
		fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left, R.anim.in_from_left, R.anim.out_to_right);
		fragmentTransaction.addToBackStack(Constant.FRAGMENT_TAG_HOME);
		fragmentTransaction.commit();
	}
}