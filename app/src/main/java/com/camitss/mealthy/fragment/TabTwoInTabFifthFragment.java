package com.camitss.mealthy.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ScrollerCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.Target;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.TwitterLoginProcess;
import com.camitss.mealthy.activity.UserRecomProfileDisplayActivity;
import com.camitss.mealthy.activity.UserRecommendationDisplayActivity;
import com.camitss.mealthy.adapter.recyclerview.MyPostItemAdapter;
import com.camitss.mealthy.adapter.recyclerview.SocialPostItemAdapter;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.CustomAlertBuilder;
import com.camitss.mealthy.utils.Utils;
import com.camitss.mealthy.utils.twitter.TwitterOnShare;
import com.camitss.mealthy.utils.twitter.TwitterParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.view.ViewGroup.FOCUS_AFTER_DESCENDANTS;
import static com.camitss.mealthy.utils.twitter.TwitterParams.TW_REQUEST_CODE_ON_SHARE;

/**
 * Created by Viseth on 4/28/2017.
 */

public class TabTwoInTabFifthFragment extends Fragment implements View.OnClickListener {
	int userId;
	MyPostItemAdapter adapter;
	RecyclerView myPostView;
	TextView noData;
	private BottomSheetDialog dialogBtmSheet;
	private AlertDialog twitterDialog;
	private int clickedItemPosition;

	public int getClickedItemPosition() {
		return clickedItemPosition;
	}

	public void setClickedItemPosition(int clickedItemPosition) {
		this.clickedItemPosition = clickedItemPosition;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_e_tab_two_fragment, container, false);

		AppSharedPreferences.getConstant(getActivity()).setNumNutriNotiPref(0);
		userId = AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
		adapter = new MyPostItemAdapter(getActivity());
		myPostView = (RecyclerView) view.findViewById(R.id.my_post_view);
		noData = (TextView) view.findViewById(R.id.no_post);
		LinearLayoutManager ll = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
		myPostView.setLayoutManager(ll);
		myPostView.setNestedScrollingEnabled(false);
		myPostView.setAdapter(adapter);
		getUserFeedInfo(String.format(Locale.ENGLISH, Constant.REQUEST_GET_FEED_USER, userId));
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		adapter.setOnItemClickListener(new SocialPostItemAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int pos) {
				Intent i;
				switch (v.getId()) {
					case R.id.image_post:
						Utils.with(getContext()).displayImageItem(getActivity(), (ImageView) v, adapter.getListPost().get(pos).getMenu().getImage());
						break;
					case R.id.img_heart:
						int method = Request.Method.POST;
						if (adapter.getListPost().get(pos).isLike())
							method = Request.Method.DELETE;
						requestFeedLike(pos, adapter.getListPost().get(pos).getId(), method, (ImageView) v, (TextView) v.getRootView().findViewById(R.id.txt_favorite));
						break;
					case R.id.img_cmt:
						i = new Intent(getActivity(), FeedDisplayActivity.class);
						i.putExtra("FeedItem", adapter.getListPost().get(pos));
						startActivityForResult(i, 250);
						break;
					case R.id.text_more:
						i = new Intent(getActivity(), UserRecommendationDisplayActivity.class);
						i.putExtra("isFromSuggestion", true);
						startActivityForResult(i, 251);
						break;
					case R.id.user_profile:
					case R.id.user_name:
						int userId = adapter.getListPost().get(pos).getFeedItemUser().getId();
						int myId = AppSharedPreferences.getConstant(getActivity()).getUserIdPref();
						if (myId == userId) {
							getActivity().getFragmentManager().beginTransaction().replace(R.id.main_container, new UserProfileFragment(), "USER_PROFILE").addToBackStack("MAIN_TAB_TWO").commit();
						} else {
							i = new Intent(getActivity(), UserRecomProfileDisplayActivity.class);
							i.putExtra("userId", userId);
							startActivityForResult(i, 250);
						}

						break;
					case R.id.img_share:
						setClickedItemPosition(pos);
						displayShareDialog();
						break;
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == TW_REQUEST_CODE_ON_SHARE) {
			if (resultCode == getActivity().RESULT_OK) {
				displayTwitterPostShareDisplay();
			}
		}
	}


	private void displayTwitterPostShareDisplay() {
		final FeedItem item = adapter.getListPost().get(getClickedItemPosition());
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_twitter_post_share_display, null);
		twitterDialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme_softkey_resize)).setView(view).show();
		final EditText edtCmt = (EditText) view.findViewById(R.id.edt_twitter_cmt);
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				twitterDialog.dismiss();
			}
		});
		final TextView textPost = (TextView) view.findViewById(R.id.txt_post);
		textPost.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!edtCmt.getText().toString().trim().isEmpty()) {
					twitterDialog.dismiss();
//					addMenuCmtRequest(edtCmt.getText().toString().trim(), getMenuId());
					new TwitterOnShare(getActivity()).execute(edtCmt.getText().toString().trim(), item.getMenu().getImageThumb());

				}
			}
		});
		String menuName = null != item.getMenu().getCgmMenuName() && !item.getMenu().getCgmMenuName().equalsIgnoreCase("null") ? item.getMenu().getCgmMenuName() + " に決めました！" : "";
		edtCmt.setText(menuName + " #mealthy #diet #healthyfood https://appsto.re/jp/Jqpx4.i");
		ImageView icon = (ImageView) view.findViewById(R.id.img_post);
		Glide.with(getContext()).load(item.getMenu().getImage()).listener(new com.bumptech.glide.request.RequestListener<String, GlideDrawable>() {
			@Override
			public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
				return false;
			}

			@Override
			public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
				return false;
			}
		}).error(R.drawable.thum_14578).into(icon);//.override(w, h)

		final TextView txtLength = (TextView) view.findViewById(R.id.txt_twitter_length);
		txtLength.setText(String.valueOf(calculateTextLength(edtCmt.getText().toString().trim())));
		twitterDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		edtCmt.setFocusable(true);
		edtCmt.setFocusableInTouchMode(true);
		edtCmt.requestFocus();
		twitterDialog.setCancelable(false);
		edtCmt.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				txtLength.setText(String.valueOf(calculateTextLength(s.toString().trim())));
			}
		});
	}

	private int calculateTextLength(String text) {
		if (!text.isEmpty()) {
			int i = 92 - (text.length());
			return i;
		} else {
			return 92;
		}
	}

	private void displayShareDialog() {
		dialogBtmSheet = new BottomSheetDialog(getActivity(), R.style.BottomSheetDialog);
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_f_change_image_profile_dialog, null);
//		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogBtmSheet.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.title)).setVisibility(View.GONE);
		TextView text = (TextView) view.findViewById(R.id.txt_two_option);
		text.setText("不適切なメニューの報告");
		text.setTextColor(ContextCompat.getColor(getActivity(), R.color.red));
		text.setOnClickListener(this);
		TextView txt = (TextView) view.findViewById(R.id.txt_one_option);
		txt.setText("Twitterでシェア");
		txt.setOnClickListener(this);
		dialogBtmSheet.setCancelable(false);
		dialogBtmSheet.show();
		dialogBtmSheet.setContentView(view);
	}

	public void shareToTwitter() {
		Intent i = new Intent(getActivity(), TwitterLoginProcess.class);
		i.putExtra("ContentLink", "oauth://com.camitss.mealthy.Twitter_oAuth");
		startActivityForResult(i, TwitterParams.TW_REQUEST_CODE_ON_SHARE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

			case R.id.txt_one_option:
				dialogBtmSheet.dismiss();
				shareToTwitter();
				break;
			case R.id.txt_two_option:
				dialogBtmSheet.dismiss();
				reportItemRequest();
				break;
		}
	}

	private void reportItemRequest() {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject params = new JSONObject();
		try {
			params.put("object", "Menu");
			params.put("oid", adapter.getListPost().get(getClickedItemPosition()).getMenu().getId());
			params.put("report_type", 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(Constant.REQUEST_REPORT_FEED_ITEM, Request.Method.POST, null, params);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					CustomAlertBuilder.create(getActivity(), getString(R.string.report_success_msg));
				} else {
					Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void getUserFeedInfo(String url) {
		RequestHandler request = new RequestHandler(getContext());
		JSONObject obj = new JSONObject();
		try {
			obj.put("user_id", userId);
			obj.put("id", userId);
			obj.put("keys", "menu.create.post_image");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.jsonRequest(url, Request.Method.GET, null, obj);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {

				if (isSuccess) {
					try {
						JSONArray array = data.getJSONArray("feeds");
						if (array.length() > 0) {
							noData.setVisibility(View.GONE);
						} else {
							noData.setVisibility(View.VISIBLE);
						}
						JSONObject jObj = data.getJSONObject("paging");
//							nextLink = jObj.getString("next");
						boolean hasMore = jObj.getBoolean("has_more");
//							Log.d("JSONOBj", array.length() + ">>" + nextLink + ">>" + hasMore);
						JSONObject obj;
						List<FeedItem> list = new ArrayList<FeedItem>();
						FeedItem item;
						FeedItem.Menu menu;
						FeedItem.FeedItemUser user;
						FeedItem.Comment cmt;
						JSONObject userObj;
						JSONObject menuObj;
						JSONArray cmtArray;
						List<FeedItem.Comment> cmtList;
						for (int i = 0; i < array.length(); i++) {
							obj = new JSONObject(array.get(i).toString());
							Log.d("JSONOBj", obj.getString("id"));
							item = new FeedItem();
							menu = new FeedItem.Menu();
							user = new FeedItem.FeedItemUser();
							cmtList = new ArrayList<FeedItem.Comment>();
							item.setId(obj.getInt("id"));
							item.setLike(obj.getBoolean("is_like"));
							item.setLikesCount(obj.getInt("likes_count"));
							item.setPrivacyCount(obj.getInt("privacy_kind"));
							menuObj = obj.getJSONObject("menu");
							menu.setId(menuObj.getInt("id"));
							menu.setCgmMenuName(menuObj.getString("cgm_menu_name"));
							menu.setCgmShopName(menuObj.getString("cgm_shop_name"));
							menu.setImage(menuObj.getString("image"));
							menu.setImageThumb(menuObj.getString("thumbnail_image_url"));
							menu.setCreatedAt(menuObj.getString("created_at"));
							menu.setCommentCount(menuObj.getInt("comments_count"));
							cmtArray = menuObj.getJSONArray("comments");
							FeedItem.FeedItemUser userCmt;
							for (int j = 0; j < cmtArray.length(); j++) {
								cmt = new FeedItem.Comment();
								JSONObject cmtObj = new JSONObject(cmtArray.getJSONObject(j).toString());
								cmt.setId(cmtObj.getInt("id"));
								cmt.setBody(cmtObj.getString("body"));
								userCmt = new FeedItem.FeedItemUser();
								userCmt.setId(cmtObj.getJSONObject("user").getInt("id"));
								userCmt.setName(cmtObj.getJSONObject("user").getString("name"));
								userCmt.setProfileImage(cmtObj.getJSONObject("user").getString("profile_image_url"));
//								userCmt.setUserKind(cmtObj.getJSONObject("user").getInt("user_kind"));
								cmt.setCmtUser(userCmt);
								cmtList.add(cmt);
							}
							menu.setCommentList(cmtList);
							item.setMenu(menu);
							item.setLikesCount(obj.getInt("likes_count"));
							userObj = obj.getJSONObject("user");
							user.setId(userObj.getInt("id"));
							user.setName(userObj.getString("name"));
							user.setProfileImage(userObj.getString("profile_image_url"));
							user.setUserKind(userObj.getInt("user_kind"));
							item.setFeedItemUser(user);
							if (obj.getInt("privacy_kind") == 3)
								list.add(item);
						}
						adapter.addPostLists(list);
						adapter.notifyDataSetChanged();
						Log.d("res ", "holder " + myPostView.getLayoutParams().height + "<>" + adapter.getmHieght() + "<>" + adapter.getItemCount());

					} catch (JSONException e) {
						e.printStackTrace();
					}
//						isLoadMore = false;
				} else {
//						isLoadMore = false;
					CustomAlertBuilder.create(getActivity(), getString(R.string.error));
				}
			}
		});
	}

	private void requestFeedLike(final int pos, int id, final int method, final ImageView v, final TextView textView) {
		RequestHandler request = new RequestHandler(getContext());
		request.jsonRequest(Constant.REQUEST_GET_FEED + "/" + id + "/like", method, null, null);
		request.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				if (isSuccess) {
					int count = adapter.getListPost().get(pos).getLikesCount();
					if (method == Request.Method.POST) {
						adapter.getListPost().get(pos).setLike(true);
						adapter.getListPost().get(pos).setLikesCount(count + 1);
					} else {
						adapter.getListPost().get(pos).setLike(false);
						adapter.getListPost().get(pos).setLikesCount(count - 1);
					}
					adapter.setFeedFavoriteAndTextCount(adapter.getListPost().get(pos).isLike(), v, textView, adapter.getListPost().get(pos).getLikesCount());
				}
			}
		});
	}


}
