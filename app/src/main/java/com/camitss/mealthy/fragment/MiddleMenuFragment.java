package com.camitss.mealthy.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.CameraAndGalleryActivity;
import com.camitss.mealthy.activity.ChoosePhotoActivity;
import com.camitss.mealthy.activity.MealthyActivity;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.object.MealthyLog;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.object.User;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MiddleMenuFragment extends Fragment {
	private String tmpNum;
	private ProgressDialog progressDialog;
	private String loadingTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_middle_menu, container, false);
		LinearLayout recodeWeighMenu = (LinearLayout) homeView.findViewById(R.id.record_weigh);
		LinearLayout choosePhotoMenu = (LinearLayout) homeView.findViewById(R.id.choose_photo);
		LinearLayout consultExpert = (LinearLayout) homeView.findViewById(R.id.consult_expertise);

		homeView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				((MealthyActivity) getActivity()).cancelMenu3();
			}
		});
		progressDialog = new ProgressDialog(getContext(), R.style.AppCompatProgressDialogStyle);
		loadingTitle = getActivity().getResources().getString(R.string.updating_weight);
		recodeWeighMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				displayWeightCalculator();
				((MealthyActivity) getActivity()).cancelMenu3();
			}
		});

		choosePhotoMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				((MealthyActivity) getActivity()).cancelMenu3();
				Intent choosePhoto = new Intent(getActivity(), ChoosePhotoActivity.class);
				startActivity(choosePhoto);
			}
		});

		consultExpert.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				User user = RealmHelper.with(getActivity()).getRealm().where(User.class).findFirst();
//				if (null != user && user.isFreeConsultation())
				startActivity(new Intent(getActivity(), CameraAndGalleryActivity.class));
//				else setupTabFourthFragment();
				((MealthyActivity) getActivity()).cancelMenu3();
			}
		});
		return homeView;
	}

	private void setupTabFourthFragment() {
		MainTabFourthFragment fragment = new MainTabFourthFragment();
//		MenuDetailFragment fragment = new MenuDetailFragment();
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.replace(R.id.main_container, fragment, Constant.FRAGMENT_TAG_HOME);
		fragmentTransaction.commit();
	}

	private void displayWeightCalculator() {
		final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.alertdialog_theme_default);
		mBottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		final View sheetView = getActivity().getLayoutInflater().inflate(R.layout.custom_calculator_layout, null);
		((TextView) sheetView.findViewById(R.id.title)).setText(getString(R.string.weight));
		final TextView text_unit = (TextView) sheetView.findViewById(R.id.txt_unit);
		text_unit.setText("kg");
		final TextView text_input = (TextView) sheetView.findViewById(R.id.edt_input);
		ImageView img_close = (ImageView) sheetView.findViewById(R.id.img_close);
		Button btn_point = (Button) sheetView.findViewById(R.id.btn_point);
		Button btn_zero = (Button) sheetView.findViewById(R.id.btn_zero);
		Button btn_c = (Button) sheetView.findViewById(R.id.btn_c);
		Button btn_three = (Button) sheetView.findViewById(R.id.btn_three);
		Button btn_two = (Button) sheetView.findViewById(R.id.btn_two);
		Button btn_one = (Button) sheetView.findViewById(R.id.btn_one);
		Button btn_six = (Button) sheetView.findViewById(R.id.btn_six);
		Button btn_five = (Button) sheetView.findViewById(R.id.btn_five);
		Button btn_four = (Button) sheetView.findViewById(R.id.btn_four);
		Button btn_nine = (Button) sheetView.findViewById(R.id.btn_nine);
		Button btn_eight = (Button) sheetView.findViewById(R.id.btn_eight);
		Button btn_seven = (Button) sheetView.findViewById(R.id.btn_seven);
		final int number = 300; // max weight

		btn_c.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				text_input.setText("");
			}
		});
		btn_eight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "8".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_seven.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "7".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_five.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "5".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_four.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "4".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_nine.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "9".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_one.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "1".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_two.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "2".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_three.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "3".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_six.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "6".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_zero.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tmpNum = text_input.getText().toString() + "0".trim();
				if (Double.valueOf(tmpNum) <= number) text_input.setText(tmpNum);
			}
		});
		btn_point.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!text_input.getText().toString().trim().isEmpty()) {
					if (text_input.getText().toString().trim().contains(".")) return;
					tmpNum = text_input.getText().toString() + ".".trim();
					text_input.setText(tmpNum);
				}
			}
		});

		img_close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mBottomSheetDialog.dismiss();
			}
		});
		TextView text_save = (TextView) sheetView.findViewById(R.id.txt_save);

		text_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				double w = Double.parseDouble(tmpNum);
				Log.d("middle", tmpNum + "<<>>" + w);
				mBottomSheetDialog.dismiss();
				updateWeight(w);
			}
		});
		mBottomSheetDialog.setContentView(sheetView);
		mBottomSheetDialog.setCancelable(false);
		mBottomSheetDialog.show();
	}

	private void updateWeight(double weight) {
		RequestHandler requestHandler = new RequestHandler(getActivity());
		User user = RealmHelper.with(getContext()).getRealm().where(User.class).findFirst();
		String token = TokenRealmHelper.with(getContext()).getRealm().where(TokenObject.class).findFirst().getToken();
		String url = Constant.REQUEST_UPDATE_WEIGHT + "/" + user.getId();
		JSONObject body = new JSONObject();
		try {
			body.put("weight", weight);
			body.put("device_id", token);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Utils.showProgressDialog(getActivity(), progressDialog, loadingTitle);
		requestHandler.jsonRequest(url, Request.Method.PUT, null, body);

		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {
				AppSharedPreferences.getConstant(getContext()).setFromUpdateTargetWeightMiddleMenuScreen(true);
				RealmHelper.with(getActivity()).save(new Gson().fromJson(data.toString(), User.class));
				AppManager.getInstance().getMealthyActivity().setupTabFifthFragmentCondition(0);
				progressDialog.dismiss();
				AppManager.getInstance().set5TabRefresh(true);
				MealthyLog.debugLog("log weight result ", data.toString());
			}
		});
	}

}