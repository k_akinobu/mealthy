package com.camitss.mealthy.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.request.RequestHandler;
import com.camitss.mealthy.request.RequestListener;

import org.json.JSONObject;

public class SupportPlanFragment extends Fragment implements View.OnClickListener {

	Toolbar toolbar;

	FragmentTransaction fragmentTransaction;
	SettingTermConditionFragment termConditionFragment;

	public SupportPlanFragment(Toolbar toolbar) {
		this.toolbar = toolbar;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		FragmentTag.getInstant().setTAG("SUPPORT_PLAN");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View homeView = inflater.inflate(R.layout.fragment_support_plan, container, false);
		homeView.findViewById(R.id.plan_5_960).setOnClickListener(this);
		homeView.findViewById(R.id.plan_10_5000).setOnClickListener(this);
		homeView.findViewById(R.id.plan_24_8800).setOnClickListener(this);
		homeView.findViewById(R.id.plan_1_round).setOnClickListener(this);

		fragmentTransaction = getFragmentManager().beginTransaction();

		toolbar.setTitle(getString(R.string.f_setting_text6));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				getActivity().finish();
			}
		});

		homeView.findViewById(R.id.detail_support_plan).setOnClickListener(this);
		homeView.findViewById(R.id.term_service_support_plan).setOnClickListener(this);
		homeView.findViewById(R.id.support_plan_commercial_transaction).setOnClickListener(this);

		return homeView;
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
			case R.id.detail_support_plan:
				termConditionFragment = new SettingTermConditionFragment(0, toolbar);
				fragmentTransaction.replace(R.id.support_plan_container, termConditionFragment).addToBackStack("SUPPORT_PLAN");
				fragmentTransaction.commit();
				break;
			case R.id.term_service_support_plan:
				termConditionFragment = new SettingTermConditionFragment(1, toolbar);
				fragmentTransaction.replace(R.id.support_plan_container, termConditionFragment).addToBackStack("SUPPORT_PLAN");
				fragmentTransaction.commit();
				break;
			case R.id.support_plan_commercial_transaction:
				termConditionFragment = new SettingTermConditionFragment(2, toolbar);
				fragmentTransaction.replace(R.id.support_plan_container, termConditionFragment).addToBackStack("SUPPORT_PLAN");
				fragmentTransaction.commit();
				break;
			case R.id.plan_5_960:
//				plan5960();
				break;
			case R.id.plan_10_5000:

				break;
			case R.id.plan_24_8800:

				break;
			case R.id.plan_1_round:

				break;
		}
	}

	public void plan5960(){
		RequestHandler requestHandler = new RequestHandler(getContext());
		requestHandler.jsonRequest("url", Request.Method.POST,null,null);
		requestHandler.setResponseListener(new RequestListener() {
			@Override
			public void onResponseResult(boolean isSuccess, JSONObject data) {

			}
		});
	}
}