package com.camitss.mealthy.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.OverviewActivity;

public class StartFragment extends Fragment {

	private Toolbar toolbar;
	public StartFragment(Toolbar toolbar){
		this.toolbar = toolbar;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		((AppCompatActivity) getActivity()).getSupportActionBar().hide();
		View homeView = inflater.inflate(R.layout.fragment_start, container, false);
		Button start = (Button) homeView.findViewById(R.id.start_mealthy);
		TextView term = (TextView) homeView.findViewById(R.id.term_condition);

		term.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//				fragmentTransaction.setTransition();
				//fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left,R.anim.out_to_right);
				fragmentTransaction.replace(R.id.start_screen_container, new TermConditionFragment(0,toolbar,"START"), "TERM").addToBackStack("START");
//				fragmentTransaction.replace(R.id.start_screen_container, new WelcomeFragment(), "TERM").addToBackStack("START");
				fragmentTransaction.commit();
			}
		});

		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent overview = new Intent(getActivity(), OverviewActivity.class);
				overview.putExtra("FROM","START");
				startActivity(overview);
				getActivity().finish();
				getActivity().overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
			}
		});

		return homeView;
	}
}