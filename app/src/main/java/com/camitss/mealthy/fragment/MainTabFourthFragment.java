package com.camitss.mealthy.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.CameraAndGalleryActivity;
import com.camitss.mealthy.activity.SocialLoginActivity;
import com.camitss.mealthy.object.FragmentTag;
import com.camitss.mealthy.utils.AppSharedPreferences;

import static com.camitss.mealthy.R.id.txt_e1_text4;

/**
 * Created by Viseth on 4/21/2017.
 */

public class MainTabFourthFragment extends Fragment implements View.OnClickListener {
	private android.app.AlertDialog dialog;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		FragmentTag.getInstant().setTAG("MAIN_TAB_FOURTH");
		View view = inflater.inflate(R.layout.tab_fourth_fragment, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		TextView textAlert = (TextView) view.findViewById(R.id.txt_alertview);
		Button btnNext = (Button) view.findViewById(R.id.e1_btn_next);
		textAlert.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		((TextView)view.findViewById(R.id.txt_e1_text4)).setText("まずはお試しで無料診断が可能なので、 最近食べた食事を投稿してみてください！");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.txt_alertview:
				displayAlertView();
				break;
			case R.id.e1_btn_next:
				if (null != AppSharedPreferences.getConstant(getActivity()) && AppSharedPreferences.getConstant(getActivity()).isLoginWithSocialPref())
					startActivity(new Intent(getActivity(), CameraAndGalleryActivity.class));
				else
					displayNextOption();
				break;
			case R.id.txt_next:
				dialog.dismiss();
				Intent socialLogin = new Intent(getActivity(), SocialLoginActivity.class);
				socialLogin.putExtra("FROM","TAB_4");
				startActivity(socialLogin);
				break;
		}
	}

	private void displayAlertView() {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_e1_alert_view, null);
		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme_no_bg)).setView(view).show();
		ImageView close = (ImageView) view.findViewById(R.id.close);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
	}

	private void displayNextOption() {
		View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_e1_next_option_dialog, null);
		dialog = new android.app.AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.alertdialog_theme)).setView(view).show();
		TextView textCancel = (TextView) view.findViewById(R.id.txt_cancel);
		textCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		((TextView) view.findViewById(R.id.txt_next)).setOnClickListener(this);
		dialog.setCancelable(false);
	}
}
