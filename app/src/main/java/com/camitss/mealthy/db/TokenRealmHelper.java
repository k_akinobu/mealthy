package com.camitss.mealthy.db;

import android.content.Context;

import com.camitss.mealthy.object.Diagnoses;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;

/**
 * Created by Viseth on 5/9/2017.
 */

public class TokenRealmHelper {
	private static TokenRealmHelper INSTANCE;

	private RealmConfiguration config;
	private Context context;

	private TokenRealmHelper(Context context) {
		this.context = context;
		Realm.init(context);
		config = new RealmConfiguration.Builder().name("MealthyToken.realm").deleteRealmIfMigrationNeeded().schemaVersion(1).build();
	}

	public static void drop(Context context) {
		Realm realm = getRealm(context);
		realm.beginTransaction();
		realm.deleteAll();
		realm.commitTransaction();
		realm.close();
	}


	public synchronized static TokenRealmHelper with(Context context) {
		if (INSTANCE == null)
			INSTANCE = new TokenRealmHelper(context);
		return INSTANCE;
	}

	public static Realm getRealm(Context context) {
		return TokenRealmHelper.with(context).getRealm();
	}

	public RealmConfiguration getConfig() {
		return config;
	}

	public void setConfig(RealmConfiguration config) {
		this.config = config;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Realm getRealm() {
		return Realm.getInstance(getConfig());
	}

	public void save(RealmObject object) {
		if (object == null)
			return;
		Realm realm = Realm.getInstance(getConfig());
		realm.setAutoRefresh(true);
		realm.beginTransaction();
		realm.copyToRealmOrUpdate(object);
		realm.commitTransaction();
		realm.close();
	}

	public void saveAll(List<RealmObject> objects) {
		if (objects == null)
			return;
		Realm realm = Realm.getInstance(getConfig());
		realm.setAutoRefresh(true);
		realm.beginTransaction();
		realm.copyToRealmOrUpdate(objects);
		realm.commitTransaction();
		realm.close();
	}

	public void editDiagnose(int id, boolean isSelected) {
		Realm realm = Realm.getInstance(getConfig());
		Diagnoses d = realm.where(Diagnoses.class).equalTo("id", id).findFirst();
		realm.beginTransaction();
		d.setSelectedQuestion(isSelected);
		realm.copyToRealmOrUpdate(d);
		realm.commitTransaction();
		realm.close();
	}
}
