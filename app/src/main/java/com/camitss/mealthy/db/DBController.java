package com.camitss.mealthy.db;


import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.internal.Context;

/**
 * Created by Senhly HENG
 * Date 7/03/2017.
 */

public class DBController {

	private static Realm realmObj;
	private static DBController instant;

	private DBController(Context activityContext) {
//		RealmConfiguration config = new RealmConfiguration
//				.Builder(activityContext)
//				.name("mealthy.realm")
//				.deleteRealmIfMigrationNeeded()
//				.build();
//		this.realmObj = Realm.getInstance(config);
	}

	public static DBController getInstant(Context appContext) {
		if (instant == null || realmObj == null) {
			instant = new DBController(appContext);
		}
		return instant;
	}


}
