package com.camitss.mealthy.db;

import android.content.Context;

import com.camitss.mealthy.object.Diagnoses;
import com.camitss.mealthy.object.HistorySearch;
import com.camitss.mealthy.object.MealthyMenu;
import com.camitss.mealthy.object.User;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Viseth on 3/29/2017.
 */

public class RealmHelper {
	private static RealmHelper INSTANCE;

	private RealmConfiguration config;
	private Context context;

	private RealmHelper(Context context) {
		this.context = context;
		Realm.init(context);
		config = new RealmConfiguration.Builder().name("Mealthy.realm").deleteRealmIfMigrationNeeded().schemaVersion(1).build();
	}

	public static void drop(Context context) {
		Realm realm = getRealm(context);
		realm.beginTransaction();
		realm.deleteAll();
		realm.commitTransaction();
		realm.close();
	}


	public synchronized static RealmHelper with(Context context) {
		if (INSTANCE == null)
			INSTANCE = new RealmHelper(context);
		return INSTANCE;
	}

	public static Realm getRealm(Context context) {
		return RealmHelper.with(context).getRealm();
	}

	public RealmConfiguration getConfig() {
		return config;
	}

	public void setConfig(RealmConfiguration config) {
		this.config = config;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Realm getRealm() {
		return Realm.getInstance(getConfig());
	}

	public void save(RealmObject object) {
		if (object == null)
			return;
		Realm realm = Realm.getInstance(getConfig());
		realm.setAutoRefresh(true);
		realm.beginTransaction();
		realm.copyToRealmOrUpdate(object);
		realm.commitTransaction();
		realm.close();
	}

	public void saveRelevant(RealmObject object) {
		if (object == null)
			return;
		Realm realm = Realm.getInstance(getConfig());
		realm.setAutoRefresh(true);
		realm.beginTransaction();
		realm.copyToRealm(object);
		realm.commitTransaction();
		realm.close();
	}

	public void saveAll(List<RealmObject> objects) {
		if (objects == null)
			return;
		Realm realm = Realm.getInstance(getConfig());
		realm.setAutoRefresh(true);
		realm.beginTransaction();
		realm.copyToRealmOrUpdate(objects);
		realm.commitTransaction();
		realm.close();
	}

	public void editDiagnose(int id, boolean isSelected) {
		Realm realm = Realm.getInstance(getConfig());
		Diagnoses d = realm.where(Diagnoses.class).equalTo("id", id).findFirst();
		realm.beginTransaction();
		d.setSelectedQuestion(isSelected);
		realm.copyToRealmOrUpdate(d);
		realm.commitTransaction();
		realm.close();
	}

	public User getUserRealmObjById(int id) {
		return Realm.getInstance(getConfig()).where(User.class).equalTo("id", id).findFirst();
	}

	public MealthyMenu getMealthyMenuFavById(int id) {
		MealthyMenu tmp = Realm.getInstance(getConfig()).where(MealthyMenu.class).equalTo("id", id).findFirst();
		if (tmp != null) {
			if (Realm.getInstance(getConfig()).where(MealthyMenu.class).equalTo("id", id).findFirst().isFavorith()) {
				return tmp;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	public void deleteMealthyMenuFavbyId(int id) {
		Realm.getInstance(getConfig()).where(MealthyMenu.class).equalTo("id", id).findFirst().deleteFromRealm();

	}

	public RealmResults<Diagnoses> getRecentInviteRealmObjects() {
		return Realm.getInstance(getConfig()).where(Diagnoses.class).findAll();
	}

//	public TokenObject getTokenObject() {
//		return Realm.getInstance(getConfig()).where(TokenObject.class).findFirst();
//	}

	public void clearHistory() {
		Realm realm = Realm.getInstance(getConfig());
		realm.beginTransaction();
		realm.delete(HistorySearch.class);
		realm.commitTransaction();
	}
}
