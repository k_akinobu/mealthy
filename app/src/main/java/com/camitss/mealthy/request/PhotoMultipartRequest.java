package com.camitss.mealthy.request;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.utils.AppSharedPreferences;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;

/**
 * Created by Viseth on 4/25/2017.
 */

public class PhotoMultipartRequest {
	private Response.Listener<String> mListener;
	private File mImageFile;
	private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
	private static final String FILE_PART_NAME = "profile_image_url";
	private Context context;

	public PhotoMultipartRequest(Context context) {
		this.context = context;
	}

	public void addBody(String url, Response.ErrorListener errorListener, Response.Listener<String> listener, final String name, final String token, File imageFile) {
		mListener = listener;
		mImageFile = imageFile;

		builderEntity(name, token);

		Request<String> request = new Request<String>(Request.Method.PUT, url, errorListener) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = super.getHeaders();
				if (headers == null
						|| headers.equals(Collections.emptyMap())) {
					headers = new HashMap<>();
				}
				String token = "";
				try {
					token = TokenRealmHelper.with(context).getRealm().where(TokenObject.class).findFirst().getToken();
				} catch (Exception e) {
					e.printStackTrace();
				}
//				token = "52B016F5-AC44-4CAB-B080-99756F88BBF6";
				headers.put("Content-Type", "multipart/form-data");
				headers.put("Accept", "multipart/form-data");
				headers.put("X-Mealthy-Token", token);
				headers.put("Authorization","Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJNZWFsdGh5IGluYy4iLCJpYXQiOjE0ODk1MTQ0NzMsImp0aSI6ImI2YmNiNzk0OTA0ZjlmYjc1NmE3ZjEwYWE4ZjYzNGI0Y2M0MzYxMGMiLCJub3RlIjoiTWVhdGxoeSBQcml2YXRlIEFQSSBBdXRob3JpemF0aW9uIEhlYWRlciJ9.BNFSfSBUBxJCxp2Y3FgHGrnFQklZbHx4K471LYMTe34");

				return headers;
			}

			@Override
			public String getBodyContentType() {
				return mBuilder.build().getContentType().getValue();
			}

			@Override
			public byte[] getBody() throws AuthFailureError {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				try {
					mBuilder.build().writeTo(bos);
				} catch (IOException e) {
					VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
				}

				return bos.toByteArray();
			}

			@Override
			protected Response<String> parseNetworkResponse(NetworkResponse response) {
				String resultResponse = new String(response.data);
				return Response.success(resultResponse, HttpHeaderParser.parseCacheHeaders(response));
			}

			@Override
			protected void deliverResponse(String response) {
				if (mListener != null)
					mListener.onResponse(response);
			}
		};
		try {
			AppManager.getInstance().addToRequestQueue(request);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}

	private void builderEntity(String name, String token) {
		mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		mBuilder.setLaxMode().setBoundary("XX.").setCharset(Charset.forName("UTF-8"));
		mBuilder.addBinaryBody(FILE_PART_NAME, mImageFile, ContentType.create("image/jpeg"), mImageFile.getName());
		if (!name.trim().isEmpty())
			mBuilder.addTextBody("name", name);
		mBuilder.addTextBody("device_id", token);
	}
}
