package com.camitss.mealthy.request;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.TokenObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.entity.mime.content.ContentBody;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.CharsetUtils;

/**
 * Created by Viseth on 4/25/2017.
 */

public class PostMenuRequest {
	private Response.Listener<String> mListener;
	private File mImageFile;
	private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
	private static final String FILE_PART_NAME = "image";
	private Context context;

	public PostMenuRequest(Context context) {
		this.context = context;
	}

	public void addBody(String url, Response.ErrorListener errorListener, Response.Listener<String> listener, final JSONObject name, File imageFile) {
		mListener = listener;
		mImageFile = imageFile;

		builderEntity(name);

		Request<String> request = new Request<String>(Request.Method.POST, url, errorListener) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = super.getHeaders();
				if (headers == null
						|| headers.equals(Collections.emptyMap())) {
					headers = new HashMap<>();
				}
//				String token = "";
//				try {
//					token = TokenRealmHelper.with(context).getRealm().where(TokenObject.class).findFirst().getToken();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
////				token = "52B016F5-AC44-4CAB-B080-99756F88BBF6";
				try {
					Constant.token = TokenRealmHelper.with(context).getRealm().where(TokenObject.class).findFirst().getToken();
				} catch (Exception e) {
//					token = "52B016F5-AC44-4CAB-B080-99756F88BBF6";
					e.printStackTrace();
				}
				headers.put("Content-Type", "multipart/form-data;");
				headers.put("Accept", "multipart/form-data");
				headers.put("X-Mealthy-Token", Constant.token);
				headers.put("Authorization","Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJNZWFsdGh5IGluYy4iLCJpYXQiOjE0ODk1MTQ0NzMsImp0aSI6ImI2YmNiNzk0OTA0ZjlmYjc1NmE3ZjEwYWE4ZjYzNGI0Y2M0MzYxMGMiLCJub3RlIjoiTWVhdGxoeSBQcml2YXRlIEFQSSBBdXRob3JpemF0aW9uIEhlYWRlciJ9.BNFSfSBUBxJCxp2Y3FgHGrnFQklZbHx4K471LYMTe34");

				return headers;
			}

			@Override
			public String getBodyContentType() {
				Log.d("postmenurequ", mBuilder.build().getContentType().getValue());
				return mBuilder.build().getContentType().getValue();
			}

			@Override
			protected String getParamsEncoding() {
				return "charset=UTF-8";
			}

			@Override
			public byte[] getBody() throws AuthFailureError {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				try {
					mBuilder.build().writeTo(bos);
				} catch (IOException e) {
					VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
				}

				return bos.toByteArray();
			}

			@Override
			protected Response<String> parseNetworkResponse(NetworkResponse response) {
				String resultResponse = null;
				String LOG_TAG = "postmenureques";
				try {
					resultResponse = new String(response.data, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				return Response.success(resultResponse, HttpHeaderParser.parseCacheHeaders(response));
			}

			@Override
			protected void deliverResponse(String response) {
				if (mListener != null)
					mListener.onResponse(response);
			}
		};
		try {
			AppManager.getInstance().addToRequestQueue(request);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}
	private void builderEntity(JSONObject params) {
		mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		try {
			mBuilder.setLaxMode().setBoundary("XX").setCharset(CharsetUtils.get("UTF-8"));// Charset.forName("UTF-8")).setBoundary("XX.");
		} catch (UnsupportedEncodingException e) {
			mBuilder.setLaxMode().setBoundary("XX").setCharset(Charset.forName("UTF-8"));//.setBoundary("XX.");
			e.printStackTrace();
		}
		mBuilder.addBinaryBody(FILE_PART_NAME, mImageFile, ContentType.create("image/jpeg"), mImageFile.getName());
		Charset utf8 = Charset.forName("utf-8");
		ContentType contentType = ContentType.create(ContentType.TEXT_PLAIN.getMimeType(), utf8);

		JSONArray paramKeys = new JSONArray();
		if (params.names() != null) paramKeys = params.names();
		try {
			for (int i = 0; i < paramKeys.length(); i++) {
				String text = new String(params.getString(paramKeys.getString(i)).trim().getBytes(), "UTF-8");
				mBuilder.addTextBody(paramKeys.getString(i), params.getString(paramKeys.getString(i)).trim(), contentType);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
