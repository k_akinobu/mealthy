package com.camitss.mealthy.request;

import org.json.JSONObject;

/**
 * Created by Senhly HENG
 * Date 7-03-2017.
 */
public interface StringRequestListener {
    void onResponseResult(boolean isSuccess, String data);
}
