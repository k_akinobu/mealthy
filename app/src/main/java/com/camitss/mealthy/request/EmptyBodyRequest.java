package com.camitss.mealthy.request;

import android.os.AsyncTask;
import android.util.Log;

import com.camitss.mealthy.object.MealthyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

/**
 * Created by User
 * Date 4/7/2017
 */

public class EmptyBodyRequest extends AsyncTask<JSONObject, Integer, String> {
	HttpClient httpclient;
	HttpPost httppost;
	String url;

	public EmptyBodyRequest(String requestUrl) {
		url = requestUrl;
		Log.d("url", "httppost>> " + url);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		httpclient = new DefaultHttpClient();
		httppost = new HttpPost(url);
	}

	@Override
	protected String doInBackground(JSONObject... data) {
		// Create a new HttpClient and Post Header
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			JSONArray keys = data[0].names();
			for (int i = 0; i < keys.length(); i++) {
				nameValuePairs.add(new BasicNameValuePair(keys.getString(i), data[0].getString(keys.getString(i))));
			}
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			//execute http post
			HttpResponse response = httpclient.execute(httppost);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (JSONException je) {

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPostExecute(String s) {
		MealthyLog.infoLog("data-----", s + "");
		super.onPostExecute(s);
	}
}
