package com.camitss.mealthy.request;

import org.json.JSONObject;

/**
 * Created by Senhly HENG
 * Date 7-03-2017.
 */
public interface RequestListener {
    void onResponseResult(boolean isSuccess, JSONObject data);
}
