package com.camitss.mealthy.request;


import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.camitss.mealthy.app.AppManager;
import com.camitss.mealthy.db.RealmHelper;
import com.camitss.mealthy.db.TokenRealmHelper;
import com.camitss.mealthy.object.Constant;
import com.camitss.mealthy.object.TokenObject;
import com.camitss.mealthy.utils.AppSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Senhly HENG
 * Date 7-03-2017
 * Make json object request
 */

public class RequestHandler {
	private Context context;

	public RequestHandler(Context context) {
		this.context = context;
	}

	private RequestListener requestListener;
	private StringRequestListener stringRequestListener;

	public void setResponseListener(RequestListener requestListener) {
		this.requestListener = requestListener;
	}

	public void setStringRequestListener(StringRequestListener stringRequestListener) {
		this.stringRequestListener = stringRequestListener;
	}

	public void jsonRequest(String url, final int method, final JSONObject header, final JSONObject body) {

		JSONObject paramsBody = body;
		final String a = url;

		Log.d("RequestHandler", "url >> " + url + ">>" + method);
		Log.d("RequestHandler", "Header >> " + header);
		Log.d("RequestHandler", "body>> " + body);

		try {
			if ((method == Request.Method.GET && null != body && body.length() > 0) || (method == Request.Method.DELETE && null != body && body.length() > 0)) {// if request method is GET and DELETE, this will work for request with params
				JSONArray keys = body.names();
				url = url.concat("?");
				for (int i = 0; i < keys.length(); i++) {
					url = url.concat(keys.getString(i)).concat("=").concat(body.getString(keys.getString(i))).concat("&");
				}
				url = url.substring(0, url.length() - 1);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, paramsBody, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				requestListener.onResponseResult(true, response);
			}

		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				try {
					VolleyError errors = new VolleyError(new String(error.networkResponse.data));

					JSONObject err = new JSONObject();
					err.put("status_code", error.networkResponse.statusCode);
					err.put("message", errors.getMessage());
					requestListener.onResponseResult(false, err);
				} catch (JSONException e) {
					requestListener.onResponseResult(false, new JSONObject());
				} catch (NullPointerException npe) {
					requestListener.onResponseResult(false, new JSONObject());
					npe.printStackTrace();
				}
			}
		}
		) {
			@Override
			protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
				try {
					if (response.data.length == 0) {
						byte[] responseData = "{}".getBytes("UTF8");
						response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				return super.parseNetworkResponse(response);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<>();

				try {
					Constant.token = TokenRealmHelper.with(context).getRealm().where(TokenObject.class).findFirst().getToken();
				} catch (Exception e) {
//					token = "52B016F5-AC44-4CAB-B080-99756F88BBF6";
					e.printStackTrace();
				}
//				token = "876899F8-27AB-4CFE-9A92-1C364266E815";//"52B016F5-AC44-4CAB-B080-99756F88BBF6";
				params.put("Content-Type", "application/json; charset=utf-8");
				params.put("Accept", "application/json");
				params.put("X-Mealthy-Token", Constant.token);
				params.put("Authorization","Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJNZWFsdGh5IGluYy4iLCJpYXQiOjE0ODk1MTQ0NzMsImp0aSI6ImI2YmNiNzk0OTA0ZjlmYjc1NmE3ZjEwYWE4ZjYzNGI0Y2M0MzYxMGMiLCJub3RlIjoiTWVhdGxoeSBQcml2YXRlIEFQSSBBdXRob3JpemF0aW9uIEhlYWRlciJ9.BNFSfSBUBxJCxp2Y3FgHGrnFQklZbHx4K471LYMTe34");
				Log.d("token", "header>> " + Constant.token);
				if (header == null) return params;
				try {
					JSONArray headerKeys = new JSONArray();
					if (header.names() != null) headerKeys = header.names();
					for (int i = 0; i < headerKeys.length(); i++) {
						params.put(headerKeys.getString(i), header.getString(headerKeys.getString(i)));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return params;
			}

			@Override
			public String getBodyContentType() {
				return "application/x-www-form-urlencoded; charset=UTF-8";
			}

			@Override
			protected String getParamsEncoding() {
				return "charset=UTF-8";
			}
		};

		try {
			AppManager.getInstance().addToRequestQueue(jsonObjectRequest);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}

	public void stringRequest(String url, final int method, final JSONObject header, final JSONObject body) {
		JSONObject paramsBody = body;
		Log.d("RequestHandler", "url >> " + url + ">>" + method);
		Log.d("RequestHandler", "Header >> " + header);
		Log.d("RequestHandler", "body>> " + body);

		try {
			if (method == Request.Method.GET && null != body && body.length() > 0) {
				JSONArray keys = body.names();
				url = url.concat("?");
				for (int i = 0; i < keys.length(); i++) {
					url = url.concat(keys.getString(i)).concat("=").concat(body.getString(keys.getString(i))).concat("&");
				}
				url = url.substring(0, url.length() - 1);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				stringRequestListener.onResponseResult(true, response);
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				stringRequestListener.onResponseResult(false, error.toString());
			}
		}) {

			@Override
			protected Response<String> parseNetworkResponse(NetworkResponse response) {
				try {
					if (response.data != null && response.data.length == 0) {
						byte[] responseData = "{}".getBytes("UTF8");
						response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				return super.parseNetworkResponse(response);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<>();
				params.put("Content-Type", "application/json; charset=utf-8");
				params.put("Accept", "application/json");
				if (header == null) return params;
				try {
					JSONArray headerKeys = new JSONArray();
					if (header.names() != null) headerKeys = header.names();
					for (int i = 0; i < headerKeys.length(); i++) {
						params.put(headerKeys.getString(i), header.getString(headerKeys.getString(i)));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return params;
			}

			@Override
			public String getBodyContentType() {
				return "application/x-www-form-urlencoded; charset=UTF-8";
			}

			@Override
			protected String getParamsEncoding() {
				return "charset=UTF-8";
			}
		};
		try {
			AppManager.getInstance().addToRequestQueue(stringRequest);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}
	}

}