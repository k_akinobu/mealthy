package com.camitss.mealthy.request.objects;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by Viseth on 4/19/2017.
 */

public abstract class BaseRequest {
	private Context context;

	public BaseRequest(Context context) {
		this.context = context;
	}

//	private RequestListener requestListener;
//	private StringRequestListener stringRequestListener;
//
//	public void setResponseListener(RequestListener requestListener) {
//		this.requestListener = requestListener;
//	}
//
//	public void setStringRequestListener(StringRequestListener stringRequestListener) {
//		this.stringRequestListener = stringRequestListener;
//	}

	public abstract String getUrl();
	public abstract int getMethod();
	public abstract JSONObject getHeader();
	public abstract JSONObject getBody();

//	public void jsonRequest() {
//
//		JSONObject paramsBody = getBody();
//		Log.d("RequestHandler", "url >> " + getUrl() + ">>" + getMethod());
//		Log.d("RequestHandler", "Header >> " + getHeader());
//		Log.d("RequestHandler", "body>> " + getBody());
//
//		try {
//			if (getMethod() == Request.Method.GET && null != getBody() && getBody().length() > 0) {
//				JSONArray keys = getBody().names();
//				String url = getUrl().concat("?");
//				for (int i = 0; i < keys.length(); i++) {
//					url = url.concat(keys.getString(i)).concat("=").concat(getBody().getString(keys.getString(i))).concat("&");
//				}
//				url = url.substring(0, url.length() - 1);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, paramsBody, new Response.Listener<JSONObject>() {
//			@Override
//			public void onResponse(JSONObject response) {
//				requestListener.onResponseResult(true, response);
//			}
//
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				try {
//					VolleyError errors = new VolleyError(new String(error.networkResponse.data));
//
//					JSONObject err = new JSONObject();
//					err.put("status_code", error.networkResponse.statusCode);
//					err.put("message", errors.getMessage());
//					requestListener.onResponseResult(false, err);
//				} catch (JSONException e) {
//					requestListener.onResponseResult(false, new JSONObject());
//				} catch (NullPointerException npe) {
//					requestListener.onResponseResult(false, new JSONObject());
//					npe.printStackTrace();
//				}
//			}
//		}
//		) {
//			@Override
//			protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//				try {
//					if (response.data.length == 0) {
//						byte[] responseData = "{}".getBytes("UTF8");
//						response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
//					}
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
//				return super.parseNetworkResponse(response);
//			}
//
//			@Override
//			public Map<String, String> getHeaders() throws AuthFailureError {
//				Map<String, String> params = new HashMap<>();
//				String token = "";
//				try {
//					token = RealmHelper.with(context).getRealm().where(TokenObject.class).findFirst().getToken();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
////				token = "52B016F5-AC44-4CAB-B080-99756F88BBF6";
//				params.put("Content-Type", "application/json; charset=utf-8");
//				params.put("Accept", "application/json");
//				params.put("X-Mealthy-Token", token);
//				if (header == null) return params;
//				try {
//					JSONArray headerKeys = new JSONArray();
//					if (header.names() != null) headerKeys = header.names();
//					for (int i = 0; i < headerKeys.length(); i++) {
//						params.put(headerKeys.getString(i), header.getString(headerKeys.getString(i)));
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//				return params;
//			}
//		};
//
//		try {
//			AppManager.getInstance().addToRequestQueue(jsonObjectRequest);
//		} catch (NullPointerException npe) {
//			npe.printStackTrace();
//		}
//	}
//
//	public void stringRequest(String url, final int method, final JSONObject header, final JSONObject body) {
//		JSONObject paramsBody = body;
//		Log.d("RequestHandler", "url >> " + url + ">>" + method);
//		Log.d("RequestHandler", "Header >> " + header);
//		Log.d("RequestHandler", "body>> " + body);
//
//		try {
//			if (method == Request.Method.GET && null != body && body.length() > 0) {
//				JSONArray keys = body.names();
//				url = url.concat("?");
//				for (int i = 0; i < keys.length(); i++) {
//					url = url.concat(keys.getString(i)).concat("=").concat(body.getString(keys.getString(i))).concat("&");
//				}
//				url = url.substring(0, url.length() - 1);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//		StringRequest stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
//			@Override
//			public void onResponse(String response) {
//				stringRequestListener.onResponseResult(true, response);
//			}
//		}, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				stringRequestListener.onResponseResult(false, error.toString());
//			}
//		}) {
//
//			@Override
//			protected Response<String> parseNetworkResponse(NetworkResponse response) {
//				try {
//					if (response.data!=null && response.data.length == 0) {
//						byte[] responseData = "{}".getBytes("UTF8");
//						response = new NetworkResponse(response.statusCode, responseData, response.headers, response.notModified);
//					}
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
//				return super.parseNetworkResponse(response);
//			}
//			@Override
//			public Map<String, String> getHeaders() throws AuthFailureError {
//				Map<String, String> params = new HashMap<>();
//				params.put("Content-Type", "application/json; charset=utf-8");
//				params.put("Accept", "application/json");
//				if (header == null) return params;
//				try {
//					JSONArray headerKeys = new JSONArray();
//					if (header.names() != null) headerKeys = header.names();
//					for (int i = 0; i < headerKeys.length(); i++) {
//						params.put(headerKeys.getString(i), header.getString(headerKeys.getString(i)));
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//				return params;
//			}
//		};
//		try {
//			AppManager.getInstance().addToRequestQueue(stringRequest);
//		} catch (NullPointerException npe) {
//			npe.printStackTrace();
//		}
//	}
}
