package com.camitss.mealthy.request.objects;

import android.content.Context;


import org.json.JSONObject;


/**
 * Created by Viseth on 4/19/2017.
 */

public class FollowUserRequest extends BaseRequest {
	public FollowUserRequest(Context context) {
		super(context);
	}

	@Override
	public String getUrl() {
		return null;
	}

	@Override
	public int getMethod() {
		return 0;
	}

	@Override
	public JSONObject getHeader() {
		return null;
	}

	@Override
	public JSONObject getBody() {
		return null;
	}

}
