package com.camitss.mealthy;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v13.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.camitss.mealthy.activity.BaseActivity;
import com.camitss.mealthy.utils.location.LocationController;
import com.camitss.mealthy.utils.location.LocationUpdaterInterface;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends BaseActivity
		implements OnMapReadyCallback,
		LocationUpdaterInterface,
		GoogleMap.OnMapClickListener,
		GoogleMap.OnMarkerDragListener {

	public final static String LAT = "lat";
	public final static String LONG = "long";
	public final static String ADDRESS = "address";
	public final static String NAME = "name";

	private GoogleMap googleMap;
	private LatLng latLng = null;
	private LatLng current = null;
	private String name;
	private String fullAddress;

	private CardView cardSearch;
	private TextView tvSearch;
	private LocationController mLocationController;
	private TextView textCurAddress;

	public static void launchForResult(Context context, String name, String fullAddress, double latitude, double longitude, int requestCode) {
		Intent intent = new Intent(context, MapsActivity.class)
				.putExtra(LAT, latitude)
				.putExtra(LONG, longitude)
				.putExtra(NAME, name)
				.putExtra(ADDRESS, fullAddress);
		((AppCompatActivity) context).startActivityForResult(intent, requestCode);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_location);

		textCurAddress = (TextView) findViewById(R.id.text_current_address);

		mLocationController = new LocationController(this);
		findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent();
				i.putExtra("location", textCurAddress.getText().toString().trim());
				setResult(RESULT_OK, i);
				finish();
			}
		});
		final LinearLayout llUseCurrent = (LinearLayout) findViewById(R.id.ll_useCurrent);
		llUseCurrent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (current != null) {
					googleMap.clear();
					googleMap.addMarker(new MarkerOptions().position(current).draggable(true));
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, 17), 1, null);
					getAddress(latLng.latitude, latLng.longitude);
				}
			}
		});

		double lat, lng;
		lat = getIntent().getDoubleExtra(LAT, 0);
		lng = getIntent().getDoubleExtra(LONG, 0);
		name = getIntent().getStringExtra(NAME);
		fullAddress = getIntent().getStringExtra(ADDRESS);
		if (lat != 0 && lng != 0) {
			latLng = new LatLng(lat, lng);
		}
		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		mLocationController.setLocationUpdateListener(MapsActivity.this);
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		mLocationController.checkPermissions();
	}

	@Override
	protected void onStart() {
		super.onStart();
		mLocationController.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mLocationController.onStop();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mLocationController.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mLocationController.onResume();
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		startActivityAnimationMain();
	}

	@Override
	public void onMapReady(final GoogleMap googleMap) {
		googleMap.setOnMapClickListener(this);
		this.googleMap = googleMap;
	}

	@Override
	public void onRealTimeUpdateLocationCallBack(Object type, Location location) {

	}

	@Override
	public void onLocationCallBack(Object type, Location location) {
		if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			googleMap.setMyLocationEnabled(true);
		}
		if (location != null) {
			current = new LatLng(location.getLatitude(), location.getLongitude());
			if (latLng == null) {
				latLng = new LatLng(location.getLatitude(), location.getLongitude());
			}
		} else {
			current = new LatLng(0, 0);
			if (latLng == null) {
				latLng = new LatLng(0, 0);
			}
		}
		googleMap.setOnMarkerDragListener(this);
		googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				getAddress(latLng.latitude, latLng.longitude);
				return true;
			}
		});
		googleMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17), 1, null);
		getAddress(latLng.latitude, latLng.longitude);
	}

	@Override
	public void onUserPermissionLocation(boolean type) {

	}

	@Override
	public void onMapClick(final LatLng latLng) {
		this.latLng = latLng;
		googleMap.clear();
		googleMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17), 1000, null);
		getAddress(latLng.latitude, latLng.longitude);
	}


	@Override
	public void onMarkerDragStart(Marker marker) {

	}

	@Override
	public void onMarkerDrag(Marker marker) {

	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		latLng = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
		getAddress(latLng.latitude, latLng.longitude);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		mLocationController.onRequestPermissionsResultCallBack(requestCode, permissions, grantResults);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		mLocationController.onActivityResultCallBack(requestCode, resultCode, data);
		if (requestCode == 300) {
			cardSearch.setVisibility(View.VISIBLE);
			if (resultCode == RESULT_OK) {
				Place place = PlaceAutocomplete.getPlace(this, data);
				if (place != null) {
					tvSearch.setText(place.getName());
					latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
					googleMap.clear();
					googleMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17), 1000, null);
				} else {
					tvSearch.setText("");
				}
			} else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
				Status status = PlaceAutocomplete.getStatus(this, data);
				Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			Address obj = addresses.get(0);
			String add = obj.getAddressLine(0);
			add = add + "\n" + obj.getCountryName();
			add = add + "\n" + obj.getCountryCode();
			add = add + "\n" + obj.getAdminArea();
			add = add + "\n" + obj.getPostalCode();
			add = add + "\n" + obj.getSubAdminArea();
			add = add + "\n" + obj.getLocality();
			add = add + "\n" + obj.getSubThoroughfare();

			Log.v("IGA", obj.getMaxAddressLineIndex() + ":" + lat + ":" + lng + "Address " + add);
			// Toast.makeText(this, "Address=>" + add,
			// Toast.LENGTH_SHORT).show();

			// TennisAppActivity.showDialog(add);
			textCurAddress.setText(obj.getAddressLine(0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
}
