package com.camitss.mealthy.utils.customcalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Viseth on 5/30/2017.
 */

public class WheelPickerYear extends WheelPicker {
	private WheelPicker.Adapter adapter;
	private int defaultIndex;

	private int todayPosition;
	private String selectedText;


	public WheelPickerYear(Context context) {
		this(context, null);
	}

	public WheelPickerYear(Context context, AttributeSet attrs) {
		super(context, attrs);
		adapter = new Adapter();
		setAdapter(adapter);
		updateValue();
		setSelectedItemPosition((int) (adapter.getItemCount() / 2));
	}

	private void updateValue() {
		final List<String> data = new ArrayList<>();
		int curYear = Calendar.getInstance().get(Calendar.YEAR);
		for (int i = 1940; i <= curYear; i++) {
			data.add(i+"年");
		}
		todayPosition = data.size();
		defaultIndex = todayPosition;

		adapter.setData(data);
	}

	@Override
	protected void onItemSelected(int position, Object item) {
		setSelectedItemPosition(position);
	}

	@Override
	protected void onItemCurrentScroll(int position, Object item) {

	}

	@Override
	protected String getFormattedValue(Object value) {
		return null;
	}

	@Override
	public int getDefaultItemPosition() {
		return defaultIndex;
	}

	public Adapter getAdapter() {
		return adapter;
	}
}
