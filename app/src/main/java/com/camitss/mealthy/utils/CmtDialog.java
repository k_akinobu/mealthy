package com.camitss.mealthy.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.MealthyMenu;


/**
 * Created by Darith on 7/6/2016.
 */
public class CmtDialog {
	private Activity mActivity;
	private MealthyMenu menu;

	public CmtDialog(Activity activity, MealthyMenu menu) {
		this.mActivity = activity;
		this.menu = menu;
	}


	public void createDialog() {
		Dialog dialog = new Dialog(mActivity);
		dialog.setContentView(R.layout.cmtdialog);
		dialog.show();
		preparedata(dialog);
		dialog.setCancelable(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER);
		window.setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT);

	}

	private void preparedata(final Dialog dialog) {
		ImageView cancel = (ImageView) dialog.findViewById(R.id.btn_close);
		ImageView profille = (ImageView) dialog.findViewById(R.id.nc_profile);
		TextView cmt = (TextView) dialog.findViewById(R.id.nc_cmt);

//

		cmt.setText(menu.getNutritionist_comments().get(0).getNc_body());
		Glide.with(mActivity).load(menu.getNutritionist_comments().get(0).getNutritionistUser().getNuser_profile_image()).placeholder(R.drawable.bg_grey).transform(new CircleTransformation(mActivity)).into(profille);

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.cancel();
			}
		});


	}
}
