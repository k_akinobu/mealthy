package com.camitss.mealthy.utils.customcalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 5/30/2017.
 */

public class WheelPickerMonth extends WheelPicker {
	private WheelPicker.Adapter adapter;
	private int defaultIndex;

	private int todayPosition;
	private Context context;

	public WheelPickerMonth(Context context) {
		this(context, null);
	}

	public WheelPickerMonth(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		adapter = new Adapter();
		setAdapter(adapter);
		updateValue();
		setSelectedItemPosition((int)(adapter.getItemCount() / 2));
	}

	private void updateValue() {
		final List<String> data = new ArrayList<>();
		String[] months = new DateFormatSymbols().getMonths();
		for (int i = 0; i < months.length; i++) {
			data.add(months[i]);
		}
		todayPosition = data.size();
		defaultIndex = todayPosition;

		adapter.setData(data);
	}

	@Override
	protected void onItemSelected(int position, Object item) {
		setSelectedItemPosition(position);
	}

	@Override
	protected void onItemCurrentScroll(int position, Object item) {

	}

	@Override
	protected String getFormattedValue(Object value) {
		return null;
	}

	@Override
	public int getDefaultItemPosition() {
		return defaultIndex;
	}

	private List<String> getMonthList() {
		List<String> monthsList = new ArrayList<String>();
		String[] months = new DateFormatSymbols().getMonths();
		for (int i = 0; i < months.length; i++) {
			monthsList.add(months[i]);
		}
		Log.d("month", months.length + ":" + getMonthList().size() + "");
		return monthsList;
	}

	public Adapter getAdapter() {
		return adapter;
	}
}
