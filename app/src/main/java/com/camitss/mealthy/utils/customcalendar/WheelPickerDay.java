package com.camitss.mealthy.utils.customcalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Viseth on 5/30/2017.
 */

public class WheelPickerDay extends WheelPicker {
	private WheelPicker.Adapter adapter;
	private int defaultIndex;
	private Context context;

	private int todayPosition;

	public WheelPickerDay(Context context) {
		this(context, null);
	}

	public WheelPickerDay(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		adapter = new Adapter();
		setAdapter(adapter);
		updateValue();
		setSelectedItemPosition((int) (adapter.getItemCount() / 2));
	}

	private void updateValue() {
		final List<String> data = new ArrayList<>();

		for (int i = 1; i <= 31; i++) {
			data.add(i + "日");
		}

		todayPosition = data.size();
		defaultIndex = todayPosition;

		adapter.setData(data);
	}

	@Override
	protected void onItemSelected(int position, Object item) {
		setSelectedItemPosition(position);
	}

	@Override
	protected void onItemCurrentScroll(int position, Object item) {

	}

	@Override
	protected String getFormattedValue(Object value) {
		return null;
	}

	@Override
	public int getDefaultItemPosition() {
		return defaultIndex;
	}


	public Adapter getAdapter() {
		return adapter;
	}
}
