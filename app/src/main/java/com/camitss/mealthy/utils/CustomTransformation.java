package com.camitss.mealthy.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/**
 * Created by Viseth on 5/5/2017.
 */

public class CustomTransformation extends BitmapTransformation{
	public CustomTransformation(Context context) {
		super(context);
	}

	@Override
	protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int maxWidth, int maxHeight) {
		if (maxHeight > 0 && maxWidth > 0) {
			int width = toTransform.getWidth();
			int height = toTransform.getHeight();

			float ratioBitmap = (float) width / (float) height;
			float ratioMax = (float) maxWidth / (float) maxHeight;

			int finalWidth = maxWidth;
			int finalHeight = maxHeight;
			if (ratioMax > ratioBitmap) {
				finalWidth = (int) ((float) maxHeight * ratioBitmap);
			} else {
				finalHeight = (int) ((float)maxWidth / ratioBitmap);
			}

			return Bitmap.createScaledBitmap(toTransform, previousEvenNumber(finalWidth), previousEvenNumber(finalHeight), true);
		}
		else {
			return toTransform;
		}
	}

	private int previousEvenNumber(int x){
		if ( (x & 1) == 0 )
			return x;
		else
			return x - 1;
	}

	@Override
	public String getId() {
		return "glide_custom_transformation";
	}

}
