package com.camitss.mealthy.utils.location;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.camitss.mealthy.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;


public class MapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {
	private final String TAG = "ZeMapFragment";
	private final String CHECK_LOCATION_ALLOW = "android.location.PROVIDERS_CHANGED";
	private GoogleMap mMap;
	private List<String> listPermission = new ArrayList<>();
	private ProgressBar progress;
	private TextView countBusiness, countPeople;
	private CameraPosition cameraPosition;

	private SharedPreferences prefs;

	private GPSTracker gps;
	private boolean isEnableCurrentLocation = true;
	private LatLngBounds.Builder builder = new LatLngBounds.Builder();
	private LatLng onLocation;
	private Timer timer;
	private LocationSource.OnLocationChangedListener listener;
	private Location location;

	private static MapFragment INSTANCE;

	public static MapFragment getNewInstance() {
		INSTANCE = new MapFragment();
		return INSTANCE;
	}

	public static MapFragment getInstance() {
		return INSTANCE;
	}

	private BroadcastReceiver mBroadcastReceiverGPS = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equalsIgnoreCase(CHECK_LOCATION_ALLOW)) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						handleGPSonCreate();
					}
				});
			}
		}
	};

	public void setEnableStreetView(boolean isEnableStreetView) {
		if (isEnableStreetView) {
			getView().findViewById(R.id.google_street_view_btn).setVisibility(View.VISIBLE);
			getView().findViewById(R.id.map_view_btn).setVisibility(View.GONE);
		} else {
			getView().findViewById(R.id.google_street_view_btn).setVisibility(View.GONE);
			getView().findViewById(R.id.map_view_btn).setVisibility(View.VISIBLE);
		}
	}

	public CameraPosition getCameraPosition() {
		return cameraPosition;
	}

	public void setCameraPosition(CameraPosition cameraPosition) {
		if (cameraPosition != null) {
			this.cameraPosition = cameraPosition;
			saveLastZoom(this.cameraPosition.zoom);
		}
	}

	public GoogleMap getMap() {
		return mMap;
	}

	public void setMap(GoogleMap mMap) {
		this.mMap = mMap;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_map, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		gps = new GPSTracker(getContext());
		countBusiness = (TextView) view.findViewById(R.id.count_business);
		countPeople = (TextView) view.findViewById(R.id.count_peoples);
		SupportMapFragment mapFragment = new SupportMapFragment();
		mapFragment.getMapAsync(this);
		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.mapFragment, mapFragment);
		transaction.commit();

		handleGPSonCreate();
		ImageView mapBtn = (ImageView) view.findViewById(R.id.map_view_btn);
		ImageView streetViewBtn = (ImageView) view.findViewById(R.id.google_street_view_btn);
		streetViewBtn.setOnClickListener(this);
		mapBtn.setOnClickListener(this);

		progress = (ProgressBar) view.findViewById(R.id.prog_product);
		progress.setVisibility(View.GONE);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int isFineLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
		if (isFineLocation != PackageManager.PERMISSION_GRANTED) {
			listPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
		}
		prefs = getActivity().getSharedPreferences("map_location", Context.MODE_PRIVATE);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case 22:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
						return;
					}
					setEnableCurrentLocation(isEnableCurrentLocation());
					if (gps.isGPSEnable()) {
					} else {
						startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				} else {
					Log.i(TAG, "permission not grand");
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				break;
		}
	}

	@Override
	public void onMapReady(final GoogleMap map) {
		mMap = map;
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		mMap.getUiSettings().setRotateGesturesEnabled(true);
		mMap.getUiSettings().setMapToolbarEnabled(true);
		moveCamera(getOnViewLatLng());
		LatLng cure = getCurrentLatLng();
		if (cure == null && gps.isGPSEnable()) {
			cure = new LatLng(gps.latitude, gps.longitude);
			setAnimateCamera(cure, 12);
		}
		if (cure != null) {
			setCurrentLocation(cure, listener, location);
			setAnimateCamera(cure);
		}
		mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {

			@Override
			public boolean onMyLocationButtonClick() {
				float zoom = mMap.getCameraPosition().zoom;
				LatLng cure = getCurrentLatLng();
				if (cure != null) {
					setAnimateCamera(cure, (zoom < 12) ? 12 : zoom);
					setCurrentLocation(cure, listener, location);
				}
				return true;
			}
		});
//		mClusterManager = new ClusterManager<BaseMarker>(getContext(), mMap) {
//			@Override
//			public void onCameraIdle() {
//				super.onCameraIdle();
//				MapFragment.this.onCameraChange(getCameraPosition());
//			}
//		};
////        mClusterManager.setRenderer(new MarkerRenderer());
//		mClusterManager.setOnClusterClickListener(this);
//		mClusterManager.setOnClusterItemClickListener(this);
//		mMap.setOnCameraIdleListener(mClusterManager);
//		mMap.setOnMarkerClickListener(mClusterManager);
//		mMap.setOnInfoWindowClickListener(mClusterManager);

		setEnableCurrentLocation(isEnableCurrentLocation());
	}

	public boolean isEnableCurrentLocation() {
		return isEnableCurrentLocation;
	}

	public void setEnableCurrentLocation(boolean enable) {
		isEnableCurrentLocation = enable;
		if (mMap != null && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			mMap.setMyLocationEnabled(enable);
			if (enable)
				mMap.setLocationSource(new CurrentLocationProvider(getContext()));
			else {
				mMap.setLocationSource(null);
			}
		}
	}

	public void onCameraChange(CameraPosition cameraPosition) {
		if (cameraPosition != null) {
			setCameraPosition(cameraPosition);
//            onLoadMarker(cameraPosition.target, ((float) (25000 / (Math.pow(2, cameraPosition.zoom)))));
		}
	}

//	@Override
//	public boolean onClusterItemClick(BaseMarker baseMarker) {
//		baseMarker.onMarkerClick();
//		return true;
//	}
//
//	@Override
//	public boolean onClusterClick(Cluster<BaseMarker> cluster) {
//		LatLngBounds.Builder builder = new LatLngBounds.Builder();
//		for (BaseMarker p : cluster.getItems()) {
//			builder.include(p.getPosition());
//		}
//		autoCameraMove(builder);
//		return true;
//	}

	public void onLoadMarker(LatLng location, float dis) {
		if (gps.isGPSEnable()) {
//            loadData(location, dis);
		}
	}

	public void setLoadingVisibility(int visibility) {
		try {
			progress.setVisibility(visibility);
		} catch (Exception e) {
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
//		clearAllMarker();
	}

//	public void addAllBusinessMarker(JSONArray business) {
//		new AsyncTask<JSONArray, List<BaseMarker>, List<BaseMarker>>() {
//			@Override
//			protected List<BaseMarker> doInBackground(JSONArray... params) {
//				List<BaseMarker> markers = new ArrayList<>();
//				try {
//					int size = params[0].length();
//					BusinessMarker tmp = null;
//					for (int i = 0; i < size; i++) {
//						try {
//							tmp = BusinessMarker.create(getContext(), params[0].getJSONObject(i));
//							if (tmp != null) {
//								markers.add(tmp);
//								builder.include(tmp.getPosition());
//							}
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					params[0] = null;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				return markers;
//			}
//
//			@Override
//			protected void onPostExecute(List<BaseMarker> baseMarkers) {
//				super.onPostExecute(baseMarkers);
//				addAllBusinessMarker(baseMarkers);
//			}
//		}.execute(business);
//
//	}
//
//	public void addAllBusinessMarker(List<BaseMarker> business) {
//		businessMarkers.clear();
//		businessMarkers.addAll(business);
//		countBusiness.setText(String.valueOf(businessMarkers.size()));
//		addAllMarker(businessMarkers);
//		business.clear();
//	}
//
//	public void addAllPeopleMarker(JSONArray people) {
//		new AsyncTask<JSONArray, List<BaseMarker>, List<BaseMarker>>() {
//
//			@Override
//			protected List<BaseMarker> doInBackground(JSONArray... params) {
//				List<BaseMarker> markers = new ArrayList<>();
//				try {
//					int size = params[0].length();
//					PeopleMarker tmp = null;
//					for (int i = 0; i < size; i++) {
//						try {
//							tmp = PeopleMarker.create(getContext(), params[0].getJSONObject(i));
//							if (tmp != null) {
//								markers.add(tmp);
//								builder.include(tmp.getPosition());
//							}
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					params[0] = null;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				return markers;
//			}
//
//			@Override
//			protected void onPostExecute(List<BaseMarker> baseMarkers) {
//				super.onPostExecute(baseMarkers);
//				addAllPeopleMarker(baseMarkers);
//			}
//		}.execute(people);
//	}
//
//	public void addAllPeopleMarker(List<BaseMarker> people) {
//		peopleMarkers.clear();
//		peopleMarkers.addAll(people);
//		countPeople.setText(String.valueOf(peopleMarkers.size()));
//		addAllMarker(peopleMarkers);
//		people.clear();
//	}
//
//	public void addAllMarker(JSONObject data) {
//		clearAllMarker();
//		JSONArray marker;
//		try {
//			marker = data.getJSONArray("business");
//			addAllBusinessMarker(marker);
//		} catch (Exception e) {
//		}
//		try {
//			marker = data.getJSONArray("profile");
//			addAllPeopleMarker(marker);
//		} catch (Exception e) {
//		}
//		marker = null;
//		data = null;
//	}

	public void autoCameraMove(LatLngBounds.Builder builder) {
		if (builder == null)
			return;
		LatLngBounds bound = builder.build();
		mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bound, 200), 1000, null);
	}

	public void autoCameraMove() {
		autoCameraMove(builder);
	}

	public void setAnimateCamera(LatLng latLng, float zoom) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
		if (mMap != null)
			mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	public void setAnimateCamera(LatLng latLng) {
		setAnimateCamera(latLng, getLastZoom());
	}

	public void moveCamera(LatLng latLng) {
		moveCamera(latLng, getLastZoom());
	}

	public void moveCamera(LatLng latLng, float zoom) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
		if (mMap != null)
			mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

//	public void addMarker(BaseMarker b) {
//		try {
//			mClusterManager.addItem(b);
//			mClusterManager.cluster();
//		} catch (Exception e) {
//		}
//	}
//
//	public void addAllMarker(final List<BaseMarker> b) {
//		try {
//			mClusterManager.addItems(b);
//			mClusterManager.cluster();
//			b.clear();
//		} catch (Exception e) {
//		}
//
//	}
//
//	private void clearAllMarker() {
//		try {
//			builder = new LatLngBounds.Builder();
//			mClusterManager.clearItems();
//			mClusterManager.cluster();
//			businessMarkers.clear();
//			peopleMarkers.clear();
////            mMap.clear();
////            setCurrentLocation(getCurrentLatLng());
//		} catch (Exception e) {
//		}
//	}

	public float getLastZoom() {
		return prefs.getFloat("zoom_view", 0);
	}

	public void saveLastZoom(float zoom) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat("zoom_view", zoom).apply();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
//            case R.id.gpsButton:
//                if (!listPermission.isEmpty()) {
//                    requestPermissions(listPermission.toArray(new String[listPermission.size()]), 22);
//                } else {
//                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                }
//                break;
			default:
				break;
		}
	}


	@Override
	public void onStart() {
		super.onStart();
		getContext().registerReceiver(mBroadcastReceiverGPS, new IntentFilter(CHECK_LOCATION_ALLOW));
	}

	@Override
	public void onStop() {
		super.onStop();
		getContext().unregisterReceiver(mBroadcastReceiverGPS);
	}

	@Override
	public void onResume() {
		super.onResume();
		handleGPSonCreate();
		if (getCameraPosition() != null)
			onCameraChange(getCameraPosition());
		setEnableCurrentLocation(isEnableCurrentLocation());
	}

	public void handleGPSonCreate() {
		if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		if (gps.isGPSEnable()) {
			setEnableStreetView(true);
		} else {
			Log.i(TAG, "GPS onCreate disable");
		}

	}

	private void setCurrentLocation(LatLng latLng, final LocationSource.OnLocationChangedListener listener, final Location location) {
		if (latLng == null)
			return;
		if (!isEnableCurrentLocation()) {
		}
	}

	private void saveOnViewLatLng(LatLng latLng) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat("view_latitude", (float) latLng.latitude);
		editor.putFloat("view_longitude", (float) latLng.longitude);
		editor.apply();
	}

	private LatLng getOnViewLatLng() {
		double lat = (double) prefs.getFloat("view_latitude", 0);
		double lon = (double) prefs.getFloat("view_longitude", 0);
		return new LatLng(lat, lon);
	}

	private void saveCurrentLatLng(LatLng latLng) {
		if (latLng == null)
			return;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putFloat("current_latitude", (float) latLng.latitude);
		editor.putFloat("current_longitude", (float) latLng.longitude);
		editor.apply();
	}

	public LatLng getCurrentLatLng() {
		if (prefs.contains("current_latitude") && prefs.contains("current_longitude") && gps.isGPSEnable()) {
			float lat = prefs.getFloat("current_latitude", 86);
			float lon = prefs.getFloat("current_longitude", 181);
			return new LatLng(lat, lon);
		}
		return null;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		/* by Rattanak if set title will affixes to search */
//        getActivity().setTitle(getString(R.string.home));
	}

//	private class MarkerRenderer extends DefaultClusterRenderer<BaseMarker> {
//
//		public MarkerRenderer() {
//			super(getContext(), mMap, mClusterManager);
//		}
//
//		@Override
//		protected void onBeforeClusterItemRendered(BaseMarker person, MarkerOptions markerOptions) {
//			person.setMarkerOption(markerOptions);
//		}
//
//		@Override
//		public void setOnClusterItemInfoWindowClickListener(ClusterManager.OnClusterItemInfoWindowClickListener<BaseMarker> listener) {
//			super.setOnClusterItemInfoWindowClickListener(listener);
//		}
//
//		@Override
//		protected boolean shouldRenderAsCluster(Cluster cluster) {
//			return cluster.getSize() > 20;
//		}
//
//	}

	public class CurrentLocationProvider implements LocationSource, android.location.LocationListener {

		private LocationManager locationManager;

		public CurrentLocationProvider(Context context) {
			locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		}

		@Override
		public void activate(OnLocationChangedListener listen) {
			listener = listen;
			LocationProvider gpsProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
			if (gpsProvider != null) {
				if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
				locationManager.requestLocationUpdates(gpsProvider.getName(), 0, 0, this);
			} else {
				LocationProvider networkProvider = locationManager.getProvider(LocationManager.NETWORK_PROVIDER);
				if (networkProvider != null) {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
				}
			}

			LatLng latLng = getCurrentLatLng();
			location = locationManager.getLastKnownLocation(gpsProvider.getName());
			if (latLng == null && location != null) {
				latLng = new LatLng(location.getLatitude(), location.getLongitude());
				float zoom = mMap.getCameraPosition().zoom;
				setAnimateCamera(latLng, (zoom < 12) ? 12 : zoom);
			} else {
				try {
					if (gps.isGPSEnable()) {
						latLng = new LatLng(gps.getLatitude(), gps.getLongitude());
					}
				} catch (Exception e) {
				}
			}
			if (listener != null && location != null) {
				listener.onLocationChanged(location);
			}

			saveCurrentLatLng(latLng);
			setCurrentLocation(latLng, listener, location);
		}

		@Override
		public void deactivate() {
			try {
				if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
			} catch (Exception e) {
			}
			locationManager.removeUpdates(this);
		}

		@Override
		public void onLocationChanged(Location locat) {
			location = locat;
			if (listener != null) {
				listener.onLocationChanged(location);
			}
			LatLng latLng = getCurrentLatLng();
			if (latLng == null) {
				latLng = new LatLng(location.getLatitude(), location.getLongitude());
				float zoom = mMap.getCameraPosition().zoom;
				setAnimateCamera(latLng, (zoom < 12) ? 12 : zoom);
			} else
				latLng = new LatLng(location.getLatitude(), location.getLongitude());

			saveCurrentLatLng(latLng);
			setCurrentLocation(latLng, listener, location);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

	}
}