package com.camitss.mealthy.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.Target;
import com.camitss.mealthy.R;
import com.camitss.mealthy.object.FeedItem;
import com.camitss.mealthy.object.UserFeedsInProfile;
import com.camitss.mealthy.utils.CustomImageView.CustomImageview;
import com.camitss.mealthy.utils.CustomImageView.ZoomageView;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Viseth on 3/30/2017.
 */

public class Utils {
	public static Context context;

	public static String IntegerWithCommaFormat(int num) {
//		int bigNumber = 1234567;
//		String formattedNumber = String.format("%,d", bigNumber);
		return NumberFormat.getNumberInstance(Locale.US).format(num);
	}

	public static void showProgressDialog(Context context, ProgressDialog progressDialog) {
		progressDialog.setCancelable(false);
		progressDialog.setMessage(context.getString(R.string.data_getting));
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setIndeterminate(true);
		progressDialog.show();
	}

	public static void showProgressDialog(Context context, ProgressDialog progressDialog, String message) {
		progressDialog.setCancelable(false);
		if (message != null) {
			progressDialog.setMessage(message);
		} else {
			progressDialog.setMessage(context.getString(R.string.indicator_title));
		}
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setIndeterminate(true);
		progressDialog.show();
	}

	public static String fromStringDateWithTimeZoneToFormate(String date) {
		String[] dates = date.split("\\.", 2);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = sdf.parse(dates[0]);
			return new SimpleDateFormat("MM/dd HH:mm").format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String fromStringDateWithTimeZoneToFormateMMddHH(String date) {
		String[] dates = date.split("\\.", 2);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = sdf.parse(dates[0]);
			return new SimpleDateFormat("MM/dd HH").format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String fromStringDateWithTimeZoneToFormateMMdd(String date) {
		String[] dates = date.split("\\.", 2);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = sdf.parse(dates[0]);
			return new SimpleDateFormat("MM/dd").format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "";
	}

	public Utils(Context context) {
		this.context = context;
	}

	public static Utils with(Context context) {
		Utils utils = new Utils(context);
		return utils;
	}

	public void displayImageItem(final Activity activity, ImageView mImageView, String url) {
		final AlertDialog dialog = new AlertDialog.Builder(activity, R.style.alertdialog_theme_no_bg).create();//context, R.style.alertdialog_theme_no_bg
		final View view = LayoutInflater.from(context).inflate(R.layout.layout_image_item, null);
		final ZoomageView imageView = (ZoomageView) view.findViewById(R.id.image_item);
		ImageView close = (ImageView) view.findViewById(R.id.close);
		dialog.setCancelable(true);
		final RelativeLayout wholeLayout = (RelativeLayout) view.findViewById(R.id.whole_layout);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ViewGroup vGroup = (ViewGroup) view.getParent();
				vGroup.removeView(view);
//				Toast.makeText(context, "close Clicked", Toast.LENGTH_SHORT).show();
				dialog.dismiss();
			}
		});

		int w = mImageView.getWidth();
		int h = mImageView.getHeight();
		Log.d("size", w + "<<>>" + h);
		Glide.with(context).load(url).listener(new com.bumptech.glide.request.RequestListener<String, GlideDrawable>() {
			@Override
			public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
				return false;
			}

			@Override
			public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
				dialog.setView(view);
				dialog.show();

				return false;
			}
		}).override(w, h).into(imageView);
	}

	public static String convertFunctionName(String s, int id) {
		return String.format(Locale.ENGLISH, s, id);
	}

	public static int getScreenWidth(Activity activity) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int height = displayMetrics.heightPixels;
		int width = displayMetrics.widthPixels;
		return width;
	}

	public static int getScreenHeight(Activity activity) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int height = displayMetrics.heightPixels;
		int width = displayMetrics.widthPixels;
		return height;
	}

	public static int getActionBarHeight(Activity activity) {
		int actionBarHeight = 56;
		// Calculate ActionBar height
		TypedValue tv = new TypedValue();
		if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
		}
		return actionBarHeight;
	}

	public static String beautyCurrentDateFormat() {
		//int year, int monthOfYear, int dayOfMonth,int hour,int minute,int second
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(calendar.getTime());
	}

	public static String getExtendedDuration(int termId) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		calendar.setTime(calendar.getTime());
		if (termId == 1) {
			calendar.add(Calendar.MONTH, 1);
			Log.d("getExtendedDu", beautyCurrentDateFormat() + ">><<" + sdf.format(calendar.getTime()));
			return sdf.format(calendar.getTime());
		} else if (termId == 2) {
			calendar.add(Calendar.MONTH, 3);
			Log.d("getExtendedDu", beautyCurrentDateFormat() + ">><<" + sdf.format(calendar.getTime()));
			return sdf.format(calendar.getTime());
		} else return "2100/12/31 23:19:47";


	}

	public static FeedItem fromUserFeedsInProfileToFeedItem(UserFeedsInProfile obj) {
		FeedItem item = new FeedItem();
		FeedItem.Menu menu = new FeedItem.Menu();
		FeedItem.FeedItemUser user = new FeedItem.FeedItemUser();
		ArrayList<FeedItem.Comment> cmtList = new ArrayList<FeedItem.Comment>();
		item.setId(obj.getId());
		item.setLike(obj.isLike());
		item.setLikesCount(obj.getLikesCount());
		item.setPrivacyCount(obj.getPrivacyKind());
		menu.setId(obj.getMenu().getId());
		menu.setCgmMenuName(obj.getMenu().getCgmMenuName());
		menu.setCgmShopName(obj.getMenu().getCgmShopName());
		menu.setImage(obj.getMenu().getImage());
		menu.setImageThumb(obj.getMenu().getThumbnailImageUrl());
		menu.setCreatedAt(obj.getMenu().getCreatedAt());
		menu.setCommentCount(obj.getMenu().getCommentsCount());
		menu.setShopId(obj.getMenu().getShopId());
		FeedItem.FeedItemUser userCmt;
		for (int j = 0; j < obj.getMenu().getComments().size(); j++) {
			FeedItem.Comment cmt = new FeedItem.Comment();
			cmt.setId(obj.getMenu().getComments().get(j).getId());
			cmt.setBody(obj.getMenu().getComments().get(j).getBody());
			userCmt = new FeedItem.FeedItemUser();
			userCmt.setId(obj.getMenu().getComments().get(j).getCmtUser().getId());
			userCmt.setName(obj.getMenu().getComments().get(j).getCmtUser().getName());
			userCmt.setProfileImage(obj.getMenu().getComments().get(j).getCmtUser().getProfileImage());
			cmt.setCmtUser(userCmt);
			cmtList.add(cmt);
		}
		menu.setCommentList(cmtList);
		item.setMenu(menu);
		item.setLikesCount(obj.getLikesCount());
		user.setId(obj.getUser().getId());
		user.setName(obj.getUser().getName());
		user.setProfileImage(obj.getUser().getProfileImageUrl());
		user.setUserKind(obj.getUser().getUserKind());
		item.setFeedItemUser(user);
		return item;
	}

	public static ArrayList<String> getImagesPath(Activity activity) {
		Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		ArrayList<String> listOfAllImages = new ArrayList<>();
//		String[] projection = { android.provider.MediaStore.MediaColumns.DATA,
//				MediaStore.Images.Media.BUCKET_DISPLAY_NAME };
		String[] projection = {MediaStore.Images.Media.DATA};
//		Cursor cursor = activity.managedQuery(uri, projection, null, null,null);

//		int column_index = cursor
//				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
		int columnIndexData = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//		int columnIndexFolderName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
		while (cursor.moveToNext()) {
			String pathOfImage = cursor.getString(columnIndexData);
			listOfAllImages.add(pathOfImage);
		}

		return listOfAllImages;
	}

	public static void getAllImagesFromDevice(File root) {
//		File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
		List<String> fileList = new ArrayList<>();
		File listFile[] = root.listFiles();
		if (listFile != null && listFile.length > 0) {
			for (File file : listFile) {
				if(file.isDirectory()) {
					getAllImagesFromDevice(file);
				}else {
					if(file.getName().endsWith(".png")
							|| file.getName().endsWith(".jpg")
							|| file.getName().endsWith(".jpeg")
							) {
						String temp = file.getPath().substring(0, file.getPath().lastIndexOf('/'));
						fileList.add(temp);
					}
				}
			}
		}
	}

	public List<String> getfile(File dir) {
		List<String> fileList = new ArrayList<>();
		File listFile[] = dir.listFiles();
		if (listFile != null && listFile.length > 0) {
			for (int i = 0; i < listFile.length; i++) {
				if (listFile[i].getName().endsWith(".png")
						|| listFile[i].getName().endsWith(".jpg")
						|| listFile[i].getName().endsWith(".jpeg")
						) {
					String temp = listFile[i].getPath().substring(0, listFile[i].getPath().lastIndexOf('/'));
					fileList.add(temp);
				}
			}
		}
		return fileList;
	}
}
