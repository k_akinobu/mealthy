package com.camitss.mealthy.utils.twitter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.camitss.mealthy.R;
import com.camitss.mealthy.activity.FeedDisplayActivity;
import com.camitss.mealthy.activity.TwitterLoginProcess;
import com.camitss.mealthy.utils.AppSharedPreferences;
import com.camitss.mealthy.utils.ConnectionDetector;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import twitter4j.GeoLocation;
import twitter4j.StatusUpdate;
import twitter4j.TweetEntity;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterOnShare extends AsyncTask<String, String, Void> {
	private Context mContext;
	private ProgressDialog pDialog;
	private static AlertDialog sAlertDialogTwitterShare;

	public TwitterOnShare(Context context) {
		mContext = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		pDialog = new ProgressDialog(mContext);
		pDialog.setMessage("Posting to twitter...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	protected Void doInBackground(String... args) {

		String status = args[0];
		String imageUrl = args[1];
		String location = args[2];
		try {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TwitterParams.TW_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TwitterParams.TW_CONSUMER_SECRET);


			// Access Token
			String access_token = TwitterParams.TW_PREF_ACCESS_TOKEN;
			// Access Token Secret
			String access_token_secret = TwitterParams.TW_PREF_ACCESS_TOKEN_SECRET;
			if (null != AppSharedPreferences.getConstant(mContext).getTwitterUserAccInfoPreference()) {
				access_token = AppSharedPreferences.getConstant(mContext).getTwitterUserAccInfoPreference().getAccessToken();
				access_token_secret = AppSharedPreferences.getConstant(mContext).getTwitterUserAccInfoPreference().getAccessTokenSecret();
			}
			AccessToken accessToken = new AccessToken(access_token, access_token_secret);
			Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

			// Update status
			StatusUpdate statusUpdate = new StatusUpdate(status);
			try {
				URL url = new URL(imageUrl);
				URLConnection urlConnection = url.openConnection();
				InputStream in = new BufferedInputStream(urlConnection.getInputStream());
				statusUpdate.setMedia("image.jpg", in);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (!location.equalsIgnoreCase("none")){
				double lat = Double.valueOf(AppSharedPreferences.getConstant(mContext).getLatitude());
				double lng = Double.valueOf(AppSharedPreferences.getConstant(mContext).getLongitude());
				GeoLocation geo = new GeoLocation(lat, lng);
				statusUpdate.setDisplayCoordinates(true);
				statusUpdate.setLocation(geo);
			}

			twitter4j.Status response = twitter.updateStatus(statusUpdate);

			Log.d("Status", response.getText());

		} catch (TwitterException e) {
			Log.d("Failed to post!", e.getMessage());
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {

			/* Dismiss the progress dialog after sharing */
		pDialog.dismiss();

		Toast.makeText(mContext, "Posted to Twitter!", Toast.LENGTH_SHORT).show();

	}

}
