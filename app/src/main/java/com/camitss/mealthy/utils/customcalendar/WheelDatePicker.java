package com.camitss.mealthy.utils.customcalendar;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.camitss.mealthy.R;

/**
 * Created by Viseth on 5/30/2017.
 */

public class WheelDatePicker extends LinearLayout {
	private int textColor;
	private int selectedTextColor;
	private int selectorColor;
	private int selectorHeight;
	private int textSize;
	private boolean isCurved;
	private boolean isCyclic;
	private boolean mustBeOnFuture;
	private int visibleItemCount;
	private boolean displayDays = true;
	private boolean displayMinutes = true;
	private boolean displayHours = true;
	public static final boolean IS_CYCLIC_DEFAULT = true;
	public static final boolean IS_CURVED_DEFAULT = false;
	public static final boolean MUST_BE_ON_FUTUR_DEFAULT = false;
	private static final int VISIBLE_ITEM_COUNT_DEFAULT = 7;

	public WheelDatePicker(Context context) {
		this(context, null);
	}

	public WheelDatePicker(Context context, @Nullable AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public WheelDatePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
		inflate(context, R.layout.layout_wheelpickerdate, this);
	}

	private void init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SingleDateAndTimePicker);

		final Resources resources = getResources();
		textColor = a.getColor(R.styleable.SingleDateAndTimePicker_picker_textColor,
				resources.getColor(R.color.picker_default_text_color));
		selectedTextColor = a.getColor(R.styleable.SingleDateAndTimePicker_picker_selectedTextColor,
				resources.getColor(R.color.picker_default_selected_text_color));
		selectorColor = a.getColor(R.styleable.SingleDateAndTimePicker_picker_selectorColor,
				resources.getColor(R.color.picker_default_selector_color));
		selectorHeight = a.getDimensionPixelSize(R.styleable.SingleDateAndTimePicker_picker_selectorHeight, resources.getDimensionPixelSize(R.dimen.wheelSelectorHeight));
		textSize = a.getDimensionPixelSize(R.styleable.SingleDateAndTimePicker_picker_textSize,
				resources.getDimensionPixelSize(R.dimen.WheelItemTextSize));
		isCurved = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_curved, IS_CURVED_DEFAULT);
		isCyclic = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_cyclic, IS_CYCLIC_DEFAULT);
		mustBeOnFuture = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_mustBeOnFuture, MUST_BE_ON_FUTUR_DEFAULT);
		visibleItemCount = a.getInt(R.styleable.SingleDateAndTimePicker_picker_visibleItemCount, VISIBLE_ITEM_COUNT_DEFAULT);

		displayDays = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_displayDays, displayDays);
		displayMinutes = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_displayMinutes, displayMinutes);
		displayHours = a.getBoolean(R.styleable.SingleDateAndTimePicker_picker_displayHours, displayHours);

		a.recycle();
	}

}
