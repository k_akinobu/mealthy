package com.camitss.mealthy.utils.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.camitss.mealthy.utils.AppSharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class LocationController
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    private static LocationController mInstance = null;

    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 2;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Boolean mRequestingLocationUpdates;
    private Context mContext;
    private Activity mActivity;

    private String mTag = "LocationController";
    private GoogleApiClient mGoogleApiClient;
    private LocationUpdaterInterface mLocationUpdaterInterface;
    private boolean isGetLocation = false;


    private enum EnLocationType {
        CURRENT_LOCATION,
        REAL_TIME_LOCATION_CHANGE,
        LOCATION_NULL,
        PERMISSION_DENIED
    }

    /**
     * Constructor
     *
     * @param context
     */
    public LocationController(Context context) {
        mContext = context;
        mActivity = (Activity) mContext;

        mRequestingLocationUpdates = false;

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    /**
     * @param context
     * @return
     */
    public synchronized static LocationController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LocationController(context);
        }
        return mInstance;
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets up the ic_location request. Android has two ic_location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current ic_location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns ic_location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time ic_location
     * updates.
     */
    private void createLocationRequest() {
        Log.d(mTag, "Creating Location Request");
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active ic_location updates. This interval is
        // inexact. You may not receive updates at all if no ic_location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting ic_location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active ic_location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed ic_location settings.
     */
    private void buildLocationSettingsRequest() {
        Log.d(mTag, "Building Location Settings Request");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check GPS if GPS is turn off show the dialog and ask for turn on.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * Check GPS turn on or turn off
     */
    public boolean checkGPS() {
        LocationManager mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Check Permissions
     */
    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            checkLocationSettings();
            Log.d(mTag, "Permission Granted");
        }
    }

    private void setLocation(Location location) {
        AppSharedPreferences.getConstant(mContext).setLongitude(String.valueOf(location.getLongitude()));
        AppSharedPreferences.getConstant(mContext).setLatitude(String.valueOf(location.getLatitude()));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(mTag, "Connected to GoogleApiClient");
    }

    /**
     * Requests ic_location updates from the FusedLocationApi.
     */
    public void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this
                ).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        mRequestingLocationUpdates = true;
                    }
                });
            }
        } else
            Log.d(mTag, "StopLocationUpdates >>> not yet Connected to GoogleApiClient");
    }

    /**
     * Removes ic_location updates from the FusedLocationApi.
     */
    public void stopLocationUpdates() {
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent ic_location updates.
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    mRequestingLocationUpdates = false;
                }
            });
        } else {
            Log.d(mTag, "StopLocationUpdates >>> not yet Connected to GoogleApiClient");
        }
    }

    private void userPermissionLocation(boolean type) {
        if (null != mLocationUpdaterInterface) {
            mLocationUpdaterInterface.onUserPermissionLocation(type);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (null != mLocationUpdaterInterface) {
            mLocationUpdaterInterface.onRealTimeUpdateLocationCallBack(
                    EnLocationType.REAL_TIME_LOCATION_CHANGE, location);
            setLocation(location);
            if (!isGetLocation) {
                isGetLocation = true;
                mLocationUpdaterInterface.onLocationCallBack(EnLocationType.CURRENT_LOCATION, location);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(mTag, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(mTag, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    /**
     * Connect to GoogleApiClient
     */
    public void onStart() {
        Log.d(mTag, "onStart >>> Connect to GoogleApiClient");
        if (null != mGoogleApiClient) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Start Location Updates.
     */
    public void onResume() {
        Log.d(mTag, "onResume >>> Start Location Updates");
        // ic_location updates if the user has requested them.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Stop Location Updates.
     */
    public void onPause() {
        Log.d(mTag, "onPause >>> Stop Location Updates");
        // Stop ic_location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    /**
     * Disconnect from GoogleApiClient
     */
    public void onStop() {
        Log.d(mTag, "onStop >>> Disconnect from GoogleApiClient");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Call back from checkLocationSettings() check GPS turn on or turn off.
     * If GPS turn off will show the dialog.
     *
     * @param locationSettingsResult
     */
    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(mTag, "User turn on GPS.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(mTag, "Show the dialog to turn on GPS. ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.d(mTag, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(mTag, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    /**
     * This function call back when user granted or denied permission.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResultCallBack(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                PackageManager.PERMISSION_GRANTED) {
                            checkLocationSettings();
                            Log.d(mTag, "Permission Granted: " + permissions[i]);
                        }
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d(mTag, "Permission Denied: " + permissions[i]);
                        userPermissionLocation(false);
                    }
                }
            }
        }
    }

    /**
     * Call back when user press ok to turn on GPS or cancel to ignore.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResultCallBack(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(mTag, "User turn on GPS.");
                        userPermissionLocation(true);
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(mTag, "User canceled turn on GPS.");
                        userPermissionLocation(false);
                        break;
                }
                break;
        }
    }

    /**
     * set Location Listener
     *
     * @param updateLocationListener
     */
    public void setLocationUpdateListener(LocationUpdaterInterface updateLocationListener) {
        mLocationUpdaterInterface = updateLocationListener;
    }
}
