package com.camitss.mealthy.utils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Darith on 5/22/2017.
 */

public class FileManager {
	public static void writeFrame(String fileName, byte[] data) {
		try {
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileName));
			bos.write(data);
			bos.flush();
			bos.close();
//            Log.e(TAG, "" + data.length + " bytes have been written to " + filesDir + fileName + ".jpg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
