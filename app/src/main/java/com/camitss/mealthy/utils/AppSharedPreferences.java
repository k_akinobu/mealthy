package com.camitss.mealthy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.camitss.mealthy.activity.TwitterLoginProcess;
import com.camitss.mealthy.object.TwitterUserAccInfo;
import com.google.gson.Gson;

import java.util.UUID;

/**
 * Created by pheakdey on 3/29/2017.
 */

public class AppSharedPreferences {

	private static final String APP_SHARED_PREFS = "MealthyApp";
	private SharedPreferences _sharedPrefs;
	private SharedPreferences.Editor _prefsEditor;
	private static AppSharedPreferences mAppShareConstant;
	private int userId = 0;
	private int numNutriNoti = 0;


	private boolean isLoginWithSocial;
	private boolean isLoginWithMealthy;
	private boolean isFavoriteMenu;
	private boolean isFirstVisitInTavTwoFragment = true;


	public enum SharedPreKeyType {
		USER_ID,
		IS_LOGIN_WITH_SOCIAL,
		CONNECTED_SOCIAL,
		TOKEN,
		IS_LOGIN_WITH_MEALTHY,
		IS_FAVORITE_MENU,
		TwitterUserAccInfo,
		IS_FIRST_VISIT_IN_TAB_TWO,
		NUM_NUTRITIONIST_NOTIFICATION,
		LONGITUDE,
		LATITUDE,
		PREVIOUS_TERM_ID
	}

	private AppSharedPreferences(Context context) {
		this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
		this._prefsEditor = _sharedPrefs.edit();
	}

	public synchronized static AppSharedPreferences getConstant(Context context) {
		if (null == mAppShareConstant) {
			mAppShareConstant = new AppSharedPreferences(context);
		}
		return mAppShareConstant;
	}


	/**
	 * @param userId
	 */
	public void setUserIdPref(int userId) {
		_prefsEditor.putInt(SharedPreKeyType.USER_ID.toString(), userId);
		_prefsEditor.apply();
	}

	/**
	 * @return
	 */
	public int getUserIdPref() {
		return _sharedPrefs.getInt(SharedPreKeyType.USER_ID.toString(), userId);
	}

//	public void setTokenPref(String token){
//		_prefsEditor.putString(SharedPreKeyType.TOKEN.toString(), token);
//		_prefsEditor.apply();
//	}
//	public String getTokenPref(){
//		return _sharedPrefs.getString(SharedPreKeyType.TOKEN.toString(),uuid);
//	}

	public TwitterUserAccInfo getTwitterUserAccInfoPreference() {
		Gson gson = new Gson();
		String json = _sharedPrefs.getString(SharedPreKeyType.TwitterUserAccInfo.toString(), "");
		return gson.fromJson(json, TwitterUserAccInfo.class);
	}

	public void setTwitterUserAccInfoPreference(TwitterUserAccInfo classModel) {
		Gson gson = new Gson();
		String jsonString = gson.toJson(classModel);
		_prefsEditor.putString(SharedPreKeyType.TwitterUserAccInfo.toString(), jsonString);
		_prefsEditor.apply();
	}

	public boolean isFirstVisitInTavTwoFragment() {
		return _sharedPrefs.getBoolean(SharedPreKeyType.IS_FIRST_VISIT_IN_TAB_TWO.toString(), isFirstVisitInTavTwoFragment);
	}

	public void setFirstVisitInTavTwoFragment(boolean isFirstVisitInTavTwoFragment) {
		_prefsEditor.putBoolean(SharedPreKeyType.IS_FIRST_VISIT_IN_TAB_TWO.toString(), isFirstVisitInTavTwoFragment);
		_prefsEditor.apply();
	}

	public boolean isFavoriteMenuPref() {
		return _sharedPrefs.getBoolean(SharedPreKeyType.IS_FAVORITE_MENU.toString(), isFavoriteMenu);
	}

	public void setFavoriteMenuPref(boolean isFavoriteMenu) {
		_prefsEditor.putBoolean(SharedPreKeyType.IS_FAVORITE_MENU.toString(), isFavoriteMenu);
		_prefsEditor.apply();
	}

	public float getPreviousTargetWeightPref() {
		return _sharedPrefs.getFloat("weight", 0);
	}

	public void setPreviousTargetWeightPref(float weight) {
		_prefsEditor.putFloat("weight", weight);
		_prefsEditor.apply();
	}

	public boolean isFromUpdateTargetWeightMiddleMenuScreen() {
		return _sharedPrefs.getBoolean("isFromUpdateTargetWeightMiddleMenuScreen", false);
	}

	public void setFromUpdateTargetWeightMiddleMenuScreen(boolean isFromUpdateTargetWeightMiddleMenuScreen) {
		_prefsEditor.putBoolean("isFromUpdateTargetWeightMiddleMenuScreen", isFromUpdateTargetWeightMiddleMenuScreen);
		_prefsEditor.apply();
	}

	public boolean isLoginWithSocialPref() {
		return _sharedPrefs.getBoolean(SharedPreKeyType.IS_LOGIN_WITH_SOCIAL.toString(), isLoginWithSocial);
	}

	public void setLoginWithSocialPref(boolean loginWithSocial) {
		_prefsEditor.putBoolean(SharedPreKeyType.IS_LOGIN_WITH_SOCIAL.toString(), loginWithSocial);
		_prefsEditor.apply();
	}

	public boolean isLoginWithMealthy() {
		return _sharedPrefs.getBoolean(SharedPreKeyType.IS_LOGIN_WITH_MEALTHY.toString(), isLoginWithMealthy);
	}

	public void setLoginWithMealthy(boolean loginWithMealthy) {
		_prefsEditor.putBoolean(SharedPreKeyType.IS_LOGIN_WITH_MEALTHY.toString(), loginWithMealthy);
		_prefsEditor.apply();
	}

	public void setConnectedSocialType(int socialType) {
		_prefsEditor.putInt(SharedPreKeyType.CONNECTED_SOCIAL.toString(), socialType);
		_prefsEditor.apply();
	}

	public int getConnectedSocialType() {
		return _sharedPrefs.getInt(SharedPreKeyType.CONNECTED_SOCIAL.toString(), 0);
	}

	public void clearAppSharedPreference(SharedPreKeyType sharedPreKeyType) {
		_prefsEditor.remove(sharedPreKeyType.name());
		_prefsEditor.apply();
	}

	public void setNotificationCount(int counter) {
		_prefsEditor.putInt("NOTI_COUNT", counter);
		_prefsEditor.apply();
	}

	public void setLunchPriceSettingIndex(int index) {
		_prefsEditor.putInt("LUNCH_PRICE_INDEX", index);
		_prefsEditor.apply();
	}

	public void setDinnerPriceSettingIndex(int index) {
		_prefsEditor.putInt("DINNER_PRICE_INDEX", index);
		_prefsEditor.apply();
	}

	public void setBreakFirstPriceSettingIndex(int index) {
		_prefsEditor.putInt("BREAKFAST_PRICE_INDEX", index);
		_prefsEditor.apply();
	}

	public int getBreakfastPriceIndex() {
		return _sharedPrefs.getInt("BREAKFAST_PRICE_INDEX", 0);
	}

	public int getLunchPriceIndex() {
		return _sharedPrefs.getInt("LUNCH_PRICE_INDEX", 0);
	}

	public int getDinnerPriceIndex() {
		return _sharedPrefs.getInt("DINNER_PRICE_INDEX", 0);
	}

	public int getNotificationCount() {
		return _sharedPrefs.getInt("NOTI_COUNT", -1);
	}

	public void setHomeMessage(String message) {
		_prefsEditor.putString("HOME_MSG", message);
		_prefsEditor.apply();
	}

	public String getHomeMessage() {
		return _sharedPrefs.getString("HOME_MSG", "");
	}

	public void setUserDietTypeFromDiagnosticResult(String userDietTypeFromDiagnosticResult) {
		_prefsEditor.putString("userDietTypeFromDiagnosticResult", userDietTypeFromDiagnosticResult);
		_prefsEditor.apply();
	}

	public String getUserDietTypeFromDiagnosticResult() {
		return _sharedPrefs.getString("userDietTypeFromDiagnosticResult", "");
	}

	public void clearAll() {
		_prefsEditor.clear().apply();
	}

	public int getNumNutriNotiPref() {
		return _sharedPrefs.getInt(SharedPreKeyType.NUM_NUTRITIONIST_NOTIFICATION.toString(), numNutriNoti);
	}

	public void setNumNutriNotiPref(int numNutriNoti) {
		_prefsEditor.putInt(SharedPreKeyType.NUM_NUTRITIONIST_NOTIFICATION.toString(), numNutriNoti);
		_prefsEditor.apply();
	}

	public int getPreviousTermIdPref() {
		return _sharedPrefs.getInt(SharedPreKeyType.PREVIOUS_TERM_ID.toString(), 1);
	}

	public void setPreviousTermIdPref(int termId) {
		_prefsEditor.putInt(SharedPreKeyType.PREVIOUS_TERM_ID.toString(), termId);
		_prefsEditor.apply();
	}

	/**
	 * Get Latitude ic_location.
	 *
	 * @return
	 */
	public String getLatitude() {
		return _sharedPrefs.getString(SharedPreKeyType.LATITUDE.toString(), "");
	}

	/**
	 * Set Latitude ic_location.
	 *
	 * @param latitude
	 */
	public void setLatitude(String latitude) {
		_prefsEditor.putString(SharedPreKeyType.LATITUDE.toString(), latitude);
		_prefsEditor.apply();
	}

	/**
	 * Get Longitude ic_location.
	 *
	 * @return
	 */
	public String getLongitude() {
		return _sharedPrefs.getString(SharedPreKeyType.LONGITUDE.toString(), "");
	}

	/**
	 * Set Longitude ic_location.
	 *
	 * @param longitude
	 */
	public void setLongitude(String longitude) {
		_prefsEditor.putString(SharedPreKeyType.LONGITUDE.toString(), longitude);
		_prefsEditor.apply();
	}

}
