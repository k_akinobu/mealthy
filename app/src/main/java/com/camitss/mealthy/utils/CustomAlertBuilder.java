package com.camitss.mealthy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.camitss.mealthy.R;

/**
 * Created by Viseth on 3/23/2017.
 */
public class CustomAlertBuilder {

	public CustomAlertBuilder() {
	}

	public static void create(Activity activity, String msg) {
		View view = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog_layout, null);

		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.alertdialog_theme)).setView(view).show();
		TextView textMsg = (TextView) view.findViewById(R.id.txt_message);
		TextView textOk = (TextView) view.findViewById(R.id.txt_ok);
		textMsg.setText(msg);
		textOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
	}

	public static void create(Activity activity, String msg,final DismissListener dismissListener) {
		View view = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog_layout, null);

		final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.alertdialog_theme)).setView(view).show();
		TextView textMsg = (TextView) view.findViewById(R.id.txt_message);
		TextView textOk = (TextView) view.findViewById(R.id.txt_ok);
		textMsg.setText(msg);
		textOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(dismissListener!=null){
					dismissListener.onDismiss();
				}
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
	}

	public interface DismissListener{
		void onDismiss();
	}
}
