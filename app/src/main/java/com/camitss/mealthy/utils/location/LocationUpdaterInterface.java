package com.camitss.mealthy.utils.location;

import android.location.Location;

public interface LocationUpdaterInterface {

    void onRealTimeUpdateLocationCallBack(Object type, Location location);

    void onLocationCallBack(Object type, Location location);

    void onUserPermissionLocation(boolean type);
}
