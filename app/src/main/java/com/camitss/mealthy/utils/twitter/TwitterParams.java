package com.camitss.mealthy.utils.twitter;

import android.content.SharedPreferences;

/**
 * Created by Chetra on 19-Aug-15.
 */
public class TwitterParams {
	public static SharedPreferences tw_pref;
	public static final Integer TW_REQUEST_CODE_ON_LOGIN = 1;
	public static final Integer TW_REQUEST_CODE_ON_SHARE = 2;


	public static final String TW_CONSUMER_KEY = "6u1k2vUt7P5JyXUksXTP7J1Vy";//"dCBJq3mw1RqyTvjv8GJJW6xXN";
	public static final String TW_CONSUMER_SECRET = "HXyDN6WaPupWO50djKecTOKeuU1i00iQlbAFR36LiboKDaWzzK";//"rPYxoEAjAN9BJ5Bk8scblyKCm7efsxtW7zm2iT9KibegFxHM64";
	// Preference Constants
	public static final String TW_PREF_CONSUMER_KEY = "CONSUMER_KEY";
	public static final String TW_PREF_CONSUMER_SECRET = "CONSUMER_SECRET";

	public static final String TW_PREF_USER_IMAGE_URL = "USER_IMAGE_URL";
	public static final String TW_PREF_USER_NAME = "USER_NAME";
	public static final String TW_PREF_USER_ID = "USER_ID";

	public static final String TW_PREF_REQUEST_TOKEN = "REQUEST_TOKEN";
	public static final String TW_PREF_OAUTH_URL = "OAUTH_URL";
	public static final String TW_PREF_OAUTH_VERIFIER = "OAUTH_VERIFIER";
	public static final String TW_PREF_ACCESS_TOKEN = "ACCESS_TOKEN";
	public static final String TW_PREF_ACCESS_TOKEN_SECRET = "ACCESS_TOKEN_SECRET";

	public static final String TW_PREF_TWITTER_LOGIN_STATUS = "LOGIN_STATUS";
}
