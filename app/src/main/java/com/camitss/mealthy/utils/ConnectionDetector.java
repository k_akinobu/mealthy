package com.camitss.mealthy.utils;

/**
 * Created by Chetra on 19-Aug-15.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class ConnectionDetector {

    private Context _context;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

    /**
     * Checking for all possible internet providers
     **/
    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        try {
                            return new MyTask()
                                    .execute("http://www.google.com")
                                    .get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }

        }
        return false;
    }

    // Here is the AsyncTask class:
    //
    // AsyncTask<Params, Progress, Result>.
    //    Params String the type (Object/primitive) you pass to the AsyncTask from .execute()
    //    Progress Integer the type that gets passed to onProgressUpdate()
    //    Result Boolean the type returns from doInBackground()
    // Any of them can be String, Integer, Void, etc.
    private class MyTask extends AsyncTask<String, Integer, Boolean> {

        // Runs in UI before background thread is called
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Do something like display a progress bar
        }

        // This is run in a background thread
        @Override
        protected Boolean doInBackground(String... params) {
            // get the string from params, which is an array
            String myString = params[0];
            // Do something that takes a long time, for example:
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL(myString).openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e(ConnectionDetector.class.getName(), "Error checking internet connection", e);
                return false;
            }
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            // Do things like update the progress bar
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            // Do things like hide the progress bar or change a TextView
        }
    }
}