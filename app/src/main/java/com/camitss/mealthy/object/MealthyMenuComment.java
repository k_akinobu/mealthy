package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Viseth on 5/29/2017.
 */

public class MealthyMenuComment extends RealmObject {
	@SerializedName("id")
	@Expose
	int id;

	@SerializedName("body")
	@Expose
	String body;

	@SerializedName("created_at")
	@Expose
	String createdAt;

	@SerializedName("user")
	@Expose
	NutritionistUser user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public NutritionistUser getUser() {
		return user;
	}

	public void setUser(NutritionistUser user) {
		this.user = user;
	}
}
