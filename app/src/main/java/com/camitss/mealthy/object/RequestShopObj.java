package com.camitss.mealthy.object;

/**
 * Created by Darith on 5/19/2017.
 */

public class RequestShopObj {
	boolean selected;
	int id;
	String shopName;

	public RequestShopObj(int id, boolean selected, String shopName) {
		this.selected = selected;
		this.id = id;
		this.shopName = shopName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}
