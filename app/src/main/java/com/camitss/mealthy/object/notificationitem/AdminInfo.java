package com.camitss.mealthy.object.notificationitem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Viseth on 6/28/2017.
 */

public class AdminInfo extends RealmObject {
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("url")
	@Expose
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}