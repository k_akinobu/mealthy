package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 4/18/2017.
 */

public class UserRecomItem extends RealmObject {
	@PrimaryKey
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("sex")
	@Expose
	private int sex;
	@SerializedName("tall")
	@Expose
	private double tall;
	@SerializedName("weight")
	@Expose
	private double weight;
	@SerializedName("target_weight")
	@Expose
	private double targetWeight;
	@SerializedName("metabolic_id")
	@Expose
	private int metabolicId;
	@SerializedName("term_id")
	@Expose
	private int termId;
	@SerializedName("eat_out_id")
	@Expose
	private int eatOutId;
	@SerializedName("diet_id")
	@Expose
	private int dietId;
	@SerializedName("figure_id")
	@Expose
	private int figureId;
	@SerializedName("category_ids")
	@Expose
	private String categoryIds;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("morning_price")
	@Expose
	private double morningPrice;
	@SerializedName("lunch_price")
	@Expose
	private double lunchPrice;
	@SerializedName("dinner_price")
	@Expose
	private double dinnerPrice;
	@SerializedName("birthdate")
	@Expose
	private String birthdate;
	@SerializedName("is_following")
	@Expose
	private boolean isFollowing;
	@SerializedName("following_count")
	@Expose
	private int followingCount;
	@SerializedName("followers_count")
	@Expose
	private int followersCount;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("profile_image_url")
	@Expose
	private String profileImageUrl;
	@SerializedName("cover_image_url")
	@Expose
	private String coverImageUrl;
	@SerializedName("score")
	@Expose
	private int score;
	@SerializedName("progress")
	@Expose
	private int progress;
	@SerializedName("total_reduction")
	@Expose
	private double totalReduction;
	@SerializedName("posted_count")
	@Expose
	private int postedCount;
	@SerializedName("user_kind")
	@Expose
	private int userKind;
	@SerializedName("rep_nutritionist_profile_image_url")
	@Expose
	private String repNutritionistProfileImageUrl;
	@SerializedName("last_posted_image_url")
	@Expose
	private String lastPostedImageUrl;
	@SerializedName("is_free_consultation")
	@Expose
	private boolean isFreeConsultation;
	@SerializedName("is_trialpack_bought")
	@Expose
	private boolean isTrialpackBought;
	@SerializedName("invite_code")
	@Expose
	private String inviteCode;
	@SerializedName("similar")
	@Expose
	private int similar;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public double getTall() {
		return tall;
	}

	public void setTall(double tall) {
		this.tall = tall;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getTargetWeight() {
		return targetWeight;
	}

	public void setTargetWeight(double targetWeight) {
		this.targetWeight = targetWeight;
	}

	public int getMetabolicId() {
		return metabolicId;
	}

	public void setMetabolicId(int metabolicId) {
		this.metabolicId = metabolicId;
	}

	public int getTermId() {
		return termId;
	}

	public void setTermId(int termId) {
		this.termId = termId;
	}

	public int getEatOutId() {
		return eatOutId;
	}

	public void setEatOutId(int eatOutId) {
		this.eatOutId = eatOutId;
	}

	public int getDietId() {
		return dietId;
	}

	public void setDietId(int dietId) {
		this.dietId = dietId;
	}

	public int getFigureId() {
		return figureId;
	}

	public void setFigureId(int figureId) {
		this.figureId = figureId;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getMorningPrice() {
		return morningPrice;
	}

	public void setMorningPrice(double morningPrice) {
		this.morningPrice = morningPrice;
	}

	public double getLunchPrice() {
		return lunchPrice;
	}

	public void setLunchPrice(double lunchPrice) {
		this.lunchPrice = lunchPrice;
	}

	public double getDinnerPrice() {
		return dinnerPrice;
	}

	public void setDinnerPrice(double dinnerPrice) {
		this.dinnerPrice = dinnerPrice;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public boolean isFollowing() {
		return isFollowing;
	}

	public void setFollowing(boolean following) {
		isFollowing = following;
	}

	public int getFollowingCount() {
		return followingCount;
	}

	public void setFollowingCount(int followingCount) {
		this.followingCount = followingCount;
	}

	public int getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getCoverImageUrl() {
		return coverImageUrl;
	}

	public void setCoverImageUrl(String coverImageUrl) {
		this.coverImageUrl = coverImageUrl;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public double getTotalReduction() {
		return totalReduction;
	}

	public void setTotalReduction(double totalReduction) {
		this.totalReduction = totalReduction;
	}

	public int getPostedCount() {
		return postedCount;
	}

	public void setPostedCount(int postedCount) {
		this.postedCount = postedCount;
	}

	public int getUserKind() {
		return userKind;
	}

	public void setUserKind(int userKind) {
		this.userKind = userKind;
	}

	public String getRepNutritionistProfileImageUrl() {
		return repNutritionistProfileImageUrl;
	}

	public void setRepNutritionistProfileImageUrl(String repNutritionistProfileImageUrl) {
		this.repNutritionistProfileImageUrl = repNutritionistProfileImageUrl;
	}

	public String getLastPostedImageUrl() {
		return lastPostedImageUrl;
	}

	public void setLastPostedImageUrl(String lastPostedImageUrl) {
		this.lastPostedImageUrl = lastPostedImageUrl;
	}

	public boolean isFreeConsultation() {
		return isFreeConsultation;
	}

	public void setFreeConsultation(boolean freeConsultation) {
		isFreeConsultation = freeConsultation;
	}

	public boolean isTrialpackBought() {
		return isTrialpackBought;
	}

	public void setTrialpackBought(boolean trialpackBought) {
		isTrialpackBought = trialpackBought;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public int getSimilar() {
		return similar;
	}

	public void setSimilar(int similar) {
		this.similar = similar;
	}
}
