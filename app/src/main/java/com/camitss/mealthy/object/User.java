package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by User
 * Date 3/7/2017
 */

public class User extends RealmObject {
	public User() {
	}

	@PrimaryKey
	@SerializedName("id")
	@Expose
	private int id;

	@SerializedName("sex")
	@Expose
	private int sex;
	@SerializedName("tall")
	@Expose
	private double tall;
	@SerializedName("weight")
	@Expose
	private double weight;
	@SerializedName("target_weight")
	@Expose
	private double targetWeight;
	@SerializedName("metabolic_id")
	@Expose
	private double metabolicId;
	@SerializedName("term_id")
	@Expose
	private int termId;
	@SerializedName("eat_out_id")
	@Expose
	private int eatOutId;
	@SerializedName("diet_id")
	@Expose
	private int dietId;
	@SerializedName("figure_id")
	@Expose
	private int figureId;
	@SerializedName("category_ids")
	@Expose
	private String categoryIds;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("start_date")
	@Expose
	private String startDate;
	@SerializedName("end_date")
	@Expose
	private String endDate;
	@SerializedName("morning_price")
	@Expose
	private double morningPrice;
	@SerializedName("lunch_price")
	@Expose
	private double lunchPrice;
	@SerializedName("dinner_price")
	@Expose
	private double dinnerPrice;
	@SerializedName("birthdate")
	@Expose
	private String birthdate;
	@SerializedName("is_following")
	@Expose
	private boolean isFollowing;
	@SerializedName("following_count")
	@Expose
	private int followingAccount;
	@SerializedName("followers_count")
	@Expose
	private int followersCount;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("profile_image_url")
	@Expose
	private String profileImageUrl;
	@SerializedName("cover_image_url")
	@Expose
	private String coverImageUrl;
	@SerializedName("score")
	@Expose
	private double score;
	@SerializedName("progress")
	@Expose
	private double progress;
	@SerializedName("total_reduction")
	@Expose
	private int totalReduction;
	@SerializedName("posted_count")
	@Expose
	private int postedCount;
	@SerializedName("user_kind")
	@Expose
	private int userKind;
	@SerializedName("rep_nutritionist_profile_image_url")
	@Expose
	private String repNutritionistProfileUrl;
	@SerializedName("last_posted_image_url")
	@Expose
	private String lastedPostedImageUrl;
	@SerializedName("is_free_consultation")
	@Expose
	private boolean isFreeConsultation;
	@SerializedName("is_trialpack_bought")
	@Expose
	private boolean isTrialpackBought;
	@SerializedName("invite_code")
	@Expose
	private String inviteCode;
	@SerializedName("calorie_details")
	@Expose
	private RealmList<CalorieDetail> calorieDetails = new RealmList<>();
	@SerializedName("diet_type")
	@Expose
	private String dietType;
	@SerializedName("point")
	@Expose
	private int point;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public double getTall() {
		return tall;
	}

	public void setTall(double tall) {
		this.tall = tall;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getTargetWeight() {
		return targetWeight;
	}

	public void setTargetWeight(double targetWeight) {
		this.targetWeight = targetWeight;
	}

	public double getMetabolicId() {
		return metabolicId;
	}

	public void setMetabolicId(double metabolicId) {
		this.metabolicId = metabolicId;
	}

	public int getTermId() {
		return termId;
	}

	public void setTermId(int termId) {
		this.termId = termId;
	}

	public int getEatOutId() {
		return eatOutId;
	}

	public void setEatOutId(int eatOutId) {
		this.eatOutId = eatOutId;
	}

	public int getDietId() {
		return dietId;
	}

	public void setDietId(int dietId) {
		this.dietId = dietId;
	}

	public int getFigureId() {
		return figureId;
	}

	public void setFigureId(int figureId) {
		this.figureId = figureId;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getMorningPrice() {
		return morningPrice;
	}

	public void setMorningPrice(double morningPrice) {
		this.morningPrice = morningPrice;
	}

	public double getLunchPrice() {
		return lunchPrice;
	}

	public void setLunchPrice(double lunchPrice) {
		this.lunchPrice = lunchPrice;
	}

	public double getDinnerPrice() {
		return dinnerPrice;
	}

	public void setDinnerPrice(double dinnerPrice) {
		this.dinnerPrice = dinnerPrice;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public boolean isFollowing() {
		return isFollowing;
	}

	public void setFollowing(boolean following) {
		isFollowing = following;
	}

	public int getFollowingAccount() {
		return followingAccount;
	}

	public void setFollowingAccount(int followingAccount) {
		this.followingAccount = followingAccount;
	}

	public int getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getCoverImageUrl() {
		return coverImageUrl;
	}

	public void setCoverImageUrl(String coverImageUrl) {
		this.coverImageUrl = coverImageUrl;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}

	public int getTotalReduction() {
		return totalReduction;
	}

	public void setTotalReduction(int totalReduction) {
		this.totalReduction = totalReduction;
	}

	public int getPostedCount() {
		return postedCount;
	}

	public void setPostedCount(int postedCount) {
		this.postedCount = postedCount;
	}

	public int getUserKind() {
		return userKind;
	}

	public void setUserKind(int userKind) {
		this.userKind = userKind;
	}

	public String getRepNutritionistProfileUrl() {
		return repNutritionistProfileUrl;
	}

	public void setRepNutritionistProfileUrl(String repNutritionistProfileUrl) {
		this.repNutritionistProfileUrl = repNutritionistProfileUrl;
	}

	public String getLastedPostedImageUrl() {
		return lastedPostedImageUrl;
	}

	public void setLastedPostedImageUrl(String lastedPostedImageUrl) {
		this.lastedPostedImageUrl = lastedPostedImageUrl;
	}

	public boolean isFreeConsultation() {
		return isFreeConsultation;
	}

	public void setFreeConsultation(boolean freeConsultation) {
		isFreeConsultation = freeConsultation;
	}

	public boolean isTrialpackBought() {
		return isTrialpackBought;
	}

	public void setTrialpackBought(boolean trialpackBought) {
		isTrialpackBought = trialpackBought;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public RealmList<CalorieDetail> getCalorieDetails() {
		return calorieDetails;
	}

	public void setCalorieDetails(RealmList<CalorieDetail> calorieDetails) {
		this.calorieDetails = calorieDetails;
	}

	public String getDietType() {
		return dietType;
	}

	public void setDietType(String dietType) {
		this.dietType = dietType;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}
}
