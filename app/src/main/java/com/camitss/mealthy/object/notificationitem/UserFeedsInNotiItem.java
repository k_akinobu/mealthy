package com.camitss.mealthy.object.notificationitem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Viseth on 6/28/2017.
 */

public class UserFeedsInNotiItem extends RealmObject {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("key")
	@Expose
	private String key;
	@SerializedName("is_like")
	@Expose
	private boolean isLike;
	@SerializedName("likes_count")
	@Expose
	private int likesCount;
	@SerializedName("privacy_kind")
	@Expose
	private int privacyKind;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("shop")
	@Expose
	private String shop;
	@SerializedName("menu")
	@Expose
	private MenuInNotiItem menu;
	@SerializedName("user_history")
	@Expose
	private UserHistoryInNotiItem userHistory;
	@SerializedName("exif_date")
	@Expose
	private String exifDate;
	@SerializedName("user")
	@Expose
	private UserInNotiItem user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean like) {
		isLike = like;
	}

	public int getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(int likesCount) {
		this.likesCount = likesCount;
	}

	public int getPrivacyKind() {
		return privacyKind;
	}

	public void setPrivacyKind(int privacyKind) {
		this.privacyKind = privacyKind;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public UserHistoryInNotiItem getUserHistory() {
		return userHistory;
	}

	public void setUserHistory(UserHistoryInNotiItem userHistory) {
		this.userHistory = userHistory;
	}

	public String getExifDate() {
		return exifDate;
	}

	public void setExifDate(String exifDate) {
		this.exifDate = exifDate;
	}

	public UserInNotiItem getUser() {
		return user;
	}

	public void setUser(UserInNotiItem user) {
		this.user = user;
	}

	public MenuInNotiItem getMenu() {
		return menu;
	}

	public void setMenu(MenuInNotiItem menu) {
		this.menu = menu;
	}


}