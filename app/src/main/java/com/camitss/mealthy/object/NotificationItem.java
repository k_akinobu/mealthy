package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Viseth on 6/28/2017.
 */

public class NotificationItem {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("action_kind")
	@Expose
	private int actionKind;
	@SerializedName("status_kind")
	@Expose
	private int statusKind;
	@SerializedName("activity_id")
	@Expose
	private int activityId;
	@SerializedName("feed")
	@Expose
	private UserFeedsInProfile feed;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("admin_info")
	@Expose
	private AdminInfo adminInfo;
	@SerializedName("from_user")
	@Expose
	private User fromUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActionKind() {
		return actionKind;
	}

	public void setActionKind(int actionKind) {
		this.actionKind = actionKind;
	}

	public int getStatusKind() {
		return statusKind;
	}

	public void setStatusKind(int statusKind) {
		this.statusKind = statusKind;
	}

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public UserFeedsInProfile getFeed() {
		return feed;
	}

	public void setFeed(UserFeedsInProfile feed) {
		this.feed = feed;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public AdminInfo getAdminInfo() {
		return adminInfo;
	}

	public void setAdminInfo(AdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public class AdminInfo {
		@SerializedName("title")
		@Expose
		private String title;
		@SerializedName("url")
		@Expose
		private String url;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
	}
}
