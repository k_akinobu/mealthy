package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 4/28/2017.
 */

public class Value extends RealmObject{
	@SerializedName("morning")
	@Expose
	private Shift morning;
	@SerializedName("lunch")
	@Expose
	private Shift lunch;
	@SerializedName("dinner")
	@Expose
	private Shift dinner;

	public Shift getMorning() {
		return morning;
	}

	public void setMorning(Shift morning) {
		this.morning = morning;
	}

	public Shift getLunch() {
		return lunch;
	}

	public void setLunch(Shift lunch) {
		this.lunch = lunch;
	}

	public Shift getDinner() {
		return dinner;
	}

	public void setDinner(Shift dinner) {
		this.dinner = dinner;
	}

}