package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Darith on 5/17/2017.
 */

public class NutritionistComments extends RealmObject {
	@PrimaryKey
	@SerializedName("id")
	@Expose
	int nc_id;


	@SerializedName("body")
	@Expose
	String nc_body;

	@SerializedName("created_at")
	@Expose
	String nc_created_at;

	@SerializedName("user")
	@Expose
	NutritionistUser nutritionistUser;


	public int getNc_id() {
		return nc_id;
	}

	public void setNc_id(int nc_id) {
		this.nc_id = nc_id;
	}

	public String getNc_body() {
		return nc_body;
	}

	public void setNc_body(String nc_body) {
		this.nc_body = nc_body;
	}

	public String getNc_created_at() {
		return nc_created_at;
	}

	public void setNc_created_at(String nc_created_at) {
		this.nc_created_at = nc_created_at;
	}

	public NutritionistUser getNutritionistUser() {
		return nutritionistUser;
	}

	public void setNutritionistUser(NutritionistUser nutritionistUser) {
		this.nutritionistUser = nutritionistUser;
	}
}
