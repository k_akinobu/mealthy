package com.camitss.mealthy.object;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Viseth on 4/6/2017.
 */

public class FeedItem implements Serializable{
	private int id;
	private boolean isLike;
	private int likesCount;
	private int privacyCount;
	private Menu menu;
	private FeedItemUser feedItemUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean like) {
		isLike = like;
	}

	public int getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(int likesCount) {
		this.likesCount = likesCount;
	}

	public int getPrivacyCount() {
		return privacyCount;
	}

	public void setPrivacyCount(int privacyCount) {
		this.privacyCount = privacyCount;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public FeedItemUser getFeedItemUser() {
		return feedItemUser;
	}

	public void setFeedItemUser(FeedItemUser feedItemUser) {
		this.feedItemUser = feedItemUser;
	}

	public static class Menu implements Serializable{
		private int id;
		private String name;
		private String image;
		private String imageThumb;
		private String createdAt;
		private int commentCount;
		private String notes;
		private String CgmMenuName;
		private String CgmShopName;
		private int shopId;
		private List<Comment> commentList;
		public Menu(){}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getImageThumb() {
			return imageThumb;
		}

		public void setImageThumb(String imageThumb) {
			this.imageThumb = imageThumb;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public int getCommentCount() {
			return commentCount;
		}

		public void setCommentCount(int commentCount) {
			this.commentCount = commentCount;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public String getCgmMenuName() {
			return CgmMenuName;
		}

		public void setCgmMenuName(String cgmMenuName) {
			CgmMenuName = cgmMenuName;
		}

		public String getCgmShopName() {
			return CgmShopName;
		}

		public void setCgmShopName(String cgmShopName) {
			CgmShopName = cgmShopName;
		}

		public List<Comment> getCommentList() {
			return commentList;
		}

		public void setCommentList(List<Comment> commentList) {
			this.commentList = commentList;
		}

		public int getShopId() {
			return shopId;
		}

		public void setShopId(int shopId) {
			this.shopId = shopId;
		}
	}

	public static class Comment implements Serializable{
		private int id;
		private String body;
		private FeedItemUser cmtUser;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public FeedItemUser getCmtUser() {
			return cmtUser;
		}

		public void setCmtUser(FeedItemUser cmtUser) {
			this.cmtUser = cmtUser;
		}
	}
	public static class FeedItemUser implements Serializable{
		private int id;
		private String name;
		private String profileImage;
		private int userKind;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getProfileImage() {
			return profileImage;
		}

		public void setProfileImage(String profileImage) {
			this.profileImage = profileImage;
		}

		public int getUserKind() {
			return userKind;
		}

		public void setUserKind(int userKind) {
			this.userKind = userKind;
		}
	}

}
