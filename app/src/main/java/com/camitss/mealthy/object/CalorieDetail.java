package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 4/28/2017.
 */

public class CalorieDetail extends RealmObject {
	public CalorieDetail() {
	}

	@PrimaryKey
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("calorie_per_day")
	@Expose
	private int caloriePerDay;
	@SerializedName("value")
	@Expose
	private Value value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCaloriePerDay() {
		return caloriePerDay;
	}

	public void setCaloriePerDay(int caloriePerDay) {
		this.caloriePerDay = caloriePerDay;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}


}
