package com.camitss.mealthy.object.notificationitem;

import com.camitss.mealthy.object.MenuTag;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Viseth on 6/28/2017.
 */

public class MenuInNotiItem extends RealmObject {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("calorie")
	@Expose
	private int calorie;
	@SerializedName("protein")
	@Expose
	private int protein;
	@SerializedName("lipid")
	@Expose
	private int lipid;
	@SerializedName("carbohydrate")
	@Expose
	private int carbohydrate;
	@SerializedName("origin")
	@Expose
	private int origin;
	@SerializedName("score")
	@Expose
	private int score;
	@SerializedName("price")
	@Expose
	private int price;
	@SerializedName("image")
	@Expose
	private String image;
	@SerializedName("thumbnail_image_url")
	@Expose
	private String thumbnailImageUrl;
	@SerializedName("start_time")
	@Expose
	private String startTime;
	@SerializedName("end_time")
	@Expose
	private String endTime;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("takeout_flag")
	@Expose
	private String takeoutFlag;
	@SerializedName("is_good")
	@Expose
	private boolean isGood;
	@SerializedName("is_bad")
	@Expose
	private boolean isBad;
	@SerializedName("goods_count")
	@Expose
	private int goodsCount;
	@SerializedName("bads_count")
	@Expose
	private int badsCount;
	@SerializedName("comments_count")
	@Expose
	private int commentsCount;
	@SerializedName("notes")
	@Expose
	private String notes;
	@SerializedName("menu_group")
	@Expose
	private int menuGroup;
	@SerializedName("social_score")
	@Expose
	private int socialScore;
	@SerializedName("kind")
	@Expose
	private int kind;
	@SerializedName("tags")
	@Expose
	private RealmList<MenuTag> tags;
	@SerializedName("nutritionist_comments")
	@Expose
	private RealmList<CommentInNotiItem> nutritionistComments;
	@SerializedName("shop_id")
	@Expose
	private int shopId;
	@SerializedName("cgm_menu_name")
	@Expose
	private String cgmMenuName;
	@SerializedName("cgm_shop_name")
	@Expose
	private String cgmShopName;
	@SerializedName("comments")
	@Expose
	private RealmList<CommentInNotiItem> comments;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCalorie() {
		return calorie;
	}

	public void setCalorie(int calorie) {
		this.calorie = calorie;
	}

	public int getProtein() {
		return protein;
	}

	public void setProtein(int protein) {
		this.protein = protein;
	}

	public int getLipid() {
		return lipid;
	}

	public void setLipid(int lipid) {
		this.lipid = lipid;
	}

	public int getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(int carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public int getOrigin() {
		return origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getThumbnailImageUrl() {
		return thumbnailImageUrl;
	}

	public void setThumbnailImageUrl(String thumbnailImageUrl) {
		this.thumbnailImageUrl = thumbnailImageUrl;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getTakeoutFlag() {
		return takeoutFlag;
	}

	public void setTakeoutFlag(String takeoutFlag) {
		this.takeoutFlag = takeoutFlag;
	}

	public boolean isGood() {
		return isGood;
	}

	public void setGood(boolean good) {
		isGood = good;
	}

	public boolean isBad() {
		return isBad;
	}

	public void setBad(boolean bad) {
		isBad = bad;
	}

	public int getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(int goodsCount) {
		this.goodsCount = goodsCount;
	}

	public int getBadsCount() {
		return badsCount;
	}

	public void setBadsCount(int badsCount) {
		this.badsCount = badsCount;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getMenuGroup() {
		return menuGroup;
	}

	public void setMenuGroup(int menuGroup) {
		this.menuGroup = menuGroup;
	}

	public int getSocialScore() {
		return socialScore;
	}

	public void setSocialScore(int socialScore) {
		this.socialScore = socialScore;
	}

	public int getKind() {
		return kind;
	}

	public void setKind(int kind) {
		this.kind = kind;
	}

	public RealmList<MenuTag> getTags() {
		return tags;
	}

	public void setTags(RealmList<MenuTag> tags) {
		this.tags = tags;
	}

	public RealmList<CommentInNotiItem> getNutritionistComments() {
		return nutritionistComments;
	}

	public void setNutritionistComments(RealmList<CommentInNotiItem> nutritionistComments) {
		this.nutritionistComments = nutritionistComments;
	}

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public String getCgmMenuName() {
		return cgmMenuName;
	}

	public void setCgmMenuName(String cgmMenuName) {
		this.cgmMenuName = cgmMenuName;
	}

	public String getCgmShopName() {
		return cgmShopName;
	}

	public void setCgmShopName(String cgmShopName) {
		this.cgmShopName = cgmShopName;
	}

	public RealmList<CommentInNotiItem> getComments() {
		return comments;
	}

	public void setComments(RealmList<CommentInNotiItem> comments) {
		this.comments = comments;
	}
}