package com.camitss.mealthy.object.notificationitem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Viseth on 6/28/2017.
 */

public class CommentInNotiItem extends RealmObject {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("body")
	@Expose
	private String body;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("user")
	@Expose
	private CommentUserInNotiItem cmtUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public CommentUserInNotiItem getCmtUser() {
		return cmtUser;
	}

	public void setCmtUser(CommentUserInNotiItem cmtUser) {
		this.cmtUser = cmtUser;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}