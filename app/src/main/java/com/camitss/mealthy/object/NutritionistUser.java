package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Darith on 5/17/2017.
 */

public class NutritionistUser extends RealmObject {
	@PrimaryKey
	@SerializedName("id")
	@Expose
	int nuser_id;

	@SerializedName("name")
	@Expose
	String nuser_name;

	@SerializedName("profile_image_url")
	@Expose
	String nuser_profile_image;

	public int getNuser_id() {
		return nuser_id;
	}

	public void setNuser_id(int nuser_id) {
		this.nuser_id = nuser_id;
	}

	public String getNuser_name() {
		return nuser_name;
	}

	public void setNuser_name(String nuser_name) {
		this.nuser_name = nuser_name;
	}

	public String getNuser_profile_image() {
		return nuser_profile_image;
	}

	public void setNuser_profile_image(String nuser_profile_image) {
		this.nuser_profile_image = nuser_profile_image;
	}
}
