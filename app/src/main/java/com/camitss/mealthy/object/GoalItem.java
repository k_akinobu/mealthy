package com.camitss.mealthy.object;

import com.camitss.mealthy.R;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 5/12/2017.
 */

public class GoalItem extends RealmObject {
	@PrimaryKey
	private int id;

	private String period;
	private String curve;
	private String size;
	private String eatOutFrequency;

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCurve() {
		return curve;
	}

	public void setCurve(String curve) {
		this.curve = curve;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getEatOutFrequency() {
		return eatOutFrequency;
	}

	public void setEatOutFrequency(String eatOutFrequency) {
		this.eatOutFrequency = eatOutFrequency;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTermId() {
		String s = getPeriod().trim();
		if (s.equalsIgnoreCase("1ヶ月")) return 1;
		else if (s.equalsIgnoreCase("3ヶ月")) return 2;
		else if (s.equalsIgnoreCase("ずっと")) return 3;
		return 1;
	}

	public int getEatOutId() {
		String s = getEatOutFrequency().trim();
		if (s.equalsIgnoreCase("週1回")) return 1;
		else if (s.equalsIgnoreCase("週2〜3回")) return 2;
		else if (s.equalsIgnoreCase("週4〜5回")) return 3;
		else if (s.equalsIgnoreCase("ほぼ毎日")) return 4;
		return 1;
	}

	public int getDietId() {
		String s = getCurve().trim();
		if (s.equalsIgnoreCase("現状維持")) return 1;
		else if (s.equalsIgnoreCase("▲1kg")) return 2;
		else if (s.equalsIgnoreCase("▲2kg")) return 3;
		else if (s.equalsIgnoreCase("▲3kg")) return 4;
		return 1;
	}

	public int getFigureId() {
		String s = getSize().trim();
		if (s.equalsIgnoreCase("ややぽっちゃり")) return 1;
		else if (s.equalsIgnoreCase("標準")) return 2;
		else if (s.equalsIgnoreCase("ややすっきり")) return 3;
		return 1;
	}
}
