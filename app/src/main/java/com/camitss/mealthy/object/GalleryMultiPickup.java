package com.camitss.mealthy.object;

/**
 * Created by User
 * Date 4/28/2017
 */

public class GalleryMultiPickup {
	private int id;
	private String path;
	private boolean isSelected;

	public GalleryMultiPickup (){
		isSelected = false;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}
}
