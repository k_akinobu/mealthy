package com.camitss.mealthy.object;

import android.util.Log;

/**
 * Created by User
 * Date 3/23/2017
 */

public class MealthyLog {

	private static boolean enableLog = true;

	public static void infoLog(String tag, String message) {
		if (enableLog) {
			Log.i(tag, message);
		}
	}

	public static void errorLog(String tag, String message) {
		if (enableLog) {
			Log.e(tag, message);
		}
	}

	public static void debugLog(String tag, String message) {
		if (enableLog) {
			Log.d(tag, message);
		}
	}
}


