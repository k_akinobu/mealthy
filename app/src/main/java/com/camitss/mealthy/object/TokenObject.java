package com.camitss.mealthy.object;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 5/9/2017.
 */

public class TokenObject extends RealmObject {
	@PrimaryKey
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}