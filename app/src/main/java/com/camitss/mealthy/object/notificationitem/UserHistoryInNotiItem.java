package com.camitss.mealthy.object.notificationitem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Viseth on 6/28/2017.
 */

public class UserHistoryInNotiItem extends RealmObject {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("kind")
	@Expose
	private int kind;
	@SerializedName("value")
	@Expose
	private String value;
	@SerializedName("history_date")
	@Expose
	private String historyDate;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("before_weight")
	@Expose
	private String beforeWeight;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getKind() {
		return kind;
	}

	public void setKind(int kind) {
		this.kind = kind;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getHistoryDate() {
		return historyDate;
	}

	public void setHistoryDate(String historyDate) {
		this.historyDate = historyDate;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getBeforeWeight() {
		return beforeWeight;
	}

	public void setBeforeWeight(String beforeWeight) {
		this.beforeWeight = beforeWeight;
	}
}