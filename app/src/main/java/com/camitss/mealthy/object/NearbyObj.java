package com.camitss.mealthy.object;

import java.io.Serializable;

/**
 * Created by Darith on 5/19/2017.
 */
@SuppressWarnings("serial")
public class NearbyObj implements Serializable {
	float latitude;
	float longitude;
	double distance;
	String takeout_flag;
	String menu_name;
	String shopName;
	String tag;
	int shop_id;
	int nut_recommend_only;
	int shop_group_kind;
	int typeSearch;
	MealthyMenu menu;

	public MealthyMenu getMenu() {
		return menu;
	}

	public void setMenu(MealthyMenu menu) {
		this.menu = menu;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getShop_id() {
		return shop_id;
	}

	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}

	public int getTypeSearch() {
		return typeSearch;
	}

	public void setTypeSearch(int typeSearch) {
		this.typeSearch = typeSearch;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getTakeout_flag() {
		return takeout_flag;
	}

	public void setTakeout_flag(String takeout_flag) {
		this.takeout_flag = takeout_flag;
	}

	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getNut_recommend_only() {
		return nut_recommend_only;
	}

	public void setNut_recommend_only(int nut_recommend_only) {
		this.nut_recommend_only = nut_recommend_only;
	}

	public int getShop_group_kind() {
		return shop_group_kind;
	}

	public void setShop_group_kind(int shop_group_kind) {
		this.shop_group_kind = shop_group_kind;
	}
}
