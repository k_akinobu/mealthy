package com.camitss.mealthy.object;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Darith on 5/22/2017.
 */

public class HistorySearch extends RealmObject {
	@PrimaryKey
	int id;
	String data;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}

