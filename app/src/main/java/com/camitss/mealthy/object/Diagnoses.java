package com.camitss.mealthy.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Darith on 3/23/2017.
 */

public class Diagnoses extends RealmObject {//implements Serializable {

	@PrimaryKey
	private int id;

	private String title;
	private String body;
	private int position;
	private String image;
	private int disgnosisKind;
	private String notes;
	private String createdDate;
	private RealmList<DiagnosisItem> diagnosisItem;
	private int selectedId;
	private boolean selectedQuestion;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getDisgnosisKind() {
		return disgnosisKind;
	}

	public void setDisgnosisKind(int disgnosisKind) {
		this.disgnosisKind = disgnosisKind;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public List<DiagnosisItem> getDiagnosisItem() {
		return diagnosisItem;
	}

	public void setDiagnosisItem(RealmList<DiagnosisItem> diagnosisItem) {
		this.diagnosisItem = diagnosisItem;

	}

	public int getSelected() {
		return selectedId;
	}

	public void setSelected(int selected) {
		this.selectedId = selected;
	}

	public boolean isSelectedQuestion() {
		return selectedQuestion;
	}

	public void setSelectedQuestion(boolean selectedQuestion) {
		this.selectedQuestion = selectedQuestion;
	}
}
