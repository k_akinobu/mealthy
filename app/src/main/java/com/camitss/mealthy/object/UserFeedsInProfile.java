package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 5/2/2017.
 */

public class UserFeedsInProfile implements Serializable{
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("key")
	@Expose
	private String key;
	@SerializedName("is_like")
	@Expose
	private boolean isLike;
	@SerializedName("likes_count")
	@Expose
	private int likesCount;
	@SerializedName("privacy_kind")
	@Expose
	private int privacyKind;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("shop")
	@Expose
	private String shop;
	@SerializedName("menu")
	@Expose
	private Menu menu;
	@SerializedName("user_history")
	@Expose
	private UserHistory userHistory;
	@SerializedName("exif_date")
	@Expose
	private String exifDate;
	@SerializedName("user")
	@Expose
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean like) {
		isLike = like;
	}

	public int getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(int likesCount) {
		this.likesCount = likesCount;
	}

	public int getPrivacyKind() {
		return privacyKind;
	}

	public void setPrivacyKind(int privacyKind) {
		this.privacyKind = privacyKind;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public UserHistory getUserHistory() {
		return userHistory;
	}

	public void setUserHistory(UserHistory userHistory) {
		this.userHistory = userHistory;
	}

	public String getExifDate() {
		return exifDate;
	}

	public void setExifDate(String exifDate) {
		this.exifDate = exifDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public class Menu implements Serializable{
		@SerializedName("id")
		@Expose
		private int id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("calorie")
		@Expose
		private int calorie;
		@SerializedName("protein")
		@Expose
		private int protein;
		@SerializedName("lipid")
		@Expose
		private int lipid;
		@SerializedName("carbohydrate")
		@Expose
		private int carbohydrate;
		@SerializedName("origin")
		@Expose
		private int origin;
		@SerializedName("score")
		@Expose
		private int score;
		@SerializedName("price")
		@Expose
		private int price;
		@SerializedName("image")
		@Expose
		private String image;
		@SerializedName("thumbnail_image_url")
		@Expose
		private String thumbnailImageUrl;
		@SerializedName("start_time")
		@Expose
		private String startTime;
		@SerializedName("end_time")
		@Expose
		private String endTime;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("takeout_flag")
		@Expose
		private String takeoutFlag;
		@SerializedName("is_good")
		@Expose
		private boolean isGood;
		@SerializedName("is_bad")
		@Expose
		private boolean isBad;
		@SerializedName("goods_count")
		@Expose
		private int goodsCount;
		@SerializedName("bads_count")
		@Expose
		private int badsCount;
		@SerializedName("comments_count")
		@Expose
		private int commentsCount;
		@SerializedName("notes")
		@Expose
		private String notes;
		@SerializedName("menu_group")
		@Expose
		private int menuGroup;
		@SerializedName("social_score")
		@Expose
		private int socialScore;
		@SerializedName("kind")
		@Expose
		private int kind;
		@SerializedName("tags")
		@Expose
		private List<MenuTag> tags;
		@SerializedName("nutritionist_comments")
		@Expose
		private List<Comment> nutritionistComments;
		@SerializedName("shop_id")
		@Expose
		private int shopId;
		@SerializedName("cgm_menu_name")
		@Expose
		private String cgmMenuName;
		@SerializedName("cgm_shop_name")
		@Expose
		private String cgmShopName;
		@SerializedName("comments")
		@Expose
		private List<Comment> comments;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getCalorie() {
			return calorie;
		}

		public void setCalorie(int calorie) {
			this.calorie = calorie;
		}

		public int getProtein() {
			return protein;
		}

		public void setProtein(int protein) {
			this.protein = protein;
		}

		public int getLipid() {
			return lipid;
		}

		public void setLipid(int lipid) {
			this.lipid = lipid;
		}

		public int getCarbohydrate() {
			return carbohydrate;
		}

		public void setCarbohydrate(int carbohydrate) {
			this.carbohydrate = carbohydrate;
		}

		public int getOrigin() {
			return origin;
		}

		public void setOrigin(int origin) {
			this.origin = origin;
		}

		public int getScore() {
			return score;
		}

		public void setScore(int score) {
			this.score = score;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getThumbnailImageUrl() {
			return thumbnailImageUrl;
		}

		public void setThumbnailImageUrl(String thumbnailImageUrl) {
			this.thumbnailImageUrl = thumbnailImageUrl;
		}

		public String getStartTime() {
			return startTime;
		}

		public void setStartTime(String startTime) {
			this.startTime = startTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getTakeoutFlag() {
			return takeoutFlag;
		}

		public void setTakeoutFlag(String takeoutFlag) {
			this.takeoutFlag = takeoutFlag;
		}

		public boolean isGood() {
			return isGood;
		}

		public void setGood(boolean good) {
			isGood = good;
		}

		public boolean isBad() {
			return isBad;
		}

		public void setBad(boolean bad) {
			isBad = bad;
		}

		public int getGoodsCount() {
			return goodsCount;
		}

		public void setGoodsCount(int goodsCount) {
			this.goodsCount = goodsCount;
		}

		public int getBadsCount() {
			return badsCount;
		}

		public void setBadsCount(int badsCount) {
			this.badsCount = badsCount;
		}

		public int getCommentsCount() {
			return commentsCount;
		}

		public void setCommentsCount(int commentsCount) {
			this.commentsCount = commentsCount;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public int getMenuGroup() {
			return menuGroup;
		}

		public void setMenuGroup(int menuGroup) {
			this.menuGroup = menuGroup;
		}

		public int getSocialScore() {
			return socialScore;
		}

		public void setSocialScore(int socialScore) {
			this.socialScore = socialScore;
		}

		public int getKind() {
			return kind;
		}

		public void setKind(int kind) {
			this.kind = kind;
		}

		public List<MenuTag> getTags() {
			return tags;
		}

		public void setTags(List<MenuTag> tags) {
			this.tags = tags;
		}

		public List<Comment> getNutritionistComments() {
			return nutritionistComments;
		}

		public void setNutritionistComments(List<Comment> nutritionistComments) {
			this.nutritionistComments = nutritionistComments;
		}

		public int getShopId() {
			return shopId;
		}

		public void setShopId(int shopId) {
			this.shopId = shopId;
		}

		public String getCgmMenuName() {
			return cgmMenuName;
		}

		public void setCgmMenuName(String cgmMenuName) {
			this.cgmMenuName = cgmMenuName;
		}

		public String getCgmShopName() {
			return cgmShopName;
		}

		public void setCgmShopName(String cgmShopName) {
			this.cgmShopName = cgmShopName;
		}

		public List<Comment> getComments() {
			return comments;
		}

		public void setComments(List<Comment> comments) {
			this.comments = comments;
		}
	}
	public class Comment implements Serializable{
		@SerializedName("id")
		@Expose
		private int id;
		@SerializedName("body")
		@Expose
		private String body;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("user")
		@Expose
		private CommentUser cmtUser;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public CommentUser getCmtUser() {
			return cmtUser;
		}

		public void setCmtUser(CommentUser cmtUser) {
			this.cmtUser = cmtUser;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}
	}
	public class CommentUser implements Serializable{
		@SerializedName("id")
		@Expose
		private int id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("profile_image_url")
		@Expose
		private String profileImage;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getProfileImage() {
			return profileImage;
		}

		public void setProfileImage(String profileImage) {
			this.profileImage = profileImage;
		}
	}
	public class UserHistory implements Serializable{
		@SerializedName("id")
		@Expose
		private int id;
		@SerializedName("kind")
		@Expose
		private int kind;
		@SerializedName("value")
		@Expose
		private String value;
		@SerializedName("history_date")
		@Expose
		private String historyDate;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("before_weight")
		@Expose
		private String beforeWeight;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getKind() {
			return kind;
		}

		public void setKind(int kind) {
			this.kind = kind;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getHistoryDate() {
			return historyDate;
		}

		public void setHistoryDate(String historyDate) {
			this.historyDate = historyDate;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getBeforeWeight() {
			return beforeWeight;
		}

		public void setBeforeWeight(String beforeWeight) {
			this.beforeWeight = beforeWeight;
		}
	}
	public class User implements Serializable{
		@PrimaryKey
		@SerializedName("id")
		@Expose
		private int id;

		@SerializedName("sex")
		@Expose
		private int sex;
		@SerializedName("tall")
		@Expose
		private double tall;
		@SerializedName("weight")
		@Expose
		private double weight;
		@SerializedName("target_weight")
		@Expose
		private double targetWeight;
		@SerializedName("metabolic_id")
		@Expose
		private int metabolicId;
		@SerializedName("term_id")
		@Expose
		private int termId;
		@SerializedName("eat_out_id")
		@Expose
		private int eatOutId;
		@SerializedName("diet_id")
		@Expose
		private int dietId;
		@SerializedName("figure_id")
		@Expose
		private int figureId;
		@SerializedName("category_ids")
		@Expose
		private String categoryIds;
		@SerializedName("created_at")
		@Expose
		private String createdAt;
		@SerializedName("updated_at")
		@Expose
		private String updatedAt;
		@SerializedName("start_date")
		@Expose
		private String startDate;
		@SerializedName("end_date")
		@Expose
		private String endDate;
		@SerializedName("morning_price")
		@Expose
		private double morningPrice;
		@SerializedName("lunch_price")
		@Expose
		private double lunchPrice;
		@SerializedName("dinner_price")
		@Expose
		private double dinnerPrice;
		@SerializedName("birthdate")
		@Expose
		private String birthdate;
		@SerializedName("is_following")
		@Expose
		private boolean isFollowing;
		@SerializedName("following_count")
		@Expose
		private int followingAccount;
		@SerializedName("followers_count")
		@Expose
		private int followersCount;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("profile_image_url")
		@Expose
		private String profileImageUrl;
		@SerializedName("cover_image_url")
		@Expose
		private String coverImageUrl;
		@SerializedName("score")
		@Expose
		private double score;
		@SerializedName("progress")
		@Expose
		private double progress;
		@SerializedName("total_reduction")
		@Expose
		private int totalReduction;
		@SerializedName("posted_count")
		@Expose
		private int postedCount;
		@SerializedName("user_kind")
		@Expose
		private int userKind;
		@SerializedName("rep_nutritionist_profile_image_url")
		@Expose
		private String repNutritionistProfileUrl;
		@SerializedName("last_posted_image_url")
		@Expose
		private String lastedPostedImageUrl;
		@SerializedName("is_free_consultation")
		@Expose
		private boolean isFreeConsultation;
		@SerializedName("is_trialpack_bought")
		@Expose
		private boolean isTrialpackBought;
		@SerializedName("invite_code")
		@Expose
		private String inviteCode;
		@SerializedName("calorie_details")
		@Expose
		private RealmList<CalorieDetail> calorieDetails = new RealmList<>();
		@SerializedName("diet_type")
		@Expose
		private String dietType;
		@SerializedName("point")
		@Expose
		private int point;


		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getSex() {
			return sex;
		}

		public void setSex(int sex) {
			this.sex = sex;
		}

		public double getTall() {
			return tall;
		}

		public void setTall(double tall) {
			this.tall = tall;
		}

		public double getWeight() {
			return weight;
		}

		public void setWeight(double weight) {
			this.weight = weight;
		}

		public double getTargetWeight() {
			return targetWeight;
		}

		public void setTargetWeight(double targetWeight) {
			this.targetWeight = targetWeight;
		}

		public int getMetabolicId() {
			return metabolicId;
		}

		public void setMetabolicId(int metabolicId) {
			this.metabolicId = metabolicId;
		}

		public int getTermId() {
			return termId;
		}

		public void setTermId(int termId) {
			this.termId = termId;
		}

		public int getEatOutId() {
			return eatOutId;
		}

		public void setEatOutId(int eatOutId) {
			this.eatOutId = eatOutId;
		}

		public int getDietId() {
			return dietId;
		}

		public void setDietId(int dietId) {
			this.dietId = dietId;
		}

		public int getFigureId() {
			return figureId;
		}

		public void setFigureId(int figureId) {
			this.figureId = figureId;
		}

		public String getCategoryIds() {
			return categoryIds;
		}

		public void setCategoryIds(String categoryIds) {
			this.categoryIds = categoryIds;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public double getMorningPrice() {
			return morningPrice;
		}

		public void setMorningPrice(double morningPrice) {
			this.morningPrice = morningPrice;
		}

		public double getLunchPrice() {
			return lunchPrice;
		}

		public void setLunchPrice(double lunchPrice) {
			this.lunchPrice = lunchPrice;
		}

		public double getDinnerPrice() {
			return dinnerPrice;
		}

		public void setDinnerPrice(double dinnerPrice) {
			this.dinnerPrice = dinnerPrice;
		}

		public String getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(String birthdate) {
			this.birthdate = birthdate;
		}

		public boolean isFollowing() {
			return isFollowing;
		}

		public void setFollowing(boolean following) {
			isFollowing = following;
		}

		public int getFollowingAccount() {
			return followingAccount;
		}

		public void setFollowingAccount(int followingAccount) {
			this.followingAccount = followingAccount;
		}

		public int getFollowersCount() {
			return followersCount;
		}

		public void setFollowersCount(int followersCount) {
			this.followersCount = followersCount;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getProfileImageUrl() {
			return profileImageUrl;
		}

		public void setProfileImageUrl(String profileImageUrl) {
			this.profileImageUrl = profileImageUrl;
		}

		public String getCoverImageUrl() {
			return coverImageUrl;
		}

		public void setCoverImageUrl(String coverImageUrl) {
			this.coverImageUrl = coverImageUrl;
		}

		public double getScore() {
			return score;
		}

		public void setScore(double score) {
			this.score = score;
		}

		public double getProgress() {
			return progress;
		}

		public void setProgress(double progress) {
			this.progress = progress;
		}

		public int getTotalReduction() {
			return totalReduction;
		}

		public void setTotalReduction(int totalReduction) {
			this.totalReduction = totalReduction;
		}

		public int getPostedCount() {
			return postedCount;
		}

		public void setPostedCount(int postedCount) {
			this.postedCount = postedCount;
		}

		public int getUserKind() {
			return userKind;
		}

		public void setUserKind(int userKind) {
			this.userKind = userKind;
		}

		public String getRepNutritionistProfileUrl() {
			return repNutritionistProfileUrl;
		}

		public void setRepNutritionistProfileUrl(String repNutritionistProfileUrl) {
			this.repNutritionistProfileUrl = repNutritionistProfileUrl;
		}

		public String getLastedPostedImageUrl() {
			return lastedPostedImageUrl;
		}

		public void setLastedPostedImageUrl(String lastedPostedImageUrl) {
			this.lastedPostedImageUrl = lastedPostedImageUrl;
		}

		public boolean isFreeConsultation() {
			return isFreeConsultation;
		}

		public void setFreeConsultation(boolean freeConsultation) {
			isFreeConsultation = freeConsultation;
		}

		public boolean isTrialpackBought() {
			return isTrialpackBought;
		}

		public void setTrialpackBought(boolean trialpackBought) {
			isTrialpackBought = trialpackBought;
		}

		public String getInviteCode() {
			return inviteCode;
		}

		public void setInviteCode(String inviteCode) {
			this.inviteCode = inviteCode;
		}

		public RealmList<CalorieDetail> getCalorieDetails() {
			return calorieDetails;
		}

		public void setCalorieDetails(RealmList<CalorieDetail> calorieDetails) {
			this.calorieDetails = calorieDetails;
		}

		public String getDietType() {
			return dietType;
		}

		public void setDietType(String dietType) {
			this.dietType = dietType;
		}

		public int getPoint() {
			return point;
		}

		public void setPoint(int point) {
			this.point = point;
		}
	}


}
