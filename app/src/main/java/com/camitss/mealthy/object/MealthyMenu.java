package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by User
 * Date 4/11/2017
 */

public class MealthyMenu extends RealmObject {
	@PrimaryKey
	@SerializedName("id")
	@Expose
	int id;

	@SerializedName("name")
	@Expose
	String name;
	@SerializedName("calorie")
	@Expose
	float calorie;
	@SerializedName("protein")
	@Expose
	float protein;
	@SerializedName("lipid")
	@Expose
	float lipid;
	@SerializedName("carbohydrate")
	@Expose
	float carbohydrate;
	@SerializedName("origin")
	@Expose
	float origin;
	@SerializedName("score")
	@Expose
	float score;
	@SerializedName("price")
	@Expose
	float price;
	@SerializedName("image")
	@Expose
	String image;

	@SerializedName("thumbnail_image_url")
	@Expose
	String thumbnal_image_url;

	@SerializedName("start_time")
	@Expose
	String start_time;
	@SerializedName("end_time")
	@Expose
	String end_time;
	@SerializedName("takeout_flag")
	@Expose
	String takeout_flag;
	@SerializedName("is_good")
	@Expose
	boolean is_good;
	@SerializedName("is_bad")
	@Expose
	boolean is_bad;
	@SerializedName("goods_count")
	@Expose
	int goods_count;
	@SerializedName("bads_count")
	@Expose
	int bads_count;
	@SerializedName("comments_count")
	@Expose
	int comments_count;
	@SerializedName("notes")
	@Expose
	String notes;
	@SerializedName("menu_group")
	@Expose
	int menu_group;
	@SerializedName("social_score")
	@Expose
	float social_score;
	@SerializedName("kind")
	@Expose
	int kind;
	@SerializedName("shop_id")
	@Expose
	int shop_id;

	@SerializedName("nutritionist_comments")
	@Expose
	RealmList<NutritionistComments> nutritionist_comments;


	@SerializedName("tags")
	@Expose
	RealmList<MenuTag> tags;

	@SerializedName("comments")
	@Expose
	private RealmList<MealthyMenuComment> comments;

	String shopName;

	String distance;
	boolean lastIndex;

	String address;
	String phoneNumber;

	float shopLat;
	float shopLng;
	boolean favorith;
	int favId;

	public int getFavId() {
		return favId;
	}

	public void setFavId(int favId) {
		this.favId = favId;
	}

	public boolean isFavorith() {
		return favorith;
	}

	public void setFavorith(boolean favorith) {
		this.favorith = favorith;
	}

	public float getShopLat() {
		return shopLat;
	}

	public void setShopLat(float shopLat) {
		this.shopLat = shopLat;
	}

	public float getShopLng() {
		return shopLng;
	}

	public void setShopLng(float shopLng) {
		this.shopLng = shopLng;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getCalorie() {
		return calorie;
	}

	public void setCalorie(float calorie) {
		this.calorie = calorie;
	}

	public float getProtein() {
		return protein;
	}

	public void setProtein(float protein) {
		this.protein = protein;
	}

	public float getLipid() {
		return lipid;
	}

	public void setLipid(float lipid) {
		this.lipid = lipid;
	}

	public float getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(float carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public float getOrigin() {
		return origin;
	}

	public void setOrigin(float origin) {
		this.origin = origin;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getThumbnal_image_url() {
		return thumbnal_image_url;
	}

	public void setThumbnal_image_url(String thumbnal_image_url) {
		this.thumbnal_image_url = thumbnal_image_url;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getTakeout_flag() {
		return takeout_flag;
	}

	public void setTakeout_flag(String takeout_flag) {
		this.takeout_flag = takeout_flag;
	}

	public boolean is_good() {
		return is_good;
	}

	public void setIs_good(boolean is_good) {
		this.is_good = is_good;
	}

	public boolean is_bad() {
		return is_bad;
	}

	public void setIs_bad(boolean is_bad) {
		this.is_bad = is_bad;
	}

	public int getGoods_count() {
		return goods_count;
	}

	public void setGoods_count(int goods_count) {
		this.goods_count = goods_count;
	}

	public int getBads_count() {
		return bads_count;
	}

	public void setBads_count(int bads_count) {
		this.bads_count = bads_count;
	}

	public int getComments_count() {
		return comments_count;
	}

	public void setComments_count(int comments_count) {
		this.comments_count = comments_count;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getMenu_group() {
		return menu_group;
	}

	public void setMenu_group(int menu_group) {
		this.menu_group = menu_group;
	}

	public float getSocial_score() {
		return social_score;
	}

	public void setSocial_score(float social_score) {
		this.social_score = social_score;
	}

	public int getKind() {
		return kind;
	}

	public void setKind(int kind) {
		this.kind = kind;
	}

	public int getShop_id() {
		return shop_id;
	}

	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}

	public List<MenuTag> getTags() {
		return tags;
	}

	public void setTags(RealmList<MenuTag> tags) {
		this.tags = tags;
	}

	public List<NutritionistComments> getNutritionist_comments() {
		return nutritionist_comments;
	}

	public void setNutritionist_comments(RealmList<NutritionistComments> nutritionist_comments) {
		this.nutritionist_comments = nutritionist_comments;
	}

	public boolean isLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(boolean lastIndex) {
		this.lastIndex = lastIndex;
	}

	public RealmList<MealthyMenuComment> getComments() {
		return comments;
	}

	public void setComments(RealmList<MealthyMenuComment> comments) {
		this.comments = comments;
	}


}
