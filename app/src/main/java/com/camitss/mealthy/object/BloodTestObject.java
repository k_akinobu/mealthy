package com.camitss.mealthy.object;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viseth on 5/8/2017.
 */

public class BloodTestObject extends RealmObject {
	@PrimaryKey
	private String btsbp;
	private String btdbp;
	private String btbgl;
	private String bthba1c;
	private String btnfat;
	private String bthdl;
	private String btldl;
	private String btast;
	private String btalt;
	private String btygtp;
	private String btuacid;

	public String getBtsbp() {
		return btsbp;
	}

	public void setBtsbp(String btsbp) {
		this.btsbp = btsbp;
	}

	public String getBtdbp() {
		return btdbp;
	}

	public void setBtdbp(String btdbp) {
		this.btdbp = btdbp;
	}

	public String getBtbgl() {
		return btbgl;
	}

	public void setBtbgl(String btbgl) {
		this.btbgl = btbgl;
	}

	public String getBthba1c() {
		return bthba1c;
	}

	public void setBthba1c(String bthba1c) {
		this.bthba1c = bthba1c;
	}

	public String getBtnfat() {
		return btnfat;
	}

	public void setBtnfat(String btnfat) {
		this.btnfat = btnfat;
	}

	public String getBthdl() {
		return bthdl;
	}

	public void setBthdl(String bthdl) {
		this.bthdl = bthdl;
	}

	public String getBtldl() {
		return btldl;
	}

	public void setBtldl(String btldl) {
		this.btldl = btldl;
	}

	public String getBtast() {
		return btast;
	}

	public void setBtast(String btast) {
		this.btast = btast;
	}

	public String getBtalt() {
		return btalt;
	}

	public void setBtalt(String btalt) {
		this.btalt = btalt;
	}

	public String getBtygtp() {
		return btygtp;
	}

	public void setBtygtp(String btygtp) {
		this.btygtp = btygtp;
	}

	public String getBtuacid() {
		return btuacid;
	}

	public void setBtuacid(String btuacid) {
		this.btuacid = btuacid;
	}
}
