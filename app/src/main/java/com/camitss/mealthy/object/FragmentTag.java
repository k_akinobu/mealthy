package com.camitss.mealthy.object;

/**
 * Created by Senhly
 * Date  07/03/2017
 */

public class FragmentTag {
	private static FragmentTag instant;
	private String TAG;

	private FragmentTag() {
	}

	public static FragmentTag getInstant() {
		if (instant == null) {
			instant = new FragmentTag();
		}
		return instant;
	}

	public void setTAG(String tag) {
		TAG = tag;
	}

	public String getTAG() {
		return TAG;
	}
}
