package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by camittss007 on 5/31/2017.
 */

public class SearchSettingTag {
	@SerializedName("id")
	@Expose
	int id;

	@SerializedName("name")
	@Expose
	String name;

	@SerializedName("children")
	@Expose
	List<MenuTag> childTag;

	private boolean isLike = true;

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean like) {
		isLike = like;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuTag> getChildTag() {
		return childTag;
	}

	public void setChildTag(List<MenuTag> childTag) {
		this.childTag = childTag;
	}
}
