package com.camitss.mealthy.object;

/**
 * Created by Senhly HENG
 * Date 3/7/2017
 */

public class Constant {
	public static final int FEMALE = 0;
	public static final int MALE = 1;
	public static String BASE_URL = null;
	public static String LOG_BASE_URL = null;
	public static String VERSION;
	public static String token;
	public static int termId;
	//upload utils
	public static final int REQUEST_OPEN_CAMERA = 204;
	public static final String JPEG_FILE_PREFIX = "IMG_";
	public static final String JPEG_FILE_SUFFIX = ".jpg";
	//    public static final int REQUEST_TAKE_PHOTO = 1;
//    public static final int RESULT_LOAD_IMAGE = 2;
//    public static final int REQUEST_CROP_PHOTO = 3;
	public static final int RESULT_LOAD_IMAGE_COVER = 22;
	public static final int REQUEST_STORAGE = 400;
	public static final int REQUEST_LOAD_PHOTO = 4;
	public static final int REQUEST_TAKE_PHOTO = 5;
	public static final int REQUEST_TAKE_PHOTO_COVER = 11;
	public static final int REQUEST_CROP_PHOTO_COVER = 33;
	public static final int REQUEST_CROP_PHOTO = 44;
	public static final int CONNECT_FACEBOOK = 1;
	public static final int CONNECT_EMAIL = 2;
	public static final int CONNECT_COOPERATE = 3;
	public static String getBaseUrl() {
		if (BASE_URL == null) {
			return "https://dev-api.mealthy.me";
		}
		return BASE_URL;
	}

	private static String getLogBaseUrl() {
		if (LOG_BASE_URL == null) {
			return "https://dev-api.mealthy.me:24224";
		}
		return LOG_BASE_URL;
	}

	public static final int CAMERA = 100;

	public static final String FRAGMENT_TAG_HOME = "home";
	public static final String CONFIG_URL = "http://static.mealthy.me/versions/api/android/1.0.0.json";//"http://static.mealthy.me/versions/api/3.10.0.json";
	/*Request Url*/
	public static final String REQUEST_DIAGNOSIS = getBaseUrl().concat("/diagnoses");
	public static final String DIAGNOSE_RESULT = "https://mealthy-contents.herokuapp.com/contents/wtm/";
	public static final String REQUEST_WITH_USER = getBaseUrl().concat("/users");
	public static final String REQUEST_WITH_USER_POST = getBaseUrl().concat("/users.json");
	public static final String MENU_MESSAGE = getBaseUrl().concat("/notices/menu_message");
	public static final String REQUEST_GET_USER_CALORIES = getBaseUrl().concat("/users/calorie_info");
	public static final String HOME_EVENT = getLogBaseUrl().concat("/events");
	public static final String REQUEST_POST = getBaseUrl() + "/user_posts";
	public static final String LOG_POST_EVENT = getLogBaseUrl()+"/events";
	public static final String REQUEST_DIAGNOSES_ANSWER = REQUEST_DIAGNOSIS + "/answer";

	public static final String REQUEST_GET_FEED = getBaseUrl() + "/feeds";
	public static final String REQUEST_MENU_CMT = getBaseUrl() + "/menus/";
	public static final String REQUEST_ADD_MENU_CMT = getBaseUrl()+"/menus/%d/comment";
	public static final String REQUEST_USERS_RECOMMEND = getBaseUrl() + "/users/recommend";
	public static final String REQUEST_FOLLOW_USER = getBaseUrl() + "/users/%d/follow";
	public static final String REQUEST_GET_USER_FEED = getBaseUrl() + "/feeds/%d/user";
	public static final String REQUEST_GET_USER_FOLLOWING = getBaseUrl() + "/users/%d/following";
	public static final String REQUEST_GET_USER_FOLLOWERS = getBaseUrl() + "/users/%d/followers";
	public static final String REQUEST_UPDATE_WEIGHT = getBaseUrl() + "/users";
	public static final String REQUEST_lOGIN_WITH_SOCIAL = getBaseUrl() + "/signin";
	public static final String REQUEST_GET_FEED_USER = getBaseUrl() + "/feeds/%d/user";
	public static final String REQUEST_LIKE_FEED = getBaseUrl()+"/feeds/%d/like";
	public static final String REQUEST_GET_USER_NOTIFICATION = getBaseUrl() + "/user_notifications";
	public static final String REQUEST_GET_USER_HISTORY = getBaseUrl() + "/users/%d/histories";
	public static final String LOGIN_EMAIL = getBaseUrl()+"/auth/login";
	public static final String REGISTER_CONFIRM = getBaseUrl()+"/auth/register_confirm";
	public static final String REGISTER_COMPLETE = getBaseUrl()+"/auth/register_complete";
	public static final String REQUEST_RESET_PASSWORD = getBaseUrl()+"/auth/password_reset";
	public static final String REQUEST_USER_WITH_ID = getBaseUrl() + "/users/%d";
	public static final String REQUEST_GET_USER_NOTIFICATION_UNREAD = getBaseUrl()+"/user_notifications/unread/info";
	public static final String REQUEST_UPDATE_MEAL_PRICE = getBaseUrl()+"/users/";
	public static final String REQUEST_ALL_TAG = getBaseUrl()+"/tags";
	public static final String REQUEST_DELETE_USER_HISTORY = getBaseUrl()+"/user_histories/%d";//APIBase/user_histories/history_id
	public static final String REQUEST_DELETE_FEED_ITEM = getBaseUrl()+"/feeds/%d";//APIBase/feeds/feedid
	public static final String REQUEST_USER_LIKE_TAG = getBaseUrl()+"/users/like_tags";
	public static final String REQUEST_REPORT_FEED_ITEM = getBaseUrl()+"/reports";//APIBase/reports
	public static final String REQUEST_READ_NOTIFICATION = getBaseUrl()+"/user_notifications/message_read";//


}

