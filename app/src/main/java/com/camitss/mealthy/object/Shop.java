package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User
 * Date 4/11/2017
 */

public class Shop {
	@SerializedName("id")
	@Expose
	int id;

	@SerializedName("name")
	@Expose
	String name;

	@SerializedName("url")
	@Expose
	String url;

	@SerializedName("address")
	@Expose
	String address;

	@SerializedName("tel")
	@Expose
	String tel;

	@SerializedName("latitude")
	@Expose
	float latitude;

	@SerializedName("longitude")
	@Expose
	float longitude;

	@SerializedName("holiday_0")
	@Expose
	boolean holiday_0;

	@SerializedName("holiday_1")
	@Expose
	boolean holiday_1;

	@SerializedName("holiday_2")
	@Expose
	boolean holiday_2;

	@SerializedName("holiday_3")
	@Expose
	boolean holiday_3;

	@SerializedName("holiday_4")
	@Expose
	boolean holiday_4;

	@SerializedName("holiday_5")
	@Expose
	boolean holiday_5;

	@SerializedName("holiday_6")
	@Expose
	boolean holiday_6;

	@SerializedName("open_time_0")
	@Expose
	String open_time_0;

	@SerializedName("close_time_0")
	@Expose
	String close_time_0;

	@SerializedName("open_time_1")
	@Expose
	String open_time_1;

	@SerializedName("close_time_1")
	@Expose
	String close_time_1;

	@SerializedName("open_time_2")
	@Expose
	String open_time_2;

	@SerializedName("close_time_2")
	@Expose
	String close_time_2;

	@SerializedName("open_time_3")
	@Expose
	String open_time_3;

	@SerializedName("close_time_3")
	@Expose
	String close_time_3;

	@SerializedName("open_time_4")
	@Expose
	String open_time_4;

	@SerializedName("close_time_4")
	@Expose
	String close_time_4;

	@SerializedName("open_time_5")
	@Expose
	String open_time_5;

	@SerializedName("close_time_5")
	@Expose
	String close_time_5;

	@SerializedName("open_time_6")
	@Expose
	String open_time_6;

	@SerializedName("close_time_6")
	@Expose
	String close_time_6;

	@SerializedName("created_at")
	@Expose
	String created_at;
	@SerializedName("station")
	@Expose
	private Station station;

	@SerializedName("menus")
	@Expose
	List<MealthyMenu> menus;

	String distance;

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public boolean isHoliday_0() {
		return holiday_0;
	}

	public void setHoliday_0(boolean holiday_0) {
		this.holiday_0 = holiday_0;
	}

	public boolean isHoliday_1() {
		return holiday_1;
	}

	public void setHoliday_1(boolean holiday_1) {
		this.holiday_1 = holiday_1;
	}

	public boolean isHoliday_2() {
		return holiday_2;
	}

	public void setHoliday_2(boolean holiday_2) {
		this.holiday_2 = holiday_2;
	}

	public boolean isHoliday_3() {
		return holiday_3;
	}

	public void setHoliday_3(boolean holiday_3) {
		this.holiday_3 = holiday_3;
	}

	public boolean isHoliday_4() {
		return holiday_4;
	}

	public void setHoliday_4(boolean holiday_4) {
		this.holiday_4 = holiday_4;
	}

	public boolean isHoliday_5() {
		return holiday_5;
	}

	public void setHoliday_5(boolean holiday_5) {
		this.holiday_5 = holiday_5;
	}

	public boolean isHoliday_6() {
		return holiday_6;
	}

	public void setHoliday_6(boolean holiday_6) {
		this.holiday_6 = holiday_6;
	}

	public String getOpen_time_0() {
		return open_time_0;
	}

	public void setOpen_time_0(String open_time_0) {
		this.open_time_0 = open_time_0;
	}

	public String getClose_time_0() {
		return close_time_0;
	}

	public void setClose_time_0(String close_time_0) {
		this.close_time_0 = close_time_0;
	}

	public String getOpen_time_1() {
		return open_time_1;
	}

	public void setOpen_time_1(String open_time_1) {
		this.open_time_1 = open_time_1;
	}

	public String getClose_time_1() {
		return close_time_1;
	}

	public void setClose_time_1(String close_time_1) {
		this.close_time_1 = close_time_1;
	}

	public String getOpen_time_2() {
		return open_time_2;
	}

	public void setOpen_time_2(String open_time_2) {
		this.open_time_2 = open_time_2;
	}

	public String getClose_time_2() {
		return close_time_2;
	}

	public void setClose_time_2(String close_time_2) {
		this.close_time_2 = close_time_2;
	}

	public String getOpen_time_3() {
		return open_time_3;
	}

	public void setOpen_time_3(String open_time_3) {
		this.open_time_3 = open_time_3;
	}

	public String getClose_time_3() {
		return close_time_3;
	}

	public void setClose_time_3(String close_time_3) {
		this.close_time_3 = close_time_3;
	}

	public String getOpen_time_4() {
		return open_time_4;
	}

	public void setOpen_time_4(String open_time_4) {
		this.open_time_4 = open_time_4;
	}

	public String getClose_time_4() {
		return close_time_4;
	}

	public void setClose_time_4(String close_time_4) {
		this.close_time_4 = close_time_4;
	}

	public String getOpen_time_5() {
		return open_time_5;
	}

	public void setOpen_time_5(String open_time_5) {
		this.open_time_5 = open_time_5;
	}

	public String getClose_time_5() {
		return close_time_5;
	}

	public void setClose_time_5(String close_time_5) {
		this.close_time_5 = close_time_5;
	}

	public String getOpen_time_6() {
		return open_time_6;
	}

	public void setOpen_time_6(String open_time_6) {
		this.open_time_6 = open_time_6;
	}

	public String getClose_time_6() {
		return close_time_6;
	}

	public void setClose_time_6(String close_time_6) {
		this.close_time_6 = close_time_6;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public List<MealthyMenu> getMenus() {
		return menus;
	}

	public void setMenus(List<MealthyMenu> menus) {
		this.menus = menus;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public class Station{
		@SerializedName("id")
		@Expose
		private int id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("prefecture")
		@Expose
		private int prefecture;
		@SerializedName("zip")
		@Expose
		private String zip;
		@SerializedName("address")
		@Expose
		private String address;
		@SerializedName("latitude")
		@Expose
		private double latitude;
		@SerializedName("longitude")
		@Expose
		private double longitude;
		@SerializedName("created_at")
		@Expose
		private String createdAt;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getPrefecture() {
			return prefecture;
		}

		public void setPrefecture(int prefecture) {
			this.prefecture = prefecture;
		}

		public String getZip() {
			return zip;
		}

		public void setZip(String zip) {
			this.zip = zip;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}
	}
}
