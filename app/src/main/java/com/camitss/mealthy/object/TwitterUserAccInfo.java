package com.camitss.mealthy.object;

/**
 * Created by Viseth on 6/7/2017.
 */

public class TwitterUserAccInfo {

	private String requestToken;
	private String oauthUrl;
	private String oauthVerifier;
	private String accessToken;
	private String accessTokenSecret;
	private String userName;
	private long userId;
	private String profileUrl;
	private boolean twitterLoginStatus;

	public String getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(String requestToken) {
		this.requestToken = requestToken;
	}

	public String getOauthUrl() {
		return oauthUrl;
	}

	public void setOauthUrl(String oauthUrl) {
		this.oauthUrl = oauthUrl;
	}

	public String getOauthVerifier() {
		return oauthVerifier;
	}

	public void setOauthVerifier(String oauthVerifier) {
		this.oauthVerifier = oauthVerifier;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}

	public void setAccessTokenSecret(String accessTokenSecret) {
		this.accessTokenSecret = accessTokenSecret;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public boolean isTwitterLoginStatus() {
		return twitterLoginStatus;
	}

	public void setTwitterLoginStatus(boolean twitterLoginStatus) {
		this.twitterLoginStatus = twitterLoginStatus;
	}
}
