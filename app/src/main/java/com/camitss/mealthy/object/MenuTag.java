package com.camitss.mealthy.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by User
 * Date 4/18/2017
 */

public class MenuTag extends RealmObject {
	@PrimaryKey
	@SerializedName("id")
	@Expose
	private int id;

	@SerializedName("name")
	@Expose
	private String name;

	private boolean isLike = false;

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean like) {
		isLike = like;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
